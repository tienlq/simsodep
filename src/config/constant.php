<?php
define('JSON_MENU_FILE', '../config/menu.json');
define('DATA_USER', '/user/');
define('LIB_FOLDER', 'img/LibraryImages/');

define ("ROUTE_NAME", serialize ([
    'listSim' => 'Danh sách SIM',
    'listNews' => 'Danh sách tin tức',
    'listNews' => 'Danh sách tin tức',
    'singleNews' => 'Hiển thị một bài viết đơn',    
	'contact ' => 'Trang liên hệ',
    'home ' => 'Trang chủ',
]));

define ("BLOCK_LANDINGPAGE_03", serialize ([
    '2' => 'block 1 - ',
    '1' => 'block 2 - Kiểu slide: nhiều hình ảnh chạy',
    '3' => 'block 3 - ',
    '4' => 'block 4 - ',
    '5' => 'block 5 - ',
    '6' => 'block 6 - ',
    '7' => 'block 7 - ',
]));

define ("NEWS_TYPE", serialize ([
    'highlight' => 'Tin nổi bật',
    'project' => 'Tin dự án',
    'job' => 'Tin tuyển dụng',
]));

define ("TYPE_SEARCH_SIM", serialize ([
    1 => 'Lọc theo điều kiện',
    2 => 'Lọc theo khoảng giá',
    3 => 'Chèn liên kết khác',
    4 => 'Giá tăng dần',
    5 => 'Giá giảm dần'
]));
