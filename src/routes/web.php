<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::group(['middleware' => \App\Http\Middleware\Language::class], function () {
    Route::get('', 'Frontend\PageController@index')->name('home');

// Authentication Routes.
    Route::post('login', 'Auth\AuthController@login');
    Route::get('login', 'Auth\AuthController@showLoginForm')->name('login');
    Route::get('admin/login', 'Auth\AuthController@showLoginForm');
    Route::post('admin/login', 'Auth\AuthController@loginAdmin');
    Route::get('logout', 'Auth\AuthController@logout');
    //news
    Route::get('{sluggable}/n{cid}.html', 'Frontend\NewsController@listNews')->name('listNews');
    Route::get('{sluggable}/dn{nid}.html', 'Frontend\NewsController@detail')->name('detailNews');
    Route::get('{sluggable}/s{nid}.html', 'Frontend\NewsController@single')->name('singleNews');
    Route::get('tim-kiem.html', 'Frontend\NewsController@search')->name('search');
    Route::get('{sluggable}/v{nid}.html', 'Frontend\NewsController@listVideo')->name('video');
    Route::get('news.html', 'Frontend\NewsController@listNews04')->name('listNews04');

    //sim
    Route::get('{phone}.html', 'Frontend\SimController@detailSim')->name('detailSim');
    Route::post('{phone}.html', 'Frontend\SimController@postDetailSim');
    Route::get('tim-sim/{phone}.html', 'Frontend\SimController@listNews')->name('searchSim');
    Route::get('khoang-gia/{sluggable}', 'Frontend\SimController@listSimByPrice')->name('listSimByPrice');
    Route::get('nha-mang/{sluggable}', 'Frontend\SimController@listSimBySupplier')->name('listSimBySupplier');
    Route::get('tim-kiem/{keyword}', 'Frontend\SimController@SearchSims')->name('searchSims');
    Route::post('tim-kiem', 'Frontend\SimController@postSearchSims')->name('postSearchSims');
    Route::get('{sluggable}/t{id}.html', 'Frontend\SimController@searchByType')->name('listSimByType');

    Route::post('sendcontact', 'Frontend\ContactController@postSaveContact')->name('saveContact');
    Route::get('sendcontact', 'Frontend\ContactController@postSaveContact');

//contact
    Route::get('lien-he.html', 'Frontend\ContactController@index')->name('contact');
    Route::post('lien-he.html', 'Frontend\ContactController@postSaveContact');

//languages
    Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

    Route::get('testaws', 'Frontend\PageController@testAws');
});
//backend
Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'admin'], function () {
        //product
        Route::get('', 'Backend\PagesController@home')->name('homeAdmin');
        Route::get('product/list', 'Backend\ProductController@listProduct');
        Route::get('product/edit/{pid}', 'Backend\ProductController@editProduct');
        Route::post('product/edit/{pid}', 'Backend\ProductController@postEditProduct');
        Route::get('product/delete/{id?}', 'Backend\ProductController@deleteProduct');

        //news
        Route::get('news/list', 'Backend\NewsController@listNews')->name('adminListNews');
        Route::get('news/edit/{id?}', 'Backend\NewsController@editNews')->name('editNews');
        Route::post('news/edit/{id?}', 'Backend\NewsController@postEditNews');
        Route::get('news/delete/{id?}', 'Backend\NewsController@deleteNews');

//        Route::get('landingpage/list/', 'Backend\NewsController@listLandingpage')->name('listLandingpage');
        Route::get('landingpage/list-block/{landId}/{newsId}', 'Backend\PagesController@listBlockLandingpage')->name('listBlockLandingpages');

        Route::get('landingpage/edit/{newsid}/{landid}/{id}', 'Backend\LandingpagesController@editLandingpage')->name('editLandingpage');
        Route::post('landingpage/edit/{newsid}/{landid}/{id}', 'Backend\LandingpagesController@postEditLandingpage');
        Route::get('news/list-block/{newsid}', 'Backend\LandingpagesController@listBlockByNews')->name('listBlockByNews');
        Route::get('news/block-land/delete/{id}', 'Backend\LandingpagesController@deleteBlock')->name('deleteLandingpage');

        //images
        Route::get('images/list', 'Backend\ImagesController@listImages');
        Route::get('images/edit/{id?}', 'Backend\ImagesController@editImages');
        Route::post('images/edit/{id?}', 'Backend\ImagesController@postEditImages');
        Route::get('images/delete/{id?}', 'Backend\ImagesController@deleteImages');

        //category
        Route::get('category/list', 'Backend\CategoryController@listCategory')->name('adminListCategory');
        Route::get('category/delete/{cid}', 'Backend\CategoryController@deleteCategory')->name('deleteCategory');
        Route::get('category/content-lst-cate', 'Backend\CategoryController@contentListCategory');
        Route::get('category/edit/{cid?}', 'Backend\CategoryController@editCategory')->name('editCategory');
        Route::post('category/edit/{cid?}', 'Backend\CategoryController@postEditCategory');

        //block
        Route::get('block/list', 'Backend\BlockController@listBlock')->name('adminListBlock');
        Route::get('block/delete', 'Backend\BlockController@deleteBlock')->name('deleteBlock');
        Route::get('block/content-lst-cate', 'Backend\BlockController@contentListBlock');
        Route::get('block/edit/{cid?}', 'Backend\BlockController@editBlock')->name('editBlock');
        Route::post('block/edit/{cid?}', 'Backend\BlockController@postEditBlock');
        
        Route::post('block-config/update', 'Backend\BlockController@updateBlockConfig')->name('updateBlockConfig');

        //config site
        Route::get('config/change-footer', 'Backend\ConfigController@changeFooter')->name('changeFooter');
        Route::post('config/change-footer', 'Backend\ConfigController@postChangeFooter');
        Route::get('config-site', 'Backend\ConfigController@configSite')->name('configSite');
        Route::post('config-site', 'Backend\ConfigController@postConfigSite');
        Route::get('config/block-home', 'Backend\ConfigController@changeBlockHome')->name('changeBlockHome');
        Route::post('config/block-home', 'Backend\ConfigController@postChangeBlockHome');

        //orders
        Route::get('orders/list', 'Backend\OrdersController@listOrders');
        Route::get('orders/delete/{id}', 'Backend\OrdersController@deleteOrders');
        Route::get('orders/detail/{id}', 'Backend\OrdersController@detailOrders');

        //contact
        Route::get('contact/list', 'Backend\ContactController@listContact')->name('adminListContact');
        Route::get('contact/delete/{id}', 'Backend\ContactController@deleteContact')->name('adminDeleteContact');
        Route::get('contact/detail/{id}', 'Backend\ContactController@detailContact')->name('adminDetailContact');

        //users
        Route::get('user/list', 'Backend\UserController@listUser')->name('adminListUser');
        Route::get('user/edit/{id}', 'Backend\UserController@editUser')->name('adminEditUser');
        Route::post('user/edit/{id}', 'Backend\UserController@postEditUser')->name('adminEditUser');
        Route::get('user/delete/{id}', 'Backend\UserController@deleteUser')->name('adminDeleteUser');
        Route::get('user/detail/{id}', 'Backend\UserController@detailUser')->name('adminDetailUser');
        Route::get('changeprofile', 'Backend\UserController@changeProfile')->name('adminChangeProfile');
        Route::post('changeprofile', 'Backend\UserController@postChangeProfile')->name('adminChangeProfile');

        //address
        Route::get('address/list', 'Backend\AddressController@listAddress')->name('adminAddress');
        Route::match(['get', 'post'], 'address/update-position', 'Backend\AddressController@updatePositionAddress');
        Route::get('address/delete/{id}', 'Backend\AddressController@delete');
        Route::get('address/content-lst-cate', 'Backend\AddressController@contentListAddress');
        Route::get('address/edit/{cid}', 'Backend\AddressController@editAddress');
        Route::post('address/edit/{cid}', 'Backend\AddressController@postEditAddress');

        //sim
        Route::get('sim/list', 'Backend\SimController@listSim')->name('adminListSim');
        Route::get('sim/edit/{id?}', 'Backend\SimController@editSim')->name('adminEditSim');
        Route::post('sim/edit/{id?}', 'Backend\SimController@postEditSim');
        Route::get('sim/delete/{id?}', 'Backend\SimController@deleteSim')->name('adminDeleteSim');
        
        //news
        Route::get('block-config/list', 'Backend\BlockConfigController@listBlockConfig')->name('adminListBlockConfig');
        Route::get('block-config/edit/{id?}', 'Backend\BlockConfigController@editBlockConfig')->name('editBlockConfig');
        Route::post('block-config/edit/{id?}', 'Backend\BlockConfigController@postEditBlockConfig');
        
        //tbl   
        Route::get('update-sort-order', 'Backend\TblController@updateSortOrder');

        Route::get('/icon/font-awesome', 'Backend\PagesController@fontAwesome')->name('fontAwesome');
    });
});
