<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Supplier;

class SimController extends Controller {

    public function __construct() {
        //todo:
    }

    public function listSim(Request $request) {

        //get all News
        $sims = app('ClassSim')->getSimByPhone($request);

        return view('Frontend/Sim/detailSim', compact('sims', 'user'));
    }

    public function detailSim(Request $request, $phone) {
        $config = app('ClassConfig')->getConfig();
        $sporder = app('ClassBlock')->getBlockByType('sporder');

        $sim = app('ClassSim')->getSimByPhone($phone);
        if (!empty($sim)) {
            return view('Frontend/Sim/detailSim', compact('sim', 'config', 'phone', 'sporder'));
        } else {
            $simDoanhNhan = app('ClassSim')->getListSimDoanhNhan();
            return view('Frontend/Sim/simNotFound', compact('sim', 'config', 'phone', 'sporder', 'simDoanhNhan'));
        }
    }

    public function postDetailSim(Request $request, $phone) {
        $sim = app('ClassSim')->getSimByPhone($phone);
        //order
        $msg = 'success';

        //get detail sim
        $config = app('ClassConfig')->getConfig();
        $sporder = app('ClassBlock')->getBlockByType('sporder');
        if (!empty($sim)) {
            return view('Frontend/Sim/detailSim', compact('sim', 'config', 'phone', 'sporder', 'msg'));
        } else {
            $simDoanhNhan = app('ClassSim')->getListSimDoanhNhan();
            return view('Frontend/Sim/simNotFound', compact('sim', 'config', 'phone', 'sporder', 'simDoanhNhan', 'msg'));
        }
    }

    public function listSimByPrice(Request $request, $sluggable) {
        $config = app('ClassConfig')->getConfig();

        $price = app('ClassBlock')->getBlockByPrice($sluggable);

        $sims = app('ClassSim')->getSimByPrice($price->price01, $price->price02, $request);

        $dauSo = app('ClassBlock')->getBlockByType('dauso');
        $sortOrder = app('ClassBlock')->getBlockByType('sortorder');
        $suppliers = app('ClassSupplier')->getAll();

        return view('Frontend/Sim/listSimByPrice', compact('sims', 'config', 'price', 'suppliers', 'dauSo', 'sortOrder'));
    }

    public function listSimBySupplier(Request $request, $keyword) {

        $config = app('ClassConfig')->getConfig();
        $supplier = Supplier::where("sluggable", $keyword)->first();
        $sims = app('ClassSim')->getSimBySupplier($supplier->id, $request);
        $khoangGia = app('ClassBlock')->getBlockByType('price');
        $dauSo = app('ClassBlock')->getBlockByType('dauso');
        $sortOrder = app('ClassBlock')->getBlockByType('sortorder');
        return view('Frontend/Sim/listSimBySupplier', compact('sims', 'config', 'supplier', 'khoangGia', 'suppliers', 'dauSo', 'sortOrder'));
    }

    public function postSearchSims(Request $request) {
        $keyword = str_replace(' ', '', $request->keyword);
        if (!empty($keyword)){
            return redirect(route('searchSims', [$keyword]));
        }else {
            return back()->withInput();
        }
    }

    public function SearchSims(Request $request, $keyword) {
        $config = app('ClassConfig')->getConfig();

        $sims = app('ClassSim')->searchSimByKeyword($keyword, $request);
        $keys = explode("*", $keyword);
        $countKey = count($keys);
        if (empty($sims) && $countKey == 1 && strlen($keyword) == 10) {
            $haveSupplier = app('ClassCommon')->detectNumber(trim($keyword));
        }

        $khoangGia = app('ClassBlock')->getBlockByType('price');
        $dauSo = app('ClassBlock')->getBlockByType('dauso');
        $sortOrder = app('ClassBlock')->getBlockByType('sortorder');
        $suppliers = app('ClassSupplier')->getAll();
        
        if (!empty($sims) && count($sims) == 1) {
            return redirect(route('detailSim', [$sims[0]->ssluggable]));
        } else {
            return view('Frontend/Sim/listSimByKeyword', 
                compact('sims', 'config', 'keyword', 'haveSupplier', 'khoangGia', 'suppliers', 'dauSo', 'sortOrder', 'keys'));
        }
    }

    public function searchByType(Request $request, $sluggable, $id) {
        $config = app('ClassConfig')->getConfig();
        $block = app('ClassBlock')->getBlockById($id);
        if ($block->image == 1) {
            // $keywords = preg_split(getAll'/\s+/', trim($block->description));
            $sims = app('ClassSim')->searchSimByType($block, $request);
        } elseif ($block->image == 2) {
            $sims = app('ClassSim')->getSimByPrice($block->price01, $block->price02);
        } elseif ($block->image == 3) {
            return redirect($block->link);
        }

        $khoangGia = app('ClassBlock')->getBlockByType('price');
        $dauSo = app('ClassBlock')->getBlockByType('dauso');
        $sortOrder = app('ClassBlock')->getBlockByType('sortorder');
        $suppliers = app('ClassSupplier')->getAll();

        return view('Frontend/Sim/listSimByType', compact('sims', 'config', 'id', 'checkSupplier', 'khoangGia', 'suppliers', 'dauSo', 'sortOrder'));
    }

}
