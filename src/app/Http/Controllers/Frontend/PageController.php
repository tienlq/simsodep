<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

class PageController extends Controller {

    public function __construct() {
        
    }

    public function index() {

        $config = app('ClassConfig')->getConfig();
        $simDoanhNhan = app('ClassSim')->getListSimDoanhNhan();

        return view('Frontend.Pages.index', compact('config','simDoanhNhan'));
    }

}
