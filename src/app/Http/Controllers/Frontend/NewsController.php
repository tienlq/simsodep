<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NewsController extends Controller {

    public function __construct() {
        
    }

    public function listStyle01($sluggable, $cid) {
        $news = app('ClassNews')->getDataByCategoryId($cid, 20);
        $category = app('ClassCategory')->getDataById($cid);

        $config = app('ClassConfig')->getConfig();

        return view('Frontend.News.listStyle01', compact('news', 'category', 'config'));
    }

    public function detail($sluggable, $id) {
        $news = app('ClassNews')->getDataNewsByColId($id);


        //get all news is project
        $conditionsProjectLatest = [
            \TblName::NEWS_DATA . '.language' => \App::getLocale(),
            \TblName::NEWS . '.category_id' => 55,
        ];
        $orderProjectLatest = [\TblName::NEWS . '.id', 'desc'];
        $listProjectLatest = app('ClassTbl')->getDatasTblByConditions(\TblName::NEWS, $conditionsProjectLatest, 4, $orderProjectLatest);

        $newsLatest = app('ClassNews')->getDataByCategoryId($news['vi']->category_id, 6);


        $config = app('ClassConfig')->getConfig();


        $news = $news['vi'];

        //get all news latest
        $conditionsNews = [
            \TblName::NEWS_DATA . '.language' => 'vi',
        ];
        $order = [\TblName::NEWS . '.id', 'desc'];
        $subCategoryID = app('ClassCategory')->getSubCategoryID(65);
        if (!empty($subCategoryID)) {
            $whereInConditionsNews = ['category_id' => $subCategoryID];
        } else {
            $whereInConditionsNews = [];
        }
        $listNewsLatest = app('ClassTbl')->getDatasTblByConditions(\TblName::NEWS, $conditionsNews, 10, $order, $whereInConditionsNews);
//			dd($listNewsLatest);
        //tin nổi bật
        $conditionsHighlight = [
            \TblName::NEWS_DATA . '.language' => 'vi',
            \TblName::NEWS . '.type' => 'highlight'
        ];
        $listHighlight = app('ClassTbl')->getDatasTblByConditions(\TblName::NEWS, $conditionsHighlight, 5, $order);


        return view('Frontend.News.detailBasic', compact('news', 'listNewsLatest', 'listHighlight', 'config', 'newsLatest'));
    }

    public function single($sluggable, $cid) {
        $conditions = [
            \TblName::NEWS_DATA . '.language' => 'vi',
            \TblName::NEWS . '.category_id' => $cid
        ];
        $order = [\TblName::NEWS . '.id', 'desc'];
        $news = app('ClassTbl')->getDatasTblByConditions(\TblName::NEWS, $conditions, 1, $order);

        $category = app('ClassCategory')->getDataById($cid);
        $newsLatest = app('ClassNews')->getDataByCategoryId(65, 6);
        $config = app('ClassConfig')->getConfig();

        return view('Frontend.News.single', [
            'news' => $news,
            'config' => $config,
            'category' => $category,
            'newsLatest' => $newsLatest
        ]);
//        }
    }

    public function listNews($sluggable, $cid) {

        //get all news by category
        $conditions = [
            \TblName::NEWS_DATA . '.language' => \App::getLocale(),
        ];
        $order = [\TblName::NEWS . '.id', 'desc'];
        $subCategoryID = app('ClassCategory')->getSubCategoryID($cid);
        if (!empty($subCategoryID)) {
            $whereInConditionsNews = ['category_id' => $subCategoryID];
        } else {
            $whereInConditionsNews = [];
        }
        $listNews = app('ClassTbl')->getDatasTblByConditions(\TblName::NEWS, $conditions, 30, $order, $whereInConditionsNews);

        $category = app('ClassCategory')->getDataById($cid);

        $config = app('ClassConfig')->getConfig();

        return view('Frontend.News.listNews', compact('listNews', 'config', 'category'));
    }

    public function search(Request $request) {
        //get all news by category
        $listNews = app('ClassNews')->searchByKeyword($request->keyword);

        $conditionsHighlight = [
            \TblName::NEWS_DATA . '.language' => \App::getLocale(),
            \TblName::NEWS . '.type' => 'highlight'
        ];
        $order = [\TblName::NEWS . '.id', 'desc'];
        $listHighlight = app('ClassTbl')->getDatasTblByConditions(\TblName::NEWS, $conditionsHighlight, 5, $order);

        $config = app('ClassConfig')->getConfig();

        return view('Frontend.News.search', compact('listNews', 'listHighlight', 'config'));
    }

}
