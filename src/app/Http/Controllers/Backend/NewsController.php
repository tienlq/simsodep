<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\News;
use App\Model\SiteConfig;
use App\Model\Category;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Component\FormatDataComponent;
use App\Http\Component\TblComponent;
use App\Http\Component\Frontend\CategoryComponent;
use Auth;
use Illuminate\Support\Facades\Config;
use Unisharp\Laravelfilemanager\LaravelFilemanagerServiceProvider;
use Intervention\Image\ImageServiceProvider;

class NewsController extends Controller {

    public function __construct() {
        //todo:
    }

    public function listNews(Request $request) {
        //read account info
        $user = Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }

        $routeName = ['listNews', 'listNews02', 'listNews03', 'video'];
        if (isset($request->categoryID)) {
            $categoryIDSelected = intval($request->categoryID);
        } else {
            $categoryIDSelected = 0;
        }
        $htmlSelectCategory = app('ClassCategory')->OptionElement(0, $routeName, $categoryIDSelected);

        $cateIds = [];
//		$categorys = app('ClassTbl')->

        $whereInConditions = [
            \TblName::NEWS_DATA . '.language' => 'vi'
        ];

        //get all News
        $news = app('ClassNews')->getAllNews($request, $whereInConditions);
        
        $conditionsCate = [
            \TblName::CATEGORY_DATA . '.language' => 'vi'
        ];
        $categorys = app('ClassTbl')->getDatasTblByConditions(\TblName::CATEGORY, $conditionsCate);
        $cates = app('ClassCommon')->mapIdAndNameTbl($categorys, 'tid', 'name');
        //
        return view('Backend/News/list', [
            'news' => $news,
            'user' => $user,
            'htmlSelectCategory' => $htmlSelectCategory,
            'cates' => $cates
        ]);
    }

    public function editNews(Request $request, $id = 0) {
//		echo file_get_contents('https://www.amazon.com/');die;
        $user = Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }

        $news = app('ClassNews')->getDataNewsByColId($id);

        $listCategorys = Category::where('route_name', '=', 'listNews')->get();
        $routeName = ['listNews', 'listNews02', 'listNews03', 'video'];
        if (isset($news['vi']->category_id)) {
            $categoryIDSelected = intval($news['vi']->category_id);
        } else {
            $categoryIDSelected = 0;
        }
        $category = app('ClassCategory')->OptionElement(0, $routeName, $categoryIDSelected);

        if (isset($_GET['dataType']) && $_GET['dataType'] == 'land02') {
            $dataType = 'land02';
        }elseif (isset($_GET['dataType']) && $_GET['dataType'] == 'land01') {
            $dataType = 'land01';
        } elseif(isset($_GET['dataType']) && $_GET['dataType'] == 'none'){
			$dataType = 'basic';
		} else {
            if (isset($news['vi']['landingpage_id']) && $news['vi']['landingpage_id'] == 2) {
                $dataType = 'land02';
            } else if (isset($news['vi']['landingpage_id']) && $news['vi']['landingpage_id'] == 1) {
                $dataType = 'land01';
            } else {
                $dataType = 'basic';
            }
        }
        
        if(!empty($news['vi']->dataLandingpage)) {
            $dataLandingpage = json_decode($news['vi']->dataLandingpage, true);
        } else {
            $dataLandingpage = [];
        }
        
        $conditionsBlock = ['news_id' => $id];
        $htmlListBlock = app('ClassTbl')->htmlListData(\TblName::BLOCK_LANDINGPAGE, 'editLandingpage', 'deleteLandingpage', 0, true, $conditionsBlock);
		
        return view('Backend/News/edit', [
            'news' => $news,
            'listCategorys' => $listCategorys,
            'user' => $user,
            'category' => $category,
            'dataType' => $dataType,
            'dataLandingpage' => $dataLandingpage,
            'id' => $id,
            'htmlList' =>$htmlListBlock
        ]);
    }

    public function postEditNews(Request $request, $id) {
        $user = Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }
		
        try {
            \DB::beginTransaction();
            //save news
            $news = app('ClassNews')->saveNews($request, $id);

            //save news by language
            $resultSaveNewsByLang = app('ClassNewsData')->saveNewsData($news->id, $request);
            \DB::commit();
        } catch (\Exception $ex) {
            \DB::rollBack();
            return 'error: ' . $ex->getMessage();
        }
        if (!empty($request->path_redirect)) {
            return redirect($request->path_redirect);
        }
        return redirect(route('editNews',[$news->id]));
    }

    public function deleteNews($id) {
        if (intval($id) > 0) {
            $news = News::find($id);
            if (isset($news->image_thumb) && file_exists($news->image_thumb)) {
                unlink($news->image_thumb);
            }
            if (isset($news->image) && file_exists($news->image)) {
                unlink($news->image);
            }
			
			if(!empty($news))
				$news->delete();
        }
        return redirect('admin/news/list');
    }

}
