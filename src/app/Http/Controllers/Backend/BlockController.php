<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class BlockController extends Controller {

    public function __construct() {
        //todo:
    }

    public function listBlock(Request $request) {
        //read account info
        $user = \Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }
        
        if (\Session::has('msgSaveBlockConfig')) {
            $resultUpdateBlockConfig = \Session::get('msgSaveBlockConfig');
            \Session::forget('msgSaveBlockConfig');
        }

        $conditions = [];
        if (!empty($request->type)) {
            $type = $request->type;
            $conditions['type'] = $request->type;
        } else {
            $type = '';
        }
//        print_r($conditions);
        $htmlList = app('ClassTbl')->htmlListData(\TblName::BLOCK, 'editBlock', 'deleteBlock', 0, false, $conditions);
        if (!empty($request->reload))
            return view('Backend/Block/listReload', compact('htmlList', 'user', 'resultUpdateBlockConfig'));
        else
            return view('Backend/Block/list', compact('htmlList', 'user', 'resultUpdateBlockConfig'));
    }

    public function contentListBlock() {
        $htmlList = app('ClassBlock')->htmlListBlockInAdmin(0);
        return view('Backend/Block/contentList', [
            'htmlList' => $htmlList,
            'user' => $user
        ]);
    }

    public function editBlock(Request $request, $id) {
        $user = \Auth::user();

        $block = app('ClassTbl')->getRowsByConditions(\TblName::BLOCK, ['id' => $id], 1);

        if (isset($block->parent_id)) {
            $parentId = intval($block->parent_id);
        } else {
            $parentId = 0;
        }
        $langData = [];
        if (count($block) > 0) {
            foreach (Config::get('languages') as $lang => $language) {
                $conditions = [
                    'block_id' => $block->id,
                    'language' => $lang
                ];
                $langData[$lang] = app('ClassTbl')->getRowsByConditions(\TblName::BLOCK_DATA, $conditions, 1);
            }
        }

        //get type block
        if (count($block) == 0) {
            if (!empty($_GET['type'])) {
                $type = $_GET['type'];
            } else {
                $type = '';
            }
        } else {
            $type = $block->type;
        }
        $data = [
            'block' => $block,
            'id' => $id,
            'user' => $user,
            'langData' => $langData,
            'type' => $type
        ];
        if ($type == 'price')
            return view('Backend/Block/edit_type_price', $data);
        elseif ($type == 'support')
            return view('Backend/Block/edit_type_support', $data);
        elseif ($type == 'fcate')
            return view('Backend/Block/edit_type_fcate', $data);
        elseif ($type == 'fcate02')
            return view('Backend/Block/edit_type_fcate', $data);
        elseif ($type == 'fdes1' || $type == 'fdes2')
            return view('Backend/Block/edit_type_fdes', $data);
        elseif ($type == 'typeSim' || $type == 'dauso' || $type = 'sortorder')
            return view('Backend/Block/edit_typesim', $data);
        else
            return view('Backend/Block/edit', $data);
    }

    public function postEditBlock(Request $request, $id) {
        $user = \Auth::user();
        try {
            \DB::beginTransaction();

            $block = app('ClassBlock')->saveBlock($id, $request);
            $retSaveCateData = app('ClassBlock')->saveBlockData($block->id, $request);
            if ($retSaveCateData == \ReturnCode::RETURN_ERROR) {
                \DB::rollback();
                return \ReturnCode::RETURN_ERROR;
            }
            \DB::commit();
            $request->session()->flash('status', sprintf(\AppConst::MSG_ERR_EDIT_SUCCESS));
            return redirect(route('adminListBlock', ['type' => $block->type]));
        } catch (\Exception $exc) {
            \DB::rollback();
            $request->session()->flash('status', sprintf(\AppConst::MSG_ERR_EDIT_FAIL, $exc->getMessage()));
            return redirect(route('editBlock', [$id]));
        }
    }

    public function deleteBlock(Request $request) {
        if (isset($request->cid)) {
            Block::find($request->cid)->delete();
        }
        return 'success';
    }

    public function updateSortOrderBlock(Request $request) {
        if (isset($request->json)) {
            $json_arr = json_decode($request->json, true);
            $result = app('ClassBlock')->recursivelyUpdateSortOrder($json_arr);
            if ($result == 'success') {
                return 'Update success';
            } else {
                return 'Update error';
            }
        }
    }

    public function updateBlockConfig(Request $request) {
        $save = app('ClassBlock')->updateBlockConfig($request);
        \Session::put('msgSaveBlockConfig', 'Lưu thông tin cấu hình block thành công');
        return redirect(route('adminListBlock', ['type' => $request->type]));
    }

}
