<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades\DB;

class TblController extends Controller {

    public function __construct() {
        //todo:
    }

    public function updateSortOrder(Request $request) {
        if (isset($request->json) && isset($request->tname)) {
            $listID = json_decode($request->json, TRUE);
            $reslutUpdate = app('ClassTbl')->updateSortOrder($request->tname, $listID);
            if ($reslutUpdate == \ReturnCode::RETURN_SUCCESS)
                return 'Cập nhật số thứ tự thành công';
        }
        return 'Cập nhật số thứ tự thất bại';
    }

}
