<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\Address;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Http\Component\TblComponent;
use App\Http\Component\FormatDataComponent;

class AddressController extends Controller {

    public function __construct() {
        //todo:
    }

    public function listAddress() {
        //read account info
        $user = Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }

        $linkEdit = '/admin/address/edit/';
        $linkDelete = '/admin/address/delete/';
        $htmlList = TblComponent::htmlList('address', $linkEdit, $linkDelete);
        return view('Backend/Address/list', [
            'htmlList' => $htmlList,
            'user' => $user
        ]);
    }

    public function editAddress(Request $request, $id) {
        $user = Auth::user();
        $address = Address::find(intval($id));
        if (count($address) > 0) {
            $des = json_decode($address, TRUE);
        }
        return view('Backend/Address/edit', [
            'address' => $address,
            'id' => $id
        ]);
    }

    public function postEditAddress(Request $request, $id) {

        if ($request->name == '' || $request->name == '') {
            return '["error","Bạn chưa nhập Địa chỉ"]';
        }
        if ($request->title == '' || $request->title == '') {
            return '["error","Bạn chưa nhập tiêu đề"]';
        }
        
        if ($id > 0) {
            $address = Address::find($id);
        } else {
            $address = new Address;
        }
        $address->name = $request->name;
        $address->title = $request->title;
        $address->save();
        return '["success"]';
    }

    public function delete(Request $request) {
        if (isset($request->id)) {
            Address::find($request->id)->delete();
        }
        return 'success';
    }

}
