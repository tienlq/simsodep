<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller {

    public function __construct() {
        //todo:
    }

    public function listCategory(Request $request) {
        //read account info
        $user = \Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }
        $conditions = [];
        if(!empty($request->type)) {
            $conditions['type'] = $request->type;
        }
        if(!empty($request->typeShow)) {
            $conditions['route_name'] = $request->typeShow;
        }
        $htmlListCategory = app('ClassTbl')->htmlListData(\TblName::CATEGORY, 'editCategory', 'deleteCategory', 0, true, $conditions);
        return view('Backend/Category/list', [
            'htmlListCategory' => $htmlListCategory,
            'user' => $user
        ]);
    }

    public function contentListCategory() {
        $htmlListCategory = app('ClassCategory')->htmlListCategoryInAdmin(0);
        return view('Backend/Category/contentListCategory', [
            'htmlListCategory' => $htmlListCategory
        ]);
    }

    public function editCategory(Request $request, $cid) {
        $user = \Auth::user();

        $category = app('ClassCategory')->getDataById($cid);

        if (isset($category->parent_id)) {
            $parentId = intval($category->parent_id);
        } else {
            $parentId = 0;
        }
        $typeShow = [];
        $categoryHtmlOption = app('ClassCategory')->OptionElement(0, $typeShow, $parentId);
        $dataOfLang = [];
        if (count($category) > 0) {
            foreach (\Config::get('languages') as $lang => $language) {
                $conditions = [
                    'category_id' => $category->id,
                    'language' => $lang
                ];
                $dataOfLang[$lang] = app('ClassTbl')->getRowsByConditions(\TblName::CATEGORY_DATA, $conditions, 1);
            }
        }
        
        $type = 'menu';
        if($cid > 0) {
            $type = $category->type;
        } else {
            if(!empty($_GET['type'])){
                $type = $_GET['type'];
            }
        }

        return view('Backend/Category/edit', [
            'category' => $category,
            'routeName' => unserialize(ROUTE_NAME),
            'cid' => $cid,
            'categoryHtmlOption' => $categoryHtmlOption,
            'dataOfLang' => $dataOfLang,
            'type' => $type
        ]);
    }

    public function postEditCategory(Request $request, $cid) {
        $user = \Auth::user();
        try {
            \DB::beginTransaction();

            $category = app('ClassCategory')->saveCategory($cid, $request);
            $retSaveCateData = app('ClassCategory')->saveCategoryData($category->id, $request);
            if ($retSaveCateData == \ReturnCode::RETURN_ERROR) {
                \DB::rollback();
                return \ReturnCode::RETURN_ERROR;
            }

            \DB::commit();
            return \ReturnCode::RETURN_SUCCESS;
        } catch (\Exception $exc) {
            \DB::rollback();
            echo $exc->getMessage();
            return \ReturnCode::RETURN_ERROR;
        }
    }

    public function deleteCategory($cid) {
        if (isset($cid)) {
            app('ClassCategory')->deleteByCateId($cid);
        }
        return 'success';
    }

    public function updateSortOrderCategory(Request $request) {
        if (isset($request->json)) {
            $json_arr = json_decode($request->json, true);
            $result = app('ClassCategory')->recursivelyUpdateSortOrder($json_arr);
            if ($result == 'success') {
                return 'Update success';
            } else {
                return 'Update error';
            }
        }
    }

}
