<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\Contact;
use App\Model\Countries;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Component\FormatDataComponent;
use App\Http\Component\CategoryComponent;
use Auth;

class ContactController extends Controller {

    public function __construct() {
        //todo:
    }

    public function listContact(Request $request) {
        //read account info
        $user = Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }

        //get all product
        $listContact = new Contact;
        //todo: Conditions
        $listContact = $listContact->orderBy('id', 'desc');
        $listContact = $listContact->paginate(30);

        if (isset($request->reload) && $request->reload == 1) {
            return view('Backend/Contact/listReload', [
                'listContact' => $listContact,
                'user' => $user
            ]);
        } else {
            return view('Backend/Contact/list', [
                'listContact' => $listContact,
                'user' => $user
            ]);
        }
    }

    public function deleteContact($id) {
        if (intval($id) > 0) {
            Contact::find($id)->delete();
        }
        return "success";
    }

    public function detailContact($id) {
        $contact = Contact::find($id);
        
        $country = Countries::where('countryCode', '=', $contact->country)->first();
        
        return view('Backend/Contact/detail', [
                'contact' => $contact,
            'country' =>$country
            ]);
    }

}
