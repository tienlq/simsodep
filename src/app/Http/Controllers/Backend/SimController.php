<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class SimController extends Controller {

    public function __construct() {
        //todo:
    }

    public function listSim(Request $request) {
        //read account info
        $user = Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }

        $sims = app('ClassSim')->getListSim($request);

        return view('Backend/Sim/list', compact('sims', 'user'));
    }

    public function editSim(Request $request, $id) {
        $user = Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }

        $listUser = app('ClassUsers')->listUser();

        if (!empty($id)) {

            $sim = app('ClassSim')->getById($id);

            return view('Backend/Sim/edit', compact('user', 'sim', 'id', 'listUser'));
        } else {
            return view('Backend/Sim/addNew', compact('user', 'listUser', 'id'));
        }
    }

    public function postEditSim(Request $request, $id) {

        $suppliers = app('ClassTbl')->getRowsByConditions(\TblName::SUPPLIER);
        $suppliersTemplate = [];
        foreach ($suppliers as $sdata) {
            $suppliersTemplate[$sdata->id] = $sdata->sim_template;
        }

        if (!empty($id)) {
            app('ClassSim')->updateSimById($id);
        } else {
            //'/\s+/': tab or space
            $phonesPrices = preg_split('/\s+/', trim($_POST['phones_prices']));
            $datas = [];
            $datasIdx = 0;
            foreach ($phonesPrices as $idx => $pp) {
                if ($idx == 0) {
                    if (substr($pp, 0, 1) != '0')
                        $datas[$datasIdx]['phone'] = '0' . $pp;
                    else
                        $datas[$datasIdx]['phone'] = $pp;
                    $datas[$datasIdx]['sluggable'] = app('ClassCommon')->formatPhone($pp);
                } else {
                    if (($idx % 2) == 0) {
                        if (substr($pp, 0, 1) != '0')
                            $datas[$datasIdx]['phone'] = '0' . $pp;
                        else
                            $datas[$datasIdx]['phone'] = $pp;

                        $datas[$datasIdx]['sluggable'] = app('ClassCommon')->formatPhone($pp);
                    } else {
                        $saveTo = 'images/sims/phone/' . $datas[$datasIdx]['phone'] . '.png';
                        $datas[$datasIdx]['price'] = $pp;
                        $datas[$datasIdx]['user_id'] = $request->user_id;
                        $datas[$datasIdx]['supplier_id'] = app('ClassCommon')->detectNumber($datas[$datasIdx]['phone']);
                        $datas[$datasIdx]['image'] = '/' . $saveTo;

                        if (!empty($datas[$datasIdx]['supplier_id'])) {
                            //create image
                            $imgBgText = 'images/sims/supplier_template/bg_text.png';
                            $config = ['size' => 25, 'x' => 20, 'y' => 20];
                            app('ClassCommon')->addText2Image($imgBgText, $saveTo, 'Phone: ' . $datas[$datasIdx]['phone'], $config);
                            $configPrice = ['size' => 25, 'x' => 20, 'y' => 40];
                            app('ClassCommon')->addText2Image($saveTo, $saveTo, 'Price: 500.000', $configPrice);
                            //$configKho = ['size' => 25, 'x' => 20, 'y' => 60];
                            //app('ClassCommon')->addText2Image($saveTo, $saveTo, 'Kho: Asimdep.com', $configKho);
                            // $configType = ['size' => 25, 'x' => 20, 'y' => 80];
                            // app('ClassCommon')->addText2Image($saveTo, $saveTo, 'Loai: Sim tam hoa', $configType);
                            //      
                            // Load the stamp and the photo to apply the watermark to
                            $imgTemplate = imagecreatefrompng($suppliersTemplate[$datas[$datasIdx]['supplier_id']]);

                            $stamp = imagecreatefrompng($saveTo);
                            $marge_right = 10;
                            $marge_bottom = 10;
                            $sx = imagesx($stamp);
                            $sy = imagesy($stamp);

                            // Merge the stamp onto our photo with an opacity of 95%
                            imagecopymerge($imgTemplate, $stamp, 260, 105, 0, 0, 260, 170, 93);

                            // Save the image to file and free memory
                            imagepng($imgTemplate, $saveTo);
                        }

                        $datasIdx++;
                    }
                }
            }
            $resultIns = app('ClassSim')->insertRows($datas);
            if ($resultIns)
                return redirect(route('adminListSim', ['add' => 'success']));
            else
                return redirect(route('adminListSim', ['add' => 'fail']));
        }
        return redirect(route('adminListSim'));
    }

    public function deleteSim(Request $request) {
        if (isset($request->id)) {
            Address::find($request->id)->delete();
        }
        return 'success';
    }

}
