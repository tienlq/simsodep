<?php

namespace App\Http\Controllers\Backend;

use Auth;
use App\Http\Controllers\Controller;

class PagesController extends Controller {

	public function __construct() {
		//todo:
	}

	public function home() {
		$user = Auth::user();
		if (intval($user->userType) == 0) {
			return redirect('/404');
		}

		return view('Backend/Pages/home', ['user' => $user]);
	}

	public function listBlockLandingpage($landType, $newsId) {
		$user = Auth::user();
		if ($landType == 1) {
			$totalLandingpages = 7;
		} else if ($landType == 2) {
			$totalLandingpages = 3;
		}
		echo $landType;

		return view('Backend/Pages/listBlockLandingpage', [
			'landType' => $landType,
			'totalLandingpages' => $totalLandingpages,
			'newsId' => $newsId
		]);
	}

	public function fontAwesome() {

		return view('Backend/Pages/fontAwesome');
	}

}
