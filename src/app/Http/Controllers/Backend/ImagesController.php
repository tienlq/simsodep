<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\Images;
use App\Model\Category;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Component\FormatDataComponent;
use Auth;

class ImagesController extends Controller {

    public function __construct() {
        //todo:
    }

    public function listImages(Request $request) {
        //get session user
        $user = Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }
        //get all images

        $listImage = new Images;
        if (isset($request->type) && $request->type != '') {
            $listImage = $listImage->where('type', '=', $request->type);
        }
        $listImage = $listImage->paginate(30);

        return view('Backend/Images/list', [
            'user' => $user,
            'listImage' => $listImage
        ]);
    }

    public function editImages(Request $request, $id = 0) {
        $user = Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }

        $image = Images::find(intval($id));

        return view('Backend/Images/edit', [
            'image' => $image,
            'user' => $user,
            'id' => $id
        ]);
    }

    public function postEditImages(Request $request, $id) {

        if ($id > 0) {
            $images = Images::find($id);
        } else {
            $images = new Images;
        }
        $file = $request->file('image');
        if (count($file) > 0) {
            $destinationPath = 'img/category';
            $filename = $file->getClientOriginalName();
            $filePath = $destinationPath . '/' . $filename;
            $file->move($destinationPath, $filename);
        } else {
            $filePath = $request->hiddenImage;
        }
        $content = [];
        if (isset($request->content) && count($request->content) > 0) {
            foreach ($request->content as $c) {
                if ($c != '') {
                    $content[] = $c;
                }
            }
        }

        $images->linkImage = $filePath;
        $images->title = $request->title;
        $images->linkClick = $request->linkClick;
        $images->description = $request->description;
        $images->type = $request->type;
        $images->save();
        return redirect('admin/images/list');
    }

    public function deleteImages($id) {
        if (intval($id) > 0) {
            $image = Images::find($id);
            if (count($image) > 0) {
                if (isset($image->linkImage) && file_exists($image->linkImage)) {
                    unlink($image->linkImage);
                }
                $image->delete();
            }
        }
        return 'success';
//        return redirect('admin/images/list');
    }

}
