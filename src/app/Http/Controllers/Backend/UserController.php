<?php

namespace App\Http\Controllers\Backend;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Customer;
use App\Model\Users;

class UserController extends Controller {

    public function __construct() {
        //todo:
    }

    public function listUser(Request $request) {
        $user = Auth::user();

        $list = new Users;
        $list = $list->where('userType', '!=', 1);
        if(!empty($request->keyword)) {
            $list = $list->where('fullname', 'like', '%'.$request->keyword.'%');
        }
        $list = $list->orderBy('id', 'desc');
        $list = $list->paginate(30);

        if ($request->reload == 1) {
            return view('Backend/User/listReload', [
                'listUser' => $list,
            ]);
        } else {
            return view('Backend/User/list', [
                'listUser' => $list,
                'user' => $user
            ]);
        }
    }

    public function detailUser($id) {
        $detailUser = Users::find($id);

        return view('Backend/User/detail', [
            'detailUser' => $detailUser,
        ]);
    }

    public function editUser($id) {
        $user = Auth::user();
           // $customer = Customer::find($id);
        $userEdit = Users::find($id);
        return view('Backend/User/edit',compact('user', 'userEdit', 'id'));
    }

    public function postEditUser(request $request, $id) {
        $user = Auth::user();

        app('ClassUsers')->saveUser($id, $request);
        return redirect(route('adminListUser'));
    }

    public function deleteUser($id) {
        if ($id > 0) {
            Users::find($id)->delete();
        }
        return 'success';
    }

    //profile
    public function changeProfile() {
        $user = Auth::user();
        return view('Backend.User.changeProfile', [
            'user' => $user,
            'id' => $user->id
        ]);
    }

    public function postChangeProfile(request $request) {
        //get detail user infomation
        $user = Auth::user();

        //save user
        $user = Users::find($user->id);
        if (isset($request->password) && $request->password != '') {
            $user->password = bcrypt($request->password);
        }
        $user->save();

        return view('Backend.User.changeProfile', [
            'user' => $user,
            'id' => $user->id
        ]);
    }

}
