<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\News;
use App\Model\Category;
use Illuminate\Http\Request;

class BlockConfigController extends Controller {

    public function __construct() {
        //todo:
    }

    public function listBlockConfig(Request $request) {
        //read account info
        $user = \Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }

        //get all News
        $blockConfig = app('ClassConfigBlock')->getAll();
        //
        return view('Backend/BlockConfig/list', [
            'blockConfig' => $blockConfig,
            'user' => $user,
        ]);
    }

    public function editBlockConfig(Request $request, $id = 0) {
        //read account info
        $user = \Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }
        
        $blockConfig = app('ClassConfigBlock')->getById($id);
        return view('Backend/BlockConfig/edit', [
            'configBlock' => $blockConfig,
            'user' => $user,
        ]);
    }

    public function postEditBlockConfig(Request $request, $id) {
        $user = \Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }
        
        app('ClassConfigBlock')->saveBlock($id, $request);
        
        return redirect(route('adminListBlockConfig'));
    }

    public function deleteNews($id) {
        if (intval($id) > 0) {
            $news = News::find($id);
            if (isset($news->image_thumb) && file_exists($news->image_thumb)) {
                unlink($news->image_thumb);
            }
            if (isset($news->image) && file_exists($news->image)) {
                unlink($news->image);
            }
			
			if(!empty($news))
				$news->delete();
        }
        return redirect('admin/news/list');
    }

}
