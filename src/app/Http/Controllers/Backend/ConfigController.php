<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Model\SiteConfig;

class ConfigController extends Controller {

    public function __construct() {
        //todo:
    }

    public function changeFooter(Request $request) {
        $user = Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }

        $siteConfig = SiteConfig::find(1);

        return view('Backend/Config/changeFooter', [
            'siteConfig' => $siteConfig,
            'user' => $user
        ]);
    }

    public function postChangeFooter(Request $request) {
        $user = Auth::user();
        $siteConfig = SiteConfig::find(1);
        $siteConfig->footer = $request->footer;
        $siteConfig->save();

        return view('Backend/Config/changeFooter', [
            'siteConfig' => $siteConfig,
            'user' => $user
        ]);
    }

    public function configSite(Request $request) {
        $user = Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }

        $siteConfig = [
            'vi' => SiteConfig::find(1),
            'en' => SiteConfig::find(2)
        ];
        return view('Backend/Config/configSite', [
            'siteConfig' => $siteConfig,
            'user' => $user
        ]);
    }

    public function postConfigSite(Request $request) {
		$user = Auth::user();
		//save favicon file
        $faviconFile = $request->file('favicon');
        if (count($faviconFile) > 0) {
            move_uploaded_file($_FILES['favicon']['tmp_name'], 'favicon.ico');
        }
		
        foreach (\Config::get('languages') as $lang => $language) {
			if($lang == 'vi') {
				$id = 1;
			} else {
				$id = 2;
			}
			
			$siteConfig = SiteConfig::find($id);
			$siteConfig->background_pc = $request->background_pc;
			$siteConfig->code01 = $request->code01;
			$siteConfig->code02 = $request->code02;
			$siteConfig->video_home = $request->video_home;
			$siteConfig->background_mobile = $request->background_mobile;
			$siteConfig->email = $request->email;
			$siteConfig->name = $request->name;
			$siteConfig->phone = $request->phone;
			$siteConfig->title = $request->title[$lang];
			$siteConfig->keyword = $request->keyword[$lang];
			$siteConfig->description = $request->description[$lang];
			$siteConfig->logo = $request->logo[$lang];
			$siteConfig->save();
		}

        return redirect(route('configSite'));
    }
	
	public function changeBlockHome(Request $request) {
        $user = Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }

        $siteConfig = [
            'vi' => SiteConfig::find(1),
            'en' => SiteConfig::find(2)
        ];
        return view('Backend/Config/blockHome', [
            'siteConfig' => $siteConfig,
            'user' => $user
        ]);
    }

    public function postChangeBlockHome(Request $request) {
		$user = Auth::user();
		//save favicon file
        $faviconFile = $request->file('favicon');
        if (count($faviconFile) > 0) {
            move_uploaded_file($_FILES['favicon']['tmp_name'], 'favicon.ico');
        }
		
        foreach (\Config::get('languages') as $lang => $language) {
			if($lang == 'vi') {
				$id = 1;
			} else {
				$id = 2;
			}
			
			$siteConfig = SiteConfig::find($id);
			$siteConfig->block01 = $request->block01[$lang];
			$siteConfig->block02 = $request->block02[$lang];
			$siteConfig->save();
		}

        return redirect(route('changeBlockHome'));
    }

}
