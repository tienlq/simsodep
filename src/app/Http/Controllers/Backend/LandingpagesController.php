<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class LandingpagesController extends Controller {

    public function __construct() {
        //todo:
    }

    public function listLandingpage(Request $request) {
        //read account info
        $user = Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }

        $conditions = [
            \TblName::CATEGORY_DATA . '.language' => 'vi',
            \TblName::CATEGORY . '.route_name' => 'landingPage01'
        ];
        $listCategorys = app('ClassTbl')->getDatasTblByConditions(\TblName::CATEGORY, $conditions);

        return view('Backend.Landingpages.listLandingpage', [
            'user' => $user,
            'listCategorys' => $listCategorys
        ]);
    }

    public function editLandingpage(Request $request, $newsId, $landId, $id) {
//        echo '$newsId:' . $newsId . '<br>';
//        echo '$landId:' . $landId . '<br>';
//        echo '$id:' . $id . '<br>';
        
        $block = app('ClassBlockLandingpage')->getById($id);
        
        if(!empty($block)) {
            $dataLandingpage = json_decode($block->postData, true);
        } else {
            $dataLandingpage = [];
        }
        
        $idx = 0;
        return view('Backend.Landingpages.editLandingpage', [
            'newsId' => $newsId,
            'landId' => $landId,
            'id' => $id,
            'block' => $block,
            'dataLandingpage' => $dataLandingpage
        ]);
    }

    public function postEditLandingpage(Request $request, $newsId, $landId, $id) {

        $user = Auth::user();
        $block = app('ClassBlockLandingpage')->save($request, $newsId, $landId, $id);

        return 'success';
    }
    
    public function listBlockByNews($newsId){
        $conditions = ['news_id' => $newsId];
        $list = app('ClassTbl')->getRowsByConditions(\TblName::BLOCK_LANDINGPAGE, $conditions);
        
        dd($list);
        
        return view('Backend.Landingpages.listBlockByNews', [
            'list' => $list,
        ]); 
    }
    
    public function deleteBlock($id) {
        if (!empty($id)) {
            app('ClassBlockLandingpage')->deleteById($id);
        }
        return 'success';
    }

}
