<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\Video;
use App\Model\Category;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Component\FormatDataComponent;
use App\Http\Component\TblComponent;
use Auth;

class VideoController extends Controller {

    public function __construct() {
        //todo:
    }

    public function listVideo() {
        //get session user
        $user = Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }

        //get all Video
        $list = Video::paginate(30);

        return view('Backend/Video/list', [
            'user' => $user,
            'list' => $list
        ]);
    }

    public function editVideo(Request $request, $id = 0) {
        $user = Auth::user();
        if (intval($user->userType) == 0) {
            return redirect('/404');
        }
        $video = Video::find(intval($id));
        if (isset($video->categoryID)) {
            $cateID = $video->categoryID;
        } else {
            $cateID = 0;
        }

        $typeShow = ['VIDEO'];
        $categoryHtmlOption = TblComponent::OptionElement('category', 0, $typeShow, $cateID);

        return view('Backend/Video/edit', [
            'video' => $video,
            'user' => $user,
            'id' => $id,
            'categoryHtmlOption' => $categoryHtmlOption
        ]);
    }

    public function postEditVideo(Request $request, $id) {

        if ($id > 0) {
            $video = Video::find($id);
        } else {
            $video = new Video;
        }
        $file = $request->file('image');
        if (count($file) > 0) {
            $destinationPath = 'img/category';
            $filename = $file->getClientOriginalName();
            $filePath = $destinationPath . '/' . $filename;
            $file->move($destinationPath, $filename);
        } else {
            $filePath = $request->hiddenImage;
        }
        $content = [];
        if (isset($request->content) && count($request->content) > 0) {
            foreach ($request->content as $c) {
                if ($c != '') {
                    $content[] = $c;
                }
            }
        }

        $video->image = $filePath;
        $video->title = $request->title;
        $video->youtube = $request->youtube;
        $video->youtubeMobie = $request->youtubeMobie;
        $video->categoryID = $request->categoryID;
        $video->sluggable = FormatDataComponent::formatText($request->title);
        $video->save();
        return redirect('admin/video/list');
    }

    public function deleteVideo($id) {
        if (intval($id) > 0) {
            Video::find($id)->delete();
        }
        return 'success';
    }

}
