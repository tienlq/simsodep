<?php

/*
  <div class="hd-col m3"><figure><ul class="mnsub">
  <li><a " href="/" rel="nofollow">Công nghệ</a></li>
  <li><a title="Đ" href="" rel="nofollow">Liên hệ</a></li>
  </ul>
  </figure>
  </div>
 */

namespace App\Http\Component\Frontend;

use App\Model\Category;

class CategoryComponent {

    public static function htmlMenuPC($parentID) {
        $listCategory = Category::where('parentID', $parentID)
                ->orderBy('position', 'asc')
                ->get();
        $htmlCategory = '<ul class="menupc">';
        if (count($listCategory) > 0) {
            $idx = 1;
            foreach ($listCategory as $category) {
                if ($idx == 1) {
                    $idx++;
                    $active = 'active';
                } else {
                    $active = '';
                }
                $link = CategoryComponent::getLinkMenu($category);
                $htmlCategory .= '<li class="hd-dropdown-hover ' . $active . '"><a href="' . $link . '">' . nl2br($category->name) . '</a>';
                $subMenu = Category::where('parentID', $category->id)->orderBy('position', 'asc')->get();
                if (count($subMenu) > 0) {

                    $htmlCategory .= '<div class="hd-dropdown-content"><article class="hd-row">';
                    foreach ($subMenu as $sm) {
                        $item = $sm->itemOfCollumn;

                        $htmlCategory .= '<div class="hd-col m3"><figure><div class="mntt">' . nl2br($sm->name) . '</div>';

                        $subSubMenu = Category::where('parentID', $sm->id)->orderBy('position', 'asc')->get();
                        $idx = 0;
                        if (count($subSubMenu) > 0) {
                            $idx++;
                            $htmlCategory .= '<ul class="ss_menu">';
                            $idx = 0;
                            foreach ($subSubMenu as $ssm) {
                                $subSubMenu_2 = Category::where('parentID', $ssm->id)->orderBy('position', 'asc')->get();

                                $link = CategoryComponent::getLinkMenu($ssm);
                                if ($item > 0 && $idx == $item) {
                                    $idx = 1;
                                    $htmlCategory .= '</ul></figure></div>';
                                    $htmlCategory .= '<div class="hd-col m3"><figure><ul class="mnsub">';
                                    $htmlCategory .= ' <li><a title="" href="' . $link . '">' . $ssm->name . '</a></li>';
                                    continue;
                                }
                                $htmlCategory .= ' <li><a title="" href="' . $link . '">' . $ssm->name . '</a>';
                                if (count($subSubMenu_2) > 0) {
                                    $htmlCategory .= '<ul class="sss_menu">';
                                    foreach ($subSubMenu_2 as $ssm2) {
                                        $link2 = CategoryComponent::getLinkMenu($ssm2);
                                        $htmlCategory .= ' <li><a href="' . $link2 . '">' . $ssm2->name . '</a></li>';
                                    }
                                    $htmlCategory .= '</ul>';
                                }
                                $htmlCategory .= '</li>';

                                $idx++;
                            }
                            $htmlCategory .= '</ul>';
                        }

                        $htmlCategory .= '</figure></div>';
                    }
                    $htmlCategory .= "</article></div></li>";
                }
            }
        }
        $htmlCategory .= '</ul>';
        return($htmlCategory);
    }

    public static function htmlMenuMobile() {
        $listCategory = Category::where('mobile', 1)
                ->orderBy('position', 'asc')
                ->get();
        $htmlCategory = '<ul class="danhmuc">';
        $htmlCategory .= '<li><a href="/"> Trang chủ</a></li>';
        if (count($listCategory) > 0) {
            $idx = 1;
            foreach ($listCategory as $category) {
                $idx++;
                $subMenu = Category::where('parentID', $category->id)->orderBy('position', 'asc')->get();
                if (count($subMenu) > 0) {
                    $htmlCategory .= '<li><a class="tablink" onclick="openMenu(\'mndv' . $idx . '\')"">' . $category->name . '</a>';
                    $htmlCategory .= '<ul id="mndv' . $idx . '" class="mnsub" style="display:none">';
                    foreach ($subMenu as $sm) {
                        $subLink = CategoryComponent::getLinkMenu($sm);
                        $htmlCategory .= '<li><a href="' . $subLink . '"> <span style="color:#fff"> > </span>' . $sm->name . '</a></li>';
                    }
                    $htmlCategory .= "</ul></li>";
                } else {
                    $link = CategoryComponent::getLinkMenu($category);
                    $htmlCategory .= '<li><a href="' . $link . '">' . $category->name . '</a>';
                }
            }
        }
        $htmlCategory .= '</ul>';
        return($htmlCategory);
    }

    public static function getLinkMenu($cate) {
        if ($cate->typeShow == 'LIST_PRODUCT') {
            $link = '/product/' . $cate->sluggable . '/c' . $cate->id . '.html';
        } else if ($cate->typeShow == 'LIST_NEWS') {
            $link = '/' . $cate->sluggable . '/ln' . $cate->id . '.html';
        } else if ($cate->typeShow == 'SINGLE_NEWS') {
            $link = '/' . $cate->sluggable . '/sn' . $cate->id . '.html';
        } else if ($cate->typeShow == 'CONTACT') {
            $link = '/contact.html';
        } else if ($cate->typeShow == 'VIDEO') {
            $link = '/' . $cate->sluggable . '/lv' . $cate->id . '.html';
        } else if ($cate->typeShow == 'LIBRARY') {
            $link = '/' . $cate->sluggable . '/lib' . $cate->id . '.html';
        } else {
            $link = $cate->typeShow;
        }
        return $link;
    }

    private static function checkSubmenu($parentID) {
        return Category::where('parentID', $parentID)->count();
    }

    private static function stringSubCategoryID($parentID, $data = '') {
        if (CategoryComponent::checkSubmenu($parentID)) {
            $category = Category::where('parentID', $parentID)->get();
            ;
            foreach ($category as $cate) {
                $data = $data . "," . $cate->id;
                if (CategoryComponent::checkSubmenu($cate->id)) {
                    $data = CategoryComponent::stringSubCategoryID($cate->id, $data);
                }
            }
        }
        return($data);
    }

    public static function getSubCategoryID($parentID) {
        $c = $parentID . CategoryComponent::stringSubCategoryID($parentID);
        $m = explode(",", $c);
        return $m;
    }

}
