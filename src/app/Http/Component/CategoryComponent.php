<?php

namespace App\Http\Component;

use App\Model\Category;

class CategoryComponent {

    public static function htmlListCategory($parentID) {
        $listCategory = Category::where('parentID', $parentID)
                ->orderBy('position', 'asc')
                ->get();
        $htmlListCategory = '';
        if (count($listCategory) > 0) {
            $htmlListCategory .= '<ol class="dd-list">';
            $idx = 0;
            foreach ($listCategory as $category) {
                $idx++;
                //check show in footer
                if(isset($category->footer) && $category->footer == 1) {
                    $showFooter = "<em style='font-size:11px; color:blue'> (footer)</em>";
                } else {
                    $showFooter = '';
                }
                
                //check show in mobile
                if(isset($category->mobile) && $category->mobile == 1) {
                    $showMobile = "<em style='font-size:11px; color:red'> (mobile)</em>";
                } else {
                    $showMobile = '';
                }
                
                $htmlListCategory .= '<li data-id="' . $category->id . '" class="dd-item">'
                        . '<div class="option-menu">'
                        . '<a onclick="loadPopupLarge(\'/admin/category/edit/' . $category->id . '\')" data-toggle="modal" data-target=".bs-modal-lg"><i data-pack="default" class="ion-edit"></i></a>&nbsp;&nbsp;'
                        . '<a onclick="deleteCategory(\'' . $category->id . '\')"  tabindex="-1"><i data-pack="default" class="ion-trash-a"></i></a></div>'
                        . '<div class="card b0 dd-handle"><div class="mda-list">'
                        . '<div class="mda-list-item"><div class="mda-list-item-icon item-grab"><em class="ion-drag icon-lg"></em></div>'
//                        . $image
                        . '<div class="mda-list-item-text mda-2-line">';
                $htmlListCategory .= '<h4>' . $category->name . $showFooter . $showMobile .' </h4>';
                $htmlListCategory .= '</div><div class="_right">'
                        . '</div></div></div></div>';
                $subMenu = Category::where('parentID', $category->id)->count();
                if ($subMenu > 0) {
                    $htmlListCategory .= CategoryComponent::htmlListCategory($category->id);
                }
                $htmlListCategory .= "</li>";
            }
            $htmlListCategory .= '</ol>';
        }
        return($htmlListCategory);
    }

    public static function recursivelyUpdatePositionCategory($listCateID, $parentId = 0) {
        $idx = 0;
        foreach ($listCateID as $cate) {

//            if (15%$idx == 0){
//                sleep(1);
//            }

            $idx++;
            $category = Category::find($cate['id']);
            $category->position = $idx;
            $category->parentID = $parentId;
            $category->save();
            if (isset($cate['children'])) {
                CategoryComponent::recursivelyUpdatePositionCategory($cate['children'], $cate['id']);
            }
        }
        return 'success';
    }

    public static function OptionElement($parentID, $typeShow, $selectedID, $space = '&nbsp;&nbsp;') {

        $listCategory = Category::where('parentID', $parentID)
                ->where('typeShow', $typeShow)
                ->orderBy('position', 'asc')
                ->get();
        $htmlOption = '';
        foreach ($listCategory as $cate) {
            if ($cate->id == $selectedID) {
                $selected = ' selected ';
            } else {
                $selected = '';
            }
            $htmlOption .= '<option value="' . $cate->id . '">' . $space . $cate->name . '</option>';

            $countSubCategory = Category::where('parentID', $cate->id)
                    ->where('typeShow', $typeShow)
                    ->count();
            if ($countSubCategory > 0) {
                $space .= $space . $space . $space;
                $htmlOption .= CategoryComponent::OptionElement($cate->id, $typeShow, $selectedID, $space);
            }
        }

        return($htmlOption);
    }

}
