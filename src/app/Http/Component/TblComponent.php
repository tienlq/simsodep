<?php

namespace App\Http\Component;

use Illuminate\Support\Facades\DB;
class TblComponent {

    public static function htmlList($table, $linkEdit, $linkDelete) {
        
        $list = DB::table($table)
                     ->orderBy('softOrder', 'asc')
                     ->get();
        
        $htmlList = '';
        if (count($list) > 0) {
            $htmlList .= '<ol class="dd-list">';
            foreach ($list as $tbl) {
                $htmlList .= '<li data-id="' . $tbl->id . '" class="dd-item">'
                        . '<div class="option-menu">'
                        . '<a onclick="loadPopupLarge(\'' . $linkEdit . $tbl->id . '\')" data-toggle="modal" data-target=".bs-modal-lg"><i data-pack="default" class="ion-edit"></i></a>&nbsp;&nbsp;'
                        . '<a onclick="deleteRow(\'' . $linkDelete . $tbl->id . '\')"  tabindex="-1"><i data-pack="default" class="ion-trash-a"></i></a></div>'
                        . '<div class="card b0 dd-handle"><div class="mda-list">'
                        . '<div class="mda-list-item"><div class="mda-list-item-icon item-grab"><em class="ion-drag icon-lg"></em></div>'
//                        . $image
                        . '<div class="mda-list-item-text mda-2-line">';
                $htmlList .= '<h4>' . $tbl->name . '</h4>';
                $htmlList .= '</div><div class="_right">'
                        . '</div></div></div></div>';
                $htmlList .= "</li>";
            }
            $htmlList .= '</ol>';
        }
        return($htmlList);
    }

    public static function updatePosition($listCateID) {
        
        return 'success';
    }

    public static function OptionElement($tbl, $parentID, $typeShow, $selectedID, $space = '&nbsp;&nbsp;&nbsp;&nbsp;') {
        $listCategory = DB::table($tbl)->where('parentID', $parentID)
                ->whereIn('typeShow', $typeShow)
                ->orderBy('position', 'asc')
                ->get();
        $htmlOption = '';
        foreach ($listCategory as $cate) {
            if ($cate->id == $selectedID) {
                $selected = ' selected ';
            } else {
                $selected = '';
            }
            $htmlOption .= '<option '.$selected.' value="' . $cate->id . '">' . $space . $cate->name . '</option>';
            
            $countSubCategory = DB::table($tbl)->where('parentID', $cate->id)
                ->whereIn('typeShow', $typeShow)
                ->count();
            if($countSubCategory > 0) {
                $space2 = $space . $space;
                $htmlOption .= TblComponent::OptionElement($tbl, $cate->id, $typeShow, $selectedID, $space2);
            }
        }

        return($htmlOption);
    }

}
