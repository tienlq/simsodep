<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //Utils
        $this->app->singleton(
                'ClassCommon', function () {
            return new \App\Services\Utils\ClassCommon;
        }); 
        $this->app->singleton(
                'AppConst', function () {
            return new \App\Services\Utils\AppConst;
        });
        $this->app->singleton(
                'ClassEmail', function () {
            return new \App\Services\Utils\ClassEmail;
        });
        $this->app->singleton(
                'ClassValidationRequest', function () {
            return new \App\Services\Utils\ClassValidationRequest;
        });
        $this->app->singleton(
                'ReturnCode', function () {
            return new \App\Services\Utils\ReturnCode;
        });
        $this->app->singleton(
                'LandingPagesHelper', function () {
            return new \App\Services\Utils\LandingPagesHelper;
        });
        $this->app->singleton(
                'ClassHelper', function () {
            return new \App\Services\Utils\ClassHelper;
        });
        
        
        //Entity
        $this->app->singleton(
                'ClassTbl', function () {
            return new \App\Services\Entity\ClassTbl;
        });
        $this->app->singleton(
                'ClassNews', function () {
            return new \App\Services\Entity\ClassNews;
        });
        $this->app->singleton(
                'ClassNewsData', function () {
            return new \App\Services\Entity\ClassNewsData;
        });
         $this->app->singleton(
                'ClassProducts', function () {
            return new \App\Services\Entity\ClassProducts;
        });
        
        $this->app->singleton(
                'ClassCategory', function () {
            return new \App\Services\Entity\ClassCategory;
        });
        
        $this->app->singleton(
                'ClassCategoryData', function () {
            return new \App\Services\Entity\ClassCategoryData;
        });
        $this->app->singleton(
                'ClassBlock', function () {
            return new \App\Services\Entity\ClassBlock;
        });
		$this->app->singleton(
                'ClassConfig', function () {
            return new \App\Services\Entity\ClassConfig;
        });
		$this->app->singleton(
                'ClassContact', function () {
            return new \App\Services\Entity\ClassContact;
        });
        $this->app->singleton(
                'ClassBlockLandingpage', function () {
            return new \App\Services\Entity\ClassBlockLandingpage;
        });
        $this->app->singleton(
                'ClassSim', function () {
            return new \App\Services\Entity\ClassSim;
        });
        $this->app->singleton(
                'ClassUsers', function () {
            return new \App\Services\Entity\ClassUsers;
        });
        $this->app->singleton(
                'ClassConfigBlock', function () {
            return new \App\Services\Entity\ClassConfigBlock;
        });
        $this->app->singleton(
                'ClassSupplier', function () {
            return new \App\Services\Entity\ClassSupplier;
        });
        
    }
}
