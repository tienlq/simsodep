<?php

namespace App\Services\Utils;

class TblName {

    const ADDRESS = 'address';
    const CATEGORY = 'category';
    const CATEGORY_DATA = 'category_data';
    const CHUYEN_GIA = 'chuyengia';
    const CONTACT = 'contact';
    const DICH_VU_HOT = 'dichvuhot';
    const HOP_TAC = 'hoptac';
    const IMAGES = 'images';
    const IMAGE_PRODUCT = 'image_product';
    const IMAGE_SPA = 'image_spa';
    const LIBRARY_IMAGES = 'library_images';
    const NEWS = 'news';
    const NEWS_DATA = 'news_data';
    const ORDERS = 'orders';
    const PAYPAL = 'paypal';
    const PRODUCT = 'products';
    const PROJECT = 'project';
    const QUESTION = 'question';
    const SITE_CONFIG = 'site_config';
    const SUPPORT_ONLINE = 'support_online';
    const USER = 'users';
    const BLOCK = 'block';
    const BLOCK_DATA = 'block_data';
    const NEWS_LANDINGPAGES = 'news_landingpages';
    const BLOCK_LANDINGPAGE = 'block_landingpage';
    const SIM = 'sim';
    const SUPPLIER = 'supplier';
    const CONFIG_BLOCK = 'block_config';
}
