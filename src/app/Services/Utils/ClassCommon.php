<?php

namespace App\Services\Utils;

class ClassCommon {

    public function formatText($string, $ext = '') {
        // remove all characters that aren"t a-z, 0-9, dash, underscore or space
        $string = strip_tags(str_replace('&nbsp;', ' ', $string));
        $string = str_replace('&quot;', '', $string);

        $string = self::_utf8ToAscii($string);
        $NOT_acceptable_characters_regex = '#[^-a-zA-Z0-9_ /]#';
        $string = preg_replace($NOT_acceptable_characters_regex, '', $string);
        // remove all leading and trailing spaces
        $string = trim($string);
        // change all dashes, underscores and spaces to dashes
        $string = preg_replace('#[-_]+#', '-', $string);
        $string = str_replace(' ', '-', $string);
        $string = preg_replace('#[-]+#', '-', $string);

        $string = str_replace('/', '-', $string);
        $string = str_replace('\'', '-', $string);

        return strtolower($string . $ext);
    }

    public static function _utf8ToAscii($str) {
        $chars = array(
            'a' => array('ấ', 'ầ', 'ẩ', 'ẫ', 'ậ', 'Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ', 'á', 'à', 'ả', 'ã', 'ạ', 'â', 'ă', 'Á', 'À', 'Ả', 'Ã', 'Ạ', 'Â', 'Ă'),
            'e' => array('ế', 'ề', 'ể', 'ễ', 'ệ', 'Ế', 'Ề', 'Ể', 'Ễ', 'Ệ', 'é', 'è', 'ẻ', 'ẽ', 'ẹ', 'ê', 'É', 'È', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê'),
            'i' => array('í', 'ì', 'ỉ', 'ĩ', 'ị', 'Í', 'Ì', 'Ỉ', 'Ĩ', 'Ị'),
            'o' => array('ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'Ố', 'Ồ', 'Ổ', 'Ô', 'Ộ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ', 'ó', 'ò', 'ỏ', 'õ', 'ọ', 'ô', 'ơ', 'Ó', 'Ò', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ơ'),
            'u' => array('ứ', 'ừ', 'ử', 'ữ', 'ự', 'Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự', 'ú', 'ù', 'ủ', 'ũ', 'ụ', 'ư', 'Ú', 'Ù', 'Ủ', 'Ũ', 'Ụ', 'Ư'),
            'y' => array('ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ', 'Ý', 'Ỳ', 'Ỷ', 'Ỹ', 'Ỵ'),
            'd' => array('đ', 'Đ'),
        );
        foreach ($chars as $key => $arr) {
            $str = str_replace($arr, $key, $str);
        }
        return $str;
    }

    public function mapIdAndNameTbl($arr, $id, $name) {
        $result = [];
        foreach ($arr as $val) {
            $result[$val->$id] = $val->$name;
        }
        return $result;
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /*
     * Detect carrier name by phone number
     *
     * @param (string) ($number) The input phone number
     * @return (mixed) Name of the carrier, false if not found
     */

    public function detectNumber($number) {
        $carriers_number = array(
            //viettel
            '096' => 1,
            '097' => 1,
            '098' => 1,
            '0162' => 1,
            '0163' => 1,
            '0164' => 1,
            '0165' => 1,
            '0166' => 1,
            '0167' => 1,
            '0168' => 1,
            '0169' => 1,
            '032' => 1,
            '033' => 1,
            '034' => 1,
            '035' => 1,
            '036' => 1,
            '037' => 1,
            '038' => 1,
            '039' => 1,
            //mobile
            '090' => 3,
            '077' => 3,
            '070' => 3,
            '078' => 3,
            '089' => 3,
            '093' => 3,
            '0120' => 3,
            '0121' => 3,
            '0122' => 3,
            '0126' => 3,
            '0128' => 3,
            //vina
            '091' => 2,
            '094' => 2,
            '088' => 2,
            '083' => 2,
            '085' => 2,
            '0123' => 2,
            '0124' => 2,
            '0125' => 2,
            '0127' => 2,
            '0129' => 2,
            //gphone
            '0993' => 5,
            '0994' => 5,
            '0995' => 5,
            '0996' => 5,
            '0997' => 5,
            '0199' => 5,
            //vietnam
            '092' => 4,
            '0186' => 4,
            '0188' => 4,
//            '095' => 'SFone'
        );

        $number = str_replace(array('-', '.', ' '), '', $number);
        if (substr($number, 0, 1) != '0') {
            $number = '0' . $number;
        }
        if (isset($carriers_number[substr($number, 0, 3)]))
            return $carriers_number[substr($number, 0, 3)];
        elseif (isset($carriers_number[substr($number, 0, 4)]))
            return $carriers_number[substr($number, 0, 4)];

        return 0;
    }

    /*
     * Check if a string is started with another string
     *
     * @param (string) ($needle) The string being searched for.
     * @param (string) ($haystack) The string being searched
     * @return (boolean) true if $haystack is started with $needle
     */

    public function startWith($needle, $haystack) {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) == $needle);
    }

    public function addText2Image($imgTemplate, $saveTo, $text, $config) {

        $img = \Image::make(public_path('/' . $imgTemplate));
        $img->text(html_entity_decode($text, ENT_QUOTES, "ISO-8859-1"), $config['x'], $config['y'], function($font) use ($config) {
//            $font->file(public_path('font/tahoma.ttf'));
            $font->file(5);
            $font->size($config['size']);
            $font->color('#000000');
            $font->angle(90);
            $font->valign('top');
            $font->align('left');
        });
        $img->save(public_path($saveTo));
    }

    public function formatPhone($phone) {
        $sluggable = str_replace(array('-', '.', ',', '-', ' '), '', $phone);

        return $sluggable;
    }

}
