<?php

namespace App\Services\Utils;

class LandingPagesHelper {

    public function blockLandingPage31($request) {
        $html = [];
        foreach (\Config::get('languages') as $lang => $language) {
            
            if(isset($request->blocks33) && !in_array(1, $request->blocks33)){
                $html[$lang] = '';
                continue;
            }
            
            if ($request->title_31[$lang] != '' || $request->description_31[$lang] != '' || $request->img31 != '') {

                if ($request->description_31[$lang] != '') {
                    $description_31 = nl2br($request->description_31[$lang]);
                } else {
                    $description_31 = '';
                }

                $html[$lang] = '<div id="landBlock01" data-src="' . $request->img31 . '"  class="background-detail1 lazyImage" >
                                    <div class="content-detail1">
                                        <div class="background-content-detail1">
                                            <h2>' . $request->title_31[$lang] . '</h2>
                                            <div class="text-content-detail1">' . $request->description_31[$lang] . '</div>
                                        </div>
                                    </div>
                                </div>';
            } else {
                $html[$lang] = '';
            }
        }

        return $html;
    }

    public function blockLandingPage32($request) {
        $html = [];
        foreach (\Config::get('languages') as $lang => $language) {
            
            if(isset($request->blocks33) && !in_array(2, $request->blocks33)){
                $html[$lang] = '';
                continue;
            }
            
            $tmpTextData = '';

            //add content to tmp data
            if ($request->title_32[$lang] != '' || $request->description_32[$lang] != '') {

                if ($request->description_32[$lang] != '') {
                    $description_32 = nl2br($request->description_32[$lang]);
                } else {
                    $description_32 = '';
                }

                $tmpTextData = '<div class="background-section2-left">
                                <div class="title-section2-Responsive">
                                    <h2 class="h2Left">' . $request->title_32[$lang] . '</h2>
                                    <h2 class="h2Right"></h2>
                                </div>
                                <div class="content-section2-left">
                                    <div class="title-section2-left">
                                        <h2>' . $request->title_32[$lang] . '</h2>
                                    </div>
                                    <div class="content-tabs-section2">
                                        <div class="cnt-tabsSection2">
                                            <ul role="tablist" class="nav nav-tabs">
                                                <li class="nav-item active"></li>
                                            </ul>
                                        </div>
                                        <!-- Tab panes-->
                                        <div class="tab-content">
                                            <div id="tong_quan" role="tabpanel" class="tab-pane active">
                                                <div class="description active">
                                                    <div class="short" style="overflow: hidden;">
                                                        <p class="MsoNormal" style="text-align:justify;">
                                                            <span style="font-size:12pt;line-height:115%;font-family:\'Times New Roman\', serif;">' . $description_32 . '</span>
                                                        </p>
                                                    </div>
                                                    <div class="xemthem-section2">
                                                        <div class="long projectline style-scroll" style="overflow-y:scroll ; ">
                                                            <p class="MsoNormal" style="text-align:justify;">
                                                                <span style="font-size:12pt;line-height:115%;font-family:\'Times New Roman\', serif;">' . $description_32 . '</span></p>
                                                        </div>
                                                        <div class="more-link">
                                                            <a class="link" data-collapse="1" data-rg="Rút gọn" data-xt="Xem thêm" >Xem thêm</a><i aria-hidden="true" class="fa fa-angle-right"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="background-section2-right">
                                <div class="content-section2-right">
                                    <div class="title-section2-right">
                                        <h2>&nbsp;</h2>
                                    </div>
                                </div>
                            </div>';
            } else {
                $tmpTextData = '';
            }

            $imgDataTmp = '';
            foreach ($request->images_32 as $img) {
                if (!empty($img)) {
                    $imgDataTmp.='<div data-src="' . $img . '" class="item-slider-detail2 lazyImage"></div>';
                }
            }

            if (!empty($imgDataTmp)) {
                $imgData = '<div class="content-sliderSlick-detail2">
                                <div class="slider-detail2">
                                    ' . $imgDataTmp . '
                                </div>
                            </div>';
            }

            //add tmp data to html
            if (!empty($tmpTextData)) {
                $html[$lang] = '<div id="landBlock02" class="content-tongQuanDuAn" id="detailSurvey">
                                    <div data-src="/jinnV2/images/img/background-d2.jpg?v=0.9.0.4" class="background-tongQuanDuAn lazyImage">
                                    ' . $tmpTextData . $imgData . '
                                    </div>
                                </div>';
            } else {
                $html[$lang] = '';
            }
        }

        return $html;
    }

    public function blockLandingPage33($request) {
        $html = [];
        foreach (\Config::get('languages') as $lang => $language) {
            
            if(isset($request->blocks33) && !in_array(3, $request->blocks33)){
                $html[$lang] = '';
                continue;
            }
            
            if ($request->title_33[$lang] != '' || $request->description_33[$lang] != '' || $request->img33 != '') {

                if ($request->description_33[$lang] != '') {
                    $description_33 = nl2br($request->description_33[$lang]);
                } else {
                    $description_33 = '';
                }

                if ($request->img33 != '') {
                    $img33 = '<div class="content-matBangDuAn">
                                <div class="block-map">
                                    <img data-src="/photos/3/gt1-jpgb8l6f5iuumfg00at1uug.jpeg" data-loaded="0" usemap="#planetmap1" class="img-responsive map lazyImage" id="project-map"/>
                                </div>
                            </div>';
                } else {
                    $img33 = '';
                }

                $html[$lang] = '<div id="landBlock03" class="content-section3" id="groundProject">
                                    <div class="background-detail3">
                                        <div class="title-content-section3">
                                            <h2>' . $request->title_33[$lang] . '</h2>
                                        </div>
                                        ' . $img33 . '
                                        <div class="note-matBangDuAn">
                                            <div class="loaiBietThu">
                                                <div class="content-LSP">
                                                    <p>' . $description_33 . '</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>';
            } else {
                $html[$lang] = '';
            }
        }
//        print_r($html);die;
        return $html;
    }

    public function blockLandingPage34($request) {
        $html = [];

        foreach (\Config::get('languages') as $lang => $language) {
            
            if(isset($request->blocks33) && !in_array(4, $request->blocks33)){
                $html[$lang] = '';
                continue;
            }
            
            $tmp = '';
            for ($x = 0; $x < 3; $x++) {

                if ($request->title_34[$x][$lang] != '' || $request->description_img_34[$x][$lang] != '' || $request->img34[$x] != '') {

                    if ($request->description_img_34[$x][$lang] != '') {
                        $description_34 = nl2br($request->description_img_34[$x][$lang]);
                    } else {
                        $description_34 = '';
                    }

                    $tmp .= '<div onclick=\'loadPageBLockInMobile("' . $request->images_34[$x] . '")\' 
                                class="item-slickSliderPhanKhu" 
                                style="background-image: url(\'' . $request->images_34[$x] . '\')">
                               <div class="infor-itemPhanKhu">
                                   <h2 title="' . $request->title_img_34[$x][$lang] . '">' . $request->title_img_34[$x][$lang] . '</h2>
                                   <div class="excerpt-itemPhanKhu">
                                       ' . $description_34 . '
                                   </div>
                               </div>
                               <div class="link-itemPhanKhu">
                                   <div class="link-cnt-PK">
                                       <a href="' . $request->link_img_34[$x][$lang] . '">
                                           <img src="/frontend/images/icon/link-PK0995.png" alt="">
                                       </a>
                                   </div>
                               </div>
                           </div>';
                }
            }

            $html[$lang] = '<div id="landBlock04" class="content-phanKhu">
                                <div class="background-phanKhu">
                                    <div class="content-slickSliderPhanKhu">' . $tmp . '</div>
                                </div>
                            </div>';
        }

        return $html;
    }

    public function blockLandingPage35($request) {
        $html = [];
        foreach (\Config::get('languages') as $lang => $language) {
            
            if(isset($request->blocks33) && !in_array(5, $request->blocks33)){
                $html[$lang] = '';
                continue;
            }
            
            $tmp = '<div id="landBlock05" class="content-section4" id="reasonChooseProject">
                            <div class="background-detail4">';
            if ($request->big_title35[$lang] != '') {

                $tmp .= '<div class="title-content-section4">
                            <h2>' . $request->big_title35[$lang] . '</h2>
                        </div>';
            }

            $tmp .='<div class="content-lyDoLuaChon">';

            for ($x = 0; $x <= 20; $x++) {
                if ($request->img35[$x] != '' || $request->title35[$lang][$x] != '' || $request->description_35[$lang][$x] != '') {
                    $tmp .= '<div class="item-lyDoLuaChon">
                                    <div class="thumbnail-lyDoLuaChon">
                                        <div class="item-img4">
                                            <img data-src="' . $request->img35[$x] . '" style="max-height: 100px;max-width: 100px;" class="lazyImage">
                                        </div>
                                    </div>
                                    <div class="item-textLyDoLuaChon">
                                        <div class="item-infor4">
                                            <h3>' . $request->title35[$lang][$x] . '</h3>
                                            <p>' . $request->description_35[$lang][$x] . '</p>
                                        </div>
                                    </div>
                                </div>';
                }
            }

            $tmp .= '</div></div></div>';
            $html[$lang] = $tmp;
        }

        return $html;
    }

    public function blockLandingPage36($request) {
        $html = [];
        foreach (\Config::get('languages') as $lang => $language) {
            
            if(isset($request->blocks33) && !in_array(6, $request->blocks33)){
                $html[$lang] = '';
                continue;
            }
            
            $tmp = '<div id="landBlock06" class="content-section5">
                        <div class="background-detail5">';
            if ($request->big_title36[$lang] != '') {

                $tmp .= '<div class="title-content-section5">
                            <h2>' . $request->big_title36[$lang] . '</h2>
                        </div>';
            }

            $tmp .='<div class="content-detail5">';

            for ($x = 0; $x <= 5; $x++) {
                if ($request->img36[$x] != '' || $request->title36[$lang][$x] != '') {
                    $tmp .= '<div class="item-utility utility'.($x+1).'">
                                    <div class="cnt-item-utility">
                                        <div data-src="' . $request->img36[$x] . '" class="lazyImage background-images-item">
                                            <span>
                                                <p>' . $request->title36[$lang][$x] . '</p>
                                            </span>
                                        </div>
                                    </div>
                                </div>';
                }
            }

            $tmp .= '</div></div></div>';
            $html[$lang] = $tmp;
        }

        return $html;
    }
	
	public function blockLandingPage37($request) {
        $html = [];
        foreach (\Config::get('languages') as $lang => $language) {
            
            if(isset($request->blocks33) && !in_array(7, $request->blocks33)){
                $html[$lang] = '';
                continue;
            }
            
            $tmpTextData = '';

            //add content to tmp data
            if ($request->title_37[$lang] != '' || $request->description_37[$lang] != '') {

                if ($request->description_37[$lang] != '') {
                    $description_37 = nl2br($request->description_37[$lang]);
                } else {
                    $description_37 = '';
                }

                $tmpTextData = '<div class="background-section2-left">
                                <div class="title-section2-Responsive">
                                    <h2 class="h2Left">' . $request->title_37[$lang] . '</h2>
                                    <h2 class="h2Right"></h2>
                                </div>
                                <div class="content-section2-left">
                                    <div class="title-section2-left">
                                        <h2>' . $request->title_37[$lang] . '</h2>
                                    </div>
                                    <div class="content-tabs-section2">
                                        <div class="cnt-tabsSection2">
                                            <ul role="tablist" class="nav nav-tabs">
                                                <li class="nav-item active"></li>
                                            </ul>
                                        </div>
                                        <!-- Tab panes-->
                                        <div class="tab-content">
                                            <div id="tong_quan" role="tabpanel" class="tab-pane active">
                                                <div class="description active">
                                                    <div class="short" style="overflow: hidden;">
                                                        <p class="MsoNormal" style="text-align:justify;">
                                                            <span style="font-size:12pt;line-height:115%;font-family:\'Times New Roman\', serif;">' . $description_37 . '</span>
                                                        </p>
                                                    </div>
                                                    <div class="xemthem-section2">
                                                        <div class="long projectline style-scroll" style="overflow-y:scroll ; ">
                                                            <p class="MsoNormal" style="text-align:justify;">
                                                                <span style="font-size:12pt;line-height:115%;font-family:\'Times New Roman\', serif;">' . $description_37 . '</span></p>
                                                        </div>
                                                        <div class="more-link">
                                                            <a class="link" data-collapse="1" data-rg="Rút gọn" data-xt="Xem thêm" >Xem thêm</a><i aria-hidden="true" class="fa fa-angle-right"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="background-section2-right">
                                <div class="content-section2-right">
                                    <div class="title-section2-right">
                                        <h2>&nbsp;</h2>
                                    </div>
                                </div>
                            </div>
							<div class="content-sliderSlick-detail2">
								' . $request->video_37[$lang] . '
							</div>';
            } else {
                $tmpTextData = '';
            }
            

            //add tmp data to html
            if (!empty($tmpTextData)) {
                $html[$lang] = '<div id="landBlock02" class="content-tongQuanDuAn" id="detailSurvey">
                                    <div data-src="/jinnV2/images/img/background-d2.jpg?v=0.9.0.4" class="background-tongQuanDuAn lazyImage">
                                    ' . $tmpTextData . '
                                    </div>
                                </div>';
            } else {
                $html[$lang] = '';
            }
        }

        return $html;
    }
	
}
