<?php

namespace App\Services\Entity;

use App\Model\Block;
use App\Model\BlockData;
use App\Model\BlockConfig;

class ClassBlock {

    public function getBlockById($id) {
        $data = Block::select([
                    \TblName::BLOCK . ".id as tid",
                    \TblName::BLOCK . ".*",
                    \TblName::BLOCK_DATA . ".id as dataId",
                    \TblName::BLOCK_DATA . ".*"
                ])
                ->where(\TblName::BLOCK . ".id", $id)
                ->where(\TblName::BLOCK_DATA . ".language", \App::getLocale())
                ->leftJoin(\TblName::BLOCK_DATA, \TblName::BLOCK_DATA . ".block_id", \TblName::BLOCK . ".id")
                ->first();
        return $data;
    }

    public function saveBlock($id, $request) {
        if ($id > 0) {
            $block = Block::find($id);
        } else {
            $block = new Block;
        }

        if (!empty($request->link))
            $block->link = $request->link;
        if (!empty($request->image))
            $block->image = $request->image;
        if (!empty($request->type))
            $block->type = $request->type;
        if (!empty($request->price01))
            $block->price01 = $request->price01;
        if (!empty($request->price02))
            $block->price02 = $request->price02;
        $block->save();
        return $block;
    }

    public function saveBlockData($id, $request) {
        try {
            \DB::beginTransaction();
            foreach (\Config::get('languages') as $lang => $language) {
                if (BlockData::where('block_id', $id)->where('language', $lang)->count() > 0)
                    $blockData = BlockData::where('block_id', $id)->where('language', $lang)->first();
                else
                    $blockData = new BlockData;

                if (!empty($request->name)) {
                    $blockData->name = $request->name["{$lang}"];
                    $blockData->sluggable = app('ClassCommon')->formatText($request->name["{$lang}"]);
                }

                if (!empty($request->description))
                    $blockData->description = $request->description["{$lang}"];

                $blockData->block_id = $id;
                $blockData->language = $lang;
                $blockData->save();
            }
            \DB::commit();
            return \ReturnCode::RETURN_SUCCESS;
        } catch (\Exception $exc) {
            \DB::rollback();
            return \ReturnCode::RETURN_ERROR;
        }
        return $blockData;
    }

    public function getBlockByType($type) {
        $data = Block::select([
                    \TblName::BLOCK . ".id as tid",
                    \TblName::BLOCK . ".*",
                    \TblName::BLOCK_DATA . ".id as dataId",
                    \TblName::BLOCK_DATA . ".*"
                ])
                ->where(\TblName::BLOCK . ".type", $type)
                ->where(\TblName::BLOCK_DATA . ".language", \App::getLocale())
                ->leftJoin(\TblName::BLOCK_DATA, \TblName::BLOCK_DATA . ".block_id", \TblName::BLOCK . ".id")
                ->orderBy('sort_order', 'asc')
                ->get();
        return $data;
    }

    public function htmlSupport() {
        $html = '';
        $blocks = self::getListSupport();
        if (!empty($blocks)) {
            foreach ($blocks as $idx => $block) {
                if ($idx <= 6)
                    $class = 'sp-item ';
                else
                    $class = 'sp-item sp-item-hidden _hidden';

                $html .= '<div class="' . $class . '">
                            <div class="sp-name">
                                <span class="sales">' . $block->name . '</span><br />
                                <a href="tel:' . $block->description . '"><span class="hotline">' . $block->description . '</span></a><br />
                            </div>
                            <a href="tel:' . $block->description . '">
                                <i class="ion-social-whatsapp fa icon-support-online"></i>
                            </a>
                            <div class="line-bottom"></div>
                        </div>';
            }
        }
        return $html;
    }

    public function getPhoneHotlineBySession() {
        $config = app('ClassConfig')->getConfig();
        $phone = $config->phone;
        $blocks = self::getListSupport();
        if ($blocks) {
            $phone = $blocks[0]->description;
        }
        return $phone;
    }

    private function getListSupport() {
        $config = app('ClassConfig')->getConfig();
        if (!session()->has('support')) {
            $blocks = self::getRandomBlockByType('support');
            session()->put('support', ['time' => time(), 'block' => $blocks]);
        } else {
            $spSession = session()->get('support');
            if (($spSession['time'] + intval($config->code01)) > time()) {
                $blocks = $spSession['block'];
            } else {
                $blocks = self::getRandomBlockByType('support');
                $spSession = session()->put('support', ['time' => time(), 'block' => $blocks]);
            }
        }
        return $blocks;
    }    

    public function htmlPrice() {
        $html = '';

        $blocks = self::getBlockByType('price');
        if (!empty($blocks)) {
            foreach ($blocks as $block) {
                $html .= '<a class="list-group-item lgit" href="' . route('listSimByPrice', [$block->sluggable]) . '">' . $block->name . '</a>';
            }
        }
        return $html;
    }

    public function htmlPhanLoaiSim() {
        $html = '';

        $blocks = self::getBlockByType('typeSim');
        if (!empty($blocks)) {
            foreach ($blocks as $block) {
                $html .= '<a class="list-group-item lgit" href="' . route('listSimByType', [$block->sluggable, $block->block_id]) . '">' . $block->name . '</a>';
            }
        }
        return $html;
    }

    public function getBlockByPrice($sluggable) {
        $data = Block::select([
                    \TblName::BLOCK . ".id as id",
                    \TblName::BLOCK . ".*",
                    \TblName::BLOCK_DATA . ".id as dataId",
                    \TblName::BLOCK_DATA . ".*"
                ])
                ->where(\TblName::BLOCK_DATA . ".sluggable", $sluggable)
                ->where(\TblName::BLOCK_DATA . ".language", \App::getLocale())
                ->leftJoin(\TblName::BLOCK_DATA, \TblName::BLOCK_DATA . ".block_id", \TblName::BLOCK . ".id")
                ->orderBy('sort_order', 'asc')
                ->first();
        return $data;
    }

    public function getRandomBlockByType($type) {
        $data = Block::select([
                    \TblName::BLOCK . ".id as id",
                    \TblName::BLOCK . ".*",
                    \TblName::BLOCK_DATA . ".id as dataId",
                    \TblName::BLOCK_DATA . ".*"
                ])
                ->where(\TblName::BLOCK . ".type", $type)
                ->where(\TblName::BLOCK_DATA . ".language", \App::getLocale())
                ->leftJoin(\TblName::BLOCK_DATA, \TblName::BLOCK_DATA . ".block_id", \TblName::BLOCK . ".id")
                ->inRandomOrder()
                ->get();
        return $data;
    }

    public function getBlockConfigByType($type) {
        $block = BlockConfig::where("type", $type)->first();
        return $block;
    }

    public function updateBlockConfig($request) {
        $block = BlockConfig::find($request->block_config_id);
        $block->name = $request->name;
        $block->save();
        return $block;
    }

    public function htmlFCate() {
        $html = '';

        $category = Block::select([
                    \TblName::BLOCK . ".id as bid",
                    \TblName::BLOCK . ".*",
                    \TblName::BLOCK_DATA . ".id as dataId",
                    \TblName::BLOCK_DATA . ".*"
                ])
                ->where(\TblName::BLOCK . ".parent_id", 0)
                ->where(\TblName::BLOCK . ".type", 'fcate')
                ->where(\TblName::BLOCK_DATA . ".language", \App::getLocale())
                ->leftJoin(\TblName::BLOCK_DATA, \TblName::BLOCK_DATA . ".block_id", \TblName::BLOCK . ".id")
                ->orderBy('sort_order', 'asc')
                ->get();

        foreach ($category as $fcate) {
            $html .= '<div class="f-item">';
            $html .= '<h3 class="h3_01">' . $fcate->name . '</h3>';
            $html .= '<ul>';
            $block = Block::select([
                        \TblName::BLOCK . ".id as bid",
                        \TblName::BLOCK . ".*",
                        \TblName::BLOCK_DATA . ".id as dataId",
                        \TblName::BLOCK_DATA . ".*"
                    ])
                    ->where(\TblName::BLOCK . ".parent_id", $fcate->bid)
                    ->where(\TblName::BLOCK . ".type", 'fcate')
                    ->where(\TblName::BLOCK_DATA . ".language", \App::getLocale())
                    ->leftJoin(\TblName::BLOCK_DATA, \TblName::BLOCK_DATA . ".block_id", \TblName::BLOCK . ".id")
                    ->orderBy('sort_order', 'asc')
                    ->get();
            foreach ($block as $b) {
                $html .= '<li><a href="' . $b->link . '">' . $b->name . '</a></li>';
            }

            $html .= '</ul>';
            $html .= '</div>';
        }

        return $html;
    }
    
    public function htmlMenuFooter() {
        $html = '';
        $category = Block::select([
                    \TblName::BLOCK . ".id as bid",
                    \TblName::BLOCK . ".*",
                    \TblName::BLOCK_DATA . ".id as dataId",
                    \TblName::BLOCK_DATA . ".*"
                ])
                ->where(\TblName::BLOCK . ".parent_id", 0)
                ->where(\TblName::BLOCK . ".type", 'fcate02')
                ->where(\TblName::BLOCK_DATA . ".language", \App::getLocale())
                ->leftJoin(\TblName::BLOCK_DATA, \TblName::BLOCK_DATA . ".block_id", \TblName::BLOCK . ".id")
                ->orderBy('sort_order', 'asc')
                ->get();
        foreach ($category as $cate) {
            $html .= '<a href="' . $cate->link . '">' . $cate->name . '</a>';
        }
        return $html;
    }

    public function htmlFDes($type) {
        $html = '';

        $block = Block::select([
                    \TblName::BLOCK . ".id as id",
                    \TblName::BLOCK . ".*",
                    \TblName::BLOCK_DATA . ".id as dataId",
                    \TblName::BLOCK_DATA . ".*"
                ])
                ->where(\TblName::BLOCK . ".type", $type)
                ->where(\TblName::BLOCK_DATA . ".language", \App::getLocale())
                ->leftJoin(\TblName::BLOCK_DATA, \TblName::BLOCK_DATA . ".block_id", \TblName::BLOCK . ".id")
                ->orderBy('sort_order', 'asc')
                ->first();
        if (!empty($block))
            $html .= '<h3>' . $block->name . '</h3>' . $block->description;


        return $html;
    }

}
