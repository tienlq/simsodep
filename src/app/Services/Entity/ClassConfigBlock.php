<?php

namespace App\Services\Entity;

use App\Model\ConfigBlock;

class ClassConfigBlock {

    public function saveBlock($id, $request) {
        if ($id > 0) {
            $block = ConfigBlock::find($id);
        } else {
            $block = new ConfigBlock;
        }

        if (!empty($request->name))
            $block->name = $request->name;
        if (!empty($request->icon))
            $block->icon = $request->icon;
        
        $block->save();
        return $block;
    }

    public function getById($id) {
        $data = ConfigBlock::find($id);
        return $data;
    }

    public function getAll() {
        $data = ConfigBlock::all();
        return $data;
    }

    public function getByType($type) {
        $data = ConfigBlock::where("type", $type)
                ->orderBy('sort_order', 'asc')
                ->first();
        return $data;
    }

    public function htmlTitleBlock($id) {
        $html = '';
        $configBlock = self::getById($id);
        if (!empty($configBlock)) {
            $link = 'javascript:void(0);';
            if (!empty($configBlock->link)) {
                $link = $configBlock->link;
            }
            $html .= '<a href="' . $link . '" class="list-group-item active">
                            ' . $configBlock->icon . '
                            &nbsp;
                            ' . $configBlock->name . '
                        </a>';
        }
        return $html;
    }

}
