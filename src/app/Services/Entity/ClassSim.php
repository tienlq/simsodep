<?php

namespace App\Services\Entity;

use App\Model\Sim;

class ClassSim {

    public function getById($id) {
        return Sim::find($id);
        ;
    }

    public function getListSim($request) {
        $sims = Sim::select([
                    \TblName::SIM . '.id as simId',
                    \TblName::SIM . '.*',
                    \TblName::SUPPLIER . '.name as sName',
        ]);
        $sims = $sims->leftJoin(\TblName::SUPPLIER, \TblName::SIM . '.supplier_id', \TblName::SUPPLIER . '.id');
        $sims = $sims->paginate(30);
        return $sims;
    }

    public function updateSimById($id, $request) {
        if (!empty($id)) {

            if (substr($request->phone, 0, 1) != '0')
                $phone = '0' . $request->phone;
            else
                $phone = $request->phone;

            $sim = Sim::find($id);
            $sim->phone = $phone;
            $sim->price = $request->price;
            $sim->user_id = $request->user_id;
            //supplier_id
            $sim->save();
            return $sim;
        }
    }

    public function insertRows($datas) {
        $sims = [];
        try {
            \DB::beginTransaction();
            $sim = Sim::insert($datas);
            \DB::commit();
            return $sim; //true;
        } catch (\Exception $exc) {
            \DB::rollback();
            //die($exc->getMessage());
            return false;
        }
    }

    public function getListSimDoanhNhan() {
        $sims = Sim::select([
                    \TblName::SIM . '.id as simId',
                    \TblName::SIM . '.*',
                    \TblName::SUPPLIER . '.name as sName',
                    \TblName::SUPPLIER . '.elementClass as elementClass',
                    \TblName::SUPPLIER . '.logo as logo',
        ]);
        $sims = $sims->leftJoin(\TblName::SUPPLIER, \TblName::SIM . '.supplier_id', \TblName::SUPPLIER . '.id');
        $sims = $sims->get(12);
        return $sims;
    }

    public function getSimByPhone($sluggable) {
        $sim = Sim::select([
                    \TblName::SIM . '.id as simId',
                    \TblName::SIM . '.*',
                    \TblName::SUPPLIER . '.name as sName',
                    \TblName::SUPPLIER . '.elementClass as elementClass',
                    \TblName::SUPPLIER . '.logo as logo',
                ])
                ->where(\TblName::SIM . '.sluggable', $sluggable)
                ->leftJoin(\TblName::SUPPLIER, \TblName::SIM . '.supplier_id', \TblName::SUPPLIER . '.id')
                ->first();
        return $sim;
    }

    public function getSimByPrice($p1, $p2, $request) {
        $sims = Sim::select([
                    \TblName::SIM . '.id as simId',
                    \TblName::SIM . '.*',
                    \TblName::SUPPLIER . '.name as sName',
                    \TblName::SUPPLIER . '.elementClass as elementClass',
                    \TblName::SUPPLIER . '.logo as logo',
                ]);
        if(!empty($request->dauso)) {
            $block = app('ClassBlock')->getBlockById($request->dauso);
            $sims = self::getSimByCondition($block, $sims);
        }
        if(!empty($request->order)) {
            $block = app('ClassBlock')->getBlockById($request->order);
            $sims = self::getSimByCondition($block, $sims);
        }
        if(!empty($request->supplier)) {
            $sims = $sims->where(\TblName::SIM . ".supplier_id", $request->supplier);
        }
        $sims = $sims->whereBetween(\TblName::SIM . ".price", [$p1, $p2])
                ->leftJoin(\TblName::SUPPLIER, \TblName::SIM . '.supplier_id', \TblName::SUPPLIER . '.id')
                ->paginate(24);
        return $sims;
    }

    public function getSimBySupplier($supplierId, $request) {
        $sims = Sim::select([
                    \TblName::SIM . '.id as simId',
                    \TblName::SIM . '.*',
                    \TblName::SUPPLIER . '.name as sName',
                    \TblName::SUPPLIER . '.elementClass as elementClass',
                    \TblName::SUPPLIER . '.logo as logo', 
                ]);
                if(!empty($request->dauso)) {
                    $block = app('ClassBlock')->getBlockById($request->dauso);
                    $sims = self::getSimByCondition($block, $sims);
                }
                if(!empty($request->order)) {
                    $block = app('ClassBlock')->getBlockById($request->order);
                    $sims = self::getSimByCondition($block, $sims);
                }
                if(!empty($request->price)) {
                    $block = app('ClassBlock')->getBlockById($request->price);
                    $sims = self::getSimByCondition($block, $sims);
                }
            $sims = $sims->where(\TblName::SIM . ".supplier_id", $supplierId)
                ->leftJoin(\TblName::SUPPLIER, \TblName::SIM . '.supplier_id', \TblName::SUPPLIER . '.id')
                ->paginate(24);
        return $sims;
    }

    public function searchSimByKeyword($keyword, $request) {
        
        $sims = Sim::select([
                    \TblName::SIM . '.id as simId',
                    \TblName::SIM . '.sluggable as ssluggable',
                    \TblName::SIM . '.*',
                    \TblName::SUPPLIER . '.name as sName',
                    \TblName::SUPPLIER . '.elementClass as elementClass',
                    \TblName::SUPPLIER . '.logo as logo',
            
                ])
                ->leftJoin(\TblName::SUPPLIER, \TblName::SIM . '.supplier_id', \TblName::SUPPLIER . '.id');

        $keys = explode("*", $keyword);
        $countKey = count($keys);
        $where = '';
        if ($countKey == 1) {
            $lenngth = strlen($keyword);
            if ($lenngth == 10) {
                $sims = $sims->where(\TblName::SIM . '.sluggable', 'like', "$keyword");
//                $sql = \DB::select('select sim.id sid, sim.sluggable ssluggable, sim.*, supplier.* from sim LEFT JOIN supplier  ON supplier.id = sim.supplier_id WHERE sim.sluggable LIKE \'' . $keyword . '\'');
            } elseif ($lenngth < 10) {
                $sims = $sims->where(\TblName::SIM . '.sluggable', 'like', "%$keyword%");
//                $sims = \DB::select('select sim.id sid, sim.sluggable ssluggable, sim.*, supplier.* from sim LEFT JOIN supplier  ON supplier.id = sim.supplier_id WHERE sim.sluggable LIKE \'%' . $keyword . '%\'');
            } elseif ($lenngth > 10) {
                $sims = $sims->where(\TblName::SIM . '.sluggable', 'like', "%" . str_replace('*', '', substr($keyword, -6)));
//                $sims = \DB::select('select sim.id sid, sim.sluggable ssluggable, sim.*, supplier.* from sim LEFT JOIN supplier  ON supplier.id = sim.supplier_id WHERE sim.sluggable LIKE \'%' . substr($keyword, -6) . '\'');
            }
        } elseif ($countKey == 2 && $keys[0] == '' && $keys[1] != '') {
            $sims = $sims->where(\TblName::SIM . '.sluggable', 'like', "%" . $keys[1]);
//            $sims = \DB::select('select sim.id sid, sim.sluggable ssluggable, sim.*, supplier.* from sim LEFT JOIN supplier  ON supplier.id = sim.supplier_id WHERE sim.sluggable LIKE \'%' . $keys[1] . '\'');
//            $sims = \DB::select('select sim.id sid, sim.sluggable ssluggable, sim.*, supplier.* from sim LEFT JOIN supplier  ON supplier.id = sim.supplier_id WHERE RIGHT(sim.sluggable, ' . strlen($keys[1]) . ') = :key1', ['key1' => $keys[1]]);
        } elseif ($countKey == 2 && $keys[0] != '' && $keys[1] == '') {
            $sims = $sims->where(\TblName::SIM . '.sluggable', 'like', $keys[0] . "%");
//            $sims = \DB::select('select sim.id sid, sim.sluggable ssluggable, sim.*, supplier.* from sim LEFT JOIN supplier  ON supplier.id = sim.supplier_id WHERE sim.sluggable LIKE \'' . $keys[0]  . '%\'');
        } else {
            $start = 1;
            $firstValue = 0;
            for ($i = 0; $i < $countKey; $i++) {
                if (!empty($keys[$i])) {
                    if ($start == 1 && $firstValue == 0) {
                        $lenngth = strlen($keys[$i]);
                        $sims = $sims->where(\TblName::SIM . '.sluggable', 'like', $keys[$i] . "%");
//                        $where .= ' AND LEFT(sim.sluggable, ' . $lenngth . ') = ' . $keys[$i];
                        $start = $start + $lenngth;
                    } else {
                        $lenngth = strlen($keys[$i]);
                        $next = $start + $lenngth;
                        if ($next <= 10) {
                            $sims = $sims->where(\TblName::SIM . '.sluggable', 'LIKE', "%" . $keys[$i] . "%");
//                            $where .= ' AND MID(sim.sluggable,' . $start . ', ' . 10 . ') LIKE ' . '\'%' . $keys[$i] . '%\'';
                            $start = $start + $lenngth;
                        } else {
                            continue;
                        }
                    }
                }
                $firstValue++;
            }
//            $sims = \DB::select('select sim.id sid, sim.sluggable ssluggable, sim.*, supplier.* from sim LEFT JOIN supplier  ON supplier.id = sim.supplier_id WHERE 1 ' . $where);
//            return $sims;
        }
//        return \DB::select('select sim.id sid, sim.sluggable ssluggable, sim.*, supplier.* from sim LEFT JOIN supplier  ON supplier.id = sim.supplier_id WHERE 1 ')->paginate(30);
        if(!empty($request->dauso)) {
            $block = app('ClassBlock')->getBlockById($request->dauso);
            $sims = self::getSimByCondition($block, $sims);
        }
        if(!empty($request->order)) {
            $block = app('ClassBlock')->getBlockById($request->order);
            $sims = self::getSimByCondition($block, $sims);
        }
        if(!empty($request->price)) {
            $block = app('ClassBlock')->getBlockById($request->price);
            $sims = self::getSimByCondition($block, $sims);
        }
        if(!empty($request->supplier)) {
            $sims = $sims->where(\TblName::SIM . ".supplier_id", $request->supplier);
        }
        $sims = $sims->paginate(30); 
        return $sims;
    }

    // 1 => 'Lọc theo điều kiện',
    // 2 => 'Lọc theo khoảng giá',
    // 3 => 'Chèn liên kết khác',
    // 4 => 'Giá tăng dần',
    // 5 => 'Giá giảm dần'
    function getSimByCondition($block, $sims) {
        switch ($block->image) {
                case '1':
                    $keywords = preg_split('/\s+/', trim($block->description));
                    $sims = $sims->where(function ($query) use ($keywords) {
                        foreach($keywords as $keyword) {
                            $query->orWhere(function ($query) use ($keyword) {
                                $query = self::addConditionSearch($query, $keyword);
                            });
                        }
                    });
                    break;
                case '2':
                    $sims = $sims->whereBetween(\TblName::SIM . ".price", [$block->price01, $block->price02]);
                    break;
                case '3':
                    # none
                    break;
                case '4':
                    $sims = $sims->orderBy('price', 'asc');
                    break;
                case '5':
                    $sims = $sims->orderBy('price', 'desc');
                    break;
                
                default:
                    # none
                    break;
            }
        return $sims;
    }

    public function searchSimByType($block, $request) {
        $sims = Sim::select([
                    \TblName::SIM . '.id as simId',
                    \TblName::SIM . '.sluggable as ssluggable',
                    \TblName::SIM . '.*',
                    \TblName::SUPPLIER . '.name as sName',
                    \TblName::SUPPLIER . '.elementClass as elementClass',
                    \TblName::SUPPLIER . '.logo as logo',
            
                ])
                ->leftJoin(\TblName::SUPPLIER, \TblName::SIM . '.supplier_id', \TblName::SUPPLIER . '.id');
        //search theo dk config trong admin
        $sims = self::getSimByCondition($block, $sims);
        // loc theo dk search
        if(!empty($request->dauso)) {
            $block = app('ClassBlock')->getBlockById($request->dauso);
            $sims = self::getSimByCondition($block, $sims);
        }
        if(!empty($request->order)) {
            $block = app('ClassBlock')->getBlockById($request->order);
            $sims = self::getSimByCondition($block, $sims);
        }
        if(!empty($request->price)) {
            $block = app('ClassBlock')->getBlockById($request->price);
            $sims = self::getSimByCondition($block, $sims);
        }
        if(!empty($request->supplier)) {
            $sims = $sims->where(\TblName::SIM . ".supplier_id", $request->supplier);
        }
        $sims = $sims->paginate(30); 
        return $sims;
    }

    public function addConditionSearch($sims, $keyword) {
        $keys = explode("*", $keyword);
        $countKey = count($keys);
        if ($countKey == 1) {
            $lenngth = strlen($keyword);
            if ($lenngth == 10) {
                $sims = $sims->where(\TblName::SIM . '.sluggable', 'like', "$keyword");
            } elseif ($lenngth < 10) {
                $sims = $sims->where(\TblName::SIM . '.sluggable', 'like', "%$keyword%");
            } elseif ($lenngth > 10) {
                $sims = $sims->where(\TblName::SIM . '.sluggable', 'like', "%" . str_replace('*', '', substr($keyword, -6)));
            }
        } elseif ($countKey == 2 && $keys[0] == '' && $keys[1] != '') {
            $sims = $sims->where(\TblName::SIM . '.sluggable', 'like', "%" . $keys[1]);
        } elseif ($countKey == 2 && $keys[0] != '' && $keys[1] == '') {
            $sims = $sims->where(\TblName::SIM . '.sluggable', 'like', $keys[0] . "%");
        } else {
            $start = 1;
            $firstValue = 0;
            for ($i = 0; $i < $countKey; $i++) {
                if (!empty($keys[$i])) {
                    if ($start == 1 && $firstValue == 0) {
                        $lenngth = strlen($keys[$i]);
                        $sims = $sims->where(\TblName::SIM . '.sluggable', 'like', $keys[$i] . "%");
                        $start = $start + $lenngth;
                    } else {
                        $lenngth = strlen($keys[$i]);
                        $next = $start + $lenngth;
                        if ($next <= 10) {
                            $sims = $sims->where(\TblName::SIM . '.sluggable', 'LIKE', "%" . $keys[$i] . "%");
                            $start = $start + $lenngth;
                        } else {
                            continue;
                        }
                    }
                }
                $firstValue++;
            }
        }

        return $sims;
    }

    private function addWhereSql($idx, $where) {
        $result = ' ';
        if ($idx == 1) {
            $result .=$where;
        } else {
            $result .= ' OR ' . $where;
        }
        return $result;
    }

    // public function searchSimByType($keywords) {
    //     $where = '';
    //     $idx = 0;
    //     foreach ($keywords as $keyword) {
    //         $idx++;
    //         $keys = explode("*", $keyword);
    //         $countKey = count($keys);
    //         if ($countKey == 1) {
    //             $lenngth = strlen($keyword);
    //             if ($lenngth == 10) {
    //                 $where .= self::addWhereSql($idx, ' sim.sluggable = \'' . $keyword . '\'');
    //             } elseif ($lenngth < 10) {
    //                 $where .= self::addWhereSql($idx, ' sim.sluggable LIKE \'%' . $keyword . '%\'');
    //             } elseif ($lenngth > 10) {
    //                 $where .= self::addWhereSql($idx, ' RIGHT(sim.sluggable, 6) = ' . substr($keyword, -6));
    //             }
    //         } elseif ($countKey == 2 && $keys[0] == '' && $keys[1] != '') {
    //             if ($idx == 1) {
    //                 $where .= self::addWhereSql($idx, ' RIGHT(sim.sluggable, ' . strlen($keys[1]) . ') = ' . $keys[1]);
    //             } else {
    //                 $where .= self::addWhereSql($idx, ' RIGHT(sim.sluggable, ' . strlen($keys[1]) . ') = ' . $keys[1]);
    //             }
    //         } elseif ($countKey == 2 && $keys[0] != '' && $keys[1] == '') {
    //             if ($idx == 1) {
    //                 $where .= self::addWhereSql($idx, ' LEFT(sim.sluggable, ' . strlen($keys[0]) . ') = ' . $keys[0]);
    //             } else {
    //                 $where .= self::addWhereSql($idx, ' LEFT(sim.sluggable, ' . strlen($keys[0]) . ') = ' . $keys[0]);
    //             }
    //         } else {
    //             $start = 1;
    //             for ($i = 0; $i < $countKey; $i++) {
    //                 if (!empty($keys[$i])) {
    //                     if ($start == 1) {
    //                         $lenngth = strlen($keys[$i]);
    //                         $where .= self::addWhereSql($idx, ' LEFT(sim.sluggable, ' . $lenngth . ') = ' . $keys[$i]);
    //                         $start = $start + $lenngth;
    //                     } else {
    //                         $lenngth = strlen($keys[$i]);
    //                         $next = $start + $lenngth;
    //                         if ($next <= 10) {
    //                             $where .= self::addWhereSql($idx, ' MID(sim.sluggable,' . $start . ', ' . 10 . ') LIKE ' . '\'%' . $keys[$i] . '%\'');
    //                             $start = $start + $lenngth;
    //                         } else {
    //                             continue;
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //     $sql = 'SELECT sim.id sid, sim.sluggable ssluggable, sim.*, supplier.name sName , supplier.* from sim LEFT JOIN supplier  ON supplier.id = sim.supplier_id WHERE ' . $where;
    //     $sims = \DB::select($sql);

    //     return $sims;
    // }

}
