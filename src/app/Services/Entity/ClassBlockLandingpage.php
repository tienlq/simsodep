<?php

namespace App\Services\Entity;

use App\Model\BlockLandingpage;

class ClassBlockLandingpage {

    public function getById($id) {
        return BlockLandingpage::find($id);
    }

    public function getByNewsId($newsId) {
        return BlockLandingpage::where(['news_id' => $newsId])->orderBy('sort_order', 'asc')->get();
    }

    public function deleteById($id) {
        BlockLandingpage::find($id)->delete();
        return true;
    }

    public function save($request, $newsId, $landId, $id) {
        if ($id > 0) {
            $block = BlockLandingpage::find($id);
        } else {
            $block = new BlockLandingpage;
            $block->sort_order = 22;
        }
        $sluggable = '';
        if (isset($request->title['vi']) && is_string($request->title['vi'])) {
            $sluggable = app('ClassCommon')->formatText($request->title['vi']);
        }

        switch ($landId) {

            case 11:
                $content = self::getContentBlock11($request);
                $name = '[Block 01] ' . $request->title['vi'];
                break;
            case 12:
                $content = self::getContentBlock12($request);
                $name = '[Block 02] ' . $request->title['vi'][0];
                break;
            case 13:
                $content = self::getContentBlock13($request);
                $name = '[Block 03] ' . $request->title['vi'];
                break;
            case 14:
                $content = self::getContentBlock14($request);
                $name = '[Block 04] ' . $request->title['vi'];
                break;
            case 15:
                $content = self::getContentBlock15($request);
                $name = '[Block 05] ' . $request->title['vi'];
                break;
            case 16:
                $content = self::getContentBlock16($request);
                $name = '[Block 06] ' . $request->title['vi'];
                break;
            case 17:
                $content = self::getContentBlock17($request);
                $name = '[Block 07] ' . $request->title['vi'];
                break;
            case 18:
                $content = self::getContentBlock18($request);
                $name = '[Block 08] ' . $request->title['vi'];
                break;
            case 21:
                $content = self::getContentBlock21($request);
                $name = '[Block 01] ' . $request->title['vi'];
                break;
            case 22:
                $content = self::getContentBlock22($request);
                $name = '[Block 02] ' . $request->title['vi'][0];
                break;
            case 23:
                $content = self::getContentBlock23($request);
                $name = '[Block 02] ' . $request->title['vi'][0];
                break;

            default:
                $content = '';
                break;
        }

        foreach (\Config::get('languages') as $lang => $language) {
            if ($lang == 'vi') {
                $block->content_vi = $content['vi'];
            }

            if ($lang == 'en') {
                $block->content_en = $content['en'];
            }
        }

        $block->landingpage_id = $landId;
        $block->news_id = $newsId;
        $block->name = $name;
        $block->sluggable = $sluggable;
        $block->postData = json_encode($_POST);
        $block->save();
        return $block;
    }

    private function getContentBlock11($request) {
        $html = [];
        foreach (\Config::get('languages') as $lang => $language) {

            $html[$lang] = '<div class="static-header plain-version light clearfix">
                                <img id="bg_homesl" src="' . $request->img[$lang] . '" class="ls-bg menuGolddesktop" alt="Slide background" style="width: 100%;"/>
                                <img id="" src="' . $request->img[$lang] . '" class="ls-bg menuGoldmobile" alt="Slide background" style="width: 100%;"/>
                                <div class="Content" id="contentMN">
                                    <b class="title_slider">' . $request->title[$lang] . '</b>
                                    <div class="contentMobile">
                                        <div class="content_slider contentScrollAll ZBZ_DKM" >
                                            ' . $request->content[$lang] . '
                                        </div>
                                    </div>
                                </div>
                            </div>';
        }

        return $html;
    }

    private function getContentBlock12($request) {
        $html = [];
        foreach (\Config::get('languages') as $lang => $language) {
            $tmp = '';
            for ($i = 0; $i <= 2; $i++) {
                $des = '';
                if (!empty($request->description[$lang][$i])) {
                    $des = nl2br($request->description[$lang][$i]);
                }
                $tmp .= '<div class="BLOCKPRODUCT col-sm-4 col-xs-12 animated" data-animation="fadeInUp" data-delay="400">
                            <article class="center">
                                <div>
                                    <img src="' . $request->img[$lang][$i] . '"/>
                                    <div class="mask_black_gradient"></div>
                                    <div class="product_image_zoom" style="background-image: url(\'' . $request->img[$lang][$i] . '\')"></div>
                                </div>
                                <a  href="' . $request->link[$lang][$i] . '" >
                                    <div  class="h7name">
                                        <div  class="h7">
                                            <p>' . $request->title[$lang][$i] . '</p>
                                        </div>
                                    </div>
                                    <div class="watetmarkProduct ">
                                        <img src="/frontend/images/VHSL_hoavan.png" style="position: absolute;bottom: 0px;z-index: 0;left: 0px;">
                                        <div class="watetmarkProductContent">
                                            <span class="h7">
                                                <p>' . $request->title[$lang][$i] . '</p>
                                            </span>
                                            <div class="thinx">
                                                <div class="thinx_ct">
                                                    <p>' . $des . '</p>
                                                </div>
                                                <div class="viewdetail">Xem chi tiết</div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </article>
                        </div>';
            }

            $html[$lang] = '<section id="features-list" class="section dark slider">
                                <div class="sliderD">
                                    <div class="row ID">
                                        ' . $tmp . '
                                    </div>
                                </div>
                            </section>';
        }

        return $html;
    }

    private function getContentBlock13($request) {
        $html = [];

        foreach (\Config::get('languages') as $lang => $language) {

            $html[$lang] = '<section id="goldLocaltion" class="section dark slider">
                                <div class=" col-sm-4 col-xs-12 goldBox2 pull-right">
                                    <div class="center-block goldBlock animated" data-animation="fadeInUp">
                                        <h3 class="text-left slideInUp animated"  >
                                            ' . $request->title[$lang] . '
                                            <div class="subtitle">' . $request->description[$lang] . '</div>
                                        </h3>
                                        <div class="contentMobile">
                                            <div class="text-justify contentScroll goldContentBlock" style="max-height: 300px;">
                                            ' . $request->content[$lang] . '
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="thumb-box menuGolddesktop">
                                        <div class="owl-swipe owl-theme">
                                            <div class="block_owl"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8 col-xs-12 fadeInLeft animated goldBox1 pull-left">
                                    <img style="max-width: 100%;height: 100%;" src="' . $request->img[$lang] . '">
                                </div>
                                <div class="clearfix"></div>
                            </section>';
        }

        return $html;
    }

    private function getContentBlock14($request) {
        $html = [];

        foreach (\Config::get('languages') as $lang => $language) {
            $tmpItem = '';
            $idx = 1;
            for ($i = 0; $i <= 50; $i++) {
                if (!empty($request->item[$lang][$i])) {
                    $tmpItem .= '<li class="col-sm-6 col-xs-6"><a href="#1" data-slide="1" data-point="' . $idx . '">' . $request->item[$lang][$i] . '</a></li>';
                    $idx++;
                }
            }
            $html[$lang] = '<section id="greenarea" class="section dark slider">
                                <div class="col-sm-4 col-xs-12 block1area">
                                    <div class="center-block goldBlock animated" data-animation="fadeInUp">
                                        <h3 data-old="<p>Quần thể tiện &iacute;ch xanh</p>" class="text-left">
                                            <div class="subtitle">' . $request->sub_title[$lang] . '</div>
                                            ' . $request->title[$lang] . '
                                        </h3>
                                        <div class="text-justify contentScroll" id="contentGreenArea">
                                            <p>' . $request->description[$lang] . '</p>
                                        </div>
                                    </div>
                                    <div class="thumb-box menuGolddesktop">
                                        <div class="owl-swipe owl-theme">
                                            <div class="block_owl">
                                                <ul class="thumbs list-inline text-left animated" data-animation="fadeInUp" id="ListTienIch" >
                                                    ' . $tmpItem . '
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8 col-xs-12 block2area" style="padding-right: 0px;">
                                    <img src="' . $request->img[$lang] . '">
                                </div>
                                <div class="clearfix"></div>
                            </section>';
        }

        return $html;
    }

    private function getContentBlock15($request) {
        $html = [];

        foreach (\Config::get('languages') as $lang => $language) {
            $tmpImages = '';
            $idx = 1;
            for ($i = 0; $i <= 20; $i++) {
                if (!empty($request['img'][$lang][$i])) {
                    $tmpImages .= '<div class="slideItem">
                                    <a href="' . $request->img[$lang][$i] . '"> 
                                    <img class="lazy" data-original="' . $request->img[$lang][$i] . '" > </a>
                                </div>';
                }
            }
            $html[$lang] = '<section id="sliderImage" class="text-center">
                                <div class="" style="position: relative">
                                    <h3 class="text-center">' . $request['title'][$lang] . '</h3>
                                    <div class="moreImage"><a href="' . $request['link'][$lang] . '">' . $request['description'][$lang] . '</a></div>
                                    <div class="carousel">
                                        <div class="slides" style="">
                                            ' . $tmpImages . '
                                        </div>
                                    </div>
                                    <div class="bongslide___">
                                        <img src="/frontend/images/shadowlight.png" style="width: 500px;margin-top: -300px;">
                                    </div>
                                </div>
                            </section>';
        }

        return $html;
    }

    private function getContentBlock16($request) {
        $html = [];

        foreach (\Config::get('languages') as $lang => $language) {
            $tmp = '';
            for ($i = 0; $i <= 20; $i++) {
                if (!empty($request->youtube_id[$lang][$i])) {
                    if (!empty($request->img[$lang][0])) {
                        $avatar = $request->img[$lang][$i];
                    } else {
                        $avatar = '/uploads/_thumbs/images/videos/skylake_vinhomes_vn_s2_sp-khac.jpg';
                    }
                    $tmp .= '<li>
                                <div class="thumbVideosub ">
                                    <a href="https://www.youtube.com/watch?v=' . $request->youtube_id[$lang][$i] . '" class="popup-youtube">
                                        <img src="' . $avatar . '">
                                        <div class="icon360">
                                            <div class="subiconvideo">&nbsp;</div>
                                        </div>
                                    </a>
                                </div>
                            </li>';
                }
            }

            ///uploads/files/Homes-Sky-Lake_video.jpg

            $html[$lang] = '<section id="tour360">
                                <div class="entry-thumb">
                                    <div>
                                        <img src="' . $request->img_bg[$lang] . '" style="__width: 100%;" class="menuGolddesktop__ tour360IMG">
                                    </div>
                                    <span class="entry-icon icon-tour360">
                                        <div class="text-center">
                                            <h3>' . $request->title[$lang] . '</h3>
                                        </div>
                                        <a href="https://www.youtube.com/watch?v=' . $request->youtube_id[$lang][0] . '" class="popup-youtube">
                                            <div class="icon360">
                                                <div class="subiconvideo">&nbsp;</div>
                                            </div>
                                        </a>
                                    </span>
                                    <div class="thumbVideo container">
                                        <ul id="videoList">
                                            ' . $tmp . '
                                        </ul>
                                    </div>
                                </div>
                            </section>';
        }

        return $html;
    }

    private function getContentBlock17($request) {
        $html = [];
        $prefix = app('ClassCommon')->generateRandomString();
        foreach (\Config::get('languages') as $lang => $language) {
            $tmpTitle = '';
            $tmpContent = '';
            $idx = 1;
            for ($i = 0; $i <= 20; $i++) {
                if (!empty($request['title_tab_111'][$lang][$i])) {
                    $class = '';
                    if ($idx == 1) {
                        $class = 'active';
                        $idx++;
                    }
                    $idTab = $prefix . $i;
                    $tmpTitle .= '<li role="presentation" class="' . $class . '"><a href="#' . $idTab . '" aria-controls="home" role="tab" data-toggle="tab">' . $request['title_tab_111'][$lang][$i] . '</a></li>';
                    $tmpContent .= '<div role="tabpanel" class="tab-pane ' . $class . '" id="' . $idTab . '">' . $request['description_tab_111'][$lang][$i] . '</div>';
                }
            }

            $html[$lang] = '<section id="greenarea" class="section dark slider">
                                <div class="row">
                                    <h2 class="title-3 animated fadeInRight delay-500 go">' . $request->title[$lang] . '</h2>
                                    <ul class="nav nav-tabs" role="tablist">
                                        ' . $tmpTitle . '
                                    </ul>
                                    <div class="tab-content">
                                        ' . $tmpContent . '
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </section>';
        }

        return $html;
    }

    private function getContentBlock19($request) {
        $html = [];

        foreach (\Config::get('languages') as $lang => $language) {
            $tmp = '';
            for ($i = 0; $i < 7; $i++) {
                if (!empty($request->title_19[$lang][$i]) && !empty($request->description_19[$lang][$i])) {
                    $title = $request->title_19[$lang][$i];
                    $des = nl2br($request->description_19[$lang][$i]);
                    $tmp .= '<section class="av_toggle_section"  itemscope="itemscope">
							<div class="single_toggle" data-tags="{All} " >
								<p data-fake-id="#toggle-id-1" class="toggler activeTitle"  itemprop="headline" ><strong>' . $title . '</strong><span class="toggle_icon">        <span class="vert_icon"></span><span class="hor_icon"></span></span></p>
								<div id="toggle-id-1-container" class="toggle_wrap active_tc" >
									<div class="toggle_content invers-color"  itemprop="text" >
									' . $des . '
									</div>
								</div>
							</div>
						</section>';
                }
            }
            $html[$lang] = '<div id="form" class="avia-section main_color avia-section-small avia-no-border-styling avia-bg-style-scroll  avia-builder-el-101  el_after_av_section  el_before_av_section  container_wrap fullsize" style = "background-color: #ffffff; background-color: #ffffff; "  >
								<div class="container">
									<div class="template-page content  av-content-full alpha units">
										<div class="post-entry post-entry-type-page post-entry-6">
											<div class="entry-content-wrapper clearfix">
												<div style="padding-bottom:10px;color:#000000;font-size:30px;" class="av-special-heading av-special-heading-h3 custom-color-heading blockquote modern-quote modern-centered  avia-builder-el-102  el_before_av_hr  avia-builder-el-first   av-inherit-size">
													<h3 class="av-special-heading-tag"  itemprop="headline"  >Q <span class="special_amp">&amp;</span> A</h3>
													<div class="special-heading-border">
														<div class="special-heading-inner-border" style="border-color:#000000"></div>
													</div>
												</div>
												<div style=" margin-top:10px; margin-bottom:10px;"  class="hr hr-custom hr-center hr-icon-no  avia-builder-el-103  el_after_av_heading  el_before_av_one_half "><span class="hr-inner   inner-border-av-border-fat" style=" width:70px; border-color:#000000;" ><span class="hr-inner-style"></span></span></div>
												<div class="flex_column av_one_half  flex_column_div av-zero-column-padding first  avia-builder-el-104  el_after_av_hr  el_before_av_one_half  " style="border-radius:0px; ">
													<div class="togglecontainer  avia-builder-el-105  avia-builder-el-no-sibling ">
														' . $tmp . '
													</div>
												</div>
												<div class="flex_column av_one_half  flex_column_div av-zero-column-padding   avia-builder-el-106  el_after_av_one_half  avia-builder-el-last  " style="border-radius:0px; ">
													<div style="height:1px; margin-top:-10px"  class="hr hr-invisible  avia-builder-el-107  el_before_av_textblock  avia-builder-el-first "><span class="hr-inner " ><span class="hr-inner-style"></span></span></div>
													<section class="av_textblock_section"  itemscope="itemscope" itemtype="https://schema.org/CreativeWork" >
														<div class="avia_textblock "   itemprop="text" >
															<p style="text-align: justify">
																<!-- This site converts visitors into subscribers and customers with the OptinMonster WordPress plugin v2.1.8 - http://optinmonster.com/ -->
															<div id="om-cf7khs4whk-post" class="optin-monster-overlay" style="">
																<script type="text/javascript" src="../ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
																<style type="text/css" class="om-theme-sample-styles">.optin-monster-success-message {font-size: 21px;font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;color: #282828;font-weight: 300;text-align: center;margin: 0 auto;}.optin-monster-success-overlay .om-success-close {font-size: 32px !important;font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif !important;color: #282828 !important;font-weight: 300 !important;position: absolute !important;top: 0px !important;right: 10px !important;background: none !important;text-decoration: none !important;width: auto !important;height: auto !important;display: block !important;line-height: 32px !important;padding: 0 !important;}.om-helper-field {display: none !important;visibility: hidden !important;opacity: 0 !important;height: 0 !important;line-height: 0 !important;}html div#om-cf7khs4whk-post * {box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;}html div#om-cf7khs4whk-post {background:none;border:0;border-radius:0;-webkit-border-radius:0;-moz-border-radius:0;float:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;height:auto;letter-spacing:normal;outline:none;position:static;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;width:auto;visibility:visible;overflow:visible;margin:0;padding:0;line-height:1;box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-shadow:none;-moz-box-shadow:none;-ms-box-shadow:none;-o-box-shadow:none;box-shadow:none;-webkit-appearance:none;}html div#om-cf7khs4whk-post .om-clearfix {clear: both;}html div#om-cf7khs4whk-post .om-clearfix:after {clear: both;content: ".";display: block;height: 0;line-height: 0;overflow: auto;visibility: hidden;zoom: 1;}html div#om-cf7khs4whk-post #om-post-sample-optin {background: #fff;position: relative;padding: 20px;text-align: center;margin: 0 auto;max-width: 100%;width: 100%;}html div#om-cf7khs4whk-post #om-post-sample-optin-title {font-size: 18px;color: #222;width: 100%;margin-bottom: 15px;}html div#om-cf7khs4whk-post #om-post-sample-optin-tagline {font-size: 16px;line-height: 1.25;color: #484848;width: 100%;margin-bottom: 15px;}html div#om-cf7khs4whk-post input,html div#om-cf7khs4whk-post #om-post-sample-optin-name,html div#om-cf7khs4whk-post #om-post-sample-optin-email {background-color: #fff;width: 100%;border: 1px solid #ddd;font-size: 16px;line-height: 24px;padding: 4px 6px;overflow: hidden;outline: none;margin: 0 0 10px;vertical-align: middle;display: inline;color: #222;height: 34px;}html div#om-cf7khs4whk-post input[type=submit],html div#om-cf7khs4whk-post button,html div#om-cf7khs4whk-post #om-post-sample-optin-submit {background: #ff370f;border: 1px solid #ff370f;color: #fff;font-size: 16px;padding: 4px 6px;line-height: 24px;text-align: center;vertical-align: middle;cursor: pointer;display: inline;margin: 0;width: 100%;}html div#om-cf7khs4whk-post input[type=checkbox],html div#om-cf7khs4whk-post input[type=radio] {-webkit-appearance: checkbox;width: auto;outline: invert none medium;padding: 0;margin: 0;}</style>
																<style type="text/css" class="om-custom-styles">div#om-cf7khs4whk-post input[type=text], div#om-cf7khs4whk-post input[type=email] {margin-bottom: 10px !important;max-width: 100% !important;color: #222222 !important;background-color: #ffffff !important;}div#om-cf7khs4whk-post input[type=submit] {display: inline-block !important;float: left !important;width: 100% !important;max-width: 100% !important;background-color: #83b828 !important; border-color: #83b828 !important;color: #ffffff !important;}div#om-cf7khs4whk-post #om-post-sample-optin {background: transparent!important;border: 1px solid #024c55 !important;}div#om-cf7khs4whk-post #om-post-action-optin {background: transparent!important;border: 1px solid #024c55 !important;}div#om-cf7khs4whk-post .alternate_color tr:nth-child(odd){background-color: transparent !important;}div#om-cf7khs4whk-post td {border-width:0px !important;padding:5px !important;background-color: transparent !important;}div#om-cf7khs4whk-post table {background: transparent !important;}div#om-cf7khs4whk-post tr {background: transparent !important;}div#om-cf7khs4whk-post b {color: #ffffff;}html div#om-cf7khs4whk-post * {line-height: 30px;}</style>
																<div id="om-post-sample-optin" class="om-post-sample om-clearfix om-theme-sample om-custom-html-form" style="background-color:#ffffff">
																	<div id="om-post-sample-optin-wrap" class="om-clearfix">
																		<div id="om-post-sample-header" class="om-clearfix" data-om-action="selectable">
																			<div id="om-post-sample-optin-title" data-om-action="editable" data-om-field="title" style="color:#222222;font-family:Helvetica;font-size:18px;"><span style="font-size:20px;"><span style="font-weight:bold;"><span style="color:#5fbf00;"><span style="font-family:arial;">CẬP NHẬT BẢNG H&Agrave;NG MỚI NHẤT</span></span></span><br />
																					<span style="color:#000000;"><span style="font-family:arial;">C&ugrave;ng những ưu đ&atilde;i hấp dẫn từ CĐT dự &aacute;n Forest City</span></span></span>
																			</div>
																		</div>
																		<div id="om-post-sample-content" class="om-clearfix" data-om-action="selectable">
																			<div id="om-post-sample-optin-tagline" data-om-action="editable" data-om-field="tagline" style="color:#484848;font-family:Helvetica;font-size:16px;"><span style="color:#DAA520;"></span></div>
																		</div>
																		<div id="om-post-sample-footer" class="om-clearfix om-has-email" data-om-action="selectable">
																			<form name="" action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
																				<input type="hidden" name="__vtrftk" value="sid:c04063b65134bde81b3d3882f7d7fcc607c9b1dc,1465263431">
																				<input type="hidden" name="publicid" value="54dc0bb43310944d78897eb858534c9b">
																				<input type="hidden" name="name" value="General FORM">
																				<input type="hidden" name="VTIGER_RECAPTCHA_PUBLIC_KEY" value="RECAPTCHA PUBLIC KEY FOR THIS DOMAIN">
																				<table style="width:100%;border:none;">
																					<tbody>
																						<tr>
																							<td>
																								<input type="text" placeholder="Họ và tên*" name="lastname" value="" required="">                
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<input type="text" placeholder="Số điện thoại*" name="mobile" value="" required="">                
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<input type="email" placeholder="Email" name="email" value="">                
																							</td>
																						</tr>
																						<tr style="display:none">
																							<td><input name="changeDesc" value="Forest City Malaysia" hidden=""></td>
																						</tr>
																						<tr style="display:none">
																							<td>
																								<select name="leadsource" hidden="">
																									<option value="">Select Value</option>
																									<option value="Website" selected="">Website</option>
																								</select>
																							</td>
																						</tr>
																						<tr style="display:none">
																							<td>
																								<input type="hidden" placeholder="Campaign" name="label:Campaign" value="">                
																							</td>
																						</tr>
																					</tbody>
																				</table>
																				<input type="submit" value="ĐĂNG KÝ">
																			</form>
																		</div>
																	</div>
																	<input type="email" name="email" value="" class="om-helper-field" /><input type="text" name="website" value="" class="om-helper-field" />
																</div>
															</div>
														</div>
													</section>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>';
        }

        return $html;
    }

    private function getContentBlock110($request) {
        $html = [];

        foreach (\Config::get('languages') as $lang => $language) {
            $tmp_left = '';
            $tmp_contentLeft = '';

            $tmp_right = '';
            $tmp_contentRight = '';

            $idx_img = 1;
            for ($i = 0; $i < 30; $i++) {

                //img
                if ($request->title_icon_110[$lang][$i] != '') {

                    if ($idx_img == 1) {
                        $tmp_contentLeft .= '<li>
                                            <div  style="background-color:#ffffff; border:1px solid #83b828; color:#83b828; " class="iconlist_icon avia-font-entypo-fontello">
                                            <span class="fa ' . $request->icon[$lang][$i] . '" aria-hidden="true" data-av_iconfont="entypo-fontello"></span>
                                            </div>
                                            <article class="article-icon-entry "  itemscope="itemscope">
                                                <div class="iconlist_content_wrap">
                                                    <header class="entry-content-header">
                                                        <h4 class="iconlist_title"  itemprop="headline"   style="color:#0a0a0a; ">' . $request->title_icon_110[$lang][$i] . '</h4>
                                                    </header>
                                                    <div class="iconlist_content "  itemprop="text"  >
                                                        <p>' . $request->description_icon_110[$lang][$i] . '</p>
                                                    </div>
                                                </div>
                                                <footer class="entry-footer"></footer>
                                            </article>
                                            <div class="iconlist-timeline"></div>
                                        </li>';
                    }

                    if ($idx_img == 2) {

                        $tmp_contentRight .= '<li>
                                            <div  style="background-color:#ffffff; border:1px solid #83b828; color:#83b828; " class="iconlist_icon avia-font-entypo-fontello">
                                                <span class="fa ' . $request->icon[$lang][$i] . '" aria-hidden="true"></span>
                                            </div>
                                            <article class="article-icon-entry "  itemscope="itemscope" itemtype="https://schema.org/CreativeWork" >
                                                <div class="iconlist_content_wrap">
                                                    <header class="entry-content-header">
                                                        <h4 class="iconlist_title"  itemprop="headline"   style="color:#0a0a0a; ">' . $request->title_icon_110[$lang][$i] . '</h4>
                                                    </header>
                                                    <div class="iconlist_content "  itemprop="text"  >
                                                        <p>' . $request->description_icon_110[$lang][$i] . '</p>
                                                    </div>
                                                </div>
                                                <footer class="entry-footer"></footer>
                                            </article>
                                            <div class="iconlist-timeline"></div>
                                        </li>';
                        $idx_img = 0;
                    }
                    $idx_img++;
                }

                $tmp_left = '<div class="flex_column av_one_third  flex_column_div av-zero-column-padding first  avia-builder-el-115  el_after_av_hr  el_before_av_one_third  " style="border-radius:0px; ">
                                <div class="avia-icon-list-container  avia-builder-el-116  avia-builder-el-no-sibling ">
                                    <ul class="avia-icon-list avia-icon-list-right av-iconlist-big avia_animate_when_almost_visible">
                                        ' . $tmp_contentLeft . '
                                    </ul>
                                </div>
                            </div>';
                $tmp_right = '<div class="flex_column av_one_third  flex_column_div av-zero-column-padding   avia-builder-el-120  el_after_av_one_third  avia-builder-el-last  " style="border-radius:0px; ">
                                <div class="avia-icon-list-container  avia-builder-el-121  avia-builder-el-no-sibling ">
                                    <ul class="avia-icon-list avia-icon-list-left av-iconlist-big avia_animate_when_almost_visible">
                                        ' . $tmp_contentRight . '
                                    </ul>
                                </div>
                            </div>';
            }

            $html[$lang] = '<div id="av_section_10" class="avia-section main_color avia-section-large avia-no-border-styling avia-bg-style-scroll  avia-builder-el-109  el_after_av_section  avia-builder-el-last  container_wrap fullsize" style = "background-color: #ffffff; background-color: #ffffff; "  >
                                <div class="container" >
                                    <div class="template-page content  av-content-full alpha units">
                                        <div class="post-entry post-entry-type-page post-entry-6">
                                            <div class="entry-content-wrapper clearfix">
                                                <div style="padding-bottom:10px;color:#000000;font-size:28px;" class="av-special-heading av-special-heading-h3 custom-color-heading blockquote modern-quote modern-centered  avia-builder-el-110  el_before_av_hr  avia-builder-el-first   av-inherit-size">
                                                    <h3 class="av-special-heading-tag"  itemprop="headline">' . $request->title[$lang] . '</h3>
                                                    <div class="special-heading-border">
                                                        <div class="special-heading-inner-border" style="border-color:#000000"></div>
                                                    </div>
                                                </div>
                                                <div style=" margin-top:10px; margin-bottom:10px;"  class="hr hr-custom hr-center hr-icon-no  avia-builder-el-111  el_after_av_heading  el_before_av_textblock "><span class="hr-inner   inner-border-av-border-fat" style=" width:70px; border-color:#000000;" ><span class="hr-inner-style"></span></span></div>
                                                <section class="av_textblock_section"  itemscope="itemscope" itemtype="https://schema.org/CreativeWork" >
                                                    <div class="avia_textblock "   itemprop="text" >
                                                        <p style="text-align: center;"><em>' . $request->description[$lang] . '</em></p>
                                                    </div>
                                                </section>
                                                <div class="avia-image-container  av-styling-   avia-builder-el-113  el_after_av_textblock  el_before_av_hr  avia-align-center "  itemscope="itemscope" itemtype="https://schema.org/ImageObject"  >
                                                    <div class="avia-image-container-inner"><img class="avia_image " src="' . $request->img110_big[$lang] . '" alt="lo trinh tien ich forest" title=""   itemprop="contentURL"  /></div>
                                                </div>
                                                <div style="height:20px" class="hr hr-invisible  avia-builder-el-114  el_after_av_image  el_before_av_one_third "><span class="hr-inner " ><span class="hr-inner-style"></span></span></div>
                                                
                                                ' . $tmp_left . '
                                                    
                                                <div class="flex_column av_one_third  flex_column_div av-zero-column-padding   avia-builder-el-117  el_after_av_one_third  el_before_av_one_third  " style="border-radius:0px; ">
                                                    <section class="av_textblock_section"  itemscope="itemscope" itemtype="https://schema.org/CreativeWork" >
                                                        <div class="avia_textblock "   itemprop="text" >
                                                            <p style="text-align: justify;">' . $request->description_thumb_110[$lang] . '</p>
                                                        </div>
                                                    </section>
                                                    <div class="avia-image-container  av-styling-   avia-builder-el-119  el_after_av_textblock  avia-builder-el-last  avia-align-center "  itemscope="itemscope" itemtype="https://schema.org/ImageObject"  >
                                                        <div class="avia-image-container-inner"><img class="avia_image " src="' . $request->img110_thumb[$lang] . '" alt="icon travel" title=""   itemprop="contentURL"  /></div>
                                                    </div>
                                                </div>
                                                
                                                ' . $tmp_right . '

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>';
        }

        return $html;
    }

    private function getContentBlock21($request) {
        $html = [];

        foreach (\Config::get('languages') as $lang => $language) {
            $title = '';
            if (!empty($request->title[$lang])) {
                $title = nl2br($request->title[$lang]);
            }

            //regits btn
            $btnRegis = '';
            if (!empty($request->btn_regis[$lang])) {
                $btnRegis = '<p><a class="awesome video tuvan btn-dk"><i class="fa fa-check-circle-o"></i>&nbsp; ' . $request->btn_regis[$lang] . '</a></p>';
            }

            $html[$lang] = '<section class=" section animatedParent animateOnce " style="background: url(../template/tint/images/background-sen.jpg);">
								<div class="position-relative ">
									<div class="container-fluid">
										<div class=" animatedParent animateOnce">
											<div class="img-absolute img-left img-1-2 responsive animated fadeInLeftShort delay-500" 
												 style="background-image: url(' . $request->img[$lang] . ');"></div>
											<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-7 none-padding content">
												<div class="right">
													<div id="divContent">
														<h2 class="title-3 animated fadeInRight delay-500">' . $title . '</h2>
														<p>' . $request->content[$lang] . '</p>
                                                        ' . $btnRegis . '
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>';
        }

        return $html;
    }

    private function getContentBlock22($request) {
        $html = [];

        foreach (\Config::get('languages') as $lang => $language) {
            $tmp = '';

            for ($i = 0; $i <= 20; $i++) {
                if (!empty($request['img'][$lang][$i])) {
                    $tmp .= '<div class="col-xs-12 col-sm-6 none-padding item" style="background-image: url(' . $request->img[$lang][$i] . ')">
							<a class="item1" data-id="831" data-href="' . $request->img[$lang][$i] . '">
								<div class="name">
									<p>' . $request->title[$lang][$i] . '</p>
								</div>
							</a>
						</div>';
                }
            }



            $html[$lang] = '<section class=" section slide-2 animatedParent animateOnce " style="background: url(/assets/uploads/myfiles/images/utilities/background-sen.jpg);">
								<div class="position-relative">
									<div class="slick-slider">
										<div class="slider-1 list-album">
											' . $tmp . '
										</div>
									</div>
								</div>
							</section>';
        }

        return $html;
    }

    private function getContentBlock23($request) {
        $html = [];
        $prefix = app('ClassCommon')->generateRandomString();
        foreach (\Config::get('languages') as $lang => $language) {
            $tmpTitle = '';
            $tmpContent = '';
            $idx = 1;
            for ($i = 0; $i <= 20; $i++) {
                if (!empty($request['title_tab_111'][$lang][$i])) {
                    $class = '';
                    if ($idx == 1) {
                        $class = 'active';
                        $idx++;
                    }
                    $idTab = $prefix . $i;
                    $tmpTitle .= '<li role="presentation" class="' . $class . '"><a href="#' . $idTab . '" aria-controls="home" role="tab" data-toggle="tab">' . $request['title_tab_111'][$lang][$i] . '</a></li>';
                    $tmpContent .= '<div role="tabpanel" class="tab-pane ' . $class . '" id="' . $idTab . '">' . $request['description_tab_111'][$lang][$i] . '</div>';
                }
            }



            $html[$lang] = '<div>
							<h2 class="title-3 animated fadeInRight delay-500 go">' . $request->title[$lang] . '</h2>
							<ul class="nav nav-tabs" role="tablist">
								' . $tmpTitle . '
							</ul>
							<div class="tab-content">
								' . $tmpContent . '
							</div>
						</div>';
        }

        return $html;
    }

}
