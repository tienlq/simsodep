<?php

namespace App\Services\Entity;

use App\Model\Users;

class ClassUsers {

    public function listUser() {
        $users = Users::get();
        return $users;
    }

    public function saveUser($id, $request){
        if ($id > 0) {
            $user = Users::find($id);
        } else {
            $user = new Users;
        }
        if(!empty($request->password))
        	$user->password = bcrypt($request->password);
        $user->username = $request->username;
        $user->fullName = $request->fullName;
        $user->email = $request->email;
        $user->skypeAccount = $request->skype;
        $user->phoneNumber = $request->phone;
        $user->save();
        return $user;
    }
}
