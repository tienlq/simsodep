<?php

namespace App\Services\Entity;

use App\Model\News;

class ClassNews {

    public function getFirstRowByCategoryId($cid) {
        $news = News::where('category_id', $cid)->first();
        return $news;
    }

    public function getDataByCategoryId($categoryId, $paginate = 30) {
        $data = News::select([
                    \TblName::NEWS . '.id as nId',
                    \TblName::NEWS . '.*',
                    \TblName::NEWS_DATA . '.id as ndId',
                    \TblName::NEWS_DATA . '.*',
                ])
                ->where(\TblName::NEWS_DATA . '.language', \App::getLocale())
                ->where('category_id', '=', $categoryId)
                ->leftJoin(\TblName::NEWS_DATA, \TblName::NEWS_DATA . '.news_id', \TblName::NEWS . '.id')
                ->paginate($paginate);
        return $data;
    }

    public function getDataNewsById($id) {
        $result = [];
        foreach (\Config::get('languages') as $lang => $language) {
            $data = News::select([
                        \TblName::NEWS . '.id as nId',
                        \TblName::NEWS . '.*',
                        \TblName::NEWS_DATA . '.id as ndId',
                        \TblName::NEWS_DATA . '.*',
                    ])
                    ->where(\TblName::NEWS . '.id', $id)
                    ->where(\TblName::NEWS_DATA . '.language', $lang)
                    ->leftJoin(\TblName::NEWS_DATA, \TblName::NEWS_DATA . '.news_id', \TblName::NEWS . '.id')
                    ->first();
            $result[$lang] = $data;
        }
        return $result;
    }

    public function getDataNewsByColId($id, $col = 'id') {
        $result = [];
        foreach (\Config::get('languages') as $lang => $language) {
            $data = News::select([
                        \TblName::NEWS . '.id as nId',
                        \TblName::NEWS . '.*',
                        \TblName::NEWS_DATA . '.id as ndId',
                        \TblName::NEWS_DATA . '.*',
                    ])
                    ->where(\TblName::NEWS . '.' . $col, $id)
                    ->where(\TblName::NEWS_DATA . '.language', $lang)
                    ->leftJoin(\TblName::NEWS_DATA, \TblName::NEWS_DATA . '.news_id', \TblName::NEWS . '.id')
                    ->first();
            $result[$lang] = $data;
        }
        return $result;
    }

    public function getDataByFront($limit = 30) {
        $data = News::where('front', 1)
                ->where(\TblName::NEWS_DATA . '.language', \App::getLocale())
                ->leftJoin(\TblName::NEWS_DATA, \TblName::NEWS . '.id', \TblName::NEWS_DATA . '.news_id')
                ->limit($limit)
                ->get();
        return $data;
    }

    public function getFirstDataByCategory($cid = 0) {
        $data = News::where('category_id', $cid)
                ->where(\TblName::NEWS_DATA . '.language', \App::getLocale())
                ->leftJoin(\TblName::NEWS_DATA, \TblName::NEWS . '.id', \TblName::NEWS_DATA . '.news_id')
                ->first();
        return $data;
    }

    public function checkCountRowByCategory($cid) {
        $count = News::where('category_id', $cid)->count();
        return $count;
    }

    public function saveNews($request, $id) {
        if ($id > 0) {
            $news = News::find($id);
        } else {
            $news = new News;
        }

        if ($request->landingpage_id > 0) {
            $news->dataLandingpage = json_encode($_POST);
        }

        $news->category_id = $request->category_id;
        $news->image = $request->image;
        $news->type = $request->type;
        $news->landingpage_id = $request->landingpage_id;
        $news->video = $request->video;

        $news->save();
        return $news;
    }

    public function getAllNews($request, $conditions = []) {
        $news = new News;
        $news = $news->select([
            \TblName::NEWS . '.id as nId',
            \TblName::NEWS . '.*',
            \TblName::NEWS_DATA . '.id as ndId',
            \TblName::NEWS_DATA . '.*'
        ]);
        if (isset($request->categoryID) && intval($request->categoryID) > 0) {
            $subCategoryID = app('ClassCategory')->getSubCategoryID($request->categoryID);
            $news = $news->whereIn('category_id', $subCategoryID);
        }

        if (!empty($conditions)) {
            foreach ($conditions as $key => $val) {
                $news = $news->where($key, $val);
            }
        }

        $news = $news->where('news.id', '!=', 1);

        if (!empty($request->type)) {
            $news = $news->where(\TblName::NEWS . '.type', $request->type);
        }

        $news = $news->leftJoin(\TblName::NEWS_DATA, \TblName::NEWS . '.id', \TblName::NEWS_DATA . '.news_id');
        $news = $news->orderBy(\TblName::NEWS . '.id', 'desc');
        $news = $news->paginate(30);
        return $news;
    }

    public function saveLandingpage($cid, $request, $type) {
        if ($type == 1 || $type == 2) {
            $dataSave = self::getData2Temp12($request, $type, $cid);
        } elseif ($type == 3) {
            $dataSave = self::getData2Temp03($request, $type, $cid);
        }
        try {
            \DB::beginTransaction();
            $news = News::where('category_id', $cid)->first();
            if (count($news) == 0) {
                $news = new News;
            }
            $news->category_id = $cid;
            $news->dataLandingpage = json_encode($dataSave['data']);
            $news->landingpage_id = $type;
            $news->background_pc = $_POST['background_pc'];
            $news->background_mobile = $_POST['background_mobile'];
            $news->save();
            $newsData = app('ClassNewsData')->saveLandingpageNewsData($news->id, $dataSave['content'], $dataSave['category']);
            \DB::commit();
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            \DB::rollBack();
            return 'error';
        }
        \DB::rollBack();
        return 'success';
    }

    private function getData2Temp03($request, $type, $cid) {
//        print_r($_POST);die;
        $data = [];
        $strContent_vi = '';
        $strContent_en = '';
        $count = count($request['block_type']);
        for ($i = 0; $i < $count; $i++) {
            switch ($request['block_type'][$i]) {
                case 1:
                    $dataTmp = app('LandingPagesHelper')->blockLandingPage31($request, $i);
                    $strContent_vi .= $dataTmp;
                    $strContent_en .= $dataTmp;
                    break;
                case 2:
                    $strContent_vi .= app('LandingPagesHelper')->blockLandingPage32($request, 'vi', $i);
                    $strContent_en .= app('LandingPagesHelper')->blockLandingPage32($request, 'en', $i);
                    break;
                case 3:
                    $strContent_vi .= app('LandingPagesHelper')->blockLandingPage33($request, 'vi', $i);
                    $strContent_en .= app('LandingPagesHelper')->blockLandingPage33($request, 'en', $i);
                    break;
                case 4:
                    $strContent_vi .= app('LandingPagesHelper')->blockLandingPage34($request, 'vi', $i);
                    $strContent_en .= app('LandingPagesHelper')->blockLandingPage34($request, 'en', $i);
                    break;
                case 5:
                    $strContent_vi .= app('LandingPagesHelper')->blockLandingPage35($request, 'vi', $i);
                    $strContent_en .= app('LandingPagesHelper')->blockLandingPage35($request, 'en', $i);
                    break;
                default:
                    echo "err";
            }
        }
        if ($strContent_vi == '' && $strContent_en == '')
            return 'error';

        $strContent = ['vi' => nl2br($strContent_vi), 'en' => nl2br($strContent_en)];
        $category = [
            'vi' => app('ClassCategory')->getDataById($cid, 'vi'),
            'en' => app('ClassCategory')->getDataById($cid, 'en')
        ];
        return [
            'content' => $strContent,
            'category' => $category,
            'data' => $_POST
        ];
    }

    private function getData2Temp12($request, $type, $cid) {
        $data = [];
        $strContent_vi = '';
        $strContent_en = '';
        $count = count($request['image']);
        for ($i = 0; $i < $count; $i++) {
            if (!empty($request['image'][$i]) || !empty($request['image_thumb'][$i]) || !empty($request['description']['vi'][$i]) || !empty($request['description']['en'][$i])) {
                $dataTmp = [
                    'title' => $request['title'],
                    'block_type' => $request['block_type'][$i],
                    'image' => $request['image'][$i],
                    'image_thumb' => $request['image_thumb'][$i],
                    'description' => [
                        'vi' => $request['description']['vi'][$i],
                        'en' => $request['description']['en'][$i]
                    ]
                ];
                $data[] = $dataTmp;
                if ($type == 1) {
                    $strContent_vi .= app('LandingPagesHelper')->blockLandingPage01($dataTmp, 'vi');
                    $strContent_en .= app('LandingPagesHelper')->blockLandingPage01($dataTmp, 'en');
                } elseif ($type == 2) {
                    $strContent_vi .= app('LandingPagesHelper')->blockLandingPage02($dataTmp, 'vi');
                    $strContent_en .= app('LandingPagesHelper')->blockLandingPage02($dataTmp, 'en');
                }
            }
        }
        if ($strContent_vi == '' && $strContent_en == '')
            return 'error';

        $strContent = ['vi' => nl2br($strContent_vi), 'en' => nl2br($strContent_en)];
        $category = [
            'vi' => app('ClassCategory')->getDataById($cid, 'vi'),
            'en' => app('ClassCategory')->getDataById($cid, 'en')
        ];
        return [
            'content' => $strContent,
            'category' => $category,
            'data' => $request
        ];
    }

    public function searchByKeyword($keyword, $limit = 30) {
        $list = News::where(\TblName::NEWS_DATA . ".title", "like", "%$keyword%")
                ->where("content", "like", "%$keyword%")
                ->where(\TblName::NEWS_DATA . '.language', \App::getLocale())
                ->leftJoin(\TblName::NEWS_DATA, \TblName::NEWS_DATA . '.news_id', \TblName::NEWS . '.id')
                ->paginate($limit);

        return $list;
    }

    public function htmlNewsLatest() {
        $html = '';
        $list = News::select([
                    \TblName::NEWS . '.id as nId',
                    \TblName::NEWS . '.*',
                    \TblName::NEWS_DATA . '.id as ndId',
                    \TblName::NEWS_DATA . '.*'
                ])
                ->where(\TblName::NEWS_DATA . '.language', \App::getLocale())
                ->leftJoin(\TblName::NEWS_DATA, \TblName::NEWS_DATA . '.news_id', \TblName::NEWS . '.id')
                ->paginate(10);

        foreach ($list as $news) {
            $html .= '<a class="list-group-item lgit" href="' . route('detailNews', [$news->sluggable,$news->nId]) . '">' . $news->title . '</a>';
        }

        return $html;
    }

}
