<?php

namespace App\Services\Entity;

use App\Model\Contact;

class ClassContact {

	public function saveContact($request) {
//		try {
			$contact = new Contact;
			$contact->name = $request->name;
			$contact->email = $request->email;
			$contact->phone = $request->phone;
//			$contact->title = $request->title;
			$contact->message = $request->content;
			$contact->save();
			return \ReturnCode::RETURN_SUCCESS;
//		} catch (\Exception $exc) {
//			echo $exc->getTraceAsString();
//			return \ReturnCode::RETURN_ERROR;
//		}
	}
    
    public function getCountNewContact() {

		try {
			$contact = Contact::where('status', 0)->count();
			return $contact;
		} catch (\Exception $exc) {
//			echo $exc->getMessage();
			return \ReturnCode::RETURN_ERROR;
		}
	}
}
