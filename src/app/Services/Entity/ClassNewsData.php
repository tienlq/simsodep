<?php

namespace App\Services\Entity;

use App\Model\NewsData;

class ClassNewsData {

	public function saveNewsData($id, $request) {
		$content = [];
		$script = [];
		if ($request->landingpage_id == 1 || $request->landingpage_id == 2) {
			foreach (\Config::get('languages') as $lang => $language) {
				$content[$lang] = '';
				$script[$lang] = '';
				if (!empty($id)) {
					$listBlock = app('ClassBlockLandingpage')->getByNewsId($id);

					$pathBlock = '';
					foreach ($listBlock as $block) {
						$keyContentName = 'content_' . $lang;
						$content[$lang] .= $block->content_vi;
						$pathBlock .= "'$block->sluggable',";
					}
					$pathBlock .= "'footer'";
					if ($request->landingpage_id == 2) {
						$script[$lang] .= '<script>
											jQuery(document).ready(function ($) {
												var sections = $("#page-tong-quan.fullpage .section");
												var fullpage = $("#page-tong-quan.fullpage");

												fullpage.fullpage({
													verticalCentered: false,
													anchors: [' . $pathBlock . '],
													onLeave: function (index, nextIndex, direction) {

														if (sections[nextIndex - 1]) {
															$(sections[nextIndex - 1]).find(".animated").addClass("go");
														}
														if (nextIndex > 1) {
															$(".fixed").addClass("top-head-sticky");
															$("header").addClass("head_sticky");
															$("a#gotop").fadeIn(250);

														} else {

															$(".fixed").removeClass("top-head-sticky");
															$("header").removeClass("head_sticky");
															$("a#gotop").fadeOut(250);
														}
													},

													afterRender: function (anchorLink, index) {
														sections.find(".animated").removeClass("go")
													},
													responsiveWidth: 992
												});
											});
										</script>';
					}
				}
			}
		} else {
			foreach (\Config::get('languages') as $lang => $language) {
				$content[$lang] = $request->content[$lang];
				$script[$lang] = '';
			}
		}

		foreach (\Config::get('languages') as $lang => $language) {
			//kiểm tra ngôn ngữ này đã có data chưa
			$newsData = NewsData::where('news_id', $id)
					->where('language', $lang)
					->first();
			if (count($newsData) == 0)
				$newsData = new NewsData;

			$newsData->sluggable = app('ClassCommon')->formatText($request->title[$lang]);
			$newsData->content = $content[$lang];
            $newsData->content02 = $request->content02[$lang];
			$newsData->script = $script[$lang];
			$newsData->summary = $request->summary[$lang];
			$newsData->title = $request->title[$lang];
			$newsData->description = $request->description[$lang];
			$newsData->keyword = $request->keyword[$lang];
			$newsData->language = $lang;
			$newsData->news_id = $id;
			$newsData->save();
		}
		return \ReturnCode::RETURN_SUCCESS;
	}

	public function saveLandingpageNewsData($newsId, $strContent, $category) {
		foreach (\Config::get('languages') as $lang => $language) {
			//kiểm tra ngôn ngữ này đã có data chưa
			$newsData = NewsData::where('news_id', $newsId)
					->where('language', $lang)
					->first();
			if (count($newsData) == 0)
				$newsData = new NewsData;
			$newsData->sluggable = app('ClassCommon')->formatText($category[$lang]->name);
			$newsData->content = $strContent[$lang];
			$newsData->title = $category[$lang]->name;
			$newsData->news_id = $newsId;
			$newsData->language = $lang;
			$newsData->save();
		}
		return \ReturnCode::RETURN_SUCCESS;
	}

}
