<?php

namespace App\Services\Entity;

use App\Model\Supplier;

class ClassSupplier {
    public function getAll() {
        $data = Supplier::orderBy('sort_order', 'asc')->get();
        return $data;
    }
}
