<?php

namespace App\Services\Entity;

use App\Model\SiteConfig;

class ClassConfig {

	public function getConfig() {
		if (\App::getLocale() == 'vi') {
			$id = 1;
		} else {
			$id = 2;
		}
		$config = SiteConfig::find($id);
		return $config;
	}

}
