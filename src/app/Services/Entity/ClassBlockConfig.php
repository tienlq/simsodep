<?php

namespace App\Services\Entity;

use App\Model\Block;

class ClassBlock {

    public function saveBlock($id, $request) {
        if ($id > 0) {
            $block = Block::find($id);
        } else {
            $block = new Block;
        }

        $block->link = $request->link;
        $block->image = $request->image;
        $block->type = $request->type;
        $block->save();
        return $block;
    }

    public function saveBlockData($id, $request) {
        try {
            \DB::beginTransaction();
            foreach (\Config::get('languages') as $lang => $language) {
                if (BlockData::where('block_id', $id)->where('language', $lang)->count() > 0)
                    $blockData = BlockData::where('block_id', $id)->where('language', $lang)->first();
                else
                    $blockData = new BlockData;
                $blockData->name = $request->name["{$lang}"];
                $blockData->description = $request->description["{$lang}"];;
                $blockData->block_id = $id;
                $blockData->language = $lang;
                $blockData->save();
            }
            \DB::commit();
            return \ReturnCode::RETURN_SUCCESS;
        } catch (\Exception $exc) {
            \DB::rollback();
            echo $exc->getTraceAsString();
            return \ReturnCode::RETURN_ERROR;
        }
        return $blockData;
    }
    
     public function getBlockByType($type) {
        $data = Block::select([
                    \TblName::BLOCK . ".id as id",
                    \TblName::BLOCK . ".*",
                    \TblName::BLOCK_DATA . ".id as dataId",
                    \TblName::BLOCK_DATA . ".*"
                ])
                ->where(\TblName::BLOCK . ".type", $type)
                ->where(\TblName::BLOCK_DATA . ".language", \App::getLocale())
                ->leftJoin(\TblName::BLOCK_DATA, \TblName::BLOCK_DATA . ".block_id", \TblName::BLOCK . ".id")
                ->orderBy('sort_order', 'asc')
                ->get();
        return $data;
    }

}
