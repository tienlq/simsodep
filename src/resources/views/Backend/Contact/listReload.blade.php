<table class="table table-striped" id="nestable">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Message</th>
            <th>Date</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php $idx = 0 ?>
        @foreach($listContact as $contact)
        <?php $idx++ ?>
        <tr>
            <td>{{ $idx }}</td>
            <td>{{ $contact->name }}</td>
            <td>{{ $contact->email }}</td>
            <td>{{ $contact->message }}</td>
            <td>{{ date('Y-D-M', strtotime($contact->created_at)) }}</td>
            <td>
                <a onclick="deleteRow('{{ url('/admin/contact/delete/' . $contact->id) }}', '{{ url('admin/contact/list') }}')">
                    <i data-pack="default" class="ion-trash-a"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>