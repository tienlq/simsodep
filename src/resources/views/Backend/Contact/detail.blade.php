<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Orders detail</h4>
</div>

<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <div class="card-body">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab-01">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>Status</td>
                                <td>
                                    @if($orders->status == 1 )
                                    <span class="_success">Success</span>
                                    @else
                                    <span class="_failed">Failed</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>Code orders</td>
                                <td>KH-{{ $orders->id or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>Product name</td>
                                <td>
                                    <a target="new" href="/product/detail/{{ $orders->productID }}">{{ $orders->productName }}</a>
                                </td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>Name</td>
                                <td>{{ $orders->fullName or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>City</td>
                                <td>{{ $orders->city or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>Zip Code</td>
                                <td>{{ $orders->zipcode or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>Country</td>
                                <td>{{ $country->countryName or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>Tel</td>
                                <td>{{ $orders->phone or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>State/Region</td>
                                <td>{{ $orders->region or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>Comment</td>
                                <td>{{ $orders->notes or '' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>