@extends('Backend.Layouts.main')

@section('title')
CONTACT MANAGEMENT
@stop

@section('breadcrumb')
CONTACT MANAGEMENT
@stop

@section('avatar')
@if($user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
</a>
<div class="mt">Welcome, {{ $user->username }}</div>
@endif
@stop

@section('scriptCustom')

$('#datepicker-1')
.datepicker({
container: '#example-datepicker-container-5'
});

@stop

@section('content')

<!--<div class="header-form-search row">
    <center>
        <input type="text" placeholder="Start date" name="startDate"/>
        <input type="text" placeholder="End date" name="endDate"/>

        <input type="submit" value="Search">
    </center>
</div>-->

<div class="container-fluid">
    <div class="card">
        <div class="table-responsive">
            <table class="table table-striped" id="nestable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Họ tên</th>
                        <th>Email</th>
                        <th>Điện thoại</th>
                        <th>Nội dung</th>
                        <th>Date</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($listContact as $idx => $contact)
                    <tr class="{{ $contact->status == 0 ? '_bold':'' }}">
                        <td>{{ $idx+1 }}</td>
                        <td>{{ $contact->name }}</td>
                        <td>{{ $contact->email }}</td>
                        <td>{{ $contact->phone }}</td>
                        <td>{{ $contact->message }}</td>
                        <td>{{ date('Y-D-M', strtotime($contact->created_at)) }}</td>
                        <td>
                            <a onclick="deleteRow('{{ url('/admin/contact/delete/' . $contact->id) }}', '{{ url('admin/contact/list') }}')">
                                <i data-pack="default" class="ion-trash-a"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $listContact->render() !!}
        </div>
    </div>
</div>
<!-- END row-->



@stop