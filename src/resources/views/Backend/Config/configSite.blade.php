@extends('Backend.Layouts.main')

@section('title')
	Cấu hình website
@stop

@section('breadcrumb')
	Cấu hình website
@stop

@section('avatar')
    @if($user->avatar != '')
        <a href="">
            <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
        </a>
        <div class="mt">Welcome, {{ $user->username }}</div>
    @endif
@stop
@section('scriptAddon')
    <script>
        var route_prefix = "{{ url(config('lfm.prefix')) }}";

        {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}

        $('.lfm').filemanager('image', {prefix: route_prefix});
    </script>
@stop
@section('content')
<div class="panel panel-primary">
    <div class="modal-body">
        <form id="form-register" name="registerForm" novalidate="" class="form-validate" method="post" enctype="multipart/form-data">
            {{ csrf_field()}}
            <div class="card-body">
				<div class="panel-heading"><b>Thông tin chung</b></div>
				<br>
				<div class="row">
					<div class="col-sm-6">
						<div class="mda-form-group mb">
							<div class="mda-form-control">
								<input name="name" 
									   value="{{ $siteConfig['vi']->name or '' }}"
									   tabindex="0" 
									   aria-required="true" 
									   aria-invalid="true" 
									   class="form-control">
								<div class="mda-form-control-line"></div>
								<label>Họ tên</label>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="mda-form-group mb">
							<div class="mda-form-control">
								<input name="email" 
									   value="{{ $siteConfig['vi']->email or '' }}"
									   required="" 
									   tabindex="0" 
									   aria-required="true" 
									   aria-invalid="true" 
									   class="form-control">
								<div class="mda-form-control-line"></div>
								<label>Email</label>
							</div>
						</div>
					</div>
				</div>
                 <div class="row">
					<div class="col-sm-6">
						<div class="mda-form-group mb">
							<div class="mda-form-control">
								<input name="phone" 
									   value="{{ $siteConfig['vi']->phone or '' }}"
									   required="" 
									   tabindex="0" 
									   aria-required="true" 
									   aria-invalid="true" 
									   class="form-control">
								<div class="mda-form-control-line"></div>
								<label>Phone</label>
							</div>
						</div>
					</div>
                    <div class="col-sm-6">
                        <div class="mda-form-group mb">
                            <div class="mda-form-control">
                                <input type="file" name="favicon">
                                <img class="img-logo" src="/favicon.ico"/>
                                <div class="mda-form-control-line"></div>
                                <label>Favicon</label>
                            </div>
                        </div>
                    </div>
                </div>
				
				<div class="row">
					<div class="col-sm-6">
						<div class="mda-form-group mb">
							<div class="mda-form-control">
								<textarea name="code01" class="form-control">{{ $siteConfig['vi']->code01 or '' }}</textarea>
								<div class="mda-form-control-line"></div>
								<label>Thời gian cho mỗi phiên hỗ trợ trực tuyến (Giây)</label>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="mda-form-group mb">
							<div class="mda-form-control">
								<textarea name="code02" class="form-control">{{ $siteConfig['vi']->code02 or '' }}</textarea>
								<div class="mda-form-control-line"></div>
								<label>Mã nhúng từ từ nguồn khác, (facebook, google, youtube....)</label>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
                    <div class="col-lg-6">
                        <label>Banner</label>
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a data-input="background_input_pc" data-preview="background_img_pc" class="btn btn-primary lfm">
                                    <i class="fa fa-picture-o"></i> Chọn ảnh
                                </a>
                            </span>
                            <input id="background_input_pc" 
                                   class="form-control" type="text" 
                                   name="background_pc" 
                                   value="{{  $siteConfig['vi']->background_pc  or '' }}" />
                        </div>
                        @if(!empty($siteConfig['vi']->background_pc))
                            <img id="background_img_pc" src="{{ $siteConfig['vi']->background_pc or '' }}" class="img-landingpage-01"/>
                        @else
                            <img id="background_img_pc" class="img-landingpage-01"/>
                        @endif
                    </div>
                    
<!--                    <div class="col-lg-6">
                        <label>Ảnh nền mặc đinh mobile - tỷ lệ chuẩn: 640x960 px</label>
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a data-input="background_input_mobile" data-preview="background_img_mobile" class="btn btn-primary lfm">
                                    <i class="fa fa-picture-o"></i> Chọn ảnh
                                </a>
                            </span>
                             <input id="background_input_mobile" 
                                       class="form-control" type="text" 
                                       name="background_mobile" 
                                       value="{{ $siteConfig['vi']->background_mobile or '' }}" />
                        </div>
                        @if(!empty($siteConfig['vi']->background_mobile))
                            <img id="background_img_mobile" src="{{ $siteConfig['vi']->background_mobile or '' }}" class="img-landingpage-01"/>
                        @else
                            <img id="background_img_mobile" class="img-landingpage-01"/>
                        @endif
                    </div>-->
                </div>
				<br>
<!--				<div class="row">
                    <label>Video Trang chủ: chọn định dạng mp4, độ phân giải full HD để đượcc chất lượng hiển thị tốt nhất</label>
					<div class="input-group">
						<span class="input-group-btn">
							<a data-input="input_video_home" data-preview="pre_video_home" class="btn btn-primary lfm">
								<i class="fa fa-picture-o"></i> Chọn ảnh
							</a>
						</span>
						<input id="input_video_home" 
							   class="form-control" type="text" 
							   name="video_home" 
							   value="{{  $siteConfig['vi']->video_home  or '' }}" />
					</div>
					<img id="pre_video_home" class="img-landingpage-01"/>
                </div>-->
				
                <div id="panelDemo14" class="panel">
                    <div class="panel-heading"><b>Nội dung cấu hình theo ngôn ngữ</b></div>
                    <div class="panel-body">
                        <div>
                            <!-- Nav tabs-->
                            <ul role="tablist" class="nav nav-tabs">
                                @foreach (Config::get('languages') as $lang => $language)
                                <li role="presentation" class="{{ $lang == 'vi' ? 'active':'' }}">
                                    <a href="#tab_{{ $lang }}" aria-controls="home" role="tab" data-toggle="tab">Nội dung {{ $language }}</a>
                                </li>
                                @endforeach
                            </ul>
                            <!-- Tab panes-->
                            <div class="tab-content">
                                @foreach (Config::get('languages') as $lang => $language)
                                <div id="tab_{{ $lang }}" role="tabpanel" 
                                         class="tab-pane {{ $lang == 'vi' ? 'active':'' }}">
                                
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="mda-form-group mb">
                                            <div class="mda-form-control">
                                                <input name="title[{{ $lang }}]" 
                                                       value="{{ $siteConfig[$lang]->title or '' }}"
                                                       tabindex="0" 
                                                       aria-required="true" 
                                                       aria-invalid="true" 
                                                       class="form-control">
                                                <div class="mda-form-control-line"></div>
                                                <label>Tiêu đề website {{ $language }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Logo  {{ $language }} - tỷ lệ chuẩn: 190x50 px</label>
											<div class="input-group">
												<span class="input-group-btn">
													<a data-input="thumbnail_33_{{ $lang }}" data-preview="holder_33_{{ $lang }}" class="btn btn-primary lfm">
														<i class="fa fa-picture-o"></i> Chọn ảnh
													</a>
												</span>
												<input id="thumbnail_33_{{ $lang }}" 
													   class="form-control" type="text" 
													   name="logo[{{ $lang }}]" 
													   value="{{ $siteConfig[$lang]->logo or '' }}" />
											</div>
											@if(!empty($siteConfig[$lang]->logo))
												<img id="holder_33_{{ $lang }}" src="{{ $siteConfig[$lang]->logo or '' }}" class="img-landingpage-01"/>
											@else
												<img id="holder_33_{{ $lang }}" class="img-landingpage-01"/>
											@endif
                                    </div>
                                </div>
									
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="mda-form-group mb">
                                            <div class="mda-form-control">
                                                <textarea name="keyword[{{ $lang }}]" 
                                                          rows="4" 
                                                          aria-multiline="true" 
                                                          tabindex="0" 
                                                          aria-invalid="false" 
                                                          class="form-control">{{ $siteConfig[$lang]->keyword or '' }}</textarea>
                                                <div class="mda-form-control-line"></div>
                                                <label>[SEO] Keyword  {{ $language }}</label>
                                            </div>
                                        </div>
                                        <span class="mda-form-msg">Độ dài từ dưới 150 ký tự.</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="mda-form-group mb">
                                            <div class="mda-form-control">
                                                <textarea name="description[{{ $lang }}]" 
                                                          rows="4" 
                                                          aria-multiline="true" 
                                                          tabindex="0" 
                                                          aria-invalid="false" 
                                                          class="form-control">{{ $siteConfig[$lang]->description or '' }}</textarea>
                                                <div class="mda-form-control-line"></div>
                                                <label>[SEO] Descrption  {{ $language }}</label>
                                            </div>
                                            <span class="mda-form-msg">Độ dài từ dưới 150 ký tự.</span>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix">
                    <div class="pull-left">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="submit" class="btn btn-default">Close</button>
                    </div>
                    <br/>
                </div>
            </div>
        </form>
    </div>
</div>

@stop