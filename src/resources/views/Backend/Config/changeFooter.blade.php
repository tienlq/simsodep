@extends('Backend.Layouts.main')

@section('title')
Thay đổi thông tin phần chân website
@stop

@section('breadcrumb')
Thay đổi thông tin phần chân website
@stop

@section('avatar')
@if($user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
</a>
<div class="mt">Welcome, {{ $user->username }}</div>
@endif
@stop

@section('scriptAddon')
<!-- Summernote-->
<script src="{{ url('vendor/summernote/dist/summernote.js') }}"></script>

<script>
var route_prefix = "{{ url(config('lfm.prefix')) }}";

{
    !! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}

$('.lfm').filemanager('image', {prefix: route_prefix});
</script>

@stop

@section('content')


<div class="container container-lg">
    <div class="card">
        <form id="form-register" name="registerForm" novalidate="" class="form-validate" method="post">
            {{ csrf_field()}}
            <div class="card-body">
                <div class="container-fluid">
                    <label>Content:</label>
                    <div class="main-editor">
                        <textarea class="summernote" name="footer">{{ $siteConfig->footer or '' }}</textarea>
                    </div>
                    <em><a onclick="addHtml2Editor('.defaultText', '.summernote')">click </a>để khôi phục về định dạng mặc định</em>
                </div>
                <div class="clearfix">
                    <div class="pull-left">
                        <button type="submit" class="btn btn-primary">Change</button>
                        <a href="/admin" class="btn btn-default">Close</a>
                    </div>
                    <br/>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="defaultText _hidden">
    <div class="container">
        <div class="footer-content row text-left">
            <div class="col-sm-4 col-xs-12">
                <div class="footer-title">
                    <p>ĐỊA CHỈ DỰ &Aacute;N</p>
                </div>
                <div>
                    <p><span>Đường Phạm H&ugrave;ng, Quận Nam Từ Li&ecirc;m, Th&agrave;nh phố H&agrave; Nội</span></p>
                    <p><span>&nbsp;</span></p>
                    <p class="footer-title">C&Ocirc;NG BỐ TH&Ocirc;NG TIN</p>
                    <p><span>Th&ocirc;ng tin về quy hoạch c&oacute; li&ecirc;n quan đến BĐS v&agrave; c&aacute;c hồ sơ ph&aacute;p l&yacute; dự &aacute;n do C&ocirc;ng ty KD &amp;QL BĐS Vinhomes ph&acirc;n phối, vui l&ograve;ng li&ecirc;n hệ với Ph&ograve;ng Giao dịch C&ocirc;ng ty để được cung cấp.</span></p>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="footer-title">
                    <p>ĐỊA CHỈ C&Ocirc;NG TY</p>
                </div>
                <div>
                    <p><span>C&Ocirc;NG TY </span><span class="_4yxo">CỔ PHẦN</span><span> KINH DOANH V&Agrave; QUẢN L&Yacute; BẤT ĐỘNG SẢN VINHOMES</span></p>
                    <table style="width: 530px;" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td valign="top" width="80">
                                    <p>Địa chỉ</p>
                                </td>
                                <td valign="top">
                                    <p>Số 7, Đường Bằng Lăng 1,</p>
                                    <p>KĐT Vinhomes Riverside, Q.Long Bi&ecirc;n, H&agrave; Nội</p>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="80">
                                    <p>Điện thoại</p>
                                </td>
                                <td valign="top">
                                    <p>+84 (24) 3974 9999</p>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="80">
                                    <p>Email</p>
                                </td>
                                <td valign="top">
                                    <p><a href="mailto:info@vinhomes.vn">info@vinhomes.vn</a></p>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="80">
                                    <p>Website</p>
                                </td>
                                <td valign="top">
                                    <p><a href="http://vinhomes.vn/">http://vinhomes.vn</a></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="footer-title">
                    <p>HOTLINE: 1800 1086</p>
                </div>
                <div>
                    <p><strong>Email:</strong>&nbsp;<a href="mailto:skylake@vinhomes.vn">skylake@vinhomes.vn</a></p>
                    <p><strong>Website</strong>:&nbsp;<a href="index.html">http://skylake.vinhomes.vn</a></p>
                    <p><strong>&nbsp;</strong></p>
                </div>
                <div>
                    <p>* Hình ảnh chỉ mang tính chất minh họa.</p>
                    <p>* Thông số trong bản vẽ là tương đối, thông số chính thức của từng căn sẽ được quy định tại văn bản ký kết giữa Chủ đầu tư và Khách hàng.</p>
                    <p>* Thông tin trong tài liệu đúng tại thời điểm phát hành và Chủ đầu tư có quyền điều chỉnh trong quá trình triển khai mà không cần thông báo trước.</p>
                </div>
            </div>
        </div>
        <br/>
    </div>
</div>
@stop