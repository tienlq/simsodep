@extends('Backend.Layouts.main')

@section('title')
    Thay đổi nội dung mô tả về block trang chủ
@stop

@section('breadcrumb')
    Thay đổi nội dung mô tả về block trang chủ
@stop

@section('avatar')
    @if($user->avatar != '')
        <a href="">
            <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
        </a>
        <div class="mt">Welcome, {{ $user->username }}</div>
    @endif
@stop

@section('scriptAddon')
<script src="{{ url('vendor/summernote/dist/summernote.js') }}"></script>
@stop

@section('content')

<div id="panelDemo14" class="panel">
	<div class="panel-heading"><b>Vui lòng nhập nội cho cho tất cả các tab ngôn ngữ phía dưới</b></div>
	<div class="panel-body">
		<div>
			<!-- Nav tabs-->
			<ul role="tablist" class="nav nav-tabs">
				@foreach (Config::get('languages') as $lang => $language)
					<li role="presentation" class="{{ $lang == 'vi' ? 'active':'' }}">
						<a href="#tab_{{ $lang }}" aria-controls="home" role="tab" data-toggle="tab">Nội dung {{ $language }}</a>
					</li>
				@endforeach
			</ul>
			<!-- Tab panes-->
            {{ csrf_field() }}
            <form id="form-register" name="registerForm" novalidate="" class="form-validate" method="post">
                {{ csrf_field()}}
                <div class="tab-content">
					<?php $idx=1 ?>
                    @foreach (Config::get('languages') as $lang => $language)
                        <div id="tab_{{ $lang }}" role="tabpanel" 
                             class="tab-pane {{ $lang == 'vi' ? 'active':'' }}">
                            <div class="row">
                                <div class="mda-form-group mb">
                                    <div class="mda-form-control">
                                        <textarea name="block01[{{$lang}}]" 
                                                  rows="4" 
                                                  class="form-control summernote">{{ $siteConfig[$lang]->block01 or '' }}</textarea>
                                        <div class="mda-form-control-line"></div>
                                        <label>Tiêu đề block 1 {{ $language }}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="mda-form-group mb">
                                    <div class="mda-form-control">
                                        <textarea name="block02[{{$lang}}]" 
                                                  rows="4" 
                                                  class="form-control summernote">{{ $siteConfig[$lang]->block02 or '' }}</textarea>
                                        <div class="mda-form-control-line"></div>
                                        <label>Tiêu đề block 2 {{ $language }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="clearfix">
                    <div class="pull-left">
                        <button type="submit" class="btn btn-primary">Cập nhật</button>
                    </div>
                    <br/>
                </div>
            </form>
		</div>
	</div>
</div>
@stop