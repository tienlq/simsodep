<input type="hidden" name="landingpage_id" value="3"/>

<div id="blueimp-gallery" class="blueimp-gallery">
    <div class="slides">
        <h3 class="title"></h3><a class="prev">&lsaquo;</a><a class="next">&rsaquo;</a><a class="close">&times;</a><a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
</div>


<div class="row">&nbsp;</div>
<div class="row">
	@foreach (Config::get('languages') as $lang => $language)

			<div class="col-sm-6">
				<div class="mda-form-group mb {{ isset($news[$lang]->nId) ? '' : 'float-label' }}">
					<div class="mda-form-control">
						<input name="title[{{ $lang }}]"
							   value="{{ $news[$lang]->title or '' }}"
							   required="" 
							   tabindex="0" 
							   aria-required="true" 
							   aria-invalid="true" 
							   class="form-control">
						<div class="mda-form-control-line"></div>
						<label>Tiêu đề {{ $language }}</label>
					</div>
				</div>
			</div>
	
	@endforeach
</div>

<div id="accordion_summary" role="tablist" aria-multiselectable="true" class="panel-group">
    <div class="panel panel-default">
        <div id="heading_summary" role="tab" class="panel-heading">
            <h4 class="panel-title">
				<label>
					<a role="button" data-toggle="collapse" data-parent="#accordion_summary" href="#collapse_summary" aria-expanded="true" aria-controls="collapse_summary" class="collapsed">
						Mô tả ngắn về dự án
					</a>
					<em> - Sẽ hiển thị khi trỏ chuột vào hình ảnh dự án ở trang chủ hoặc trang danh sách dự án</em>
				</label>
				
				<br>
            </h4>
        </div>
        <div id="collapse_summary" role="tabpanel" aria-labelledby="heading_summary" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                @foreach (Config::get('languages') as $lang => $language)

						<div class="container-fluid">
							<label>Mô tả ngắn {{ $language }}</label>
							<textarea class="ckeditor summernote" name="summary[{{ $lang }}]">{!! $news[$lang]->summary or '' !!}</textarea>
						</div>

				@endforeach
            </div>
        </div>
    </div>
</div>


<div id="accordion01" role="tablist" aria-multiselectable="true" class="panel-group">
    <div class="panel panel-default">
        <div id="heading01" role="tab" class="panel-heading">
            <h4 class="panel-title">
				
				<a href="/img/template/31.png" data-gallery="" title="Block 01">
					<img class="img_des_block"  src="/img/template/31.png"/>
				</a>
				
				<label>
					<a role="button" data-toggle="collapse" data-parent="#accordion01" href="#collapse01" aria-expanded="true" aria-controls="collapse01" class="collapsed">
						block 01
					</a>
				</label>
				
				<br>
				<input id="block31" 
					   {{ isset($dataLandingpage['blocks33']) && in_array(1, $dataLandingpage['blocks33']) ? 'checked':'' }}
					   type="checkbox" 
					   name="blocks33[]" 
					   value="1">
				<label for="block31">Cho phép hiển thị block này</label>
				
            </h4>
        </div>
        <div id="collapse01" role="tabpanel" aria-labelledby="heading01" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                @include('Backend.Elements.Landingpage.template03.formTemplate31')
            </div>
        </div>
    </div>
</div>

<div id="accordion07" role="tablist" aria-multiselectable="true" class="panel-group">
    <div class="panel panel-default">
        <div id="heading07" role="tab" class="panel-heading">
            <h4 class="panel-title">
				<a href="/img/template/37.png" data-gallery="" title="Block 02">
					<img class="img_des_block"  src="/img/template/37.png"/>
				</a>
                <label>
					<a role="button" data-toggle="collapse" data-parent="#accordion07" href="#collapse07" aria-expanded="true" aria-controls="collapse07" class="collapsed">
						block 02
					</a>
				</label>
				<br>
				<input id="block37" 
					   type="checkbox" 
					   name="blocks33[]" 
					   {{ isset($dataLandingpage['blocks33']) && in_array(7, $dataLandingpage['blocks33']) ? 'checked':'' }}
					   value="7">
				<label for="block37">Cho phép hiển thị block này</label>
            </h4>
        </div>
        <div id="collapse07" role="tabpanel" aria-labelledby="heading02" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                @include('Backend.Elements.Landingpage.template03.formTemplate37')
            </div>
        </div>
    </div>
</div>

<div id="accordion03" role="tablist" aria-multiselectable="true" class="panel-group">
    <div class="panel panel-default">
        <div id="heading03" role="tab" class="panel-heading">
            <h4 class="panel-title">
                
				
				<a href="/img/template/33.png" data-gallery="" title="Block 03">
					<img class="img_des_block"  src="/img/template/33.png"/>
				</a>
				
				<label>
					<a role="button" data-toggle="collapse" data-parent="#accordion03" href="#collapse03" aria-expanded="true" aria-controls="collapse03" class="collapsed">
						block 03
					</a>
				</label>
				
				<br>
				<input id="block33" 
					type="checkbox"
					name="blocks33[]" 
					{{ isset($dataLandingpage['blocks33']) && in_array(3, $dataLandingpage['blocks33']) ? 'checked':'' }} 
					value="3">
				<label for="block33">Cho phép hiển thị block này</label>
            </h4>
        </div>
        <div id="collapse03" role="tabpanel" aria-labelledby="heading03" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                @include('Backend.Elements.Landingpage.template03.formTemplate33')
            </div>
        </div>
    </div>
</div>

<div id="accordion02" role="tablist" aria-multiselectable="true" class="panel-group">
    <div class="panel panel-default">
        <div id="heading02" role="tab" class="panel-heading">
            <h4 class="panel-title">
				<a href="/img/template/32.png" data-gallery="" title="Block 02">
					<img class="img_des_block"  src="/img/template/32.png"/>
				</a>
                <label>
					<a role="button" data-toggle="collapse" data-parent="#accordion02" href="#collapse02" aria-expanded="true" aria-controls="collapse02" class="collapsed">
						block 04
					</a>
				</label>
				<br>
				<input id="block32" 
					   type="checkbox" 
					   name="blocks33[]" 
					   {{ isset($dataLandingpage['blocks33']) && in_array(2, $dataLandingpage['blocks33']) ? 'checked':'' }}
					   value="2">
				<label for="block32">Cho phép hiển thị block này</label>
            </h4>
        </div>
        <div id="collapse02" role="tabpanel" aria-labelledby="heading02" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                @include('Backend.Elements.Landingpage.template03.formTemplate32')
            </div>
        </div>
    </div>
</div>

<div id="accordion04" role="tablist" aria-multiselectable="true" class="panel-group">
    <div class="panel panel-default">
        <div id="heading04" role="tab" class="panel-heading">
            <h4 class="panel-title">
                
				
				<a href="/img/template/34.png" data-gallery="" title="Block 04">
					<img class="img_des_block"  src="/img/template/34.png"/>
				</a>
				
				<label>
					<a role="button" data-toggle="collapse" data-parent="#accordion04" href="#collapse04" aria-expanded="true" aria-controls="collapse04" class="collapsed">
						block 5
					</a>
				</label>
				
				<br>
				<input id="block34" 
					   type="checkbox" 
					   name="blocks33[]" 
					   {{ isset($dataLandingpage['blocks33']) && in_array(4, $dataLandingpage['blocks33']) ? 'checked':'' }}
					   value="4">
				<label for="block34">Cho phép hiển thị block này</label>
				
            </h4>
        </div>
        <div id="collapse04" role="tabpanel" aria-labelledby="heading04" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                @include('Backend.Elements.Landingpage.template03.formTemplate34')
            </div>
        </div>
    </div>
</div>

<div id="accordion05" role="tablist" aria-multiselectable="true" class="panel-group">
    <div class="panel panel-default">
        <div id="heading05" role="tab" class="panel-heading">
            <h4 class="panel-title">
                
				<a href="/img/template/35.png" data-gallery="" title="Block 05">
					<img class="img_des_block"  src="/img/template/35.png"/>
				</a>
				
				<label>
					<a role="button" data-toggle="collapse" data-parent="#accordion05" href="#collapse05" aria-expanded="true" aria-controls="collapse05" class="collapsed">
						block 06
					</a>
				</label>
				
				<br>
				<input id="block35" 
					   type="checkbox" 
					   name="blocks33[]" 
					   {{ isset($dataLandingpage['blocks33']) && in_array(5, $dataLandingpage['blocks33']) ? 'checked':'' }}
					   value="5">
				<label for="block35">Cho phép hiển thị block này</label>
				
            </h4>
        </div>
        <div id="collapse05" role="tabpanel" aria-labelledby="heading05" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                @include('Backend.Elements.Landingpage.template03.formTemplate35')
            </div>
        </div>
    </div>
</div>

<div id="accordion06" role="tablist" aria-multiselectable="true" class="panel-group">
    <div class="panel panel-default">
        <div id="heading06" role="tab" class="panel-heading">
            <h4 class="panel-title">
                
				<a href="/img/template/36.png" data-gallery="" title="Block 06">
					<img class="img_des_block"  src="/img/template/36.png"/>
				</a>
				
				<label>
					<a role="button" data-toggle="collapse" data-parent="#accordion06" href="#collapse06" aria-expanded="true" aria-controls="collapse06" class="collapsed">
						block 07
					</a>
				</label>
				
				<br>
				<input id="block36" 
					   type="checkbox" 
					   name="blocks33[]"
					   {{ isset($dataLandingpage['blocks33']) && in_array(6, $dataLandingpage['blocks33']) ? 'checked':'' }}
					   value="6">
				<label for="block36">Cho phép hiển thị block này</label>
            </h4>
        </div>
        <div id="collapse06" role="tabpanel" aria-labelledby="heading06" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                @include('Backend.Elements.Landingpage.template03.formTemplate36')
            </div>
        </div>
    </div>
</div>

<div id="accordion" role="tablist" aria-multiselectable="true" class="panel-group">
    <div class="panel panel-default">
        <div id="headingTwo" role="tab" class="panel-heading">
            <h4 class="panel-title">
                <a role="button" 
                   data-toggle="collapse" 
                   data-parent="#accordion" 
                   href="#collapseSEO" 
                   aria-expandcollapseded="false" 
                   aria-controls="collapseSEO" 
                   class="">
                    Click để bổ xung thêm nội dung liên quan đến SEO
                </a>
            </h4>
        </div>
        <div id="collapseSEO" role="tabpanel" aria-labelledby="headingTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                @foreach (Config::get('languages') as $lang => $language)	
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mda-form-group mb {{ isset($news[$lang]->id) ? '' : 'float-label' }}">
                            <div class="mda-form-control">
                                <textarea name="keyword[{{ $lang }}]" 
                                          rows="4" 
                                          aria-multiline="true" 
                                          tabindex="0" 
                                          aria-invalid="false" 
                                          class="form-control">{{ $news[$lang]->keyword or '' }}</textarea>
                                <div class="mda-form-control-line"></div>
                                <label>[SEO] Keyword</label>
                            </div>
                            <span class="mda-form-msg">Keyword should be a maximum of 155 characters long.</span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mda-form-group mb {{ isset($news[$lang]->id) ? '' : 'float-label' }}">
                            <div class="mda-form-control">
                                <textarea name="description[{{ $lang }}]" 
                                          rows="4" 
                                          aria-multiline="true" 
                                          tabindex="0" 
                                          aria-invalid="false" 
                                          class="form-control">{{ $news[$lang]->description or '' }}</textarea>
                                <div class="mda-form-control-line"></div>
                                <label>[SEO] Descrption</label>
                            </div>
                            <span class="mda-form-msg">Descrption should be a maximum of 70-150 characters long.</span>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>