<input type="hidden" name="landingpage_id" value="0"/>


<div class="row">
	<div class="mda-form-group mb">
		<div class="mda-form-control">
			<textarea name="video"
					  tabindex="0" 
					  style="height: 100px"
					  aria-required="true" 
					  aria-invalid="true" 
					  class="form-control">{{ $news['vi']->video or '' }}</textarea>
			<div class="mda-form-control-line"></div>
			<label>Video - copy mã nhúng từ youtube</label>
			{!! $news['vi']->video or ''  !!}
		</div>
	</div>
</div>

<div id="panelDemo14" class="panel">
	<div class="panel-heading"><b>Vui lòng nhập nội cho cho tất cả các tab ngôn ngữ phía dưới</b></div>
	<div class="panel-body">
		<div>
			<!-- Nav tabs-->
			<ul role="tablist" class="nav nav-tabs">
				@foreach (Config::get('languages') as $lang => $language)
				<li role="presentation" class="{{ $lang == 'vi' ? 'active':'' }}">
					<a href="#tab_{{ $lang }}" aria-controls="home" role="tab" data-toggle="tab">Nội dung {{ $language }}</a>
				</li>
				@endforeach
			</ul>
			<!-- Tab panes-->
			<div class="tab-content">
				@foreach (Config::get('languages') as $lang => $language)
				<div id="tab_{{ $lang }}" role="tabpanel" 
					 class="tab-pane {{ $lang == 'vi' ? 'active':'' }}">
					<div class="row">
						<div class="col-sm-6">
							<div class="mda-form-group mb {{ isset($news[$lang]->nId) ? '' : 'float-label' }}">
								<div class="mda-form-control">
									<input name="title[{{ $lang }}]"
										   value="{{ $news[$lang]->title or '' }}"
										   required="" 
										   tabindex="0" 
										   aria-required="true" 
										   aria-invalid="true" 
										   class="form-control">
									<div class="mda-form-control-line"></div>
									<label>Title</label>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="mda-form-group mb {{ isset($news[$lang]->id) ? '' : 'float-label' }}">
							<div class="mda-form-control">
								<textarea name="summary[{{ $lang }}]"
										  tabindex="0" 
										  style="height: 100px"
										  aria-required="true" 
										  aria-invalid="true" 
										  class="form-control">{{ $news[$lang]->summary or '' }}</textarea>
								<div class="mda-form-control-line"></div>
								<label>Summary</label>
							</div>
						</div>
					</div>
					<div class="container-fluid">
						<label>Content</label>
						<textarea class="ckeditor summernote" name="content[{{ $lang }}]">{!! $news[$lang]->content or '' !!}</textarea>
					</div>
					
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>