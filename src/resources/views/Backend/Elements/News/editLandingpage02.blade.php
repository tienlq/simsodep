<?php $index = 1 ?>
<input type="hidden" name="landingpage_id" value="2"/>

<div id="blueimp-gallery" class="blueimp-gallery">
    <div class="slides">
        <h3 class="title"></h3><a class="prev">&lsaquo;</a><a class="next">&rsaquo;</a><a class="close">&times;</a><a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
</div>


<div class="row">&nbsp;</div>
<div class="row {{ $id == 1 ? '_hidden':'' }}">
	@foreach (Config::get('languages') as $lang => $language)

			<div class="col-sm-6">
				<div class="mda-form-group mb {{ isset($news[$lang]->nId) ? '' : 'float-label' }}">
					<div class="mda-form-control">
						<input name="title[{{ $lang }}]"
							   value="{{ $news[$lang]->title or '' }}"
							   required="" 
							   tabindex="0" 
							   aria-required="true" 
							   aria-invalid="true" 
							   class="form-control">
						<div class="mda-form-control-line"></div>
						<label>Tiêu đề {{ $language }}</label>
					</div>
				</div>
			</div>
	
	@endforeach
</div>

<div id="accordion_summary" role="tablist" aria-multiselectable="true" class="panel-group {{ $id == 1 ? '_hidden':'' }}">
    <div class="panel panel-default">
        <div id="heading_summary" role="tab" class="panel-heading">
            <h4 class="panel-title">
				<label>
					<a role="button" data-toggle="collapse" data-parent="#accordion_summary" href="#collapse_summary" aria-expanded="true" aria-controls="collapse_summary" class="collapsed">
                        Mô tả ngắn
					</a>
				</label>
				
				<br>
            </h4>
        </div>
        <div id="collapse_summary" role="tabpanel" aria-labelledby="heading_summary" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                @foreach (Config::get('languages') as $lang => $language)

						<div class="container-fluid">
							<label>Mô tả ngắn {{ $language }}</label>
							<textarea class="ckeditor summernote" name="summary[{{ $lang }}]">{!! $news[$lang]->summary or '' !!}</textarea>
						</div>

				@endforeach
            </div>
        </div>
    </div>
</div>

<div id="accordion" role="tablist" aria-multiselectable="true" class="panel-group {{ $id == 1 ? '_hidden':'' }}">
    <div class="panel panel-default">
        <div id="headingTwo" role="tab" class="panel-heading">
            <h4 class="panel-title">
                <a role="button" 
                   data-toggle="collapse" 
                   data-parent="#accordion" 
                   href="#collapseSEO" 
                   aria-expandcollapseded="false" 
                   aria-controls="collapseSEO" 
                   class="">
                    <b>Click để bổ xung thêm nội dung liên quan đến SEO</b>
                </a>
            </h4>
        </div>
        <div id="collapseSEO" role="tabpanel" aria-labelledby="headingTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                @foreach (Config::get('languages') as $lang => $language)	
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mda-form-group mb {{ isset($news[$lang]->id) ? '' : 'float-label' }}">
                            <div class="mda-form-control">
                                <textarea name="keyword[{{ $lang }}]" 
                                          rows="4" 
                                          aria-multiline="true" 
                                          tabindex="0" 
                                          aria-invalid="false" 
                                          class="form-control">{{ $news[$lang]->keyword or '' }}</textarea>
                                <div class="mda-form-control-line"></div>
                                <label>[SEO] Keyword</label>
                            </div>
                            <span class="mda-form-msg">Độ dài khoảng 155-200 ký tự</span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mda-form-group mb {{ isset($news[$lang]->id) ? '' : 'float-label' }}">
                            <div class="mda-form-control">
                                <textarea name="description[{{ $lang }}]" 
                                          rows="4" 
                                          aria-multiline="true" 
                                          tabindex="0" 
                                          aria-invalid="false" 
                                          class="form-control">{{ $news[$lang]->description or '' }}</textarea>
                                <div class="mda-form-control-line"></div>
                                <label>[SEO] Descrption</label>
                            </div>
                            <span class="mda-form-msg">Độ dài khoảng 70-150 ký tự.</span>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<h6>
    <b>Danh sách block của landingpage</b>
    <br>
    <em><b>Lưu ý:</b> ấn nút cập nhật lại thứ tự block trước khi lưu</em>
</h6>
<hr/>

<div class="main_block">
    <div class="container-fluid">
        <div class="js-nestable-action">
            <a class="btn btn-primary btn-sm" onclick="updateSoftOrder('{{ \TblName::BLOCK_LANDINGPAGE }}')">Cập nhật lại thứ tự block</a>
            <a class="btn btn-primary btn-sm" 
                onclick="loadPopupLarge('{{ route('listBlockLandingpages',[2, $id]) }}')" 
                data-toggle="modal" 
                data-target=".bs-modal-lg">Thêm block</a>
            <div class="loader-primary _success status-update"></div>
            <div class="row">
				<div id="nestable" class="dd">
					{!! $htmlList !!}
				</div>
				<div id="nestable-output" class="well hidden"></div>
            </div>
        </div>
    </div>
</div>
