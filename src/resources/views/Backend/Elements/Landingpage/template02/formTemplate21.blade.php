<script src="/backend/js/summernote.js"></script>

<div class="item-temp">
    <div class="row">
        <div id="panelDemo14" class="panel">
            <div class="panel-heading"><b>Vui lòng nhập nội cho cho tất cả các tab ngôn ngữ phía dưới</b></div>
            <div class="panel-body">
                <div>
                    <!-- Nav tabs-->
                    <ul role="tablist" class="nav nav-tabs">
                        @foreach (Config::get('languages') as $lang => $language)
						<li role="presentation" class="{{ $lang == 'vi' ? 'active':'' }}">
							<a href="#tab_{{ $lang }}" aria-controls="home" role="tab" data-toggle="tab">Nội dung {{ $language }}</a>
						</li>
                        @endforeach
                    </ul>
                    <!-- Tab panes-->
                    <div class="tab-content">
                        @foreach (Config::get('languages') as $lang => $language)
                        <div id="tab_{{ $lang }}" role="tabpanel" 
                             class="tab-pane {{ $lang == 'vi' ? 'active':'' }}">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mda-form-group mb">
                                        <div class="mda-form-control">
                                            <textarea name="title[{{$lang}}]" 
                                                      rows="4" 
                                                      aria-multiline="true" 
                                                      tabindex="0" 
                                                      aria-invalid="false" 
                                                      class="form-control">{{ $dataLandingpage['title'][$lang] or '' }}</textarea>
                                            <div class="mda-form-control-line"></div>
                                            <label>Tiêu đề {!! $language !!}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mda-form-group mb">
                                        <div class="mda-form-control">
                                            <textarea name="btn_regis[{{$lang}}]" 
                                                      rows="4" 
                                                      aria-multiline="true" 
                                                      tabindex="0" 
                                                      aria-invalid="false" 
                                                      class="form-control">{{ $dataLandingpage['btn_regis'][$lang] or '' }}</textarea>
                                            <div class="mda-form-control-line"></div>
                                            <label>Tiêu đề nút <b>Đăng ký</b>, bỏ trống nếu bạn muốn ẩn nó</label>
                                        </div>
                                    </div>
                                </div>
							</div>
                            <div class="row">
								<div class="col-md-6">
									<label>Hình ảnh (tỷ lệ chuẩn 2100x700 (px))</label>
									<div class="input-group">
										<span class="input-group-btn">
											<a data-input="thumbnail_21" data-preview="holder_21" class="btn btn-primary lfm">
												<i class="fa fa-picture-o"></i> Chọn ảnh
											</a>
										</span>
										<input id="thumbnail_21" 
											   class="form-control" type="text" 
											   name="img[{{ $lang }}]" 
											   value="{{ $dataLandingpage['img'][$lang] or '' }}"/>
									</div>
									@if(!empty($dataLandingpage['img'][$lang]))   
									<img id="holder_21" src="{{ $dataLandingpage['img'][$lang] or '' }}" class="img-landingpage-01"/>
									@else
									<img id="holder_21" class="img-landingpage-01"/>
									@endif
								</div>
							</div>
							<div class="row">
								<label>Nội dung {{ $language }}</label>
								<textarea class="ckeditor summernote" name="content[{{ $lang }}]">{{ $dataLandingpage['content'][$lang] or '' }}</textarea>
							</div>

						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<script>
var route_prefix = "{{ url(config('lfm.prefix')) }}";
{!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
$('.lfm').filemanager('image', {prefix: route_prefix});
</script>

