<script src="/backend/js/summernote.js"></script>


<input type="hidden" name="block" value="21"/>
<div class="item-temp">
    <div class="row">

        <div id="panelDemo14" class="panel">

            <div class="panel-heading"><b>Vui lòng nhập nội cho cho tất cả các tab ngôn ngữ phía dưới</b></div>
            <div class="panel-body">
                <div>
                    <!-- Nav tabs-->
                    <ul role="tablist" class="nav nav-tabs">
                        @foreach (Config::get('languages') as $lang => $language)
						<input type="hidden" name="title[{{$lang}}]" value="Slide" \>
                        <li role="presentation" class="{{ $lang == 'vi' ? 'active':'' }}">
                            <a href="#tab_{{ $lang }}" aria-controls="home" role="tab" data-toggle="tab">Nội dung {{ $language }}</a>
                        </li>
                        @endforeach
                    </ul>
                    <!-- Tab panes-->
                    <div class="tab-content">
                        @foreach (Config::get('languages') as $lang => $language)
                        <div id="tab_{{ $lang }}" role="tabpanel" 
                             class="tab-pane {{ $lang == 'vi' ? 'active':'' }}">

							
                            @for($i=0; $i<=20; $i++)
                            @php
                            if (!empty($dataLandingpage['img'][$lang][$i])) {
                                $hiddenClass01 = 'img_active';
                            } else {
                                if ($i > 1) {
                                    $hiddenClass01 = '_hidden img_hidden';
                                } else {
                                    $hiddenClass01 = 'img_active';
                                }
                            }
                            @endphp
							<div class="row">
								<div class="{{ $hiddenClass01 }}">

										<div class="col-md-6">
											<div class="mda-form-group mb">
												<div class="mda-form-control">
													<textarea name="title[{{$lang}}][{{ $i }}]" 
															  rows="4" 
															  aria-multiline="true" 
															  tabindex="0" 
															  aria-invalid="false" 
															  class="form-control">{{ $dataLandingpage['title'][$lang][$i] or '' }}</textarea>
													<div class="mda-form-control-line"></div>
													<label>Tiêu đề hình ảnh {!! $i+1 !!}</label>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<label>Hình ảnh {{ $i+1 }} (tỷ lệ chuẩn 1000x500 (px))</label>
											<div class="input-group">
												<span class="input-group-btn">
													<a data-input="thumbnail_21_{{ $i }}" data-preview="holder_21_{{ $i }}" class="btn btn-primary lfm">
														<i class="fa fa-picture-o"></i> Chọn ảnh
													</a>
												</span>
												<input id="thumbnail_21_{{ $i }}" 
													   class="form-control" type="text" 
													   name="img[{{ $lang }}][{{ $i }}]" 
													   value="{{ $dataLandingpage['img'][$lang][$i] or '' }}"/>
											</div>
											@if(!empty($dataLandingpage['img'][$lang][$i]))   
											<img id="holder_21_{{ $i }}" src="{{ $dataLandingpage['img'][$lang][$i] or '' }}" class="img-landingpage-01"/>
											@else
											<img id="holder_21_{{ $i }}" class="img-landingpage-01"/>
											@endif
										</div>
									</div>
								</div>
                           
                            @endfor
                        </div>
                        @endforeach
						<div class="row">
							<p><a onclick="addItemTemplate('.img_hidden', 'img_active', 'img_hidden')">Thêm hình ảnh</a></p>
						</div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    var route_prefix = "{{ url(config('lfm.prefix')) }}";
    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
    $('.lfm').filemanager('image', {prefix: route_prefix});
</script>



