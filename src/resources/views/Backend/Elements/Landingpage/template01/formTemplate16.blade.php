<script src="/backend/js/summernote.js"></script>


<input type="hidden" name="block" value="23"/>
<div class="item-temp">
    <div class="row">

        <div id="panelDemo14" class="panel">
			
            <div class="panel-heading"><b>Vui lòng nhập nội cho cho tất cả các tab ngôn ngữ phía dưới</b></div>
            <div class="panel-body">
                <div>
                    <!-- Nav tabs-->
                    <ul role="tablist" class="nav nav-tabs">
                        @foreach (Config::get('languages') as $lang => $language)
                            <li role="presentation" class="{{ $lang == 'vi' ? 'active':'' }}">
                                <a href="#tab_{{ $lang }}" aria-controls="home" role="tab" data-toggle="tab">Nội dung {{ $language }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <!-- Tab panes-->
                    <div class="tab-content">
                        @foreach (Config::get('languages') as $lang => $language)
                        <div id="tab_{{ $lang }}" role="tabpanel" 
                             class="tab-pane {{ $lang == 'vi' ? 'active':'' }}">
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mda-form-group mb">
                                        <div class="mda-form-control">
                                            <input name="title[{{$lang}}]" 
                                                      aria-multiline="true" 
                                                      tabindex="0" 
                                                      aria-invalid="false" 
                                                      value="{{ $dataLandingpage['title'][$lang] or '' }}"
                                                      class="form-control"/>
                                            <div class="mda-form-control-line"></div>
                                            <label>Tiêu đề block</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Ảnh nền (tỷ lệ chuẩn 1200x600 (px))</label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <a data-input="thumbnail_15" data-preview="holder_15" class="btn btn-primary lfm">
                                                <i class="fa fa-picture-o"></i> Chọn ảnh
                                            </a>
                                        </span>
                                        <input id="thumbnail_15" 
                                               class="form-control" type="text" 
                                               name="img_bg[{{ $lang }}]" 
                                               value="{{ $dataLandingpage['img_bg'][$lang] or '' }}"/>
                                    </div>
                                    @if(!empty($dataLandingpage['img'][$lang]))   
                                    <img id="holder_15" src="{{ $dataLandingpage['img_bg'][$lang] or '' }}" class="img-landingpage-01"/>
                                    @else
                                    <img id="holder_15" class="img-landingpage-01"/>
                                    @endif
                                </div>
                            </div>
                                

                            <div class="row">
                                <strong>
                                    Nhập ID của video
                                </strong>
                                <br>
                                Hướng dẫn: click chuột phải vào video từ youtube và chọn sao chép url video
                                <br>
                                Ví dụ ta được URL là: https://youtu.be/<b style="color:red">TqGYQce_edQ</b>
                                <br>
                                => bạn chỉ cần copy đoạn mã cuối cùng như đã tô đỏ ở trên là <b style="color:red">TqGYQce_edQ</b> và paste vào ô nhập ID ở dưới.
                            </div>
                            @for($i=0; $i<10; $i++)
                                <?php
                                if(!empty($dataLandingpage['youtube_id'][$lang][$i])) {
                                    $hiddenClass02 = 'tab_active';
                                } else {
                                    if($i > 0) {
                                        $hiddenClass02 = '_hidden tab_hidden';
                                    } else {
                                        $hiddenClass02 = 'tab_active';
                                    }
                                }  
                                ?>
                            <div class="{{ $hiddenClass02 }}">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Hình ảnh đại diện{{ $i+1 }} (tỷ lệ chuẩn 1000x500 (px))</label>
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <a data-input="thumbnail_{{ $i }}" data-preview="holder_{{ $i }}" class="btn btn-primary lfm">
                                                    <i class="fa fa-picture-o"></i> Chọn ảnh
                                                </a>
                                            </span>
                                            <input id="thumbnail_{{ $i }}" 
                                                   class="form-control" type="text" 
                                                   name="img[{{ $lang }}][{{ $i }}]" 
                                                   value="{{ $dataLandingpage['img'][$lang][$i] or '' }}"/>
                                        </div>
                                        @if(!empty($dataLandingpage['img'][$lang][$i]))   
                                        <img id="holder_{{ $i }}" src="{{ $dataLandingpage['img'][$lang][$i] or '' }}" class="img-landingpage-01"/>
                                        @else
                                        <img id="holder_{{ $i }}" class="img-landingpage-01"/>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mda-form-group mb">
                                            <div class="mda-form-control">
                                                <input name="youtube_id[{{$lang}}][{{ $i }}]" 
                                                       rows="4"
                                                       aria-multiline="true" 
                                                       tabindex="0" 
                                                       aria-invalid="false" 
                                                       value="{{ $dataLandingpage['youtube_id'][$lang][$i] or '' }}"
                                                       class="form-control">
                                                <div class="mda-form-control-line"></div>
                                                <label>ID Video {{ $i+1 }}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endfor
                            <div class="row">
                                <p><a onclick="addItemTemplate('.tab_hidden', 'tab_active', 'tab_hidden')">Thêm video</a></p>
                            </div>
                            
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
	var route_prefix = "{{ url(config('lfm.prefix')) }}";
	{!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
	$('.lfm').filemanager('image', {prefix: route_prefix});
</script>



