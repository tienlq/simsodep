<script src="/backend/js/summernote.js"></script>


<input type="hidden" name="block" value="14"/>
<div class="item-temp">
    <div class="row">
        <div id="panelDemo14" class="panel">
            <div class="panel-heading"><b>Vui lòng nhập nội cho cho tất cả các tab ngôn ngữ phía dưới</b></div>
            <div class="panel-body">
                <div>
                    <!-- Nav tabs-->
                    <ul role="tablist" class="nav nav-tabs">
                        @foreach (Config::get('languages') as $lang => $language)
                        <li role="presentation" class="{{ $lang == 'vi' ? 'active':'' }}">
                            <a href="#tab_{{ $lang }}" aria-controls="home" role="tab" data-toggle="tab">Nội dung {{ $language }}</a>
                        </li>
                        @endforeach
                    </ul>
                    <!-- Tab panes-->
                    <div class="tab-content">
                        @foreach (Config::get('languages') as $lang => $language)
                        <div id="tab_{{ $lang }}" role="tabpanel" 
                             class="tab-pane {{ $lang == 'vi' ? 'active':'' }}">
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mda-form-group mb">
                                        <div class="mda-form-control">
                                            <textarea name="sub_title[{{$lang}}]" 
                                                      rows="4"
                                                      aria-multiline="true" 
                                                      tabindex="0" 
                                                      aria-invalid="false" 
                                                      class="form-control">{{ $dataLandingpage['sub_title'][$lang] or '' }}</textarea>
                                            <div class="mda-form-control-line"></div>
                                            <label>Tiêu đề nhỏ</label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="mda-form-group mb">
                                        <div class="mda-form-control">
                                            <textarea name="title[{{$lang}}]" 
                                                      rows="4"
                                                      aria-multiline="true" 
                                                      tabindex="0" 
                                                      aria-invalid="false" 
                                                      class="form-control">{{ $dataLandingpage['title'][$lang] or '' }}</textarea>
                                            <div class="mda-form-control-line"></div>
                                            <label>Tiêu đề </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mda-form-group mb">
                                        <div class="mda-form-group mb">
                                            <div class="mda-form-control">
                                                <textarea name="description[{{$lang}}]" 
                                                          rows="4"
                                                          aria-multiline="true" 
                                                          tabindex="0" 
                                                          aria-invalid="false" 
                                                          class="form-control">{{ $dataLandingpage['description'][$lang] or '' }}</textarea>
                                                <div class="mda-form-control-line"></div>
                                                <label>Mô tả </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Hình ảnh (tỷ lệ chuẩn 700x500 (px))</label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <a data-input="thumbnail_15" data-preview="holder_15" class="btn btn-primary lfm">
                                                <i class="fa fa-picture-o"></i> Chọn ảnh
                                            </a>
                                        </span>
                                        <input id="thumbnail_15" 
                                               class="form-control" type="text" 
                                               name="img[{{ $lang }}]" 
                                               value="{{ $dataLandingpage['img'][$lang] or '' }}"/>
                                    </div>
                                    @if(!empty($dataLandingpage['img'][$lang]))   
                                    <img id="holder_15" src="{{ $dataLandingpage['img'][$lang] or '' }}" class="img-landingpage-01"/>
                                    @else
                                    <img id="holder_15" class="img-landingpage-01"/>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="panel-heading"><b>Các đoạn mô tả ngắn, bạn có thể thêm nhiều item</b></div>
                        @for($i=0; $i<=50; $i++)
                            @php
                            if (!empty($dataLandingpage['item'][$lang][$i])) {
                                $hiddenClass01 = 'img_active';
                            } else {
                                if ($i > 1) {
                                    $hiddenClass01 = '_hidden img_hidden';
                                } else {
                                    $hiddenClass01 = 'img_active';
                                }
                            }
                            @endphp
                            <div class="row">
                                <div class="{{ $hiddenClass01 }}">
                                    <div class="mda-form-group mb">
                                        <div class="mda-form-control">
                                            <textarea name="item[{{$lang}}][{{ $i }}]" 
                                                      rows="4" 
                                                      aria-multiline="true" 
                                                      tabindex="0" 
                                                      aria-invalid="false" 
                                                      class="form-control">{{ $dataLandingpage['item'][$lang][$i] or '' }}</textarea>
                                            <div class="mda-form-control-line"></div>
                                            <label>item {!! $i+1 !!}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endfor
                            
                            <div class="row">
                                <p><a onclick="addItemTemplate('.img_hidden', 'img_active', 'img_hidden')">Thêm item</a></p>
                            </div>
                        
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
        var route_prefix = "{{ url(config('lfm.prefix')) }}";
{!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
$('.lfm').filemanager('image', {prefix: route_prefix});</script>



