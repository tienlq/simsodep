<script src="/backend/js/summernote.js"></script>


<input type="hidden" name="block" value="23"/>
<div class="item-temp">
    <div class="row">

        <div id="panelDemo14" class="panel">
			
            <div class="panel-heading"><b>Vui lòng nhập nội cho cho tất cả các tab ngôn ngữ phía dưới</b></div>
            <div class="panel-body">
                <div>
                    <!-- Nav tabs-->
                    <ul role="tablist" class="nav nav-tabs">
                        @foreach (Config::get('languages') as $lang => $language)
                            <li role="presentation" class="{{ $lang == 'vi' ? 'active':'' }}">
                                <a href="#tab_{{ $lang }}" aria-controls="home" role="tab" data-toggle="tab">Nội dung {{ $language }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <!-- Tab panes-->
                    <div class="tab-content">
                        @foreach (Config::get('languages') as $lang => $language)
                        <div id="tab_{{ $lang }}" role="tabpanel" 
                             class="tab-pane {{ $lang == 'vi' ? 'active':'' }}">
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mda-form-group mb">
                                        <div class="mda-form-control">
                                            <textarea name="title[{{$lang}}]" 
                                                      rows="4"
                                                      aria-multiline="true" 
                                                      tabindex="0" 
                                                      aria-invalid="false" 
                                                      class="form-control">{{ $dataLandingpage['title'][$lang] or '' }}</textarea>
                                            <div class="mda-form-control-line"></div>
                                            <label>Tiêu đề</label>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                            </div>
                                

                            <div class="row">
                                <strong>Tab</strong>
                                <p><a onclick="addItemTemplate('.tab_hidden', 'tab_active', 'tab_hidden')">Thêm tab</a></p>
                            </div>
                            @for($i=0; $i<10; $i++)
                                <?php
                                if(!empty($dataLandingpage['title_tab_111'][$lang][$i])) {
                                    $hiddenClass02 = 'tab_active';
                                } else {
                                    if($i > 0) {
                                        $hiddenClass02 = '_hidden tab_hidden';
                                    } else {
                                        $hiddenClass02 = 'tab_active';
                                    }
                                }  
                                ?>
                                <div class="{{ $hiddenClass02 }}">
                                    <div class="row">
                                        <div class="mda-form-group mb">
                                            <div class="mda-form-control">
                                                <textarea name="title_tab_111[{{$lang}}][{{ $i }}]" 
                                                          rows="4"
                                                          aria-multiline="true" 
                                                          tabindex="0" 
                                                          aria-invalid="false" 
                                                          class="form-control">{{ $dataLandingpage['title_tab_111'][$lang][$i] or '' }}</textarea>
                                                <div class="mda-form-control-line"></div>
                                                <label>Tiêu đề tab {{ $i+1 }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="mda-form-group mb">
                                            <div class="mda-form-control">
                                                <textarea name="description_tab_111[{{ $lang }}][{{ $i }}]" 
                                                          rows="4" 
                                                          aria-multiline="true" 
                                                          tabindex="0" 
                                                          aria-invalid="false" 
                                                          class="form-control summernote">{{ $dataLandingpage['description_tab_111'][$lang][$i] or '' }}</textarea>
                                                <div class="mda-form-control-line"></div>
                                                <label>Mô tả về tab {{ $i+1 }}</label>
                                            </div>
                                        </div>
                                        <hr/>
                                    </div>
                                </div>
                            @endfor
                            <div class="row">
                                <p><a onclick="addItemTemplate('.tab_hidden', 'tab_active', 'tab_hidden')">Thêm tab</a></p>
                            </div>
                            
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
	var route_prefix = "{{ url(config('lfm.prefix')) }}";
	{!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
	$('.lfm').filemanager('image', {prefix: route_prefix});
</script>



