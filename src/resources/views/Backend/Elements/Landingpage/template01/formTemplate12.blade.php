<script src="/backend/js/summernote.js"></script>

<div class="item-temp">
    <div class="row">

        <div id="panelDemo14" class="panel">
            <div class="panel-heading"><b>Vui lòng nhập nội cho cho tất cả các tab ngôn ngữ phía dưới</b></div>
            <div class="panel-body">
                <div>
                    <!-- Nav tabs-->
                    <ul role="tablist" class="nav nav-tabs">
                        @foreach (Config::get('languages') as $lang => $language)
                        <li role="presentation" class="{{ $lang == 'vi' ? 'active':'' }}">
                            <a href="#tab_{{ $lang }}" aria-controls="home" role="tab" data-toggle="tab">Nội dung {{ $language }}</a>
                        </li>
                        @endforeach
                    </ul>
                    <!-- Tab panes-->
                    <div class="tab-content">
                        @foreach (Config::get('languages') as $lang => $language)
                        <div id="tab_{{ $lang }}" role="tabpanel" 
                             class="tab-pane {{ $lang == 'vi' ? 'active':'' }}">

                            @for($i=0; $i<=2; $i++)

                            <div class="row">
                                <div class="col-md-6">
                                    <label>Hình ảnh {{$i+1}} (tỷ lệ chuẩn 500x700 (px))</label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <a data-input="thumbnail_15_{{$i}}" data-preview="holder_15_{{$i}}" class="btn btn-primary lfm">
                                                <i class="fa fa-picture-o"></i> Chọn ảnh
                                            </a>
                                        </span>
                                        <input id="thumbnail_15_{{$i}}" 
                                               class="form-control" type="text" 
                                               name="img[{{ $lang }}][{{$i}}]" 
                                               value="{{ $dataLandingpage['img'][$lang][$i] or '' }}"/>
                                    </div>
                                    @if(!empty($dataLandingpage['img'][$lang]))   
                                    <img id="holder_15_{{$i}}" src="{{ $dataLandingpage['img'][$lang][$i] or '' }}" class="img-landingpage-01"/>
                                    @else
                                    <img id="holder_15_{{$i}}" class="img-landingpage-01"/>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <div class="mda-form-group mb">
                                        <div class="mda-form-control">
                                            <textarea name="link[{{$lang}}][{{$i}}]" 
                                                      rows="4" 
                                                      aria-multiline="true" 
                                                      tabindex="0" 
                                                      aria-invalid="false" 
                                                      class="form-control">{{ $dataLandingpage['link'][$lang][$i] or '' }}</textarea>
                                            <div class="mda-form-control-line"></div>
                                            <label>Đường dẫn khi click {{$i+1}}</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mda-form-group mb">
                                        <div class="mda-form-control">
                                            <textarea name="title[{{$lang}}][{{$i}}]" 
                                                      rows="4" 
                                                      aria-multiline="true" 
                                                      tabindex="0" 
                                                      aria-invalid="false" 
                                                      class="form-control">{{ $dataLandingpage['title'][$lang][$i] or '' }}</textarea>
                                            <div class="mda-form-control-line"></div>
                                            <label>Tiêu đề hinh ảnh {{$i+1}}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="mda-form-group mb">
                                        <div class="mda-form-control">
                                            <textarea name="description[{{$lang}}][{{$i}}]" 
                                                      rows="4" 
                                                      aria-multiline="true" 
                                                      tabindex="0" 
                                                      aria-invalid="false" 
                                                      class="form-control">{{ $dataLandingpage['description'][$lang][$i] or '' }}</textarea>
                                            <div class="mda-form-control-line"></div>
                                            <label>mô tả cho hình ảnh {{$i+1}}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @endfor

                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var route_prefix = "{{ url(config('lfm.prefix')) }}";
{!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
$('.lfm').filemanager('image', {prefix: route_prefix});
</script>

