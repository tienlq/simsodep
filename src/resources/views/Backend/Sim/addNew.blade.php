
<div class="panel panel-primary">
    <div class="panel panel-heading">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
        <h4 id="myLargeModalLabel" class=" panel-title">Thêm mới số điện thoại</h4>
    </div>
    <div class="modal-body ">
        <div class="card">
            <form id="form-register" 
                  action="{{ route('adminEditSim', [$id]) }}"
                  name="registerForm" 
                  novalidate="" 
                  class="form-validate category-form" 
                  method="post" 
                  enctype="multipart/form-data">
                {{ csrf_field()}}
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-6">
                        <select id="select2-2" class="form-control">
                            <option value="">Click để chọn thợ</option>
                            @foreach($listUser as $u)
                             <option value="{{ $u->id }}">{{ $u->fullName }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                  </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mda-form-group mb">
                            <div class="">
                                <label>Số điện thoại & Giá bán</label>
                                <textarea name="phones_prices" 
                                          rows="8" 
                                          aria-multiline="true" 
                                          tabindex="0" 
                                          aria-invalid="false" 
                                          class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                <div class="clearfix">
                    <div class="pull-left">
                        <button type="submit" lass="btn btn-primary">Thêm mới</button>
                        <!--<button type="submit" class="">Submit</button>-->
                        <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" >Đóng</button>
                    </div> 

                </div>
                <div class="pull-left edit-category-result _success"></div>
            </div>
        </form>
    </div>
</div>
</div>
<script type="text/javascript">$('#select2-2').select2();</script>>