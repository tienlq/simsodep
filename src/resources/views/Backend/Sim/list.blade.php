@extends('Backend.Layouts.main')

@section('title')
NEWS MANAGEMENT
@stop

@section('breadcrumb')
NEWS MANAGEMENT
@stop

@section('avatar')
@if($user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
</a>
<div class="mt">Welcome, {{ $user->username }}</div>
@endif
@stop

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-heading">

        </div>
        <div class="table-responsive">
            <form id="form-register" 
                  name="registerForm" 
                  novalidate="" 
                  action=""
                  class="form-validate form-edit" 
                  method="get" 
                  enctype="multipart/form-data">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th colspan="2">
                                <select name="categoryID" class="form-control" required="">
                                    <option value="0">--- Chọn theo loại sim ---</option>
                                    
                                </select>
                            </th>
                            <th>
                                <select name="type" class="form-control" required="">
                                    <option value="0">--- Chọn theo khoảng giá ---</option>
                                    
                                </select>
                            </th>
                            <th>
                                <input type="submit" value="Lọc dữ liệu" class="btn btn-primary"/>
                            </th>
                            <th>
                                <a  onclick="loadPopupLarge('{{ route('adminEditSim', [0]) }}')"
                                    data-toggle="modal" 
                                    data-target=".bs-modal-lg"
                                    class="btn btn-primary">Thêm mới</a>
                            </th>
                        </tr>
                        <tr class="tr_title">
                            <th>#</th>
                            <th>Image</th>
                            <th>Phone</th>
                            <th>Giá bán</th>
                            <th>Nhà mạng</th>
                            <th></th>
                        </tr>
                    </thead>.
                    <tbody>
                        @if(!empty($sims) > 0)
                            @foreach($sims as $idx => $n)
                                <tr>
                                    <td>{{ $idx+1 }}</td>
                                    <td>
                                        <img class="image-sim" src="{{ $n->image }}">
                                    </td>
                                    <td>
                                        {{ $n->phone }}
                                    </td>
                                    <td>{{ $n->price }}</td>
                                    <td>{{ $n->sName }}</td>
                                    <td>
<!--                                        <a onclick="loadPopupLarge('http://localhost:373/admin/category/edit/55')"
                                            data-toggle="modal" 
                                            data-target=".bs-modal-lg">
                                            <i data-pack="default" class="ion-edit"></i>
                                        </a>
                                        &nbsp;
                                        <a onclick="loadPopupLarge('http://localhost:373/admin/category/edit/55')"
                                            data-toggle="modal" 
                                            data-target=".bs-modal-lg">
                                            <i data-pack="default" class="ion-trash-a"></i>
                                        </a>-->
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        <tr>
                            <td colspan="5">{!! !empty($sims) ? $sims->render():'' !!}</td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
<!-- END row-->



@stop