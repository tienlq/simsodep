@extends('Backend.Layouts.main')

@section('title')
EDIT NEWS
@stop

@section('breadcrumb')
EDIT NEWS
@stop

@section('avatar')
@if($user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
</a>
<div class="mt">Welcome, {{ $user->username }}</div>
@endif
@stop

@section('scriptAddon')

<script>

$(document).ready(function(){
    $('.fontawesome-icon-list div a').click(function () {
        console.log($(this).text());
        $('.icon').val('<i class="fa fa-' + $(this).text() + '" aria-hidden="true"></i>');
        $('.main_icon').addClass('_hidden');
        $('#icon-demo').html('<i class="fa fa-' + $(this).text() + '" aria-hidden="true"></i>');
    });
})

</script>

@stop

@section('content')
<div class="container container-lg">
    <div class="panel panel-primary">
        <div class="modal-body">
            <div class="card">
                <form id="form-register" 
                      action="{{ url(route('editBlockConfig',[ $configBlock->id])) }}"
                      name="registerForm" 
                      novalidate="" 
                      class="form-validate update" 
                      method="post" 
                      enctype="multipart/form-data">
                    {{ csrf_field()}}

                    <input type="hidden" name="type" value="{{ $configBlock->type }}" />
                    <input type="hidden" name="block_config_id" value="{{ $configBlock->id }}" />
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Tiêu đề</label>
                                <input class="form-control"
                                       name="name"
                                       type="text" 
                                       placeholder="Nhập tên block"
                                       autocomplete="off"
                                       value="{{ $configBlock->name or '' }}" \>
                            </div>
                        </div>

                        <div class="row">
                            <hr>
                            
                            <strong class="result icon-des">
                                <span id="icon-demo">{!! $configBlock->icon or '' !!}</span>
                                <a onclick="displayElement()">Click để chọn icon</a>
                            </strong>
                            <input class="icon" 
                                   type="hidden" 
                                   value="" 
                                   name="icon"/>
                            <div class="main_icon _hidden">
                                @include('Backend/Pages/fontAwesome')
                            </div>
                        </div>

                        <div class="clearfix">
                            <br/>
                            <div class="pull-left">
                                <input type="submit" value="Cập nhật" />
                                <div class="loading"></div>
                            </div> 
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@stop