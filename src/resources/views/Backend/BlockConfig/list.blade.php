@extends('Backend.Layouts.main')

@section('title')
NEWS MANAGEMENT
@stop

@section('breadcrumb')
NEWS MANAGEMENT
@stop

@section('avatar')
@if($user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
</a>
<div class="mt">Welcome, {{ $user->username }}</div>
@endif
@stop

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-heading">

        </div>
        <div class="table-responsive">
            <form id="form-register" 
                  name="registerForm" 
                  novalidate="" 
                  action=""
                  class="form-validate form-edit" 
                  method="get" 
                  enctype="multipart/form-data">
                <table class="table table-striped">
                    <thead>
                        <tr class="tr_title">
                            <th style="width: 20px">#</th>
                            <th>Tiêu đề</th>
                            <th>Sửa</th>
                        </tr>
                    </thead>.
                    <tbody>
                        <?php $idx = 0; ?>
                        @if(!empty($blockConfig))
                            @foreach($blockConfig as $idx => $n)
                                <tr>
                                    <td>{{ $idx+1 }}</td>
                                    <td style="background: #fe5f09; color: #fff;border-bottom: 2px #fff solid">{!! $n->icon !!} {{ $n->name }}</td>
                                    <td>
                                        <a href="{{ route('editBlockConfig',[$n->id]) }}"><i data-pack="default" class="ion-edit"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
<!-- END row-->



@stop