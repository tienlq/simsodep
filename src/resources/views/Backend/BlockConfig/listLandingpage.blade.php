@extends('Backend.Layouts.main')

@section('title')
NEWS MANAGEMENT
@stop

@section('breadcrumb')
NEWS MANAGEMENT
@stop

@section('avatar')
@if($user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
</a>
<div class="mt">Welcome, {{ $user->username }}</div>
@endif
@stop

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-heading">

        </div>
        <div class="table-responsive">
            <form id="form-register" 
                  name="registerForm" 
                  novalidate="" 
                  action=""
                  class="form-validate form-edit" 
                  method="get" 
                  enctype="multipart/form-data">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th colspan="3">
                                 <a href="/admin/news/edit/0" class="btn btn-primary">Thêm mới</a>
                            </th>
                        </tr>
                        <tr class="tr_title">
                            <th>#</th>
                            <th>Tên danh mục</th>
                            <th>Tùy chọn</th>
                        </tr>
                    </thead>.
                    <tbody>
                        @foreach($listCategorys as $key => $n)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $n->name }}</td>
                                <td>
                                    <a href="{{ url(route('editLandingpage', [$n->tid])) }}">
                                        <i data-pack="default" class="ion-edit "></i>
                                    </a>
                                    <a target="new"
                                        href="{{ url(route('landingPage01', [app('ClassCommon')->formatText($n->name), $n->tid])) }}">
                                        <i data-pack="default" class="ion-ios-information-empty"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="6"></td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
<!-- END row-->



@stop