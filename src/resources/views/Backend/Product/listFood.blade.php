@extends('Backend.Layouts.main')

@section('title')
FOOD MANAGEMENT
@stop

@section('breadcrumb')
FOOD MANAGEMENT
@stop

@section('avatar')
@if($user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
</a>
<div class="mt">Welcome, {{ $user->username }}</div>
@endif
@stop

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-heading">

            <div class="card-heading">
                <div class="row">
                    <div class="col-xs-6 col-md-4">
                        <a href="/admin/food/edit/0" class="btn btn-primary">Add new</a>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-8 main-form-search">
                        <div class="main-form-search">
                            <form action="" method="get">
                                <select name="cate" >
                                    <option value="">Choose category</option>
                                    @foreach($listCategorys as $cate)
                                    <?php
                                    if(isset($_GET['cate']) && $_GET['cate'] == $cate->id) {
                                        $cateSelected = ' selected ';
                                    } else {
                                        $cateSelected = '';
                                    }
                                    ?>
                                    <option {{ $cateSelected }} value="{{ $cate->id }}">{{ $cate->name }}</option>
                                    @endforeach
                                </select>
                                <input class="" type="submit" value="Search"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>images</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>option</th>
                    </tr>
                </thead>.
                <tbody>
                    <?php $idx = 0 ?>
                    @foreach($products as $product)
                    <?php $idx++ ?>
                    <tr>
                        <td>{{ $idx }}</td>
                        <td>
                            @if(isset($product->image) && $product->image != '')
                            <img class="list_img_pro" src="{{ url($product->image) }}" />
                            @endif
                        </td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->summary }}</td>
                        <td>
                            <a href="{{ url('admin/food/edit/'.$product->id) }}"><i data-pack="default" class="ion-edit"></i></a>
                            &nbsp;
                            <a href="{{ url('admin/product/delete/'.$product->id) }}?type=food"><i data-pack="default" class="ion-trash-a"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="6">{!! $products->render() !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END row-->



@stop