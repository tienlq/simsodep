@extends('Backend.Layouts.main')

@section('title')
EDIT FOOD
@stop

@section('breadcrumb')
EDIT FOOD
@stop

@section('avatar')
    @if($user->avatar != '')
        <a href="">
            <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
        </a>
        <div class="mt">Welcome, {{ $user->username }}</div>
    @endif
@stop

@section('content')
<div class="container container-lg">
    <div class="card">
        <!--        <div class="card-heading">
                    <div class="card-title">Form edit product</div>
                </div>-->
        <form id="form-register" 
              name="registerForm" 
              novalidate="" 
              class="form-validate" 
              method="post" 
              action="{{ url('admin/product/save/' . $pid) }}"
              enctype="multipart/form-data">
            {{ csrf_field()}}
            <input type="hidden" name="type" value="food" />
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mda-form-group mb {{ isset($product->id) ? '' : 'float-label' }}">
                            <div class="mda-form-control">
                                <input name="name"
                                       value="{{ $product->name or '' }}"
                                       required="" 
                                       tabindex="0" 
                                       aria-required="true" 
                                       aria-invalid="true" 
                                       class="form-control">
                                <div class="mda-form-control-line"></div>
                                <label>Food Name</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mda-form-group">
                            <div class="mda-form-control">
                                <select name="category" class="form-control" required="">
                                    <option value="">&nbsp; Select category</option>
                                    {!! $htmlCategoryOption !!}
                            </select>
                            <div class="mda-form-control-line"></div>
                            <label>Category</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="mda-form-group mb {{ isset($product->id) ? '' : 'float-label' }}">
                    <div class="mda-form-control">
                        <textarea name="summary" 
                               class="form-control">{{ $product->summary or '' }}</textarea>
                        <div class="mda-form-control-line"></div>
                        <label>Summary</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="mda-form-group mb {{ isset($product->id) ? '' : 'float-label' }}">
                    <div class="mda-form-control">
                        <textarea name="youtubeCode" 
                               class="form-control">{{ $product->youtubeCode or '' }}</textarea>
                        <div class="mda-form-control-line"></div>
                        <label>youtube code</label>
                    </div>
                </div>
                @if(isset($product->youtubeCode) && $product->youtubeCode != '')
                {!! $product->youtubeCode !!}
                @endif
            </div>
            <div class="row">
                @if(isset($product->image) && $product->image != '')
                <div class="col-sm-4">
                    <a onclick="" class="close">X</a>
                    <img class="img-thumbnail" src="{{ url($product->image) }}">
                </div>
                @endif
                <div class="col-sm-6">
                    <div class="mda-form-group mb">
                        <div class="mda-form-control">
                            <input name="image" 
                                   type="file"
                                   tabindex="0" 
                                   aria-required="true" 
                                   aria-invalid="true" 
                                   class="form-control">
                            <input type="hidden" name="hiddenImage" value="{{ $product->image or '' }}"/>
                            <div class="mda-form-control-line"></div>
                            <label>Image</label>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="container-fluid">
                <label>content</label>
                <textarea class="summernote" name="content">{{ $product->content or '' }}</textarea>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="mda-form-group mb {{ isset($product->id) ? '' : 'float-label' }}">
                        <div class="mda-form-control">
                            <textarea name="keyword" 
                                      rows="4" 
                                      aria-multiline="true" 
                                      tabindex="0" 
                                      aria-invalid="false" 
                                      class="form-control">{{ $product->keyword or '' }}</textarea>
                            <div class="mda-form-control-line"></div>
                            <label>[SEO] Keyword</label>
                        </div>
                        <span class="mda-form-msg">Keyword should be a maximum of 155 characters long.</span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="mda-form-group mb {{ isset($product->id) ? '' : 'float-label' }}">
                        <div class="mda-form-control">
                            <textarea name="description" 
                                      rows="4" 
                                      aria-multiline="true" 
                                      tabindex="0" 
                                      aria-invalid="false" 
                                      class="form-control">{{ $product->description or '' }}</textarea>
                            <div class="mda-form-control-line"></div>
                            <label>[SEO] Descrption</label>
                        </div>
                        <span class="mda-form-msg">Descrption should be a maximum of 70-150 characters long.</span>
                    </div>
                </div>
            </div>
            <div class="clearfix">
                <div class="pull-left">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="submit" class="btn btn-default">Close</button>
                </div>
                <br/>
            </div>
        </div>
    </form>
</div>
</div>


@stop