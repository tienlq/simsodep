<link rel="stylesheet" href="{{ url('backend/vendor/animate.css/animate.css') }}">
<!-- Bootstrap-->
<link rel="stylesheet" href="{{ url('backend/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
<!-- Ionicons-->
<link rel="stylesheet" href="{{ url('backend/vendor/ionicons/css/ionicons.css') }}">
<!-- Bluimp Gallery-->
<link rel="stylesheet" href="{{ url('backend/vendor/blueimp-gallery/css/blueimp-gallery.css') }}">
<link rel="stylesheet" href="{{ url('backend/vendor/blueimp-gallery/css/blueimp-gallery-indicator.css') }}">
<link rel="stylesheet" href="{{ url('backend/vendor/blueimp-gallery/css/blueimp-gallery-video.css') }}">
<!-- Datepicker-->
<link rel="stylesheet" href="{{ url('backend/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}">
<!-- Rickshaw-->
<link rel="stylesheet" href="{{ url('backend/vendor/rickshaw/rickshaw.css') }}">
<!-- Select2-->
<link rel="stylesheet" href="{{ url('backend/vendor/select2/dist/css/select2.css') }}">
<!-- Clockpicker-->
<link rel="stylesheet" href="{{ url('backend/vendor/clockpicker/dist/bootstrap-clockpicker.css') }}">
<!-- Range Slider-->
<link rel="stylesheet" href="{{ url('backend/vendor/nouislider/distribute/nouislider.min.css') }}">
<!-- ColorPicker-->
<link rel="stylesheet" href="{{ url('backend/vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css') }}">
<!-- Summernote-->
<link rel="stylesheet" href="{{ url('backend/vendor/summernote/dist/summernote.css') }}">
<!-- Dropzone-->
<link rel="stylesheet" href="{{ url('backend/vendor/dropzone/dist/basic.css') }}">
<link rel="stylesheet" href="{{ url('backend/vendor/dropzone/dist/dropzone.css') }}">
<!-- Xeditable-->
<link rel="stylesheet" href="{{ url('backend/vendor/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css') }}">
<!-- Bootgrid-->
<link rel="stylesheet" href="{{ url('backend/vendor/jquery.bootgrid/dist/jquery.bootgrid.css') }}">
<!-- Datatables-->
<link rel="stylesheet" href="{{ url('backend/vendor/datatables/media/css/jquery.dataTables.css') }}">
<!-- Sweet Alert-->
<link rel="stylesheet" href="{{ url('backend/vendor/sweetalert/dist/sweetalert.css') }}">
<!-- Loaders.CSS-->
<link rel="stylesheet" href="{{ url('backend/vendor/loaders.css/loaders.css') }}">
<!-- Material Floating Button-->
<link rel="stylesheet" href="{{ url('backend/vendor/ng-material-floating-button/mfb/dist/mfb.css') }}">
<!-- Material Colors-->
<link rel="stylesheet" href="{{ url('backend/vendor/material-colors/dist/colors.css') }}">
<!-- endbuild-->
<!-- Application styles-->
<link rel="stylesheet" href="{{ url('backend/css/app.css') }}">
<!-- custom styles-->
<link rel="stylesheet" href="{{ url('backend/css/custom.css') }}">