<form action="" method="get">

    <input type="text" name="keyword" value="{{ $_GET['keyword'] or '' }}" placeholder="Nhập từ khóa cần tìm" />
    <button type="submit">
        <span class="ion-search"></span>
        Tìm kiếm
    </button>
    <div class="col-sm-12 actionBar">
        <a class="btn btn-primary btn-sm " href="/admin/user/edit/0">
            <span class="ion-android-add-circle"></span>
            Thêm mới
        </a>
        <button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown">
            <span class="dropdown-text">hiển thị</span> 
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu pull-right" role="menu">
            <li class="{{ isset($_GET['show']) && intval($_GET['show']) == 20 ? 'active':'' }}" aria-selected="false">
                <a data-action="20" 
                   href="?show=20"
                   class="dropdown-item dropdown-item-button">20</a>
            </li>
            <li class="{{ isset($_GET['show']) && intval($_GET['show']) == 30 ? 'active':'' }}" aria-selected="true">
                <a data-action="30" 
                   href="?show=30"
                   class="dropdown-item dropdown-item-button">30</a>
            </li>
        </ul>
    </div>

</form>