<?php
$perJson = file_get_contents(JSON_PERMISSION_FILE);
$permission = json_decode($perJson, true);
?>
<div class="row">
    <div class="mda-form-control">
        <select name="groupID"
                required="" 
                tabindex="0" 
                aria-required="true" 
                aria-invalid="true" 
                onchange="changePermisson(this, '{{ url('admin/group/get-permission') }}')"
                class="form-control input-sm">
            <option value="0">Chọn nhóm quyền</option>
            @foreach($listGroup as $group)
                <?php 
                if(isset($userEdit->groupID) && $group->id == $userEdit->groupID) {
                    $selected = ' selected ';
                } else {
                    $selected ='';
                }
                ?>
                <option {{ $selected }} value="{{ $group->id }}">{{ $group->name }}</option>
            @endforeach
        </select>
        <div class="mda-form-control-line"></div>
    </div>
</div>
<div class="row">
    <div class="mda-form-control">
        <input type="number" name="customerID" value="{{ $userEdit->customerID or '' }}" placeholder="Nhập mã khách hàng" />
        <div class="mda-form-control-line"></div>
    </div>
</div>
<br>
@foreach($permission['Permission'] as $per)
<div class="row">
    <div class="mda-form-control">
        <label>{{ $per['name'] }}</label>
    </div>
</div>
<div class="row">
    @foreach($per['sub'] as $sub)
    <?php
//    print_r($listPermission);die;
    if (in_array($sub['id'], $listPermission)) {
        $checked = " checked ";
    } else {
        $checked = '';
    }
    ?>
    <div class="col-sm-4">
        <div class="checkbox c-checkbox">
            <label class="small">
                <input {{ $checked }} name="permission[]" 
                    onclick="return false;" 
                    onkeydown="return false;" 
                    type="checkbox" 
                    value="{{ $sub['id'] }}">
                <span class="ion-checkmark-round"></span> 
                {{ $sub['name'] }}
            </label>
        </div>
    </div>
    @endforeach
</div>
@endforeach
