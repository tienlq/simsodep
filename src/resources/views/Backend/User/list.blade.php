@extends('Backend.Layouts.main')

@section('title')
Quản lý nhân viên
@stop

@section('breadcrumb')
Quản lý nhân viên
@stop

@section('content')
<div class="container-fluid">
    <div class="card">
        <div id="bootgrid-basic-header" class="bootgrid-header container-fluid">
            <div class="row">
                @include('Backend.User.formSreach')
                {{ csrf_field()}}
            </div>
        </div>
        <!--<div class="card-heading">Product management</div>-->
        <div class="table-responsive list-user" id="nestable">
            <table class="table table-striped">
                <thead>
                    <tr>
<!--                        <th width="20px">
                            <input onclick="checkAllCheckbox(this)" type="checkbox">
                        </th>-->
                        <th>#</th>
                        <th>Họ tên</th>
                        <th>Tên đăng nhập</th>
                        <th>Điện thoại</th>
                        <th>Email</th>
                        <th>Skyper</th>
                        <th>Thêm nhanh Credit</th>
                        <th>Tùy chọn</th>
                    </tr>
                </thead>.
                <tbody>
                    <?php $idx = 0 ?>
                    @foreach($listUser as $user)
                    <?php $idx++ ?>
                    <tr class="_pointer">
<!--                        <td width="20px">
                            <input name="option[]" onclick="checkExport()" value="76" type="checkbox">
                        </td>-->
                        <td onclick="loadData('.modal-content02', '/admin/user/detail/{{ $user->id }}')"
                            data-toggle="modal" 
                            data-target="#myModal">NV-{{ $user->id }}</td>
                        <td onclick="loadData('.modal-content02', '/admin/user/detail/{{ $user->id }}')"
                            data-toggle="modal" 
                            data-target="#myModal">
                            <a>{{ $user->fullName }}</a>
                        </td>
                        <td onclick="loadData('.modal-content02', '/admin/user/detail/{{ $user->id }}')"
                            data-toggle="modal" 
                            data-target="#myModal">
                            <a>{{ $user->username }}</a>
                        </td>
                        <td>{{ $user->phone }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->skype }}</td>
                        <td class="result_{{ $user->id }}">{{ $user->creditUsed or 0 }}/{{ $user->creditTotal or 0 }}</td>
                        <td>
                            <a href="/admin/user/edit/{{ $user->id }}">
                                <i data-pack="default" class="ion-edit"></i>
                            </a>
                            
                            &nbsp;
                            
                            <a onclick="deleteRow('/admin/user/delete/{{ $user->id }}', '/admin/user/list')">
                                <i data-pack="default" class="ion-trash-a"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="7">{!! $listUser ->render() !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END row-->



@stop