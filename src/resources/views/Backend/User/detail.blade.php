<?php
// $perJson = file_get_contents(JSON_PERMISSION_FILE);
// $permission = json_decode($perJson, true);
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Thông tin chi tiết nhân viên</h4>
</div>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#tab" 
           aria-controls="home" 
           role="tab" 
           data-toggle="tab">Thông tin nhân viên</a>
    </li>
</ul>

<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <div class="card-body">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td><a class="ion-person ion-person mr"></a>Họ tên</td>
                                <td>{{ $detailUser->fullName or '' }}</td>
                            </tr>
                            @if(empty($_GET['viewBasic']))
                                <tr>
                                    <td><a class="ion-person ion-person mr"></a>Tên đăng nhập</td>
                                    <td>{{ $detailUser->username or '' }}</td>
                                </tr>
                            @endif
                            <tr>
                                <td><a class="ion-email mr"></a>Email</td>
                                <td>{{ $detailUser->email or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-ios-telephone mr"></a>Điện thoại</td>
                                <td>{{ $detailUser->phoneNumber or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-ios-telephone mr"></a>Skyper</td>
                                <td>{{ $detailUser->skypeAccount or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-android-person mr"></a>Ngày sinh</td>
                                <td>{{ $detailUser->birthday or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-location icon-fw mr"></a>Địa chỉ</td>
                                <td><span class="is-editable text-inherit">{{ $detailUser->address or '' }}</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
</div>