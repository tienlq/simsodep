<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Sửa thông tin nhân viên</h4>
</div>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#tab-01" aria-controls="home" role="tab" data-toggle="tab">Sửa thông tin nhân viên</a></li>
    <li role="presentation"><a href="#tab-02" aria-controls="profile" role="tab" data-toggle="tab">Phân quyền</a></li>
</ul>
<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <form id="form-register" 
              action="http://localhost:280/admin/user/edit/65"
              class="form-validate form-edit" 
              method="post" 
              enctype="multipart/form-data">
            <input type="hidden" name="_token" value="izOnGvqnrOKlXt1DLSKJyNTwVbn47rl6GMxIBAwz">
            <div class="card-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab-01">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mda-form-group">
                                    <div class="mda-form-control">
                                        <input name="fullName"
                                               value="123"
                                               required="" 
                                               tabindex="0" 
                                               aria-required="true" 
                                               aria-invalid="true" 
                                               class="form-control input-sm">
                                        <div class="mda-form-control-line"></div>
                                        <label>Họ tên</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group">
                                    <div class="mda-form-control">
                                        <input name="username"
                                               value="tienlq311"
                                               required="" 
                                               tabindex="0" 
                                               aria-required="true" 
                                               aria-invalid="true" 
                                               class="form-control input-sm">
                                        <div class="mda-form-control-line"></div>
                                        <label>Tên đăng nhập</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mda-form-group">
                                    <div class="mda-form-control">
                                        <input name="password"
                                               type="password"
                                               value=""
                                               tabindex="0" 
                                               aria-required="true" 
                                               aria-invalid="true" 
                                               placeholder="Bỏ trống nếu không thay đổi"
                                               class="form-control input-sm">
                                        <div class="mda-form-control-line"></div>
                                        <label>Mật khẩu</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group">
                                    <div class="mda-form-control">
                                        <input name="passwordConfirm"
                                               type="password"
                                               value=""
                                               tabindex="0" 
                                               aria-required="true" 
                                               aria-invalid="true" 
                                               class="form-control input-sm">
                                        <div class="mda-form-control-line"></div>
                                        <label>Xác nhận lại mật khẩu</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mda-form-group">
                                    <div class="mda-form-control">
                                        <input name="email"
                                               value="tien.luu@j-job.com.vn"
                                               tabindex="0" 
                                               aria-required="true" 
                                               aria-invalid="true" 
                                               class="form-control input-sm">
                                        <div class="mda-form-control-line"></div>
                                        <label>Email</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group">
                                    <label class="label-title01">Ngày sinh</label>
                                    <div class="mda-form-control rel-wrapper ui-datepicker ui-datepicker-popup dp-theme-success" 
                                         id="example-datepicker-container-5">
                                        <input name="birthday"
                                               value="0000-00-00"
                                               tabindex="0" 
                                               data-date-format="yyyy-mm-dd"
                                               id="datepicker-1"
                                               aria-required="true" 
                                               aria-invalid="true" 
                                               class="form-control datepicker">
                                        <div class="mda-form-control-line"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mda-form-group">
                                    <div class="mda-form-control">
                                        <input name="skype"
                                               value=""
                                               tabindex="0" 
                                               aria-required="true" 
                                               aria-invalid="true" 
                                               class="form-control input-sm">
                                        <div class="mda-form-control-line"></div>
                                        <label>Skyper</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group">
                                    <div class="mda-form-control">
                                        <input name="phone"
                                               type="number"
                                               value=""
                                               tabindex="0" 
                                               aria-required="true" 
                                               aria-invalid="true" 
                                               class="form-control input-sm">
                                        <div class="mda-form-control-line"></div>
                                        <label>Điện thoại</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="mda-form-group">
                                    <div class="mda-form-control">
                                        <textarea name="address"
                                                  tabindex="0" 
                                                  class="form-control input-sm"></textarea>
                                        <div class="mda-form-control-line"></div>
                                        <label>Địa chỉ</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab-02">
                        <div class="row">
                            <div class="mda-form-control">
                                <select name="groupID"
                                        required="" 
                                        tabindex="0" 
                                        aria-required="true" 
                                        aria-invalid="true" 
                                        onchange="changePermisson(this, 'http://localhost:280/admin/group/get-permission')"
                                        class="form-control input-sm">
                                    <option value="0">Chọn nhóm quyền</option>
                                    <option  selected  value="1">admin</option>
                                    <option  value="3">Tuyển dụng</option>
                                </select>
                                <div class="mda-form-control-line"></div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="mda-form-control">
                                <label>Quản lý ứng viên</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="VACANCY_LIST_VIEW">
                                        <span class="ion-checkmark-round"></span> 
                                        Xem danh sách ứng viên
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="VACANCY_VIEW">
                                        <span class="ion-checkmark-round"></span> 
                                        Xem chi tiết ứng viên
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="VACANCY_EXPORT_EDIT">
                                        <span class="ion-checkmark-round"></span> 
                                        Nhập từ file excel
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="VACANCY_EMAIL">
                                        <span class="ion-checkmark-round"></span> 
                                        Xem email
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="VACANCY_MOBILE">
                                        <span class="ion-checkmark-round"></span> 
                                        Xem số điện thoại
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="VACANCY_SKYPE">
                                        <span class="ion-checkmark-round"></span> 
                                        Xem skype
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="VACANCY_EDIT">
                                        <span class="ion-checkmark-round"></span> 
                                        Thêm, sửa thông tin ứng viên
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="VACANCY_EXPORT_EDIT">
                                        <span class="ion-checkmark-round"></span> 
                                        Xuất ra file excel
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="VACANCY_DOWNLOAD">
                                        <span class="ion-checkmark-round"></span> 
                                        Download file
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="VACANCY_DELETE">
                                        <span class="ion-checkmark-round"></span> 
                                        Xóa ứng viên
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mda-form-control">
                                <label>Quản lý khách hàng</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="CUSTOMER_LIST_VIEW">
                                        <span class="ion-checkmark-round"></span> 
                                        Xem danh sách khách hàng
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="CUSTOMER_VIEW">
                                        <span class="ion-checkmark-round"></span> 
                                        Xem chi tiết khách hàng
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="CUSTOMER_CONTACT">
                                        <span class="ion-checkmark-round"></span> 
                                        Xem thông tin liên hệ
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="CUSTOMER_EDIT">
                                        <span class="ion-checkmark-round"></span> 
                                        Thêm, sửa thông tin khách hàng
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="CUSTOMER_EXPORT_EDIT">
                                        <span class="ion-checkmark-round"></span> 
                                        Xuất ra file excel
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="CUSTOMER_EXPORT_EDIT">
                                        <span class="ion-checkmark-round"></span> 
                                        Nhập từ file excel
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="CUSTOMER_DELETE">
                                        <span class="ion-checkmark-round"></span> 
                                        Xóa khách hàng
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mda-form-control">
                                <label>Quản lý job</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="JOB_LIST_VIEW">
                                        <span class="ion-checkmark-round"></span> 
                                        Xem danh sách job
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="JOB_VIEW">
                                        <span class="ion-checkmark-round"></span> 
                                        Xem chi tiết job
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="JOB_EDIT">
                                        <span class="ion-checkmark-round"></span> 
                                        Thêm, sửa job
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="JOB_CV_DOWNLOAD">
                                        <span class="ion-checkmark-round"></span> 
                                        Download file CV
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="JOB_CV_VIEW">
                                        <span class="ion-checkmark-round"></span> 
                                        Xem file CV
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="JOB_EXPORT_EDIT">
                                        <span class="ion-checkmark-round"></span> 
                                        Xuất ra file excel
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="JOB_EXPORT_EDIT">
                                        <span class="ion-checkmark-round"></span> 
                                        Nhập từ file excel
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="JOB_DELETE">
                                        <span class="ion-checkmark-round"></span> 
                                        Xóa job
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mda-form-control">
                                <label>Quản lý nhân viên</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="USER_LIST_VIEW">
                                        <span class="ion-checkmark-round"></span> 
                                        Xem danh sách NV
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="USER_VIEW">
                                        <span class="ion-checkmark-round"></span> 
                                        Xem chi tiết sách NV
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="USER_EDIT">
                                        <span class="ion-checkmark-round"></span> 
                                        Thêm, sửa thông tin NV
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="USER_DELETE">
                                        <span class="ion-checkmark-round"></span> 
                                        Xóa nhân viên
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mda-form-control">
                                <label>Quản lý nhóm quyền</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="GROUP_VIEW">
                                        <span class="ion-checkmark-round"></span> 
                                        Xem danh sách nhóm quyền
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="GROUP_EDIT">
                                        <span class="ion-checkmark-round"></span> 
                                        Thêm, sửa nhóm quyền
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="GROUP_DELETE">
                                        <span class="ion-checkmark-round"></span> 
                                        Xóa nhóm quyền
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mda-form-control">
                                <label>Setting</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="CLIENT">
                                        <span class="ion-checkmark-round"></span> 
                                        Client
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="LINH_VUC">
                                        <span class="ion-checkmark-round"></span> 
                                        Lĩnh vực
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="ENGLISH">
                                        <span class="ion-checkmark-round"></span> 
                                        Level tiếng anh
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="JAPANESE">
                                        <span class="ion-checkmark-round"></span> 
                                        Level tiếng nhật
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="QUOCTICH">
                                        <span class="ion-checkmark-round"></span> 
                                        Quốc tịch
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="SOURCING">
                                        <span class="ion-checkmark-round"></span> 
                                        Sourcing
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox c-checkbox">
                                    <label class="small">
                                        <input  name="permission[]" type="checkbox" value="VITRI">
                                        <span class="ion-checkmark-round"></span> 
                                        Vị trí tuyển dụng
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                       <!--<input type="submit">-->
                        <button type="button" 
                                class="btn btn-primary"
                                onclick="popupSubmitForm('.form-edit', '.loading', '.list-user', 'http://localhost:280/admin/user/list')">Cập nhật</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" >Close</button>
                        <div class="loading"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $('#datepicker-1').datepicker({
        container: '#example-datepicker-container-5'
    });

</script>