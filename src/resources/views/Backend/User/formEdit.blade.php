
<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <input name="fullName"
                       value="{{ $userEdit->fullName or '' }}"
                       required="" 
                       tabindex="0" 
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control input-sm">
                <div class="mda-form-control-line"></div>
                <label>Họ tên</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <input name="username"
                       value="{{ $userEdit->username or '' }}"
                       required="" 
                       tabindex="0" 
                       @if(isset($_GET['changeProfile']) && intval($_GET['changeProfile']) == 1)
                       readonly 
                       @endif
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control input-sm">
                <div class="mda-form-control-line"></div>
                <label>Tên đăng nhập</label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <input name="password"
                       type="password"
                       value=""
                       tabindex="0" 
                       aria-required="true" 
                       aria-invalid="true" 
                       placeholder="Bỏ trống nếu không thay đổi"
                       class="form-control input-sm">
                <div class="mda-form-control-line"></div>
                <label>Mật khẩu</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <input name="passwordConfirm"
                       type="password"
                       value=""
                       tabindex="0" 
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control input-sm">
                <div class="mda-form-control-line"></div>
                <label>Xác nhận lại mật khẩu</label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <input name="email"
                       value="{{ $userEdit->email or '' }}"
                       tabindex="0" 
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control input-sm">
                <div class="mda-form-control-line"></div>
                <label>Email</label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <input name="skype"
                       value="{{ $userEdit->skype or '' }}"
                       tabindex="0" 
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control input-sm">
                <div class="mda-form-control-line"></div>
                <label>Skyper</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <input name="phone"
                       type="number"
                       value="{{ $userEdit->phone or '' }}"
                       tabindex="0" 
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control input-sm">
                <div class="mda-form-control-line"></div>
                <label>Điện thoại</label>
            </div>
        </div>
    </div>
</div>