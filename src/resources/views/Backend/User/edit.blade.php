@extends('Backend.Layouts.main')

@section('title')
Sửa thông nhân viên
@stop

@section('breadcrumb')
Sửa thông nhân viên
@stop

@section('addScript')
<script>
    $('#datepicker-1').datepicker({
        container: '#example-datepicker-container-5'
    });
</script>
@stop

@section('content')

<?php $disableCustomerForm = ' disabled ' ?>

<!-- Nav tabs -->
@if(isset($_GET['changeProfile']) && intval($_GET['changeProfile']) == 1)
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Sửa thông tin cá nhân</h4>
    @if(isset($_GET['ret']) && $_GET['ret'] == 'success') 
    <p class="_success">Thay đổi thông tin cá nhân thành công</p>
    @endif
</div>
@else
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Sửa thông tin nhân viên</h4>
</div>
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#tab-01" aria-controls="home" role="tab" data-toggle="tab">Sửa thông tin nhân viên</a></li>
</ul>
@endif

<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <form id="form-register" 
              action="{{ url('admin/user/edit/'.$id) }}"
              name="registerForm" 
              novalidate="" 
              class="form-validate form-edit" 
              method="post" 
              enctype="multipart/form-data">

            {{ csrf_field()}}

            @if(isset($_GET['changeProfile']) && intval($_GET['changeProfile']) == 1)
            <input type="hidden"  value="1" name="changeProfile"/>
            @else 
            <input type="hidden"  value="0" name="changeProfile"/>
            @endif
            <div class="card-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab-01">
                    @include('Backend.User.formEdit')
                </div>
                <div class="modal-footer" style="text-align: left">
                    <!--<input type="submit">-->
                    <button type="submit" 
                            class="btn btn-primary" >Cập nhật</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" >Close</button>
                    <div class="loading"></div>
                </div>
            </div>
    </div>
</form>
</div>
</div>


@stop

