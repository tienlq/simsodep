@extends('Backend.Layouts.main')

@section('title')
home page
@stop

@section('breadcrumb')
HOME
@stop

@section('avatar')
    @if($user->avatar != '')
        <a href="">
            <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
        </a>
        <div class="mt">Welcome, {{ $user->username }}</div>
    @endif
@stop

@section('content')
home
@stop