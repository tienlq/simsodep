<script src="/backend/js/addFont.js"></script>
<section id="web-application">
    <h2 class="page-header">Vui lòng chọn 1 trong số các icon bên dưới:</h2>
    <div class="row fontawesome-icon-list">
        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/address-book"><i class="fa fa-address-book" aria-hidden="true"></i>address-book</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/address-book-o"><i class="fa fa-address-book-o" aria-hidden="true"></i>address-book-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/address-card"><i class="fa fa-address-card" aria-hidden="true"></i>address-card</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/address-card-o"><i class="fa fa-address-card-o" aria-hidden="true"></i>address-card-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/adjust"><i class="fa fa-adjust" aria-hidden="true"></i>adjust</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/american-sign-language-interpreting"><i class="fa fa-american-sign-language-interpreting" aria-hidden="true"></i>american-sign-language-interpreting</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/anchor"><i class="fa fa-anchor" aria-hidden="true"></i>anchor</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/archive"><i class="fa fa-archive" aria-hidden="true"></i>archive</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/area-chart"><i class="fa fa-area-chart" aria-hidden="true"></i>area-chart</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/arrows"><i class="fa fa-arrows" aria-hidden="true"></i>arrows</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/arrows-h"><i class="fa fa-arrows-h" aria-hidden="true"></i>arrows-h</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/arrows-v"><i class="fa fa-arrows-v" aria-hidden="true"></i>arrows-v</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/american-sign-language-interpreting"><i class="fa fa-asl-interpreting" aria-hidden="true"></i>asl-interpreting <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/assistive-listening-systems"><i class="fa fa-assistive-listening-systems" aria-hidden="true"></i>assistive-listening-systems</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/asterisk"><i class="fa fa-asterisk" aria-hidden="true"></i>asterisk</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/at"><i class="fa fa-at" aria-hidden="true"></i>at</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/audio-description"><i class="fa fa-audio-description" aria-hidden="true"></i>audio-description</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/car"><i class="fa fa-automobile" aria-hidden="true"></i>automobile <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/balance-scale"><i class="fa fa-balance-scale" aria-hidden="true"></i>balance-scale</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/ban"><i class="fa fa-ban" aria-hidden="true"></i>ban</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/university"><i class="fa fa-bank" aria-hidden="true"></i>bank <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bar-chart"><i class="fa fa-bar-chart" aria-hidden="true"></i>bar-chart</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bar-chart"><i class="fa fa-bar-chart-o" aria-hidden="true"></i>bar-chart-o <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/barcode"><i class="fa fa-barcode" aria-hidden="true"></i>barcode</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bars"><i class="fa fa-bars" aria-hidden="true"></i>bars</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bath"><i class="fa fa-bath" aria-hidden="true"></i>bath</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bath"><i class="fa fa-bathtub" aria-hidden="true"></i>bathtub <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/battery-full"><i class="fa fa-battery" aria-hidden="true"></i>battery <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/battery-empty"><i class="fa fa-battery-0" aria-hidden="true"></i>battery-0 <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/battery-quarter"><i class="fa fa-battery-1" aria-hidden="true"></i>battery-1 <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/battery-half"><i class="fa fa-battery-2" aria-hidden="true"></i>battery-2 <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/battery-three-quarters"><i class="fa fa-battery-3" aria-hidden="true"></i>battery-3 <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/battery-full"><i class="fa fa-battery-4" aria-hidden="true"></i>battery-4 <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/battery-empty"><i class="fa fa-battery-empty" aria-hidden="true"></i>battery-empty</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/battery-full"><i class="fa fa-battery-full" aria-hidden="true"></i>battery-full</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/battery-half"><i class="fa fa-battery-half" aria-hidden="true"></i>battery-half</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/battery-quarter"><i class="fa fa-battery-quarter" aria-hidden="true"></i>battery-quarter</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/battery-three-quarters"><i class="fa fa-battery-three-quarters" aria-hidden="true"></i>battery-three-quarters</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bed"><i class="fa fa-bed" aria-hidden="true"></i>bed</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/beer"><i class="fa fa-beer" aria-hidden="true"></i>beer</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bell"><i class="fa fa-bell" aria-hidden="true"></i>bell</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bell-o"><i class="fa fa-bell-o" aria-hidden="true"></i>bell-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bell-slash"><i class="fa fa-bell-slash" aria-hidden="true"></i>bell-slash</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bell-slash-o"><i class="fa fa-bell-slash-o" aria-hidden="true"></i>bell-slash-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bicycle"><i class="fa fa-bicycle" aria-hidden="true"></i>bicycle</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/binoculars"><i class="fa fa-binoculars" aria-hidden="true"></i>binoculars</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/birthday-cake"><i class="fa fa-birthday-cake" aria-hidden="true"></i>birthday-cake</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/blind"><i class="fa fa-blind" aria-hidden="true"></i>blind</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bluetooth"><i class="fa fa-bluetooth" aria-hidden="true"></i>bluetooth</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bluetooth-b"><i class="fa fa-bluetooth-b" aria-hidden="true"></i>bluetooth-b</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bolt"><i class="fa fa-bolt" aria-hidden="true"></i>bolt</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bomb"><i class="fa fa-bomb" aria-hidden="true"></i>bomb</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/book"><i class="fa fa-book" aria-hidden="true"></i>book</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bookmark"><i class="fa fa-bookmark" aria-hidden="true"></i>bookmark</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bookmark-o"><i class="fa fa-bookmark-o" aria-hidden="true"></i>bookmark-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/braille"><i class="fa fa-braille" aria-hidden="true"></i>braille</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/briefcase"><i class="fa fa-briefcase" aria-hidden="true"></i>briefcase</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bug"><i class="fa fa-bug" aria-hidden="true"></i>bug</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/building"><i class="fa fa-building" aria-hidden="true"></i>building</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/building-o"><i class="fa fa-building-o" aria-hidden="true"></i>building-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bullhorn"><i class="fa fa-bullhorn" aria-hidden="true"></i>bullhorn</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bullseye"><i class="fa fa-bullseye" aria-hidden="true"></i>bullseye</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bus"><i class="fa fa-bus" aria-hidden="true"></i>bus</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/taxi"><i class="fa fa-cab" aria-hidden="true"></i>cab <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/calculator"><i class="fa fa-calculator" aria-hidden="true"></i>calculator</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/calendar"><i class="fa fa-calendar" aria-hidden="true"></i>calendar</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/calendar-check-o"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>calendar-check-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/calendar-minus-o"><i class="fa fa-calendar-minus-o" aria-hidden="true"></i>calendar-minus-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/calendar-o"><i class="fa fa-calendar-o" aria-hidden="true"></i>calendar-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/calendar-plus-o"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i>calendar-plus-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/calendar-times-o"><i class="fa fa-calendar-times-o" aria-hidden="true"></i>calendar-times-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/camera"><i class="fa fa-camera" aria-hidden="true"></i>camera</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/camera-retro"><i class="fa fa-camera-retro" aria-hidden="true"></i>camera-retro</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/car"><i class="fa fa-car" aria-hidden="true"></i>car</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/caret-square-o-down"><i class="fa fa-caret-square-o-down" aria-hidden="true"></i>caret-square-o-down</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/caret-square-o-left"><i class="fa fa-caret-square-o-left" aria-hidden="true"></i>caret-square-o-left</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/caret-square-o-right"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i>caret-square-o-right</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/caret-square-o-up"><i class="fa fa-caret-square-o-up" aria-hidden="true"></i>caret-square-o-up</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/cart-arrow-down"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>cart-arrow-down</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/cart-plus"><i class="fa fa-cart-plus" aria-hidden="true"></i>cart-plus</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/cc"><i class="fa fa-cc" aria-hidden="true"></i>cc</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/certificate"><i class="fa fa-certificate" aria-hidden="true"></i>certificate</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/check"><i class="fa fa-check" aria-hidden="true"></i>check</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/check-circle"><i class="fa fa-check-circle" aria-hidden="true"></i>check-circle</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/check-circle-o"><i class="fa fa-check-circle-o" aria-hidden="true"></i>check-circle-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/check-square"><i class="fa fa-check-square" aria-hidden="true"></i>check-square</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/check-square-o"><i class="fa fa-check-square-o" aria-hidden="true"></i>check-square-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/child"><i class="fa fa-child" aria-hidden="true"></i>child</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/circle"><i class="fa fa-circle" aria-hidden="true"></i>circle</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/circle-o"><i class="fa fa-circle-o" aria-hidden="true"></i>circle-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/circle-o-notch"><i class="fa fa-circle-o-notch" aria-hidden="true"></i>circle-o-notch</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/circle-thin"><i class="fa fa-circle-thin" aria-hidden="true"></i>circle-thin</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/clock-o"><i class="fa fa-clock-o" aria-hidden="true"></i>clock-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/clone"><i class="fa fa-clone" aria-hidden="true"></i>clone</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/times"><i class="fa fa-close" aria-hidden="true"></i>close <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/cloud"><i class="fa fa-cloud" aria-hidden="true"></i>cloud</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/cloud-download"><i class="fa fa-cloud-download" aria-hidden="true"></i>cloud-download</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/cloud-upload"><i class="fa fa-cloud-upload" aria-hidden="true"></i>cloud-upload</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/code"><i class="fa fa-code" aria-hidden="true"></i>code</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/code-fork"><i class="fa fa-code-fork" aria-hidden="true"></i>code-fork</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/coffee"><i class="fa fa-coffee" aria-hidden="true"></i>coffee</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/cog"><i class="fa fa-cog" aria-hidden="true"></i>cog</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/cogs"><i class="fa fa-cogs" aria-hidden="true"></i>cogs</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/comment"><i class="fa fa-comment" aria-hidden="true"></i>comment</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/comment-o"><i class="fa fa-comment-o" aria-hidden="true"></i>comment-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/commenting"><i class="fa fa-commenting" aria-hidden="true"></i>commenting</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/commenting-o"><i class="fa fa-commenting-o" aria-hidden="true"></i>commenting-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/comments"><i class="fa fa-comments" aria-hidden="true"></i>comments</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/comments-o"><i class="fa fa-comments-o" aria-hidden="true"></i>comments-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/compass"><i class="fa fa-compass" aria-hidden="true"></i>compass</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/copyright"><i class="fa fa-copyright" aria-hidden="true"></i>copyright</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/creative-commons"><i class="fa fa-creative-commons" aria-hidden="true"></i>creative-commons</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/credit-card"><i class="fa fa-credit-card" aria-hidden="true"></i>credit-card</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/credit-card-alt"><i class="fa fa-credit-card-alt" aria-hidden="true"></i>credit-card-alt</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/crop"><i class="fa fa-crop" aria-hidden="true"></i>crop</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/crosshairs"><i class="fa fa-crosshairs" aria-hidden="true"></i>crosshairs</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/cube"><i class="fa fa-cube" aria-hidden="true"></i>cube</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/cubes"><i class="fa fa-cubes" aria-hidden="true"></i>cubes</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/cutlery"><i class="fa fa-cutlery" aria-hidden="true"></i>cutlery</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/tachometer"><i class="fa fa-dashboard" aria-hidden="true"></i>dashboard <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/database"><i class="fa fa-database" aria-hidden="true"></i>database</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/deaf"><i class="fa fa-deaf" aria-hidden="true"></i>deaf</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/deaf"><i class="fa fa-deafness" aria-hidden="true"></i>deafness <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/desktop"><i class="fa fa-desktop" aria-hidden="true"></i>desktop</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/diamond"><i class="fa fa-diamond" aria-hidden="true"></i>diamond</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/dot-circle-o"><i class="fa fa-dot-circle-o" aria-hidden="true"></i>dot-circle-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/download"><i class="fa fa-download" aria-hidden="true"></i>download</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/id-card"><i class="fa fa-drivers-license" aria-hidden="true"></i>drivers-license <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/id-card-o"><i class="fa fa-drivers-license-o" aria-hidden="true"></i>drivers-license-o <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/pencil-square-o"><i class="fa fa-edit" aria-hidden="true"></i>edit <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/ellipsis-h"><i class="fa fa-ellipsis-h" aria-hidden="true"></i>ellipsis-h</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/ellipsis-v"><i class="fa fa-ellipsis-v" aria-hidden="true"></i>ellipsis-v</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/envelope"><i class="fa fa-envelope" aria-hidden="true"></i>envelope</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/envelope-o"><i class="fa fa-envelope-o" aria-hidden="true"></i>envelope-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/envelope-open"><i class="fa fa-envelope-open" aria-hidden="true"></i>envelope-open</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/envelope-open-o"><i class="fa fa-envelope-open-o" aria-hidden="true"></i>envelope-open-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/envelope-square"><i class="fa fa-envelope-square" aria-hidden="true"></i>envelope-square</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/eraser"><i class="fa fa-eraser" aria-hidden="true"></i>eraser</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/exchange"><i class="fa fa-exchange" aria-hidden="true"></i>exchange</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/exclamation"><i class="fa fa-exclamation" aria-hidden="true"></i>exclamation</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/exclamation-circle"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>exclamation-circle</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/exclamation-triangle"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>exclamation-triangle</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/external-link"><i class="fa fa-external-link" aria-hidden="true"></i>external-link</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/external-link-square"><i class="fa fa-external-link-square" aria-hidden="true"></i>external-link-square</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/eye"><i class="fa fa-eye" aria-hidden="true"></i>eye</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/eye-slash"><i class="fa fa-eye-slash" aria-hidden="true"></i>eye-slash</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/eyedropper"><i class="fa fa-eyedropper" aria-hidden="true"></i>eyedropper</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/fax"><i class="fa fa-fax" aria-hidden="true"></i>fax</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/rss"><i class="fa fa-feed" aria-hidden="true"></i>feed <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/female"><i class="fa fa-female" aria-hidden="true"></i>female</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/fighter-jet"><i class="fa fa-fighter-jet" aria-hidden="true"></i>fighter-jet</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/file-archive-o"><i class="fa fa-file-archive-o" aria-hidden="true"></i>file-archive-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/file-audio-o"><i class="fa fa-file-audio-o" aria-hidden="true"></i>file-audio-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/file-code-o"><i class="fa fa-file-code-o" aria-hidden="true"></i>file-code-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/file-excel-o"><i class="fa fa-file-excel-o" aria-hidden="true"></i>file-excel-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/file-image-o"><i class="fa fa-file-image-o" aria-hidden="true"></i>file-image-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/file-video-o"><i class="fa fa-file-movie-o" aria-hidden="true"></i>file-movie-o <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/file-pdf-o"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>file-pdf-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/file-image-o"><i class="fa fa-file-photo-o" aria-hidden="true"></i>file-photo-o <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/file-image-o"><i class="fa fa-file-picture-o" aria-hidden="true"></i>file-picture-o <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/file-powerpoint-o"><i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>file-powerpoint-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/file-audio-o"><i class="fa fa-file-sound-o" aria-hidden="true"></i>file-sound-o <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/file-video-o"><i class="fa fa-file-video-o" aria-hidden="true"></i>file-video-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/file-word-o"><i class="fa fa-file-word-o" aria-hidden="true"></i>file-word-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/file-archive-o"><i class="fa fa-file-zip-o" aria-hidden="true"></i>file-zip-o <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/film"><i class="fa fa-film" aria-hidden="true"></i>film</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/filter"><i class="fa fa-filter" aria-hidden="true"></i>filter</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/fire"><i class="fa fa-fire" aria-hidden="true"></i>fire</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/fire-extinguisher"><i class="fa fa-fire-extinguisher" aria-hidden="true"></i>fire-extinguisher</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/flag"><i class="fa fa-flag" aria-hidden="true"></i>flag</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/flag-checkered"><i class="fa fa-flag-checkered" aria-hidden="true"></i>flag-checkered</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/flag-o"><i class="fa fa-flag-o" aria-hidden="true"></i>flag-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bolt"><i class="fa fa-flash" aria-hidden="true"></i>flash <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/flask"><i class="fa fa-flask" aria-hidden="true"></i>flask</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/folder"><i class="fa fa-folder" aria-hidden="true"></i>folder</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/folder-o"><i class="fa fa-folder-o" aria-hidden="true"></i>folder-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/folder-open"><i class="fa fa-folder-open" aria-hidden="true"></i>folder-open</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/folder-open-o"><i class="fa fa-folder-open-o" aria-hidden="true"></i>folder-open-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/frown-o"><i class="fa fa-frown-o" aria-hidden="true"></i>frown-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/futbol-o"><i class="fa fa-futbol-o" aria-hidden="true"></i>futbol-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/gamepad"><i class="fa fa-gamepad" aria-hidden="true"></i>gamepad</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/gavel"><i class="fa fa-gavel" aria-hidden="true"></i>gavel</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/cog"><i class="fa fa-gear" aria-hidden="true"></i>gear <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/cogs"><i class="fa fa-gears" aria-hidden="true"></i>gears <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/gift"><i class="fa fa-gift" aria-hidden="true"></i>gift</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/glass"><i class="fa fa-glass" aria-hidden="true"></i>glass</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/globe"><i class="fa fa-globe" aria-hidden="true"></i>globe</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/graduation-cap"><i class="fa fa-graduation-cap" aria-hidden="true"></i>graduation-cap</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/users"><i class="fa fa-group" aria-hidden="true"></i>group <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hand-rock-o"><i class="fa fa-hand-grab-o" aria-hidden="true"></i>hand-grab-o <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hand-lizard-o"><i class="fa fa-hand-lizard-o" aria-hidden="true"></i>hand-lizard-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hand-paper-o"><i class="fa fa-hand-paper-o" aria-hidden="true"></i>hand-paper-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hand-peace-o"><i class="fa fa-hand-peace-o" aria-hidden="true"></i>hand-peace-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hand-pointer-o"><i class="fa fa-hand-pointer-o" aria-hidden="true"></i>hand-pointer-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hand-rock-o"><i class="fa fa-hand-rock-o" aria-hidden="true"></i>hand-rock-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hand-scissors-o"><i class="fa fa-hand-scissors-o" aria-hidden="true"></i>hand-scissors-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hand-spock-o"><i class="fa fa-hand-spock-o" aria-hidden="true"></i>hand-spock-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hand-paper-o"><i class="fa fa-hand-stop-o" aria-hidden="true"></i>hand-stop-o <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/handshake-o"><i class="fa fa-handshake-o" aria-hidden="true"></i>handshake-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/deaf"><i class="fa fa-hard-of-hearing" aria-hidden="true"></i>hard-of-hearing <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hashtag"><i class="fa fa-hashtag" aria-hidden="true"></i>hashtag</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hdd-o"><i class="fa fa-hdd-o" aria-hidden="true"></i>hdd-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/headphones"><i class="fa fa-headphones" aria-hidden="true"></i>headphones</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/heart"><i class="fa fa-heart" aria-hidden="true"></i>heart</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/heart-o"><i class="fa fa-heart-o" aria-hidden="true"></i>heart-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/heartbeat"><i class="fa fa-heartbeat" aria-hidden="true"></i>heartbeat</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/history"><i class="fa fa-history" aria-hidden="true"></i>history</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/home"><i class="fa fa-home" aria-hidden="true"></i>home</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bed"><i class="fa fa-hotel" aria-hidden="true"></i>hotel <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hourglass"><i class="fa fa-hourglass" aria-hidden="true"></i>hourglass</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hourglass-start"><i class="fa fa-hourglass-1" aria-hidden="true"></i>hourglass-1 <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hourglass-half"><i class="fa fa-hourglass-2" aria-hidden="true"></i>hourglass-2 <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hourglass-end"><i class="fa fa-hourglass-3" aria-hidden="true"></i>hourglass-3 <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hourglass-end"><i class="fa fa-hourglass-end" aria-hidden="true"></i>hourglass-end</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hourglass-half"><i class="fa fa-hourglass-half" aria-hidden="true"></i>hourglass-half</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hourglass-o"><i class="fa fa-hourglass-o" aria-hidden="true"></i>hourglass-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/hourglass-start"><i class="fa fa-hourglass-start" aria-hidden="true"></i>hourglass-start</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/i-cursor"><i class="fa fa-i-cursor" aria-hidden="true"></i>i-cursor</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/id-badge"><i class="fa fa-id-badge" aria-hidden="true"></i>id-badge</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/id-card"><i class="fa fa-id-card" aria-hidden="true"></i>id-card</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/id-card-o"><i class="fa fa-id-card-o" aria-hidden="true"></i>id-card-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/picture-o"><i class="fa fa-image" aria-hidden="true"></i>image <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/inbox"><i class="fa fa-inbox" aria-hidden="true"></i>inbox</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/industry"><i class="fa fa-industry" aria-hidden="true"></i>industry</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/info"><i class="fa fa-info" aria-hidden="true"></i>info</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/info-circle"><i class="fa fa-info-circle" aria-hidden="true"></i>info-circle</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/university"><i class="fa fa-institution" aria-hidden="true"></i>institution <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/key"><i class="fa fa-key" aria-hidden="true"></i>key</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/keyboard-o"><i class="fa fa-keyboard-o" aria-hidden="true"></i>keyboard-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/language"><i class="fa fa-language" aria-hidden="true"></i>language</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/laptop"><i class="fa fa-laptop" aria-hidden="true"></i>laptop</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/leaf"><i class="fa fa-leaf" aria-hidden="true"></i>leaf</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/gavel"><i class="fa fa-legal" aria-hidden="true"></i>legal <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/lemon-o"><i class="fa fa-lemon-o" aria-hidden="true"></i>lemon-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/level-down"><i class="fa fa-level-down" aria-hidden="true"></i>level-down</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/level-up"><i class="fa fa-level-up" aria-hidden="true"></i>level-up</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/life-ring"><i class="fa fa-life-bouy" aria-hidden="true"></i>life-bouy <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/life-ring"><i class="fa fa-life-buoy" aria-hidden="true"></i>life-buoy <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/life-ring"><i class="fa fa-life-ring" aria-hidden="true"></i>life-ring</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/life-ring"><i class="fa fa-life-saver" aria-hidden="true"></i>life-saver <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/lightbulb-o"><i class="fa fa-lightbulb-o" aria-hidden="true"></i>lightbulb-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/line-chart"><i class="fa fa-line-chart" aria-hidden="true"></i>line-chart</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/location-arrow"><i class="fa fa-location-arrow" aria-hidden="true"></i>location-arrow</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/lock"><i class="fa fa-lock" aria-hidden="true"></i>lock</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/low-vision"><i class="fa fa-low-vision" aria-hidden="true"></i>low-vision</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/magic"><i class="fa fa-magic" aria-hidden="true"></i>magic</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/magnet"><i class="fa fa-magnet" aria-hidden="true"></i>magnet</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/share"><i class="fa fa-mail-forward" aria-hidden="true"></i>mail-forward <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/reply"><i class="fa fa-mail-reply" aria-hidden="true"></i>mail-reply <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/reply-all"><i class="fa fa-mail-reply-all" aria-hidden="true"></i>mail-reply-all <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/male"><i class="fa fa-male" aria-hidden="true"></i>male</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/map"><i class="fa fa-map" aria-hidden="true"></i>map</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/map-marker"><i class="fa fa-map-marker" aria-hidden="true"></i>map-marker</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/map-o"><i class="fa fa-map-o" aria-hidden="true"></i>map-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/map-pin"><i class="fa fa-map-pin" aria-hidden="true"></i>map-pin</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/map-signs"><i class="fa fa-map-signs" aria-hidden="true"></i>map-signs</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/meh-o"><i class="fa fa-meh-o" aria-hidden="true"></i>meh-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/microchip"><i class="fa fa-microchip" aria-hidden="true"></i>microchip</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/microphone"><i class="fa fa-microphone" aria-hidden="true"></i>microphone</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/microphone-slash"><i class="fa fa-microphone-slash" aria-hidden="true"></i>microphone-slash</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/minus"><i class="fa fa-minus" aria-hidden="true"></i>minus</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/minus-circle"><i class="fa fa-minus-circle" aria-hidden="true"></i>minus-circle</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/minus-square"><i class="fa fa-minus-square" aria-hidden="true"></i>minus-square</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/minus-square-o"><i class="fa fa-minus-square-o" aria-hidden="true"></i>minus-square-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/mobile"><i class="fa fa-mobile" aria-hidden="true"></i>mobile</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/mobile"><i class="fa fa-mobile-phone" aria-hidden="true"></i>mobile-phone <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/money"><i class="fa fa-money" aria-hidden="true"></i>money</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/moon-o"><i class="fa fa-moon-o" aria-hidden="true"></i>moon-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/graduation-cap"><i class="fa fa-mortar-board" aria-hidden="true"></i>mortar-board <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/motorcycle"><i class="fa fa-motorcycle" aria-hidden="true"></i>motorcycle</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/mouse-pointer"><i class="fa fa-mouse-pointer" aria-hidden="true"></i>mouse-pointer</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/music"><i class="fa fa-music" aria-hidden="true"></i>music</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bars"><i class="fa fa-navicon" aria-hidden="true"></i>navicon <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/newspaper-o"><i class="fa fa-newspaper-o" aria-hidden="true"></i>newspaper-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/object-group"><i class="fa fa-object-group" aria-hidden="true"></i>object-group</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/object-ungroup"><i class="fa fa-object-ungroup" aria-hidden="true"></i>object-ungroup</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/paint-brush"><i class="fa fa-paint-brush" aria-hidden="true"></i>paint-brush</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/paper-plane"><i class="fa fa-paper-plane" aria-hidden="true"></i>paper-plane</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/paper-plane-o"><i class="fa fa-paper-plane-o" aria-hidden="true"></i>paper-plane-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/paw"><i class="fa fa-paw" aria-hidden="true"></i>paw</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/pencil"><i class="fa fa-pencil" aria-hidden="true"></i>pencil</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/pencil-square"><i class="fa fa-pencil-square" aria-hidden="true"></i>pencil-square</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/pencil-square-o"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>pencil-square-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/percent"><i class="fa fa-percent" aria-hidden="true"></i>percent</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/phone"><i class="fa fa-phone" aria-hidden="true"></i>phone</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/phone-square"><i class="fa fa-phone-square" aria-hidden="true"></i>phone-square</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/picture-o"><i class="fa fa-photo" aria-hidden="true"></i>photo <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/picture-o"><i class="fa fa-picture-o" aria-hidden="true"></i>picture-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/pie-chart"><i class="fa fa-pie-chart" aria-hidden="true"></i>pie-chart</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/plane"><i class="fa fa-plane" aria-hidden="true"></i>plane</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/plug"><i class="fa fa-plug" aria-hidden="true"></i>plug</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/plus"><i class="fa fa-plus" aria-hidden="true"></i>plus</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/plus-circle"><i class="fa fa-plus-circle" aria-hidden="true"></i>plus-circle</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/plus-square"><i class="fa fa-plus-square" aria-hidden="true"></i>plus-square</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/plus-square-o"><i class="fa fa-plus-square-o" aria-hidden="true"></i>plus-square-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/podcast"><i class="fa fa-podcast" aria-hidden="true"></i>podcast</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/power-off"><i class="fa fa-power-off" aria-hidden="true"></i>power-off</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/print"><i class="fa fa-print" aria-hidden="true"></i>print</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/puzzle-piece"><i class="fa fa-puzzle-piece" aria-hidden="true"></i>puzzle-piece</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/qrcode"><i class="fa fa-qrcode" aria-hidden="true"></i>qrcode</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/question"><i class="fa fa-question" aria-hidden="true"></i>question</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/question-circle"><i class="fa fa-question-circle" aria-hidden="true"></i>question-circle</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/question-circle-o"><i class="fa fa-question-circle-o" aria-hidden="true"></i>question-circle-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/quote-left"><i class="fa fa-quote-left" aria-hidden="true"></i>quote-left</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/quote-right"><i class="fa fa-quote-right" aria-hidden="true"></i>quote-right</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/random"><i class="fa fa-random" aria-hidden="true"></i>random</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/recycle"><i class="fa fa-recycle" aria-hidden="true"></i>recycle</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/refresh"><i class="fa fa-refresh" aria-hidden="true"></i>refresh</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/registered"><i class="fa fa-registered" aria-hidden="true"></i>registered</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/times"><i class="fa fa-remove" aria-hidden="true"></i>remove <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bars"><i class="fa fa-reorder" aria-hidden="true"></i>reorder <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/reply"><i class="fa fa-reply" aria-hidden="true"></i>reply</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/reply-all"><i class="fa fa-reply-all" aria-hidden="true"></i>reply-all</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/retweet"><i class="fa fa-retweet" aria-hidden="true"></i>retweet</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/road"><i class="fa fa-road" aria-hidden="true"></i>road</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/rocket"><i class="fa fa-rocket" aria-hidden="true"></i>rocket</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/rss"><i class="fa fa-rss" aria-hidden="true"></i>rss</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/rss-square"><i class="fa fa-rss-square" aria-hidden="true"></i>rss-square</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/bath"><i class="fa fa-s15" aria-hidden="true"></i>s15 <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/search"><i class="fa fa-search" aria-hidden="true"></i>search</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/search-minus"><i class="fa fa-search-minus" aria-hidden="true"></i>search-minus</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/search-plus"><i class="fa fa-search-plus" aria-hidden="true"></i>search-plus</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/paper-plane"><i class="fa fa-send" aria-hidden="true"></i>send <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/paper-plane-o"><i class="fa fa-send-o" aria-hidden="true"></i>send-o <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/server"><i class="fa fa-server" aria-hidden="true"></i>server</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/share"><i class="fa fa-share" aria-hidden="true"></i>share</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/share-alt"><i class="fa fa-share-alt" aria-hidden="true"></i>share-alt</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/share-alt-square"><i class="fa fa-share-alt-square" aria-hidden="true"></i>share-alt-square</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/share-square"><i class="fa fa-share-square" aria-hidden="true"></i>share-square</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/share-square-o"><i class="fa fa-share-square-o" aria-hidden="true"></i>share-square-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/shield"><i class="fa fa-shield" aria-hidden="true"></i>shield</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/ship"><i class="fa fa-ship" aria-hidden="true"></i>ship</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/shopping-bag"><i class="fa fa-shopping-bag" aria-hidden="true"></i>shopping-bag</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/shopping-basket"><i class="fa fa-shopping-basket" aria-hidden="true"></i>shopping-basket</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/shopping-cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i>shopping-cart</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/shower"><i class="fa fa-shower" aria-hidden="true"></i>shower</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sign-in"><i class="fa fa-sign-in" aria-hidden="true"></i>sign-in</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sign-language"><i class="fa fa-sign-language" aria-hidden="true"></i>sign-language</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sign-out"><i class="fa fa-sign-out" aria-hidden="true"></i>sign-out</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/signal"><i class="fa fa-signal" aria-hidden="true"></i>signal</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sign-language"><i class="fa fa-signing" aria-hidden="true"></i>signing <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sitemap"><i class="fa fa-sitemap" aria-hidden="true"></i>sitemap</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sliders"><i class="fa fa-sliders" aria-hidden="true"></i>sliders</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/smile-o"><i class="fa fa-smile-o" aria-hidden="true"></i>smile-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/snowflake-o"><i class="fa fa-snowflake-o" aria-hidden="true"></i>snowflake-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/futbol-o"><i class="fa fa-soccer-ball-o" aria-hidden="true"></i>soccer-ball-o <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sort"><i class="fa fa-sort" aria-hidden="true"></i>sort</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sort-alpha-asc"><i class="fa fa-sort-alpha-asc" aria-hidden="true"></i>sort-alpha-asc</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sort-alpha-desc"><i class="fa fa-sort-alpha-desc" aria-hidden="true"></i>sort-alpha-desc</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sort-amount-asc"><i class="fa fa-sort-amount-asc" aria-hidden="true"></i>sort-amount-asc</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sort-amount-desc"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i>sort-amount-desc</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sort-asc"><i class="fa fa-sort-asc" aria-hidden="true"></i>sort-asc</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sort-desc"><i class="fa fa-sort-desc" aria-hidden="true"></i>sort-desc</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sort-desc"><i class="fa fa-sort-down" aria-hidden="true"></i>sort-down <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sort-numeric-asc"><i class="fa fa-sort-numeric-asc" aria-hidden="true"></i>sort-numeric-asc</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sort-numeric-desc"><i class="fa fa-sort-numeric-desc" aria-hidden="true"></i>sort-numeric-desc</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sort-asc"><i class="fa fa-sort-up" aria-hidden="true"></i>sort-up <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/space-shuttle"><i class="fa fa-space-shuttle" aria-hidden="true"></i>space-shuttle</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/spinner"><i class="fa fa-spinner" aria-hidden="true"></i>spinner</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/spoon"><i class="fa fa-spoon" aria-hidden="true"></i>spoon</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/square"><i class="fa fa-square" aria-hidden="true"></i>square</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/square-o"><i class="fa fa-square-o" aria-hidden="true"></i>square-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/star"><i class="fa fa-star" aria-hidden="true"></i>star</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/star-half"><i class="fa fa-star-half" aria-hidden="true"></i>star-half</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/star-half-o"><i class="fa fa-star-half-empty" aria-hidden="true"></i>star-half-empty <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/star-half-o"><i class="fa fa-star-half-full" aria-hidden="true"></i>star-half-full <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/star-half-o"><i class="fa fa-star-half-o" aria-hidden="true"></i>star-half-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/star-o"><i class="fa fa-star-o" aria-hidden="true"></i>star-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sticky-note"><i class="fa fa-sticky-note" aria-hidden="true"></i>sticky-note</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sticky-note-o"><i class="fa fa-sticky-note-o" aria-hidden="true"></i>sticky-note-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/street-view"><i class="fa fa-street-view" aria-hidden="true"></i>street-view</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/suitcase"><i class="fa fa-suitcase" aria-hidden="true"></i>suitcase</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sun-o"><i class="fa fa-sun-o" aria-hidden="true"></i>sun-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/life-ring"><i class="fa fa-support" aria-hidden="true"></i>support <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/tablet"><i class="fa fa-tablet" aria-hidden="true"></i>tablet</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/tachometer"><i class="fa fa-tachometer" aria-hidden="true"></i>tachometer</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/tag"><i class="fa fa-tag" aria-hidden="true"></i>tag</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/tags"><i class="fa fa-tags" aria-hidden="true"></i>tags</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/tasks"><i class="fa fa-tasks" aria-hidden="true"></i>tasks</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/taxi"><i class="fa fa-taxi" aria-hidden="true"></i>taxi</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/television"><i class="fa fa-television" aria-hidden="true"></i>television</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/terminal"><i class="fa fa-terminal" aria-hidden="true"></i>terminal</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thermometer-full"><i class="fa fa-thermometer" aria-hidden="true"></i>thermometer <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thermometer-empty"><i class="fa fa-thermometer-0" aria-hidden="true"></i>thermometer-0 <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thermometer-quarter"><i class="fa fa-thermometer-1" aria-hidden="true"></i>thermometer-1 <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thermometer-half"><i class="fa fa-thermometer-2" aria-hidden="true"></i>thermometer-2 <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thermometer-three-quarters"><i class="fa fa-thermometer-3" aria-hidden="true"></i>thermometer-3 <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thermometer-full"><i class="fa fa-thermometer-4" aria-hidden="true"></i>thermometer-4 <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thermometer-empty"><i class="fa fa-thermometer-empty" aria-hidden="true"></i>thermometer-empty</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thermometer-full"><i class="fa fa-thermometer-full" aria-hidden="true"></i>thermometer-full</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thermometer-half"><i class="fa fa-thermometer-half" aria-hidden="true"></i>thermometer-half</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thermometer-quarter"><i class="fa fa-thermometer-quarter" aria-hidden="true"></i>thermometer-quarter</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thermometer-three-quarters"><i class="fa fa-thermometer-three-quarters" aria-hidden="true"></i>thermometer-three-quarters</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thumb-tack"><i class="fa fa-thumb-tack" aria-hidden="true"></i>thumb-tack</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thumbs-down"><i class="fa fa-thumbs-down" aria-hidden="true"></i>thumbs-down</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thumbs-o-down"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i>thumbs-o-down</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thumbs-o-up"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>thumbs-o-up</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/thumbs-up"><i class="fa fa-thumbs-up" aria-hidden="true"></i>thumbs-up</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/ticket"><i class="fa fa-ticket" aria-hidden="true"></i>ticket</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/times"><i class="fa fa-times" aria-hidden="true"></i>times</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/times-circle"><i class="fa fa-times-circle" aria-hidden="true"></i>times-circle</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/times-circle-o"><i class="fa fa-times-circle-o" aria-hidden="true"></i>times-circle-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/window-close"><i class="fa fa-times-rectangle" aria-hidden="true"></i>times-rectangle <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/window-close-o"><i class="fa fa-times-rectangle-o" aria-hidden="true"></i>times-rectangle-o <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/tint"><i class="fa fa-tint" aria-hidden="true"></i>tint</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/caret-square-o-down"><i class="fa fa-toggle-down" aria-hidden="true"></i>toggle-down <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/caret-square-o-left"><i class="fa fa-toggle-left" aria-hidden="true"></i>toggle-left <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/toggle-off"><i class="fa fa-toggle-off" aria-hidden="true"></i>toggle-off</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/toggle-on"><i class="fa fa-toggle-on" aria-hidden="true"></i>toggle-on</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/caret-square-o-right"><i class="fa fa-toggle-right" aria-hidden="true"></i>toggle-right <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/caret-square-o-up"><i class="fa fa-toggle-up" aria-hidden="true"></i>toggle-up <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/trademark"><i class="fa fa-trademark" aria-hidden="true"></i>trademark</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/trash"><i class="fa fa-trash" aria-hidden="true"></i>trash</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/trash-o"><i class="fa fa-trash-o" aria-hidden="true"></i>trash-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/tree"><i class="fa fa-tree" aria-hidden="true"></i>tree</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/trophy"><i class="fa fa-trophy" aria-hidden="true"></i>trophy</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/truck"><i class="fa fa-truck" aria-hidden="true"></i>truck</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/tty"><i class="fa fa-tty" aria-hidden="true"></i>tty</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/television"><i class="fa fa-tv" aria-hidden="true"></i>tv <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/umbrella"><i class="fa fa-umbrella" aria-hidden="true"></i>umbrella</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/universal-access"><i class="fa fa-universal-access" aria-hidden="true"></i>universal-access</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/university"><i class="fa fa-university" aria-hidden="true"></i>university</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/unlock"><i class="fa fa-unlock" aria-hidden="true"></i>unlock</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/unlock-alt"><i class="fa fa-unlock-alt" aria-hidden="true"></i>unlock-alt</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/sort"><i class="fa fa-unsorted" aria-hidden="true"></i>unsorted <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/upload"><i class="fa fa-upload" aria-hidden="true"></i>upload</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/user"><i class="fa fa-user" aria-hidden="true"></i>user</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/user-circle"><i class="fa fa-user-circle" aria-hidden="true"></i>user-circle</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/user-circle-o"><i class="fa fa-user-circle-o" aria-hidden="true"></i>user-circle-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/user-o"><i class="fa fa-user-o" aria-hidden="true"></i>user-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/user-plus"><i class="fa fa-user-plus" aria-hidden="true"></i>user-plus</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/user-secret"><i class="fa fa-user-secret" aria-hidden="true"></i>user-secret</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/user-times"><i class="fa fa-user-times" aria-hidden="true"></i>user-times</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/users"><i class="fa fa-users" aria-hidden="true"></i>users</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/address-card"><i class="fa fa-vcard" aria-hidden="true"></i>vcard <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/address-card-o"><i class="fa fa-vcard-o" aria-hidden="true"></i>vcard-o <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/video-camera"><i class="fa fa-video-camera" aria-hidden="true"></i>video-camera</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/volume-control-phone"><i class="fa fa-volume-control-phone" aria-hidden="true"></i>volume-control-phone</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/volume-down"><i class="fa fa-volume-down" aria-hidden="true"></i>volume-down</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/volume-off"><i class="fa fa-volume-off" aria-hidden="true"></i>volume-off</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/volume-up"><i class="fa fa-volume-up" aria-hidden="true"></i>volume-up</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/exclamation-triangle"><i class="fa fa-warning" aria-hidden="true"></i>warning <span class="text-muted">(alias)</span></a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/wheelchair"><i class="fa fa-wheelchair" aria-hidden="true"></i>wheelchair</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/wheelchair-alt"><i class="fa fa-wheelchair-alt" aria-hidden="true"></i>wheelchair-alt</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/wifi"><i class="fa fa-wifi" aria-hidden="true"></i>wifi</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/window-close"><i class="fa fa-window-close" aria-hidden="true"></i>window-close</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/window-close-o"><i class="fa fa-window-close-o" aria-hidden="true"></i>window-close-o</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/window-maximize"><i class="fa fa-window-maximize" aria-hidden="true"></i>window-maximize</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/window-minimize"><i class="fa fa-window-minimize" aria-hidden="true"></i>window-minimize</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/window-restore"><i class="fa fa-window-restore" aria-hidden="true"></i>window-restore</a></div>

        <div class="fa-hover col-md-3 col-sm-4"><a href-old="../icon/wrench"><i class="fa fa-wrench" aria-hidden="true"></i>wrench</a></div>

    </div>

</section>