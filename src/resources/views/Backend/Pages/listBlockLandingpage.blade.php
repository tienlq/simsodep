
@if(empty($newsId))
<h5 class="text_center">
    
    <br><br><br>
    Bạn phải khởi tạo landingpage 
    sau đó mới thực hiện thêm mới block được
    <br><br><br>
    <button type="button" data-dismiss="modal" aria-label="Close" class=" btn btn-primary">Đóng</button>
</h5>
@else
    <h5 class="text_center">
        <br>
        Vui lòng click chọn block landingpage ở bên dưới
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">× &nbsp;&nbsp;&nbsp;</span></button>
        <hr><br>
    </h5>
    @for($i=1;$i<=$totalLandingpages;$i++)
		
        <img class="img_template" 
             onclick="loadPopupLarge('{{ route('editLandingpage', [$newsId, $landType . $i, 0 ]) }}')"
             src="/img/template/{{ $landType . $i }}.png"/>
    @endfor
    <h5 class="text_center">
        <br><br>
        <button type="button" data-dismiss="modal" aria-label="Close" class=" btn btn-primary">Đóng</button>
    </h5>
@endif