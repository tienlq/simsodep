<div class="panel panel-primary">
    <div class="panel panel-heading">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
        <h4 id="myLargeModalLabel" class=" panel-title">{{ $cid == 0 ? 'Thêm mới danh mục':'Sửa danh mục' }}</h4>
    </div>
    <div class="modal-body ">
        <div class="card">
            <form id="form-register" 
                  action="{{ url('/admin/category/edit/'.$cid) }}"
                  name="registerForm" 
                  novalidate="" 
                  class="form-validate category-form" 
                  method="post" 
                  enctype="multipart/form-data">
                {{ csrf_field()}}
                <input type="hidden" name="type" value="{{ $type }}" />
                <div class="card-body">

                    <div class="row">
                        @foreach (Config::get('languages') as $lang => $language)
                        <div class="col-sm-6">
                            <div class="mda-form-group mb ">
                                <div class="mda-form-control">
                                    <textarea name="name_{{$lang}}"
                                              required="" 
                                              tabindex="0" 
                                              aria-required="true" 
                                              aria-invalid="true" 
                                              class="form-control">{{ $dataOfLang[$lang]->name or '' }}</textarea>
                                    <div class="mda-form-control-line"></div>
                                    <label>Tên {{ $language }}</label>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mda-form-group">
                                <div class="mda-form-control">
                                    <select name="route_name" class="form-control" required="">
                                        <option value="">Select type show</option>
                                        @foreach($routeName as $key => $value)
                                            <option  {{ isset($category->route_name) && $key == $category->route_name ? 'selected':'' }}
                                                value="{{ $key }}">
                                                {{ $value }}
                                            </option>
                                        @endforeach
                                </select>
                                <div class="mda-form-control-line"></div>
                                <label>Kiểu hiển thị</label>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-sm-6">
                        <div class="mda-form-group mb">
                            <div class="mda-form-control">
                                <select class="form-control" name="parent_id">
                                    <option value="">Cấp đầu</option>
                                    {!! $categoryHtmlOption !!}
                                </select>
                                <div class="mda-form-control-line"></div>
                                <label>Menu cha</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mda-form-group mb ">
                            <div class="mda-form-control">
                                <input name="sort_order"
                                       value="{{ $category->sort_order or '' }}"
                                       required="" 
                                       type="number"
                                       tabindex="0" 
                                       aria-required="true" 
                                       aria-invalid="true" 
                                       class="form-control">
                                <div class="mda-form-control-line"></div>
                                <label>Thứ tự sắp xếp</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mda-form-group mb">
                            <div class="mda-form-control">
                                <textarea name="seo_keyword" 
                                          rows="4" 
                                          aria-multiline="true" 
                                          tabindex="0" 
                                          aria-invalid="false" 
                                          class="form-control">{{ $category->seo_keyword or '' }}</textarea>
                                <div class="mda-form-control-line"></div>
                                <label>[SEO] Keyword</label>
                            </div>
                            <span class="mda-form-msg">Keyword tối đa 155 ký tự</span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mda-form-group mb">
                            <div class="mda-form-control">
                                <textarea name="seo_description" 
                                          rows="4" 
                                          aria-multiline="true" 
                                          tabindex="0" 
                                          aria-invalid="false" 
                                          class="form-control">{{ $category->seo_description or '' }}</textarea>
                                <div class="mda-form-control-line"></div>
                                <label>[SEO] Descrption</label>
                            </div>
                            <span class="mda-form-msg">Descrption nằm trong khoảng 70->155 ký tự</span>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="pull-left">
                        <button type="button" onclick="editCategory()" class="btn btn-primary">{{ $cid == 0 ? 'Thêm mới':'Cập nhật' }}</button>
                        <!--<button type="submit" class="">Submit</button>-->
                        <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" >Đóng</button>
                    </div> 

                </div>
                <div class="pull-left edit-category-result _success"></div>
            </div>
        </form>
    </div>
</div>
</div>