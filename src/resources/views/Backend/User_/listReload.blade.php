<div class="container-fluid">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Họ tên</th>
                <th>Vị trí</th>
                <th>Ngày sinh</th>
                <th>Tiếng nhật</th>
                <th>Tiếng anh</th>
                <th>Tùy chọn</th>
            </tr>
        </thead>.
        <tbody>
            <?php $idx = 0 ?>
            @foreach($listVacancy as $vacancy)
            <?php $idx++ ?>
            <tr>
                <td>{{ $idx }}</td>
                <td>{{ $vacancy->candidate }}</td>
                <td>{{ $functionArr[$vacancy->functionID] or 'N/A' }}</td>
                <td>{{ $vacancy->birthday }}</td>
                <td>{{ $japaneseArr[$vacancy->japaneseID] or 'N/A' }}</td>
                <td>{{ $englishArr[$vacancy->englishID] or 'N/A' }}</td>
                <td>
                    <a href="{{ url('jjob/vacancy-mapping/edit/'.$vacancy->id) }}">
                        <i data-pack="default" class="ion-edit"></i>
                    </a>
                    &nbsp;
                    <a onclick="deleteRow('/jjob/vacancy/delete/{{ $vacancy->id }}', '.list-vacancy', '/jjob/vacancy/manager')">
                        <i data-pack="default" class="ion-trash-a"></i>
                    </a>
                </td>
            </tr>
            @endforeach
            <tr>
                <td colspan="7">{!! $listVacancy ->render() !!}</td>
            </tr>
        </tbody>
    </table>
</div>