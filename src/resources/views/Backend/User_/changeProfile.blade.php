@extends('Backend.Layouts.main')

@section('title')
    Change password
@stop

@section('breadcrumb')
    Change password
@stop

@section('avatar')
@if(isset($user->avatar))
<a href="">
    <img src="{{ url($user->avatar) }}" alt="Profile" class="img-circle thumb64">
</a>
@endif
<div class="mt">Welcome, {{ $user->username }}</div>
@stop

@section('content')
<div class="container-fluid">
    <div class="card">

        <!-- Tab panes -->
        <div class="modal-body">
            <div class="card">
                <form id="form-register" 
                      action=""
                      name="registerForm" 
                      novalidate="" 
                      class="form-validate form-edit" 
                      method="post" 
                      enctype="multipart/form-data">
                    {{ csrf_field()}}
                    <div class="card-body">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab-01">
                                 <div class="row">
                                    <div class="col-sm-6">
                                        <div class="mda-form-group">
                                            <div class="mda-form-control">
                                                <input name="password"
                                                       type="password"
                                                       value=""
                                                       required=""
                                                       tabindex="0" 
                                                       aria-required="true" 
                                                       aria-invalid="true" 
                                                       placeholder=""
                                                       class="form-control input-sm">
                                                <div class="mda-form-control-line"></div>
                                                <label>Password</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane active" id="tab-01">
                                 <div class="row">
                                    <div class="col-sm-6">
                                        <div class="mda-form-group">
                                            <div class="mda-form-control">
                                                <input name="passwordConfirm"
                                                       type="password"
                                                       value=""
                                                       tabindex="0" 
                                                       required=""
                                                       aria-required="true" 
                                                       aria-invalid="true" 
                                                       class="form-control input-sm">
                                                <div class="mda-form-control-line"></div>
                                                <label>Password confirm</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="text-align: left">
                                <button type="submit" 
                                        class="btn btn-primary" >Cập nhật</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" >Close</button>
                                <div class="loading"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script>
            $('#datepicker-1').datepicker({
                container: '#example-datepicker-container-5'
            });

        </script>

    </div>
</div>

@stop



