@extends('Backend.Layouts.main')

@section('title')
Quản lý ứng viên
@stop

@section('breadcrumb')
Quản lý ứng viên
@stop

@section('avatar')
@if($user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar) }}" alt="Profile" class="img-circle thumb64">
</a>
@endif
<div class="mt">Welcome, {{ $user->username }}</div>

@stop

@section('content')
<div class="container-fluid">
    <div class="card">

        <div id="bootgrid-basic-header" class="bootgrid-header container-fluid">
            <div class="row">
                <!--include('Backend.Element.formSreachVacancy')-->
            </div>
        </div>
        <!--<div class="card-heading">Product management</div>-->
        <div class="table-responsive list-user" id="nestable">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Full name</th>
                        <th>Birthday</th>
                        <th>email</th>
                        <th>Tel</th>
                        <th>Option</th>
                    </tr>
                </thead>.
                <tbody>
                    @foreach($list as $u)
                    <tr class="_pointer">
                        <td data-toggle="modal" 
                            data-target="#myModal" 
                            class="_pointer"
                            onclick="loadData('.modal-content02', '/admin/user/detail/{{ $u->id }}')">
                            U-{{ $u->id }}</td>
                        
                        <td data-toggle="modal" 
                            data-target="#myModal" 
                            class="_pointer"
                            onclick="loadData('.modal-content02', '/admin/user/detail/{{ $u->id }}')">{{ $u->fullName }}</td>
                        
                        <td data-toggle="modal" 
                            data-target="#myModal" 
                            class="_pointer"
                            onclick="loadData('.modal-content02', '/admin/user/detail/{{ $u->id }}')">{{ $u->birthday }}</td>
                        
                        <td data-toggle="modal" 
                            data-target="#myModal" 
                            class="_pointer"
                            onclick="loadData('.modal-content02', '/admin/user/detail/{{ $u->id }}')">{{ $u->email }}</td>
                        
                        <td data-toggle="modal" 
                            data-target="#myModal" 
                            class="_pointer"
                            onclick="loadData('.modal-content02', '/admin/user/detail/{{ $u->id }}')">{{ $u->phone }}</td>
                        <td>
                            &nbsp;
                            <a href="{{ route('adminEditUser',[$u->id]) }}">
                                <i data-pack="default" class="ion-edit"></i>
                            </a>
                            &nbsp;
                            <a onclick="deleteRow('/admin/user/delete/{{ $u->id }}', '/admin/user/list')">
                                <i data-pack="default" class="ion-trash-a"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="7">{!! $list ->render() !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END row-->



@stop