@extends('Backend.Layouts.main')

@section('title')
{{ empty($user) ? 'Add new user':'Edit user' }}
@stop

@section('breadcrumb')
{{ empty($user) ? 'Add new user':'Edit user' }}
@stop

@section('addScript')
<script>
    $('#datepicker-1').datepicker({
        container: '#example-datepicker-container-5'
    });
</script>
@stop

@section('content')

<div class="modal-body fix-content-detail">
    <div class="card main-content-detail-vacancy">
        <div class="card-body main-content-detail-vacancy">
            <h3 class="title-04">
                {{ empty($user) ? 'Add new user':'Edit user' }}
            </h3>
            <div class="row table-vacancy">
                <form id="form-user" 
                        action="{{ url()->current() }}"
                        method="post"
                        name="form.formValidate" 
                        novalidate="" 
                        enctype="multipart/form-data"
                        class="form-validate form-horizontal">
                    {{ csrf_field()}}
                    <table class="table-bordered table-vacancy">
                        <tr>
                            <td>ID</td>
                            <td>
                                <input name="title" disabled="" value="{{ !empty($userEdit) ? str_pad($userEdit->uid, 5, '0', STR_PAD_LEFT):'' }}" class="form-control">
                            </td>
                            <td>Full Name</td>
                            <td><input name="fullName" class="form-control" type="text" value="{{ $userEdit->fullName or '' }}"/></td>
                            <td>Input&nbsp;Date</td>
                            <td><input class="form-control" type="text" disabled="disable" value="{{ $userEdit->created_at or date('Y-m-d H:i:s') }}"/></td>
                            <td>Update&nbsp;Date</td>
                            <td><input class="form-control" type="text" disabled="disable" value="{{ $userEdit->created_at or date('Y-m-d H:i:s') }}"/></td>
                        </tr>
                        <tr>
                            <td>Username</td>
                            <td>
                                <input name="username"
                                    value="{{ $userEdit->username or '' }}"
                                    required="" 
                                    {{ isset($_GET['changeProfile']) && intval($_GET['changeProfile']) == 1 ? 'readonly':'' }}
                                    tabindex="0" 
                                    class="form-control"/>
                            </td>
                            <td>DOB</td>
                            <td>
                                <input name="birthday" value="{{ $userEdit->birthday or '' }}" data-date-format="yyyy-mm-dd" id="datepicker-1" autocomplete="off" class="form-control datepicker"/>
                            </td>
                            <td>Password&nbsp;confirm</td>
                            <td>
                                <input name="password"
                                    type="password"
                                    value=""
                                    tabindex="0" 
                                    aria-required="true" 
                                    aria-invalid="true" 
                                    placeholder="Bỏ trống nếu không thay đổi"
                                    class="form-control input-sm"/>
                            </td>
                            <td>Re password</td>
                            <td>
                                <input name="passwordConfirm"
                                    type="password"
                                    value=""
                                    tabindex="0" 
                                    aria-required="true" 
                                    aria-invalid="true" 
                                    class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>
                                <input name="email" value="{{ $userEdit->email or '' }}" class="form-control input-sm"/>
                            </td>
                            <td>{{ \Auth::user()->groupID == 1 ? 'Group':'' }}</td>
                            <td>
                                @if(\Auth::user()->groupID == 1)
                                    <select name="groupID" onchange="changePermisson(this, '{{ url('admin/group/get-permission') }}')" class="form-control input-sm">
                                        <option value="0">Please choose</option>
                                        @foreach($listGroup as $group)
                                            <?php 
                                            if(isset($userEdit->groupID) && $group->id == $userEdit->groupID) {
                                                $selected = ' selected ';
                                            } else {
                                                $selected ='';
                                            }
                                            ?>
                                            <option {{ $selected }} value="{{ $group->id }}">{{ $group->name }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </td>
                            <td>Skype</td>
                            <td>
                                <input name="skype" value="{{ $userEdit->skype or '' }}"  class="form-control input-sm"/>
                            </td>
                            <td>Phone</td>
                            <td>
                                <input name="phone" type="number" value="{{ $userEdit->phone or '' }}" tabindex="0" class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td colspan="3">
                                <input name="address" value="{{ $userEdit->address or '' }}" class="form-control">
                            </td>
                            <td>{{ \Auth::user()->groupID == 1 ? 'Salary':'' }}</td>
                            <td colspan="3">
                                @if(\Auth::user()->groupID == 1)
                                    <input name="salary" value="{{ $userEdit->salary or '' }}"  class="form-control input-sm"/>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Note</td>
                            <td colspan="3">
                                <textarea name="address" tabindex="0" class="form-control textarea-des">{{ $userEdit->note or '' }}</textarea>
                            </td>
                            <td></td>
                            <td colspan="3">
                                
                            </td>
                        </tr>
                        
                    </table>
                    <fieldset>
                        <div class="clearfix">
                            <div class="pull-left">
                                <a onclick="formEditCustomer(this)" class="btn btn-primary">{{ empty($user) ? 'Add new':'Update' }}</a>
                                <a href="{{ route('adminListUser') }}" class="btn btn-default">Close</a>
                                <div class="submit_result"></div>
                            </div>
                            <br/>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

