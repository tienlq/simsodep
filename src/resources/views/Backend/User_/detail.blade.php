<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Orders member</h4>
</div>

<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <div class="card-body">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab-01">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>Code member</td>
                                <td>U-{{ $detailUser->id or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>Full name</td>
                                <td>{{ $detailUser->fullName or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>Email</td>
                                <td>{{ $detailUser->email or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>City</td>
                                <td>{{ $detailUser->city or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>Zip Code</td>
                                <td>{{ $detailUser->zipcode or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>Country</td>
                                <td>{{ $country->countryName or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>Tel</td>
                                <td>{{ $detailUser->phoneNumber or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-chevron-down mr"></a>State/Region</td>
                                <td>{{ $detailUser->region or '' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>