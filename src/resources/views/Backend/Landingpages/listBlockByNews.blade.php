<div class="container-fluid">
    <div class="js-nestable-action">
        <a data-action="expand-all" class="btn btn-default btn-sm mr-sm">mở rộng</a>
        <a data-action="collapse-all" class="btn btn-default btn-sm">Thu gọn</a>
        <!--<a class="btn btn-primary btn-sm" onclick="updateSequenceCategory()">Cập nhật lại thứ tự danh mục</a>-->
        <a class="btn btn-primary btn-sm" onclick="updateSoftOrder('{{ \TblName::CATEGORY }}')">Cập nhật lại thứ tự danh mục</a>
        <a class="btn btn-primary btn-sm" onclick="loadPopupLarge('{{ route('editCategory',[0]) }}?type={{ isset($_GET['type']) ? $_GET['type']:'' }}')" data-toggle="modal" data-target=".bs-modal-lg">Thêm mới</a>
        <a class="btn btn-primary btn-sm" onclick="loadContent('#nestable', '{{ url('admin/category/content-lst-cate') }}')">Reload</a>

        <div class="btn-group mb-sm">
            <button type="button" data-toggle="dropdown" 
                    class="btn dropdown-toggle ripple btn-info" 
                    aria-expanded="false">
                {{ !empty($_GET['typeShow']) ? unserialize(ROUTE_NAME)[$_GET['typeShow']]:'Chọn loại category' }}
                <span class="caret"></span>
                <span class="md-ripple" style="width: 0px; height: 0px; margin-top: -27.5px; margin-left: -27.5px;"></span></button>
            <ul role="menu" class="dropdown-menu">
                @foreach(unserialize(ROUTE_NAME) as $key => $value)
                    @if(!empty($_GET['typeShow']) && $_GET['typeShow'] == $key)
                    <li><b>{{ $value }}</b></li>
                    @else
                        <li><a href="?typeShow={{ $key }}">{{ $value }}</a></li>
                    @endif
                    
                @endforeach
            </ul>
        </div>


        <div class="loader-primary _success status-update"></div>
        <div class="row">
            <div class="col-md-6">
                <div id="nestable" class="dd">
                    {!! $htmlListCategory !!}
                </div>
                <div id="nestable-output" class="well hidden"></div>
            </div>
        </div>
    </div>
</div>