<div class="panel panel-primary">
    <div class="panel panel-heading">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
        <h4 id="myLargeModalLabel" class=" panel-title">{{ empty($id) ? 'Thêm mới block ' . substr($landId, 1):'Sửa block ' . substr($landId, 1) }}</h4>
    </div>
    <div class="modal-body ">
        <div class="card">
            <form id="form-register" 
                  action="{{ route('editLandingpage', [$newsId, $landId, $id ]) }}"
                  name="registerForm" 
                  novalidate="" 
                  class="form-validate form-edit" 
                  method="post" 
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card-body">    
                    @for($i=1; $i<=7; $i++)

                        @if($i == substr($landId, 1)) 

                            @include('Backend.Elements.Landingpage.template01.formTemplate' . $landId)

                        @endif

                    @endfor
                    
                    <div class="clearfix"></div>
                    
                    <div class="row">
                        <div class="pull-left">
                            <button type="button" class="btn btn-primary btn-submit">{{ $id == 0 ? 'Thêm mới':'Cập nhật' }}</button>
                            <button type="submit" class="">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" >Đóng</button>
                            <div class="result-submit"></div>
                        </div> 
                    </div>
                    
                </div>
                
            </form>
        </div>
    </div>
</div>
<script>
var classReload = '.main_block';
var urlReload = '';
</script>
<script src="/backend/js/submitPopupForm.js"></script>