<div class="panel panel-primary">
    <div class="panel panel-heading">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
        <h4 id="myLargeModalLabel" class=" panel-title">
            {{ intval($id) == 0 ? 'Thêm mới địa chỉ':'Sửa địa chỉ' }}
        </h4>
    </div>
    <div class="modal-body ">
        <div class="card">
            <form id="form-register" 
                  action="{{ url('admin/address/edit/'.$id) }}"
                  name="registerForm" 
                  novalidate="" 
                  class="form-validate edit-form" 
                  method="post" 
                  enctype="multipart/form-data">
                {{ csrf_field()}}
                <div class="card-body">
                    <div class="col-sm-9">
                        <div class="mda-form-group mb ">
                            <label>Tiêu đề:</label>
                            <div class="">
                                <input name="title"
                                       value="{{ $address->title or '' }}"
                                       required="" 
                                       tabindex="0" 
                                       aria-required="true" 
                                       aria-invalid="true" 
                                       class="form-control">
                                <div class="mda-form-control-line"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="mda-form-group mb ">
                            <label>Địa chỉ Cở sở:</label>
                            <div class="">
                                <textarea name="name"
                                       value=""
                                       style="height: 100px"
                                       required=""  
                                       class="form-control">{{ $address->name or '' }}</textarea>
                                <div class="mda-form-control-line"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="pull-left">
                            <button type="button" onclick="submitForm('.edit-result', '.edit-form')" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" >Close</button>
                        </div> 
                        <div class="pull-left edit-result _success"></div>
                    </div>
                   
                </div>
            </form>
        </div>
    </div>
</div>