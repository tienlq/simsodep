@extends('Backend.Layouts.main')

@section('title')
    Quản lý các cơ sở của himalaya
@stop

@section('breadcrumb')
    Quản lý các cơ sở của himalaya
@stop

@section('avatar')
    @if($user->avatar != '')
        <a href="">
            <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
        </a>
        <div class="mt">Welcome, {{ $user->username }}</div>
    @endif
@stop

@section('content')
<div class="container-fluid">
    
    
    <div class="js-nestable-action">
        <a class="btn btn-primary btn-sm" onclick="updateSoftOrder('address')">Cập nhật số thứ tự</a>
        <a class="btn btn-primary btn-sm" 
           onclick="loadPopupLarge('/admin/address/edit/0')" 
           data-toggle="modal" 
           data-target=".bs-modal-lg">Thêm mới</a>
        <div class="loader-primary _success status-update"></div>
        <div class="row">
            <div class="col-md-6">
                <div id="nestable" class="dd">
                    {!! $htmlList !!}
                </div>
                <div id="nestable-output" class="well hidden"></div>
            </div>
        </div>
    </div>
</div>
@stop