@extends('Backend.Layouts.main')

@section('title')
Sửa hình ảnh
@stop

@section('breadcrumb')
Sửa hình ảnh
@stop

@section('avatar')
@if($user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
</a>
<div class="mt">Welcome, {{ $user->username }}</div>
@endif
@stop

@section('script')
<!--ckeditor-->
<script src="{{ url('backend/vendor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ url('backend/vendor/ckeditor/samples/js/sample.js') }}"></script>
<script>
initSample();
</script>

@stop

@section('content')
<div class="container container-lg">
    <div class="card">
        <!--        <div class="card-heading">
                    <div class="card-title">Form edit product</div>
                </div>-->
        <form id="form-register" 
              name="registerForm" 
              novalidate="" 
              action=""
              class="form-validate form-edit" 
              method="post" 
              enctype="multipart/form-data">
            {{ csrf_field()}}
            <div class="card-body">
                <form id="form-register" 
                      action="{{ url('admin/images/edit/'.$id) }}"
                      name="registerForm" 
                      novalidate="" 
                      class="form-validate edit-form" 
                      method="post" 
                      enctype="multipart/form-data">
                    {{ csrf_field()}}
                    <div class="card-body">
                        <div class="col-sm-9">
                            <div class="mda-form-group mb ">
                                <label>Tiêu đề</label>
                                <div class="">
                                    <input name="title"
                                           value="{{ $image->title or '' }}"
                                           tabindex="0" 
                                           aria-required="true" 
                                           aria-invalid="true" 
                                           class="form-control">
                                    <div class="mda-form-control-line"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            @if(isset($image->linkImage) && $image->linkImage != '')
                            <div class="col-sm-4">
                                <!--<a onclick="" class="close">X</a>-->
                                <img class="img-cate-edit" src="{{ url($image->linkImage) }}">
                            </div>
                            @endif
                            <div class="col-sm-6">
                                <div class="mda-form-group mb">
                                    <label>Ảnh đại diện</label>
                                    <div>
                                        <input name="image" 
                                               type="file"
                                               tabindex="0"  
                                               class="form-control">
                                        <input type="hidden" name="hiddenImage" value="{{ $image->linkImage or '' }}"/>
                                        <div class="mda-form-control-line"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="mda-form-group mb ">
                                    <label>Link</label>
                                    <div class="">
                                        <input name="linkClick"
                                               value="{{ $image->linkClick or '' }}"
                                               required="" 
                                               tabindex="0" 
                                               aria-required="true" 
                                               aria-invalid="true" 
                                               class="form-control">
                                        <div class="mda-form-control-line"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="mda-form-group mb ">
                                    <label>Vị Trí hiển thị</label>
                                    <div class="">
                                        <select name="type" required="" >
                                            <option  value="">Chọn vị trí hiển thị</option>
                                            <option {{ isset($image->type) && $image->type == 'slide'?'selected="selected"':'' }} value="slide">Slide hình ảnh (1350 x 480px)</option>
                                            <option {{ isset($image->type) && $image->type == 'imgLeft'?'selected="selected"':'' }} value="run_left">Ảnh quảng cáo chạy bên trái (200 x 160 px)</option>
                                        </select>
                                        <div class="mda-form-control-line"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="mda-form-group mb ">
                                    <label>Mô tả</label>
                                    <div class="">
                                        <textarea name="description"
                                                  tabindex="0" 
                                                  aria-required="true" 
                                                  aria-invalid="true" 
                                                  class="form-control">{{ $image->description or '' }}</textarea>
                                        <div class="mda-form-control-line"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-9">
                                <div class="pull-left">
                                    <button type="submit"  class="btn btn-primary">Submit</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" >Close</button>
                                </div> 
                                <div class="pull-left edit-result _success"></div>
                            </div>
                        </div>
                    </div>
            </div>
        </form>


    </div>
</div>

@stop

