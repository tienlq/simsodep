@extends('Backend.Layouts.main')

@section('title')
Quản lý hình ảnh
@stop

@section('breadcrumb')
Quản lý hình ảnh
@stop

@section('avatar')
@if($user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
</a>
<div class="mt">Welcome, {{ $user->username }}</div>
@endif
@stop

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-heading">

        </div>
        <div class="table-responsive">
            <form id="form-register" 
                  name="registerForm" 
                  novalidate="" 
                  action=""
                  class="form-validate form-edit" 
                  method="get" 
                  enctype="multipart/form-data">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th colspan="2">
                                <select name="type" required="" class="form-control" >
                                    <option  value="">Lọc theo vị trí hiển thị</option>
                                    <option {{ isset($image->type) && $image->type == 'slide'?'selected="selected"':'' }} value="slide">Slide hình ảnh (1350 x 480px)</option>
                                    <option {{ isset($image->type) && $image->type == 'imgLeft'?'selected="selected"':'' }} value="run_left">Ảnh quảng cáo chạy bên trái (200 x 160 px)</option>
                                </select>
                            </th>
                            <th>
                                <input type="submit" value="Lọc dữ liệu" class="btn btn-primary"/>
                            </th>
                            <th>
                            <th>
                                <a href="/admin/images/edit/0" class="btn btn-primary">Thêm mới</a>
                            </th>
                        </tr>
                        <tr class="tr_title">
                            <th>#</th>
                            <th>Hình ảnh</th>
                            <th>Tiêu đề</th>
                            <th>Link</th>
                            <th>Tùy chọn</th>
                        </tr>
                    </thead>.
                    <tbody>
                        <?php $idx = 0 ?>
                        @foreach($listImage as $img)
                        <?php $idx++ ?>
                        <tr>
                            <td>{{ $idx }}</td>
                            <td>
                                <img class="list_img_pro" src="{{ url($img->linkImage) }}" />
                            </td>
                            <td>{{ $img->title }}</td>
                            <td>{{ $img->linkClick }}</td>
                            <td>
                                <a href="/admin/images/edit/{{ $img->id }}"><i data-pack="default" class="ion-edit"></i></a>
                                &nbsp;
                                <a onclick="deleteRow('/admin/images/delete/{{ $img->id }}')"><i data-pack="default" class="ion-trash-a"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="6">{!! $listImage->render() !!}</td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
<!-- END row-->



@stop