@extends('Backend.Layouts.main')

@section('title')
EDIT NEWS
@stop

@section('breadcrumb')
EDIT NEWS
@stop

@section('avatar')
    @if($user->avatar != '')
        <a href="">
            <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
        </a>
        <div class="mt">Welcome, {{ $user->username }}</div>
    @endif
@stop

@section('scriptAddon')
<!-- Summernote-->
<script src="{{ url('vendor/summernote/dist/summernote.js') }}"></script>

<script>
	var route_prefix = "{{ url(config('lfm.prefix')) }}";

	{!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}

	$('.lfm').filemanager('image', {prefix: route_prefix});
</script>

@stop

@section('content')
<div class="container container-lg">
    <div class="card">
        <form id="form-register" 
              name="registerForm" 
              novalidate="" 
              action=""
              class="form-validate" 
              method="post" 
              enctype="multipart/form-data">
            {{ csrf_field()}}
            <div class="card-body">

				<div class="row {{ $id == 1 ? '_hidden':'' }}">
					<!--<div class="col-sm-6">-->
					<div class="mda-form-group">
						<div class="mda-form-control">
							<label>Chọn kiểu nội dung hiển thị:</label>
							
							<br>
						
							
							</a>
							<div class="mda-form-control-line"></div>
						</div>
					</div>
					<!--</div>-->
                </div>
				
				<div class="row {{ $id == 1 ? '_hidden':'' }}">
					<div class="col-sm-6">
						<div class="mda-form-group">
							<div class="mda-form-control">
								<select name="category_id" class="form-control">
									<option value="">--- Chọn danh mục ---</option>
									{!! $category !!}
								</select>
								<div class="mda-form-control-line"></div>
								<label>Danh mục</label>
							</div>
						</div>
					</div>

					<div class="col-sm-6">
						<div class="mda-form-group">
							<div class="mda-form-control">
								<select name="type" class="form-control">
									<option value="">--- Chọn loại bài viết ---</option>
									@foreach(unserialize(NEWS_TYPE) as $key => $value)
									<option {{ !empty($news['vi']->type) && $news['vi']->type == $key ? 'selected':'' }} 
										value="{{ $key }}">{{ $value }}</option>
									@endforeach
								</select>
								<div class="mda-form-control-line"></div>
								<label>phân loại bài viết</label>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row {{ $id == 1 ? '_hidden':'' }}">
					<div class="col-md-6">
						<label>Hình ảnh</label>
						<div class="input-group">
							<span class="input-group-btn">
								<a data-input="thumbnail" data-preview="holder1" class="btn btn-primary lfm">
									<i class="fa fa-picture-o"></i> Chọn ảnh
								</a>
							</span>
							<input id="thumbnail" 
								   class="form-control" 
								   type="text" 
								   name="image" 
								   value="{{ $news['vi']->image or '' }}" />
						</div>
						@if(!empty($news['vi']->image))
						<img id="holder1" src="{{ $news['vi']->image }}" style="margin-top:15px;max-height:100px;"/>
						@else
						<img id="holder1" style="margin-top:15px;max-height:100px;"/>
						@endif
					</div>
				</div>
                
                @if($dataType == 'land02')

                    @include('Backend.Elements.News.editLandingpage02')

                @elseif($dataType == 'land01')

                    @include('Backend.Elements.News.editLandingpage01')

                @else

                    @include('Backend.Elements.News.editBasic')

                @endif

                <div class="clearfix">
                    <div class="pull-left">
                        <button type="submit" class="btn btn-primary">{{ empty($news['vi']->nId) ?'Thêm mới':'Cập nhật' }}</button>
                        <button type="submit" class="btn btn-default">Đóng</button>
                        <div class="submit_result"></div>
                    </div>
                    <br/>
                </div>
            </div>
        </form>
    </div>
</div>

@stop