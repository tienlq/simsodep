@extends('Backend.Layouts.main')

@section('title')
NEWS MANAGEMENT
@stop

@section('breadcrumb')
NEWS MANAGEMENT
@stop

@section('avatar')
@if($user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
</a>
<div class="mt">Welcome, {{ $user->username }}</div>
@endif
@stop

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-heading">

        </div>
        <div class="table-responsive">
            <form id="form-register" 
                  name="registerForm" 
                  novalidate="" 
                  action=""
                  class="form-validate form-edit" 
                  method="get" 
                  enctype="multipart/form-data">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th colspan="2">
                                <select name="categoryID" class="form-control" required="">
                                    <option value="0">--- Chọn danh mục ---</option>
                                    {!! $htmlSelectCategory !!}
                                </select>
                            </th>
                            <th>
                                <select name="type" class="form-control" required="">
                                    <option value="0">--- Chọn loại tin tức ---</option>
                                    @foreach(unserialize(NEWS_TYPE) as $key => $value)
                                        <option {{ !empty($_GET['type']) && $_GET['type'] == $key ? 'selected':'' }}  value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </th>
                            <th colspan="3">
                                <input type="submit" value="Lọc dữ liệu" class="btn btn-primary"/>
                            </th>
                            <th>
                                <a href="/admin/news/edit/0?type={{ !empty($_GET['type']) ? $_GET['type']:'' }}" class="btn btn-primary">Thêm mới</a>
                            </th>
                        </tr>
                        <tr class="tr_title">
                            <th>#</th>
                            <th>images</th>
                            <th>Tiêu đề</th>
                            <th>Category</th>
                            <th>Keyword</th>
                            <th>Description</th>
                            <th>option</th>
                        </tr>
                    </thead>.
                    <tbody>
                        <?php $idx = 0; ?>
                        @if(count($news) > 0)
                        @foreach($news as $n)
                        <?php $idx++ ?>
                        <tr>
                            <td>{{ $idx }}</td>
                            <td>
                                @if(isset($n->image) && $n->image!='')
                                <img class="list_img_pro" src="{{ url($n->image) }}" />
                                @endif
                            </td>
                            <td>{{ $n->title }}</td>
                            <td>{{ $cates[$n->category_id] or '' }}</td>
                            <td>{{ $n->keyword }}</td>
                            <td>{{ $n->description }}</td>
                            <td>
                                <a href="{{ url('admin/news/edit/'.$n->nId) }}"><i data-pack="default" class="ion-edit"></i></a>
                                &nbsp;
                                <a href="{{ url('admin/news/delete/'.$n->nId) }}"><i data-pack="default" class="ion-trash-a"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        <tr>
                            <td colspan="6">{!! $news->render() !!}</td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
<!-- END row-->



@stop