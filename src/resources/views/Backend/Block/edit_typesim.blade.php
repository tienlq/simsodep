@extends('Backend.Layouts.main')

@section('title')
CATEGORY MANAGEMENT
@stop

@section('breadcrumb')
Quản lý block
@stop

@section('avatar')
@if($user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
</a>
<div class="mt">Welcome, {{ $user->username }}</div>
@endif
@stop

@section('scriptAddon')
<script src="{{ url('vendor/summernote/dist/summernote.js') }}"></script>
<script>
var route_prefix = "{{ url(config('lfm.prefix')) }}";

{
    !! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}

$('#lfm').filemanager('image', {prefix: route_prefix});
</script>

@stop


@section('content')

<div class="panel panel-primary">
    <div class="modal-body">
        <div class="card">
            <form id="form-register" 
                  action="{{ url(route('editBlock',[$id])) }}"
                  name="registerForm" 
                  novalidate="" 
                  class="form-validate category-form" 
                  method="post" 
                  enctype="multipart/form-data">
                {{ csrf_field()}}
                <input name="type" type="hidden" value="{{ $type or '' }}" >
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Tên hiển thị</label>
                            <br/>
                            <input name="name[vi]" type="text" value="{{ $langData['vi']->name or '' }}" class="form-control" />
                            <br/>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Cách lọc dữ liệu </label>
                            <br/>
                            <select class="form-control" name="image" onchange="showDataType(this)">
                                @foreach(unserialize(TYPE_SEARCH_SIM) as $key => $value)
                                    <option {{ !empty($block->image) && $block->image ==$key ? 'selected="selected"':'' }} value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                            </select>
                            <br/>
                        </div>
                    </div>
                    <div class="row dieu-kien-search {{ !empty($block->image) && ($block->image ==2 || $block->image ==3) ? '_hidden':'' }}">
                        <div class="col-sm-6">
                            <label>Điều kiện tìm kiếm</label>
                            <br/>
                            <textarea style="height: 200px" name="description[vi]" class="form-control">{{ $langData['vi']->description or '' }}</textarea>
                            <br/>
                        </div>
                    </div>
                    <div class="row khoang-gia {{ !empty($block->image) && $block->image ==2 ? '':'_hidden' }}">
                        <div class="col-sm-6">
                            <div class="mda-form-group mb ">
                                <div class="mda-form-control">
                                    <input name="price01" type="text" autocomplete="off" value="{{ $block->price01 or '' }}" \>
                                    <div class="mda-form-control-line"></div>
                                    <label>Giá bắt đầu</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mda-form-group mb ">
                                <div class="mda-form-control">
                                    <input name="price02" type="text" autocomplete="off" value="{{ $block->price02 or '' }}" \>
                                    <div class="mda-form-control-line"></div>
                                    <label>Giá kết thúc</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row lien-ket-khac {{ !empty($block->image) && $block->image ==3 ? '':'_hidden' }}">
                        <div class="col-sm-6">
                            <div class="mda-form-group mb ">
                                <div class="mda-form-control">
                                    <input name="link" type="text" autocomplete="off" value="{{ $block->link or '' }}" \>
                                    <div class="mda-form-control-line"></div>
                                    <label>Đường dẫn khi click</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="pull-left">
                            <button type="submit" class="btn btn-primary">{{ $id == 0 ? 'Thêm mới':'Cập nhật' }}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" >Đóng</button>
                        </div> 
                    </div>
                    <div class="pull-left edit-category-result _success"></div>
                </div>
            </form>
        </div>
    </div>
</div>


@stop
