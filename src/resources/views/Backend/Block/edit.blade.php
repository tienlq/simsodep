@extends('Backend.Layouts.main')

@section('title')
CATEGORY MANAGEMENT
@stop

@section('breadcrumb')
Quản lý block
@stop

@section('avatar')
@if($user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
</a>
<div class="mt">Welcome, {{ $user->username }}</div>
@endif
@stop

@section('scriptAddon')
<script src="{{ url('vendor/summernote/dist/summernote.js') }}"></script>
<script>
    var route_prefix = "{{ url(config('lfm.prefix')) }}";

    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}

    $('#lfm').filemanager('image', {prefix: route_prefix});
</script>

@stop


@section('content')

<div class="panel panel-primary">
    <div class="modal-body">
        <div class="card">
            <form id="form-register" 
                  action="{{ url(route('editBlock',[$id])) }}"
                  name="registerForm" 
                  novalidate="" 
                  class="form-validate category-form" 
                  method="post" 
                  enctype="multipart/form-data">
                {{ csrf_field()}}
                <input name="type" type="hidden" value="{{ $type or '' }}" >
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mda-form-group mb ">
                                <div class="mda-form-control">
                                    <input name="link" type="text" autocomplete="off" value="{{ $block->link or '' }}" \>
                                    <div class="mda-form-control-line"></div>
                                    <label>Đường dẫn khi click</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($type != 'categoryJob')
                        <div class="row">
                            <div class="col-md-6">
                                <label>Hình ảnh</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                            <i class="fa fa-picture-o"></i> Chọn ảnh
                                        </a>
                                    </span>
                                    <input id="thumbnail" class="form-control" type="text" name="image" value="{{ $block->image or '' }}" />
                                </div>
                                @if(!empty($block->image))
                                    <img id="holder" src="{{ $block->image }}" style="margin-top:15px;max-height:100px;"/>
                                @else
                                    <img id="holder" style="margin-top:15px;max-height:100px;"/>
                                @endif
                            </div>
                        </div>
                    @endif
                    <div id="panelDemo14" class="panel">
                        <div class="panel-heading"><b>Vui lòng nhập nội cho cho tất cả các tab ngôn ngữ phía dưới</b></div>
                        <div class="panel-body">
                            <div>
                                <!-- Nav tabs-->
                                <ul role="tablist" class="nav nav-tabs">
                                    @foreach (Config::get('languages') as $lang => $language)
                                        <li role="presentation" class="{{ $lang == 'vi' ? 'active':'' }}">
                                            <a href="#tab_{{ $lang }}" aria-controls="home" role="tab" data-toggle="tab">Nội dung {{ $language }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <!-- Tab panes-->
                                <div class="tab-content">
                                    @foreach (Config::get('languages') as $lang => $language)
                                    <div id="tab_{{ $lang }}" role="tabpanel" 
                                         class="tab-pane {{ $lang == 'vi' ? 'active':'' }}">
                                        <div class="row">
                                            <div class="mda-form-group mb ">
                                                <div class="mda-form-control">
                                                    <textarea name="name[{{$lang}}]"
                                                              tabindex="0" 
                                                              aria-required="true" 
                                                              aria-invalid="true" 
                                                              class="form-control">{{ $langData[$lang]->name or '' }}</textarea>
                                                    <div class="mda-form-control-line"></div>
                                                    <label>Tên {{ $language }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="mda-form-group mb">
                                                <div class="mda-form-control">
                                                    <textarea name="description[{{$lang}}]" 
                                                              rows="4" 
                                                              aria-multiline="true" 
                                                              tabindex="0" 
                                                              aria-invalid="false" 
                                                              class="form-control summernote">{{ $langData[$lang]->description or '' }}</textarea>
                                                    <div class="mda-form-control-line"></div>
                                                    <label>Mô tả </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="pull-left">
                            <button type="submit" class="btn btn-primary">{{ $id == 0 ? 'Thêm mới':'Cập nhật' }}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" >Đóng</button>
                        </div> 
                    </div>
                    <div class="pull-left edit-category-result _success"></div>
                </div>
            </form>
        </div>
    </div>
</div>


@stop
