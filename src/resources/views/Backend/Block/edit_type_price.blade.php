@extends('Backend.Layouts.main')

@section('title')
Quản lý khoảng giá
@stop

@section('breadcrumb')
Quản lý khoảng giá
@stop

@section('avatar')
@if($user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
</a>
<div class="mt">Welcome, {{ $user->username }}</div>
@endif
@stop

@section('scriptAddon')
<script src="{{ url('vendor/summernote/dist/summernote.js') }}"></script>
<script>
var route_prefix = "{{ url(config('lfm.prefix')) }}";

{
    !! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}

$('#lfm').filemanager('image', {prefix: route_prefix});
</script>

@stop


@section('content')

<div class="panel panel-primary">
    <div class="modal-body">
        <div class="card">
            <form id="form-register" 
                  action="{{ url(route('editBlock',[$id])) }}"
                  name="registerForm" 
                  novalidate="" 
                  class="form-validate category-form" 
                  method="post" 
                  enctype="multipart/form-data">
                {{ csrf_field()}}
                <input name="type" type="hidden" value="{{ $type or '' }}" >
                <input name="image" type="hidden" value="2" >
                <div class="card-body">
                    <div class="row">
                        <div class="mda-form-group mb ">
                            <div class="mda-form-control">
                                <textarea name="name[vi]"
                                          tabindex="0" 
                                          aria-required="true" 
                                          aria-invalid="true" 
                                          class="form-control">{{ $langData['vi']->name or '' }}</textarea>
                                <div class="mda-form-control-line"></div>
                                <label>tiêu đề </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mda-form-group mb ">
                                <div class="mda-form-control">
                                    <input name="price01" type="text" autocomplete="off" value="{{ $block->price01 or '' }}" \>
                                    <div class="mda-form-control-line"></div>
                                    <label>Giá bắt đầu</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mda-form-group mb ">
                                <div class="mda-form-control">
                                    <input name="price02" type="text" autocomplete="off" value="{{ $block->price02 or '' }}" \>
                                    <div class="mda-form-control-line"></div>
                                    <label>Giá kết thúc</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">
                        <div class="pull-left">
                            <button type="submit" class="btn btn-primary">{{ $id == 0 ? 'Thêm mới':'Cập nhật' }}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" >Đóng</button>
                        </div> 
                    </div>
                    <div class="pull-left edit-category-result _success"></div>
                </div>
            </form>
        </div>
    </div>
</div>


@stop
