@extends('Backend.Layouts.main')

@section('title')
CATEGORY MANAGEMENT
@stop

@section('breadcrumb')
CATEGORY MANAGEMENT
@stop

@section('avatar')
    @if($user->avatar != '')
        <a href="">
            <img src="{{ url($user->avatar)}}" alt="Profile" class="img-circle thumb64">
        </a>
        <div class="mt">Welcome, {{ $user->username }}</div>
    @endif
@stop

@section('content')

<div class="container-fluid">
    <div class="js-nestable-action">
        <a data-action="expand-all" class="btn btn-default btn-sm mr-sm">mở rộng</a>
        <a data-action="collapse-all" class="btn btn-default btn-sm">Thu gọn</a>
        <a class="btn btn-primary btn-sm" onclick="updateSoftOrder('{{ \TblName::BLOCK }}')">Cập nhật lại thứ tự danh mục</a>
<!--        <a class="btn btn-primary btn-sm" 
		   onclick="loadPopupLarge('{{ route('editBlock', [0]) }}')" 
		   data-toggle="modal"
		   data-target=".bs-modal-lg">Thêm mới</a>-->
		<a class="btn btn-primary btn-sm" 
			href="{{ route('editBlock', [0]) }}?type={{ !empty($_GET['type']) ? $_GET['type']:'' }}">Thêm mới</a>
        <!--<a class="btn btn-primary btn-sm" onclick="loadContent('#nestable', '{{ url(route('adminListBlock')) }}?reload=1')">Reload</a>-->
        <div class="loader-primary _success status-update"></div>
        <div class="row">
            <div class="col-md-6">
                <div id="nestable" class="dd">
                    {!! $htmlList !!}
                </div>
                <div id="nestable-output" class="well hidden"></div>
            </div>
        </div>
    </div>
</div>
@stop