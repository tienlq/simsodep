@foreach (Config::get('languages') as $lang => $language)
@if ($lang != App::getLocale())
<li>
    <a href="{{ route('lang.switch', $lang) }}">{{$language}}</a>
</li>
@endif
@endforeach