
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://ogp.me/ns/fb#" lang="vi">

    <head>
        <title>{{ $meta['title'] or $config->title }}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>

        <link rel="stylesheet" href="/vendor/font-awesome/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="/vendor/ionicons/css/ionicons.css"/>
        <link rel="stylesheet" href="/vendor/bootstrap/dist/css/bootstrap.min.css"/>

        <link href="/frontend/css/style.css" rel="stylesheet" type="text/css" />
        <script src="{{ url('vendor/jquery/dist/jquery.js') }}"></script>
        <script type="text/javascript" src="/frontend/js/script.js"></script>

        <link href="/favicon.ico" rel="icon" type="image/x-icon" />
    </head>
    <body>

        <div id="main_wrapper">
            <header>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="header-right">

                            <div class="banner">
                                <a class="logo" href="">
                                    <img src="{{ $config->logo }}"/>
                                </a>
                                <img class="img-banner" src="{{ $config->background_pc }}"/>
                            </div>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="main-menu">
                                        <label for="show-menu" class="show-menu">
                                            <div id="show-menu-btn">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </div>Menu
                                        </label>
                                        <input type="checkbox" id="show-menu" role="button">
                                            {!! app('ClassCategory')->htmlMenuTop() !!}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </header>
            <div id="main">
                <article>
                    <form id="timkiem" name="timkiem" method="post" action="{{ route('postSearchSims') }}">
                        {{ csrf_field()}}
                        <div class="row">
                            <input class="search-input" name="keyword" type="tel" placeholder="Nhập số cần tìm" value="{{ $keyword or '' }}" autocomplete="off">
                                <button class="search-btn">
                                    <i class="ion-search fa fix-icon-font"></i>
                                    &nbsp;
                                    Tìm kiếm
                                </button>
                                <ul class="ul-search-des">
                                    <li><em>Tìm sim có số <strong>{{ $keyword or '6789' }}</strong> bạn hãy gõ <strong>{{ $keyword or '6789' }}</strong></em></li>
                                    <li><em>Tìm sim có đầu <strong>090 </strong>đuôi <strong>{{ $keyword or '5555' }}</strong> hãy gõ <strong>090*{{ $keyword or '5555' }}</strong></em></li>
                                    <li><em>Tìm sim bắt đầu bằng <strong>0914</strong> đuôi bất kỳ, hãy gõ:&nbsp;<strong>0914<font size="2">*</font></strong></em></li>
                                </ul>
                        </div>
                    </form>

                    @yield('content')

                    <script type="text/javascript">
                        function ctsim(sosim) {
                            sosim = sosim.replace(/\D/g, "");
                            document.getElementById("chitietsim").action = sosim;
                            document.getElementById("chitietsim").submit();
                        }
                    </script>
                    <br />
                </article>
                <nav>
                    @include('Frontend.Elements.Layout.simTheoGia')
                    @include('Frontend.Elements.Layout.simTheoNhaMang')
                    @include('Frontend.Elements.Layout.simTheoLoai')
                </nav>
                <aside>
                    @include('Frontend.Elements.Layout.banHangOnline')
                    @include('Frontend.Elements.Layout.donHangMoi')
                    @include('Frontend.Elements.Layout.news')
                </aside>
            </div>
            <footer>
                @include('Frontend.Elements.Layout.footer')
            </footer>
        </div>


        <a id="ads-left" style="position: fixed; top: 50px; left: 15px;" href="" target="_blank" rel="nofollow"></a>
        <a id="ads-right" style="position: fixed; top: 50px; right: 15px;" href="" target="_blank" rel="nofollow"></a>
        <script type="text/javascript">
            //var elem = document.createElement("img");
            //elem.setAttribute("src", "images/sim-phong-thuy-20180711.png");
            //elem.setAttribute("width", "185");
            //elem.setAttribute("alt", "sim thăng long");

            //document.getElementById("ads-left").appendChild(elem);
            //elem = document.createElement("img");
            //elem.setAttribute("src", "images/tra-gop-sim-so-dep.jpg");
            //elem.setAttribute("width", "150");
            //elem.setAttribute("alt", "sim thăng long");
            //document.getElementById("ads-right").appendChild(elem);

            document.body.style.background = "#f3f7fa";


        </script>

        <div class="hotlinebottom" id="hlbt">
            <a class="bottom-hotline" href="tel:{{ $config->phone }} ">
                <div id="phone"><span>&nbsp;</span></div>
                <strong style=" padding-left: 10px;">
                    <span class="red" style=" color: #fff; font-size: 18px; "> {{ app('ClassBlock')->getPhoneHotlineBySession() }} &nbsp;</span>
                </strong>
            </a>
        </div>

    </body>
</html>
