@extends('Frontend.Layouts.main')

@section('content')

<div id='showh' style='overflow: hidden;height:1500px;background-image: url(/lh4.googleusercontent.com/-h1XM8QoFULA/Wlh4Z78iu6I/AAAAAAAAFY0/eCZ5nLyeiNETKQg90m86PNyHaQtm43tNQCLcBGAs/s1600/daimond-brg.png);'>
	<div class='container_column' style='text-align: left;background-color: #ffff;padding: 15px;box-shadow: 0px 2px 5px #BBB;'>

		@include('Frontend.Elements.Home.block01')

		@include('Frontend.Elements.Home.block02')

		@include('Frontend.Elements.Home.block03')

		@include('Frontend.Elements.Home.block04')

		@include('Frontend.Elements.Home.block05')

		@include('Frontend.Elements.Home.block06')

		@include('Frontend.Elements.Home.block07')
	</div>
</div>

@stop