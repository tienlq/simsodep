@extends('Frontend.Layouts.main')



@section('content')


<section id="contactPage">
    <img src="public/images/Vinhomes-Sky-Lake_contact.jpg" class="backgroundCT visible-lg">
    <div class="intro-body container">
        <div class="col-sm-6 col-xs-12">
            <h3 class="text-left">FLC ECO CHARM DA NANG CO., LTD. </h3>
            <div class="">
                <div class="addressContact">
                    <img class="imgCt" src="public/images/address.png" style="">
                    <div style="margin-left: 33px;">
                        <p><span><strong>Địa chỉ:</strong> </span><span>Tầng 8 tòa nhà Sannam, số 78 Duy Tân , Phường Dịch Vọng Hậu, Quận Cầu giấy, Hà Nội.</span></p>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="clearfix"></div>
                <div class="EmailContact">
                    <img class="imgCt" src="public/images/Email.png" style="">
                    <div style="margin-left: 33px;">
                        <p><b>HOTLINE: </b> {{ $config->phone }}</p>
                    </div>
                </div>
                <div class="EmailContact">
                    <img class="imgCt" src="public/images/Email.png" style="">
                    <div style="margin-left: 33px;">
                        <p><b>EMAIL: </b> {{ $config->email }}</p>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="WebsiteContact">
                    <img class="imgCt" src="public/images/web.png" style="">
                    <div style="margin-left: 33px;">
                        <p><b>WEBSITE: </b> flcecocharmdanang.com.vn</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <h3 class="text-left">Để lại lời nhắn</h3>
            <div>
                <p style="color: #06a74c">{{$result or ''}}</p>
                <form class="form-horizontal" id="contactPageSub" action="{{ route('contact') }}" method="post">
                    {{ csrf_field()}}
                    <div class="form-group">
                        <label for="addressContact" class="col-sm-2 control-label">Họ tên</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" id="addressContact" class="form-control" id="" placeholder="" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="emailContact" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" name="email" id="emailContact" class="form-control" id="" placeholder="" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phoneContact" class="col-sm-2 control-label">Điện thoại</label>
                        <div class="col-sm-10">
                            <input type="text" name="phone" class="form-control" id="" placeholder="" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phoneContact" class="col-sm-2 control-label">Tiêu đề</label>
                        <div class="col-sm-10">
                            <input type="text" name="title" id="phoneContact" class="form-control" id="" placeholder="" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="messageContact" class="col-sm-2 control-label">Nội dung</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="messageContact" id="message" class="form-control" id="" placeholder=""></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="text-right col-sm-12">
                            <input type="submit" value="Gửi liên hệ" style="
                                    background: #4268b1;
                                    color: white;
                                    background-size: 100%;
                                    height: 40px;
                                    text-transform: uppercase;
                                    font-weight: bold;
                                    line-height: 10px;" class="btn"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>

<section id="goldLocaltion" class="section dark slider">
    <div class="col-sm-12 col-xs-12 fadeInLeft animated goldBox1 pull-left undefined visible">
        <div class="animated fadeInLeft visible" data-animation="fadeInLeft" id="goldLocationGGM" style="min-height: 400px; position: relative; overflow: hidden;"></div>
    </div>
</section>



@stop