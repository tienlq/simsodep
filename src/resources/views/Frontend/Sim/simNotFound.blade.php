@extends('Frontend.Layouts.main')

@section('content')

<article>
    <form style=" display: none; " id="timkiem" name="timkiem" method="post" onsubmit="return simple_search(this.tag.value, 'tim-sim/', '.html');">
        <div class="row">
            <input class="search-input" name="tag" type="tel" placeholder="Nhập số cần tìm" value="0973849666">
            <button class="search-btn">
                <svg width="15px" height="15px">
                <path d="M11.618 9.897l4.224 4.212c.092.09.1.23.02.312l-1.464 1.46c-.08.08-.222.072-.314-.02L9.868 11.66M6.486 10.9c-2.42 0-4.38-1.955-4.38-4.367 0-2.413 1.96-4.37 4.38-4.37s4.38 1.957 4.38 4.37c0 2.412-1.96 4.368-4.38 4.368m0-10.834C2.904.066 0 2.96 0 6.533 0 10.105 2.904 13 6.486 13s6.487-2.895 6.487-6.467c0-3.572-2.905-6.467-6.487-6.467 "></path>
                </svg> Tìm <span class="hide480">kiếm</span>
            </button>
            <ul style=" padding-left: 15px; ">
                <li>Tìm sim có số <strong>9666</strong> bạn hãy gõ <strong>9666</strong></li>
                <li>Tìm sim có đầu <strong>097 </strong>đuôi <strong>849666</strong> hãy gõ <strong>097<font size="2">*</font>849666</strong></li>
                <li>Tìm sim bắt đầu bằng <strong>097384</strong> đuôi bất kỳ, hãy gõ:&nbsp;<strong>097384<font size="2">*</font></strong></li>
            </ul>
        </div>
    </form>
    <center><img src="images/filter-loader.gif" name="loading_div" width="95" height="15" border="0" id="loading_div" style="display:none"></center>
    
    <h2 style=" font-size: 28px; color: #FF9800; ">Đường link bạn truy cập không tồn tại !</h2>
    
    @include('Frontend.Elements.Home.simDoanhNhan')

    @include('Frontend.Elements.Home.simKhuyenMaiTheoNgay')
    
    <br>
    <div align="center"><strong>&lt;&lt; <a href="javascript:history.back(-1)">Về trang trước</a> </strong></div>

    <br/>
</article>


@stop