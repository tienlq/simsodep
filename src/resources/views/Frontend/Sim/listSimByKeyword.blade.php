@extends('Frontend.Layouts.main')

@section('content') 

<div class="number_01 ">
    <h2>

        <i class="fa ion-briefcase icon-content"></i>
        &nbsp;
        @if(!empty($checkSupplier))
            <a target="_blank">Tim thấy <b>{{ (!empty($sims)) ? count($sims):'0' }}</b> kết quả với từ khoá <b>'{{ $keyword }}'</b></a>
        @else
            <a target="_blank">Tim thấy <b>{{ (!empty($sims)) ? count($sims):'0' }}</b> kết quả với từ khoá <b>'{{ $keyword }}'</b></a>
        @endif
    </h2>
</div>
<div id="wrapper" class="wrapper">
    @if(!empty($haveSupplier))
        <table width="100%" border="0" cellpadding="8" cellspacing="1" bgcolor="#FF0000">
            <tbody><tr align="right">
                    <td align="center" valign="top" bgcolor="#FFFFCC">
                        <strong>
                            Sim dạng 
                            <font color="red" size="3">{{ $keyword }}</font> 
                            chưa được cập nhật lên web
                        </strong>
                        <br>
                        Quý khách có nhu cầu sử dụng số dạng này có thể gọi trực tiếp đến Hotline&nbsp;
                        <strong>
                            <a href="tel:{{ $config->phone }}"> <font color="red">{{ $config->phone }}</font></a>
                        </strong>để được phục vụ nhanh nhất!
                    </td>
                </tr>
            </tbody>
        </table>
    @else
        <form id="form-search-02" action="" method="get">
            <div class="row">
                <div class="col-xs-6 col-sm-3">
                    <select name="dauso" class="form-control" onchange="searchByConditions()">
                        <option value="">Lọc theo đầu số</option>
                        @foreach($dauSo as $ds)
                            <option {{ isset($_GET['dauso']) && $ds->tid == $_GET['dauso'] ? 'selected="selected"':'' }} value="{{ $ds->tid }}">{{ $ds->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <select  name="supplier" class="form-control" onchange="searchByConditions()">
                        <option value="">Lọc theo nhà mạng</option>
                        @foreach($suppliers as $supplier)
                            <option {{ isset($_GET['supplier']) && $supplier->id == $_GET['supplier'] ? 'selected="selected"':'' }} value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="clearfix visible-xs-block"></div>
                <div class="col-xs-6 col-sm-3">
                    <select name="price" class="form-control" onchange="searchByConditions()">
                        <option value="">Lọc theo khoảng giá</option>
                        @foreach($khoangGia as $gia)
                            <option {{ isset($_GET['dauso']) && $gia->tid == $_GET['price'] ? 'selected="selected"':'' }} value="{{ $gia->tid }}">{{ $gia->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <select name="order" class="form-control" onchange="searchByConditions()">
                        <option value="">Sắp sếp</option>
                        @foreach($sortOrder as $so)
                            <option {{ isset($_GET['order']) && $so->tid == $_GET['order'] ? 'selected="selected"':'' }} value="{{ $so->tid }}">{{ $so->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </form>
        <div class="row">
            <br/>
        </div>
        <table class="tblsim-res2" width="100%" border="0" cellpadding="2" cellspacing="1" bgcolor="#CCCCCC">
            <tr class="hide480">
                <th class="hide480 tdw35" align="center" height="25" align="center" valign="middle" background="images/grid_bg.gif"><span class="grid_title">STT</span></th>
                <th class="tdw140" align="center" valign="middle"><span class="grid_title">Số Sim</span></th>
                <th class="tdw140" align="center" valign="middle"><span class="grid_title">Giá bán</span></th>
                <th class="tdw120 hide480" align="center" valign="middle"><span class="grid_title">Mạng di động</span></th>
                <th class="hide480 hide555 tdw90" align="center" valign="middle"><span class="grid_title">Loại sim</span></th>
                <th class=" tdw85" valign="middle" align="center"><span class="grid_title">Mua sim</span></th>
            </tr>
            <tbody>
                
                @if(!empty($sims))
                    @foreach($sims as $idx => $sim)
                        @if(!empty($sim->phone))
                            <tr>
                                <td class="hide480" height="32" align="center" valign="middle" bgcolor="#FFFFFF"><span class="sott">{{ ($idx+1) }}</span></td>
                                <td align="center" valign="middle" bgcolor="#FFFFFF">
                                    <span class="simso">
                                        <a class="phone" href="{{ route('detailSim',[$sim->sluggable]) }}">
                                            @php
                                            $phone = $sim->phone;
                                            if(!empty($keys)) {
                                                foreach($keys as $key) {
                                                    $phone = str_replace($key, "<b style='color:red'>".$key."</b>", $phone);
                                                }
                                            }
                                            @endphp
                                            {!! $phone !!}
                                        </a>
                                    </span>
                                </td>
                                <td align="center" valign="middle" bgcolor="#FFFFFF"><strong>{{ !empty($sim->price) ? number_format($sim->price):0 }}₫</strong></td>
                                <td class="hide480" align="center" valign="middle" bgcolor="#FFFFFF">
                                    <span class="lg-vinaphone">{{ $sim->sName }}</span>
                                </td>
                                <td class="hide480 hide555" align="center" valign="middle" bgcolor="#FFFFFF">
                                    <span class="cat2">Sim Hung</span>
                                </td>
                                <td class="" align="center" valign="middle" bgcolor="#FFFFFF">
                                    <a href="{{ route('detailSim',[$sim->sluggable]) }}">
                                        <span class="news2 btn-mua" onclick="ctsim('0856222294')" onmouseover="this.style.textDecoration = 'underline';
                                            this.style.cursor = 'pointer';" onmouseout="this.style.textDecoration = 'none';">Mua sim</span>
                                    </a>
                                </td>
                            </tr>
                        @endif    
                    @endforeach
                @endif
            </tbody>
        </table>
        <div class="pagination" align="center">
             @if(!empty($sims) && $sims->lastPage() > 0)
                @for($i=0; $i < $sims->lastPage(); $i++)
                    @if( ($i+1) == $sims->currentPage()) 
                        <span class="page active">{{ $i + 1 }}</span>
                    @else
                        <a href="?page={{ $i  + 1 }}{{ !empty($_GET) ? '&dauso='.$_GET['dauso'].'&supplier='.$_GET['supplier'].'&price='.$_GET['price'].'&order='.$_GET['order']:'' }}" title="Đến trang {{ $i + 1 }}" class="page">{{ $i + 1 }}</a>
                    @endif
                @endfor
             @endif
            <!-- <a href="" title="Đến trang tiếp" class="page">Tiếp »</a> -->
            <br>
            <br>
        </div>
    @endif
</div>

<script type="text/javascript">
    // $( document ).ready(function() {
    //     $(".phone").each(function(index) {
    //         @if(!empty($keys))
    //             var result = $(this).text();
    //             @foreach($keys as $key)
    //                 result = result.replace("{{$key}}", "<b style='color:red'>{{ $key }}</b>");
    //             @endforeach
    //             $(this).html(result);
    //         @endif
    //     });
    //     String.prototype.replaceAll = function(search, replacement) {
    //         var target = this;
    //         return target.split(search).join(replacement);
    //     };
    // });
</script>

@stop