@extends('Frontend.Layouts.main')

@section('content')

<div class="number_01 ">
    <h2>

        <i class="fa ion-briefcase icon-content"></i>
        &nbsp;
        <a target="_blank">{{ $price->name }}</a>
    </h2>
</div>

<div id="wrapper" class="wrapper">
    <div id="container-home" class="container-home">
        <form id="form-search-02" action="" method="get">
            <div class="row">
                <div class="col-md-4">
                    <select name="dauso" class="form-control" onchange="searchByConditions()">
                        <option value="">Lọc theo đầu số</option>
                        @foreach($dauSo as $ds)
                            <option {{ isset($_GET['dauso']) && $ds->tid == $_GET['dauso'] ? 'selected="selected"':'' }} value="{{ $ds->tid }}">{{ $ds->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <select  name="supplier" class="form-control" onchange="searchByConditions()">
                        <option value="">Lọc theo nhà mạng</option>
                        @foreach($suppliers as $supplier)
                            <option {{ isset($_GET['supplier']) && $supplier->id == $_GET['supplier'] ? 'selected="selected"':'' }} value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <select name="order" class="form-control" onchange="searchByConditions()">
                        <option value="">Sắp sếp</option>
                        @foreach($sortOrder as $so)
                            <option {{ isset($_GET['order']) && $so->tid == $_GET['order'] ? 'selected="selected"':'' }} value="{{ $so->tid }}">{{ $so->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </form>
        <div class="row">
            <br/>
        </div>
        <table class="tblsim-res2" width="100%" border="0" cellpadding="2" cellspacing="1" bgcolor="#CCCCCC">
            <tbody>
                <tr class="hide480">
                    <th class="hide480 tdw35" height="25" align="center" valign="middle" background="images/grid_bg.gif"><span class="grid_title">STT</span></th>
                    <th class="tdw140" align="center" valign="middle" background="images/grid_bg.gif"><span class="grid_title">Số Sim</span></th>
                    <th class="tdw140" align="center" valign="middle" background="images/grid_bg.gif"><span class="grid_title">Giá bán</span></th>
                    <th class="tdw120 hide480" align="center" valign="middle" background="images/grid_bg.gif"><span class="grid_title">Mạng di động</span></th>
                    <th class="hide480 hide555 tdw90" align="center" valign="middle" background="images/grid_bg.gif"><span class="grid_title">Loại sim</span></th>
                    <th class=" tdw85" valign="middle" align="center" background="images/grid_bg.gif"><span class="grid_title">Mua sim</span></th>
                </tr>
                @if(!empty($sims))
                    @foreach($sims as $sim)
                        @if(!empty($sim->phone))
                            <tr>
                                <td class="hide480" height="32" align="center" valign="middle" bgcolor="#FFFFFF"><span class="sott">2</span></td>
                                <td align="center" valign="middle" bgcolor="#FFFFFF">
                                    <span class="simso">
                                        <a href="{{ route('detailSim',[$sim->sluggable]) }}">0856222294</a>
                                    </span>
                                </td>
                                <td align="center" valign="middle" bgcolor="#FFFFFF"><strong>{{ !empty($sim->price) ? number_format($sim->price):0 }}₫</strong></td>
                                <td class="hide480" align="center" valign="middle" bgcolor="#FFFFFF">
                                    <span class="lg-vinaphone">{{ $sim->sName }}</span>
                                </td>
                                <td class="hide480 hide555" align="center" valign="middle" bgcolor="#FFFFFF">
                                    <span class="cat2">Sim Hung</span>
                                </td>
                                <td class="" align="center" valign="middle" bgcolor="#FFFFFF">
                                    <a href="{{ route('detailSim',[$sim->sluggable]) }}">
                                        <span class="news2 btn-mua" onclick="ctsim('0856222294')" onmouseover="this.style.textDecoration = 'underline';
                                            this.style.cursor = 'pointer';" onmouseout="this.style.textDecoration = 'none';">Mua sim</span>
                                    </a>
                                </td>
                            </tr>
                        @endif    
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>


@stop