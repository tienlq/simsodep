@extends('Frontend.Layouts.main')

@section('content')

<article>
    <form style=" display: none; " id="timkiem" name="timkiem" method="post" onsubmit="return simple_search(this.tag.value, 'tim-sim/', '.html');">
        <div class="row">
            <input class="search-input" name="tag" type="tel" placeholder="Nhập số cần tìm" value="0973849666">
            <button class="search-btn">
                <svg width="15px" height="15px">
                <path d="M11.618 9.897l4.224 4.212c.092.09.1.23.02.312l-1.464 1.46c-.08.08-.222.072-.314-.02L9.868 11.66M6.486 10.9c-2.42 0-4.38-1.955-4.38-4.367 0-2.413 1.96-4.37 4.38-4.37s4.38 1.957 4.38 4.37c0 2.412-1.96 4.368-4.38 4.368m0-10.834C2.904.066 0 2.96 0 6.533 0 10.105 2.904 13 6.486 13s6.487-2.895 6.487-6.467c0-3.572-2.905-6.467-6.487-6.467 "></path>
                </svg> Tìm <span class="hide480">kiếm</span>
            </button>
            <ul style=" padding-left: 15px; ">
                <li>Tìm sim có số <strong>9666</strong> bạn hãy gõ <strong>9666</strong></li>
                <li>Tìm sim có đầu <strong>097 </strong>đuôi <strong>849666</strong> hãy gõ <strong>097<font size="2">*</font>849666</strong></li>
                <li>Tìm sim bắt đầu bằng <strong>097384</strong> đuôi bất kỳ, hãy gõ:&nbsp;<strong>097384<font size="2">*</font></strong></li>
            </ul>
        </div>
    </form>
    <center><img src="images/filter-loader.gif" name="loading_div" width="95" height="15" border="0" id="loading_div" style="display:none"></center>
    <style>
        /*  SECTIONS  */
        .section {
            clear: both;
            padding: 0px;
            margin: 0px;
        }

        /*  COLUMN SETUP  */
        .col {
            display: block;
            float:left;
        }
        .col:first-child { margin-left: 0; }

        /*  GROUPING  */
        .group:before,
        .group:after { content:""; display:table; }
        .group:after { clear:both;}
        .group { zoom:1; /* For IE 6/7 */ }
        /*  GRID OF TWELVE  */

        .span_7_of_12 {
            width: 58.33%;
        }


        .span_5_of_12 {
            width: 41.66%;
        }


        /*  GO FULL WIDTH BELOW 480 PIXELS */
        @media only screen and (max-width: 480px) {
            .col {  margin: 1% 0 1% 0%; }
            .span_5_of_12, .span_7_of_12 {
                width: 100%; 
            }
        }
        #hoten,#dienthoai,#diachi,#yeucau, #sosim{
            width: 100%;
            padding: 10px 10px;
            margin: 3px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        #order_form label {
            display: inline-block;
            line-height: 30px;
            width: 115px;
        }
    </style>
    <h1>Sim {{$sim->phone}} - Bán sim {{ $sim->phone }} giá rẻ</h1>
    <form id="order_form" name="order_form" method="post" action="">
        {{ csrf_field()}}
        <div class="section group" style=" background: #fff; border: 1px solid #ccc; ">
            <div class="col span_7_of_12">
                <div style="width: fit-content;margin: 10px auto;padding: 15px;">
                    <label style=" width: 65px; text-align: left; ">Số sim:</label>
                    <span class="simso2">{{ !empty($sim->phone) ? ($sim->phone):'' }}</span><br>
                    <label style=" width: 65px; text-align: left; ">Giá bán:</label>
                    <strong><font size="3">{{ !empty($sim->price) ? number_format($sim->price):0 }}₫</font></strong>
                    <br>
                    <label style=" width: 65px; text-align: left; ">Mạng:</label>
                    <a style="position: absolute;" href="">
                        <img src="{{ $sim->logo }}" alt="viettel" border="0">
                    </a>
                </div>
            </div>
            <div class="col span_5_of_12">
                <img id="img-product" style="width: 100%;" src="{{ $sim->image }}" alt="{{ !empty($sim->phone) ? ($sim->phone):'' }}">
            </div>
        </div>
        <table width="100%" border="0" cellpadding="2" cellspacing="1" bgcolor="#CCCCCC">
            <tbody>
            </tbody>
        </table>
        <br>
        <div id="frm-order" style=" background: #c7eeff; border: 1px solid #ccc; padding: 10px;">
            @if(!empty($msg) && $msg == 'success')
                <em style="color: #067d0b">Đặt hàng thành công, Chúng tôi sẽ liên hệ với bạn trong vòng 24h</em>
                <em style="color: #067d0b">Cám ơn quý khách đã tin tưởng xử dụng dịch vụ của chúng tôi</em>
            @endif
            <h3 style=" text-align: center; "><strong>ĐẶT MUA SIM</strong></h3>
            <label for="dienthoai">Điện thoại liên hệ*:</label>
            <input placeholder="Điện thoại liên hệ" name="phone" type="tel" id="dienthoai" value=""><br>
            <label for="hoten">Họ tên*:</label>
            <input placeholder="Họ tên của bạn" name="name" type="text" id="hoten" value=""> <br>
            <label for="diachi">Địa chỉ:</label>
            <input placeholder="Địa chỉ " name="address" type="text" id="diachi" value="">
            <div style="display:none">
                <input style=" width: 20px; height: 20px; border-radius: 50%;-webkit-appearance: checkbox; " placeholder="Trả góp " name="tragop" type="checkbox" id="tragop" value="Trả góp"> <label for="tragop" style=" text-align: left; position: absolute; padding-top: 3px; padding-left: 5px; ">Mua trả góp </label>
            </div>
            <h3 style=" text-align: center; "><input type="submit" name="Submit2" value="Đặt sim" class="btn-mua" style=" font-size: 18px; padding: 5px 10px;"></h3>
            <div style=" background: #fff; border: 1px solid #ccc; ">
                <div style=" padding:10px; ">
                    @if(!empty($sporder))
                        <strong>{{ $sporder[0]->name or '' }} {{ !empty($phone) ? ($phone):'' }}</strong>
                        {!! $sporder[0]->description or 'des' !!}    
                        <em>
                            Chúc quý khách gặp nhiều may mắn khi sở hữu thuê bao 
                            <strong>{!! !empty($phone) ? ($phone):'' !!}</strong>
                        </em>
                    @endif
                </div>
            </div>

        </div>
    </form>
    <br>
    <div align="center"><strong>&lt;&lt; <a href="javascript:history.back(-1)">Về trang trước</a> </strong></div>

    <br/>
</article>


@stop