<section class="section duy-nhat " style="background: url(template/tint/images/background-3.jpg);">
    <div class="position-relative animatedParent ">
        <div class="container-fluid">
            <div class="row">
                <div class="img-absolute  img-1-2 responsive animated fadeInLeftShort delay-250" style="background-image: url(template/tint/images/bg-duy-nhat.jpg);"></div>
                <div class="container">
                    <div class="col-xs-12 col-sm-12 col-md-4 none-padding content">
                        <img src="template/tint/images/text-logo.png" alt="" class="animated fadeInLeftShort delay-250">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-md-offset-4 col-lg-offset-2 none-padding content">
                        <div class="left">
                            <h2 class="title-3 animated fadeInRight delay-750">Công trình<br />
                                <strong>duy nhất</strong> có góc nhìn <span><strong>360<sup>0</sup> toàn cảnh Đà Lạt</strong></span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>