<section class="section lego animatedParent " style="background: url('template/tint/images/background-sen.jpg');">
	<div class="position-relative">
		<div class="loiich">
            
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 hinh">
				<div class="hinh-lego hinh1 animated fadeInLeftShort delay-250" style="background: url('assets/uploads/myfiles/images/home/lego-left.jpg');">
					<h3 class="lego">Thiết kế thi công bởi<strong>đơn vị uy tín</strong>
					</h3>
				</div>
			</div>
            
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 hinh">
				<div class="hinh-lego hinh2 animated fadeInDownShort delay-500" style="background: url('assets/uploads/myfiles/images/home/lego-center-top.jpg');">
					<h3 class="lego">Tốc độ bàn giao<strong>đúng thời hạn</strong>
					</h3>
				</div>
				<div class="hinhnho-2">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 hinh animated fadeInUpShort delay-750">
						<div class="hinh-lego hinh2-1" style="background: url('assets/uploads/myfiles/images/home/lego-center-bottom1.jpg');">
							<h3 class="lego">
								<strong>Uy tín</strong>của chủ đầu tư
							</h3>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 hinh animated fadeInUpShort delay-750">
						<div class="hinh-lego hinh2-2">
							<h3 class="lego">
								Đầu tư<strong>cho tương lai</strong>
							</h3>
						</div>
					</div>
				</div>
			</div>
            
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 hinh">
				<div class="loiich-2 hinh hinhcontent">
					<h2 class="title-3 animated fadeInRightShort delay-750"> <strong>Lợi ích </strong>đầu tư The Panorama Đà Lạt</h2>
				</div>
				<div class="hinh-lego hinh3 animated fadeInUpShort delay-750" style="background: url('assets/uploads/myfiles/images/home/lego-center-bottom2.jpg');">
					<h3 class="lego"> Tiện ích<b> 4 sao</b>
					</h3>
				</div>
			</div>
            
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 hinh">
				<div class="hinhnho-1">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 hinh">
						<div class="hinh-lego hinh4-1 animated fadeInDownShort delay-750">
							<h3 class="lego">Quản lý<strong>linh hoạt</strong>
							</h3>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 hinh">
						<div class="hinh-lego hinh4-2 animated fadeInRightShort delay-750" style="background: url('assets/uploads/myfiles/images/home/lego-center-bottom3.jpg');">
							<h3 class="lego">Chính sách<strong>Vay ưu đãi</strong>
							</h3>
						</div>
					</div>
				</div>
			</div>
            
		</div>
	</div>
</section>