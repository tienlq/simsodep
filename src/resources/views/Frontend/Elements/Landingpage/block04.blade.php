<section class="section toa-lac" style="background: url('template/tint/images/background-sen.jpg');">
	<div class="position-relative ">
		<div class="container-fluid">
			<div class="animatedParent ">
				<div class="img-absolute img-left img-1-2 responsive animated fadeInLeftShort delay-250" style="background-image: url('template/tint/images/bg-toalac.jpg');"></div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-7 none-padding content">
					<div class="right">
						<h2 class="title-3 animated fadeInRight delay-500">Tọa lạc trên <strong>trục đường di sản</strong> cổ kính đậm phong cách Pháp</h2>
						<div class="animated fadeInRight delay-750">
							<p>Đà Lạt là thành phố may mắn được sở hữu một di sản kiến trúc giá trị, ví như một bảo tàng kiến trúc châu Âu thế kỷ 20. Từ một đô thị nghỉ dưỡng do người Pháp xây dựng. Lịch sử phát triển quy hoạch đô thị Đà Lạt nửa đầu thế kỷ 20 dường như gắn liền với sự phát triển nghệ thuật quy hoạch đương đại của thế giới.</p>
							<p>The Panorama Đà Lạt nằm ngay trên trục đường di sản của thành phố, nơi kiến trúc cổ điển đậm phong cách Pháp.</p>
							<a href="gioi-thieu/gioi-thieu-du-an.html" target="_blank">
								<p class="readmore">Xem thêm</p>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>