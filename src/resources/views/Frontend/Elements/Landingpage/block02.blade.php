<section class="section dl-panorama" style="background: url('template/tint/images/dl-panorama.jpg');">
	<div class="position-relative ">
		<div class="container animatedParent ">
			<div class="row mo-ta">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4  animated fadeInDownShort delay-250">
					<h2 class="title-4">Nơi thịnh vượng, chốn bình an</h2>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6  animated fadeInDownShort delay-500">
					<p>Trong cuộc sống, với bao lo toan, hối hả... đã bao giờ ta chợt nhận thấy cần lắm một cái nắm tay siết chặt, cần lắm một sự kết nối quyện hòa, cần lắm những phút giây tĩnh tại để trở về chính con người thật của mình, ...</p>
				</div>
			</div>
		</div>
		<div class=" animatedParent ">
			<div class="container-fluid">
				<div class="img-absolute img-left img-1-2 responsive animated fadeInLeftShort delay-750" style="background-image: url(template/tint/images/bg-dl-panorama.jpg);"></div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-7 none-padding content animated fadeInRightShort delay-750">
					<div class="right">
						<div class="">
							<p>... chỉ cần nghe tiếng gió xào xạc, được đánh thức bởi tiếng chim hót xa xa trên bụi thông già, chỉ cần cảm được thở ấm áp trong tiết trời se lạnh, lặng ngắm những bông hoa tươi sắc sớm mai... chỉ cần ... một nơi chốn cho ta cảm giác bình an ...</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>