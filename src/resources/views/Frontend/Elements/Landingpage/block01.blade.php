<section class="section slider">
    <div>
        <ul class="bx-slider">
            <li data-title="<strong> </strong>" data-description="">
                <div class="main-img" style="background-image: url(assets/uploads/myfiles/images/Slide/panorama-dalat-slide1.jpg)"></div>
            </li>
        </ul>
        <div class="control"></div>
        <div class="headline">
            <div class="container-headline"></div>
        </div>
        <a href="#dl-panorama" class="go-bottom"></a>
    </div>
</section>