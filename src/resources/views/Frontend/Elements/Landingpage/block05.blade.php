<section class="section dang-cap animatedParent " style="background: url(template/tint/images/background-2.jpg);">
	<div class="position-relative">
		<div class="container-fluid">
			<div class="img-absolute img-right img-1-2 responsive animated fadeInRightShort delay-250" style="background-image: url(template/tint/images/bg-dang-cap.jpg);"></div>
			<div class="col-xs-12 col-sm-12 col-md-4 none-padding content">
				<div class="left">
					<h2 class="title-3 animated fadeInLeftShort delay-500">Tiện ích<strong>đẳng cấp </strong>xoa diệu những ưu tư, căng thẳng</h2>
					<div class="animated fadeInLeft delay-750">
						<p>Nép mình bên rặng thông xanh rì rào, nơi đây hội tụ những tiện nghi đẳng cấp của xứ ngàn hoa giúp lữ khách xoa dịu những ưu tư, căng thẳng nhưng vẫn giữ được các ưu nét riêng của vùng xứ hoa đào như bể sục Jacuzzi cho cả đại gia đình, các liệu pháp Spa độc quyền với các bài thuốc quý từ cao nguyên, nhà hàng nướng cao cấp, và cả một bầu trời sao mở rộng tầm nhìn với tách trà atiso ấm nóng tại café sân thượng – Matineux Coffee Bar. </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>