<section class=" section slide-1 animatedParent animateOnce " style="background: url(../template/tint/images/background-sen.jpg);">
	<div class="position-relative ">
		<div class="container-fluid">
			<div class=" animatedParent animateOnce">
				<h2 class="title-tongquan animated fadeIn delay-500">Tiện ích</h2>
				<p class="link animated fadeIn delay-500">Trang chủ/ Tiện ích</p>
				<div class="img-absolute img-left img-1-2 responsive animated fadeInLeftShort delay-500" style="background-image: url(../assets/uploads/myfiles/images/utilities/tienich-1.jpg);"></div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-7 none-padding content">
					<div class="right">
						<div id="divContent">
							<h2 class="title-3 animated fadeInRight delay-500">
								Hội tụ<strong> tiện ích cao cấp</strong> mang giá trị văn hóa ngàn hoa
							</h2>
							<p>
								The Panorama Đà Lạt&nbsp;hội tụ các tiện ích cao cấp, đáp ứng nhu cầu cho cuộc sống hiện đại với phong cách hoàn toàn khác biệt. Nhưng khi lưu trú tại đây, Đà Lạt Panorama còn đem đến cảm giác yên bình, gạt bỏ những ưu phiền trong tâm hồn bằng những giá trị văn hoá của thành phố ngàn hoa trường tồn với thời gian.
							</p>
							<ul class="list-loiich">
								<li class="content-loiich">
									Khu trung tâm Thương mại – Dịch vụ - Giải trí
								</li>
								<li class="content-loiich">
									Cả một bầu trời ngàn sao mở rộng tại café sân thượng – Matineux Coffee Bar
								</li>
								<li class="content-loiich">
									Trung tâm thương mại và giải trí ( từ F1- F3)
								</li>
								<li class="content-loiich">
									Bể Jacuzzi
								</li>
								<li class="content-loiich">
									Khu vực Gym
								</li>
								<li class="content-loiich">
									Khu vực Spa
								</li>
								<li class="content-loiich">
									Nhà hàng nướng cao cấp – Da Lat Cuisine
								</li>
								<li class="content-loiich">
									Khu vực giải trí (karaoke, bar…)
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
