


<div class="list-group">
    {!! app('ClassConfigBlock')->htmlTitleBlock(3) !!}
    <div class="contact-right" id="contact-right">
        <div id="hlkd">
            {!! app('ClassBlock')->htmlSupport() !!}
        </div>
        <div class="line-bottom"></div>
        <a>
            <span id="xemthem" class="is-hidden" >Xem thêm </span>
            <div class="arrow-right"></div>
        </a>
        <br/>   
        <h3 style="margin-bottom: 5px">Góp ý, khiếu nại:</h3>
        <a href="tel:{{ $config->phone }}"><strong>{{ $config->phone }} </strong></a>
    </div>

</div>

<script>

    $('#xemthem').click(function () {
        if ($(this).hasClass('is-hidden')) {
            $(this).removeClass('is-hidden');
            $(this).text('Thu gọn');
            $('.sp-item-hidden').removeClass('_hidden');
        } else {
            $(this).text('Xem thêm');
            $(this).addClass('is-hidden');
            $('.sp-item-hidden').addClass('_hidden');
        }
    });
</script>
