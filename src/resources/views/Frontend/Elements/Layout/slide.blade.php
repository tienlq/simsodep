@php
    $slides = app('ClassBlock')->getBlockByType('slide');
    $countSlide = count($slides);
@endphp
<div class='carousel slide' id='myCarousel'>
    <!-- Carousel items -->
    <div class='carousel-inner'>
        <div class='item active'>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @for($i=0; $i<$countSlide; $i++)
                        <li data-target="#carousel-example-generic" data-slide-to="{{$i}}" class="{{$i == 0 ? 'active':''}}"></li>
                    @endfor
                </ol>

                <div class="carousel-inner" role="listbox">
                    @foreach($slides as $idx=>$slide)
                        <div class="item {{$idx == 0 ? 'active':''}}">
                            <img src="{{ $slide->image }}" alt="{{ $slide->name }}">
                        </div>
                    @endforeach
                </div>

                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class='item '>
            <img alt='Vinhomes Metropolis' 
                 class='imagebanner' 
                 src='/lh4.googleusercontent.com/-dTu1oOIests/Wlmvt4osJ-I/AAAAAAAAFZ0/8hgm8uXz-asjQAGX3CyLWkIVnGx4s8eNwCLcBGAs/s1170/slide2.jpg'/>
        </div>
        <img src='/lh4.googleusercontent.com/-Ase05lCabQg/WlXujxOd7zI/AAAAAAAAFWw/Ux9VK6bHwHILaZVoL0x76mfczY53LDNWgCLcBGAs/s1920/bgs.png' 
             style='bottom: 0px;width: 100%;position: absolute;z-index:100'/>
      
        <!-- Carousel nav -->
    </div>
</div>



