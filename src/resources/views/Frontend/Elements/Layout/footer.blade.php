<div  class="bg-footer-image"style="background: url('/images/bg_footer.jpg'); width: 100%;min-height: 300px">
    <div class="bg-footer-color" style="background: #fe5f09; opacity: 0.9; min-height: 300px">
        <br/>

        {!! app('ClassBlock')->htmlFCate() !!}

        <br/>


        <div class="f-menu">
            <span class="f-item-menu">
                {!! app('ClassBlock')->htmlMenuFooter() !!}
            </span>
        </div>

        <br/>

        <div class="f-address">
            <div class="f-item-address-01">
                {!! app('ClassBlock')->htmlFDes('fdes1') !!}
            </div>
            <div class="f-item-address-02">
                {!! app('ClassBlock')->htmlFDes('fdes2') !!}

            </div>
        </div>
    </div>
</div>