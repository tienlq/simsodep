<nav class="navigation navigation-header" style="bottom: 0px">
    <div class="container">
        <div class="navigation-brand">
            <div class="brand-logo visible-lg visible-sm visible-md">
                <!--<a class="logo"></a>-->
                <span class="sr-only"></span>
            </div>
        </div>
        <div class="navigation-navbar greedy-nav" id="menu111">
            {!! app('ClassCategory')->htmlMenuFooter('navigation-bar navigation-bar-right navclass') !!}
            <ul class='hidden-links hidden'></ul>
        </div>
        <div class="navigation-brand">
            <div class="brand-logo visible-xs">
                <a class="logo"></a>
                <span class="sr-only"></span>
            </div>
            <button class="navigation-toggle visible-xs" type="button" data-toggle="dropdown" data-target=".navigation-navbar">
                Menu
            </button>
        </div>
    </div>
</nav>