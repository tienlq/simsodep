<div class="list-group">
    {!! app('ClassConfigBlock')->htmlTitleBlock(2) !!}
    <div class="content_sidebar">
        <a class="list-group-item lgit" href="{{ route('listSimBySupplier',['viettel']) }}">Sim Viettel</a>
        <a class="list-group-item lgit" href="{{ route('listSimBySupplier',['vinaphone']) }}">Sim Vinaphone</a>
        <a class="list-group-item lgit" href="{{ route('listSimBySupplier',['mobifone']) }}">Sim Mobifone</a>
        <a class="list-group-item lgit" href="{{ route('listSimBySupplier',['vietnamobile']) }}">Sim Vietnamobile</a>
        <a class="list-group-item lgit" href="{{ route('listSimBySupplier',['gmobile']) }}">Sim Gmobile</a>
        <span class="list-group-item lgit desktop-hide"> </span>
    </div>
</div>