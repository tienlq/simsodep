<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCq1seF2E-cZLFtueRMcXiL8ThoJjVYgss"></script>
<script type="text/javascript" src="/frontend/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/frontend/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="/frontend/js/jquery.nav.js"></script>
<script type="text/javascript" src="/frontend/js/jquery.appear.js"></script>
<script type="text/javascript" src="/frontend/js/jquery.plugin.js"></script>
<script type="text/javascript" src="/frontend/js/headhesive.min.js"></script>
<script type="text/javascript" src="/frontend/js/snap.svg-min.js"></script>
<script type="text/javascript" src="/frontend/js/mb.bgndgallery.js"></script>
<script type="text/javascript" src="/frontend/js/mb.bgndGallery.effects.js"></script>
<script type="text/javascript" src="/frontend/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="/frontend/js/jquery.film_roll.min.js"></script>
<script type="text/javascript" src="/frontend/js/jquery.zoomtour.js"></script>
<script type="text/javascript" src="/frontend/js/jquery.transform-0.9.3.min_.js"></script>
<script type="text/javascript" src="/frontend/js/jquery.cycle2.min.js"></script>
<script type="text/javascript" src="/frontend/plugin/slick/slick.js"></script>
<script type="text/javascript" src="/frontend/plugin/layerslider/js/greensock.js"></script>
<script type="text/javascript" src="/frontend/plugin/layerslider/js/layerslider.transitions.js"></script>
<script type="text/javascript" src="/frontend/plugin/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
<script type="text/javascript" src="/frontend/plugin/slippry/slippry.min.js"></script>
<script type="text/javascript" src="/frontend/plugin/pinterest-grid/pinterest_grid.js"></script>
<script type="text/javascript" src="/frontend/plugin/mapplic/js/hammer.min.js"></script>
<script type="text/javascript" src="/frontend/plugin/mapplic/js/jquery.easing.js"></script>
<script type="text/javascript" src="/frontend/plugin/mapplic/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/frontend/plugin/mapplic/mapplic/mapplic.js"></script>
<script type="text/javascript" src="/frontend/plugin/fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/frontend/plugin/evolution/scripts/jquery.carousel-1.1.min.js"></script>
<script type="text/javascript" src="/frontend/plugin/magnificpopup/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="/frontend/plugin/bigvideo/lib/video.js"></script>
<script type="text/javascript" src="/frontend/plugin/bigvideo/lib/bigvideo.js"></script>
<script type="text/javascript" src="/frontend/plugin/mcscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="/frontend/plugin/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="/frontend/plugin/tubula/jquery.tubular.1.0.js"></script>
<script type="text/javascript" src="/frontend/plugin/lazyload-db/jquery.lazyload.min.js"></script>
<script type="text/javascript" src="/frontend/plugin/justifiedGallery/jquery.justifiedGallery.min.js"></script>
<script>
//    var locationCurent = new google.maps.LatLng({{$config->location_curent}})
    locationTitle = '{{$config->location_title}}';
    locationImages = '{{$config->location_image}}';
    locationDescription = '{{$config->location_description}}';
</script>
<script type="text/javascript" src="/frontend/js/scripts.js"></script>
<!--[if lt IE]>
<script type="text/javascript" src="http://skylake.vinhomes.vn//frontend/js/html5shiv.min.js"></script>
<script src="http://skylake.vinhomes.vn//frontend/js/respond.min.js"></script>
<![endif]-->