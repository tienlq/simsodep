<section class="wrap-banner-top">
	<div class="container">
		<div class="wrap-banner">
			<div class="col-md-2 col-sm-2">
				<div class="logo-top">
					<img style="height:80px" src="/logo/linkvietnam-condotel_03.png" alt="linkvietnam-condotel.com.vn">
				</div>
			</div>
			<div class="col-md-2 col-sm-2"></div>
			<div class="col-md-8 col-sm-8">
				<div class="banner-top">
					<img src="/wp-content/themes/news/img/advertise.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="bg-menu">
	<div class="container">
		<div class="wrap-menu">

			<ul class="wrap-icon" id="icon-news">
				<li class="item-icon" id="item-icon" >
					<a href="">
						<div class="icon-header-white" id="icon-header-white">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="21px" height="19px">
								<style type="text/css">
									.st0{fill:#FFFFFF;}
								</style>
								<g>
									<g>
                                        <path class="st0" d="M448,112V48H0v352c0,0,0,64,64,64h400c0,0,48-1,48-64V112H448z M64,432c-32,0-32-32-32-32V80h384v320
                                              c0,14.7,4.5,24.9,10.8,32H64z"/>
                                        <rect x="64" y="144" class="st0" width="320" height="32"/>
                                        <rect x="240" y="336" class="st0" width="112" height="32"/>
                                        <rect x="240" y="272" class="st0" width="144" height="32"/>
                                        <rect x="240" y="208" class="st0" width="144" height="32"/>
                                        <rect x="64" y="208" class="st0" width="144" height="160"/>
									</g>
								</g>
							</svg>
						</div>
						<div class="icon-header-red" id="icon-header-red">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 32 32" style="enable-background:new 0 0 32 32;" xml:space="preserve" width="21px" height="19px">
								<g>
									<g>
                                        <path d="M28,7V3H0v22c0,0,0,4,4,4h25c0,0,3-0.062,3-4V7H28z M4,27c-2,0-2-2-2-2V5h24v20    c0,0.921,0.284,1.559,0.676,2H4z" fill="#ac1a27"/>
                                        <rect x="4" y="9" width="20" height="2" fill="#ac1a27"/>
                                        <rect x="15" y="21" width="7" height="2" fill="#ac1a27"/>
                                        <rect x="15" y="17" width="9" height="2" fill="#ac1a27"/>
                                        <rect x="15" y="13" width="9" height="2" fill="#ac1a27"/>
                                        <rect x="4" y="13" width="9" height="10" fill="#ac1a27"/>
									</g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g><g></g>
							</svg>
						</div>
						<div class="icon-header-grey" id="icon-header-grey">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 32 32" style="enable-background:new 0 0 32 32;" xml:space="preserve" width="21px" height="19px">
								<g><g>
                                        <path d="M28,7V3H0v22c0,0,0,4,4,4h25c0,0,3-0.062,3-4V7H28z M4,27c-2,0-2-2-2-2V5h24v20 c0,0.921,0.284,1.559,0.676,2H4z" fill="#5e5e5e"/>
                                        <rect x="4" y="9" width="20" height="2" fill="#5e5e5e"/>
                                        <rect x="15" y="21" width="7" height="2" fill="#5e5e5e"/>
                                        <rect x="15" y="17" width="9" height="2" fill="#5e5e5e"/>
                                        <rect x="15" y="13" width="9" height="2" fill="#5e5e5e"/>
                                        <rect x="4" y="13" width="9" height="10" fill="#5e5e5e"/>
									</g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g>
							</svg>
						</div>
					</a>
				</li>
			</ul>

			{!! app('ClassCategory')->htmlMenuNews(56) !!}

		</div>
	</div>
</section>