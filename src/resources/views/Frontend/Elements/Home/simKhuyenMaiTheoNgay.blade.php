<div class="number_01 ">
    
    <h2>
        <i class="fa ion-star icon-content"></i>
        &nbsp;
        <a>Sim khuyến mãi trong ngày</a>
    </h2>
</div>
<div id="wrapper" class="wrapper">
    <div id="container-home" class="container-home">
        <div class="photo-grid clearfix">
            <ul class="clearfix">
                @foreach($simDoanhNhan as $sim)
                    @if(!empty($sim->phone))
                        <li>
                            <div style=" padding: 2px; ">
                                <a href="{{ route('detailSim',[$sim->sluggable]) }}">
                                    <div class="simcard">
                                        <div class="{{ $sim->elementClass }} spritestlvn"></div>
                                        <div class="so-sim-home">{{ ($sim->phone) }}</div>
                                        {{ !empty($sim->price) ? number_format($sim->price):0 }}₫
                                    </div>
                                </a>
                            </div>
                        </li>
                    @endif    
                @endforeach
            </ul>
        </div>
    </div>
</div>