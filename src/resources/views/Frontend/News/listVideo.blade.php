@extends('Frontend.Layouts.news01')

@section('styleSheet')
@include('Frontend.Elements.ListNews01.stylesheet')
@stop

@section('script')
@include('Frontend.Elements.ListNews01.script')
@stop

@section('menuNews')
@include('Frontend.Elements.ListNews01.menuNews')
@stop

@section('content')

<section class="bg-silver mobile-bg-silver">
    <div class="container">

        <div class="wrap-item">
            <div class="row">
				@foreach($listNews as $news)
				
					<div class="col-md-6 col-sm-6 col-xs-12 box-big-item">

						<div class="wrap-big-img">
							<a href="{{ route('detailNews',[$news->sluggable, $news->tid]) }}">
								<img class="lazyload-category-so-big" data-src="{{ $news->image or '' }}"/>
							</a>
						</div>

						<div class="banner-info-view3">
							<div class="background-banner">
								<div class="priceHouse">
									<h2>{{ $news->title or '' }}</h2>
								</div>
							</div>
						</div>

					</div>
				@endforeach
            </div>

            <div class="row">

                <div class="pagination">
                    {!! $listNews->render() !!}
				</div>

            </div>
        </div>
    </div>
</section>

@stop