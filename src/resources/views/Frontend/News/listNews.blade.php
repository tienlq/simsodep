@extends('Frontend.Layouts.main')

@section('content')

<section id="news" >
    <div class="intro-body container-fluid content-section text-center">
        <div class="container">
            <h3 class="text-center">{{ $category->name }}</h3>
            <div class="row blog-grid_" id = "blog-landing" >

                @foreach($listNews as $news)
                <div class="animated fadeInUp" data-animation="fadeInUp" data-delay="0">
                    <img alt="{{ $news->title }}" src="{{ $news->image }}" />
                    <div class="watetmarkProduct">
                        <a href="{{ route('detailNews',[$news->sluggable, $news->tid]) }}">
                            <p>{{ $news->title }}</p>
                            <div class="timeDetail">				
                                <div>Xem chi tiết</div>
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach

            </div>

            <!--mobile-->
            <div class="blog-grid_moble">
                @foreach($listNews as $idx => $news)

                <div class="top_new_mobile">
                    <article class="white-panel animated" style="max-height: 350px;" data-animation="fadeInUp" __data-delay="1500">
                        <img alt="{{ $news->title }}" src="{{ $news->image }}" style="width: 100%;" />
                        <div class="watetmarkProduct">
                            <a href="{{ route('detailNews',[$news->sluggable, $news->tid]) }}">
                                <p>{{ $news->title }}</p>
                                <div class="timeDetail">                                
                                    <div class="readmorenews">Xem chi tiết</div>
                                </div>
                            </a>
                        </div>
                    </article>
                    <div class="cleafix"></div>
                </div>
               
                @endforeach
            </div>
            {!! $listNews->render() !!}
        </div>
        <br><br><br>
    </div>
</section>

@stop