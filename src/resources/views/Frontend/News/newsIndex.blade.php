@extends('Frontend.Layouts.news01')

@stop

@section('content')
<div id="main" style="padding-top: 45px;">
	<section class="wrap-hot-news">
		<div class="container">
            @if(!empty($listNews))
                <div class="row">
                    <div class="col-md-6 col-sm-12 wrap-col-big">
                        <div class="wrap-hot-news-big">
                            <a href="{{ route('detailNews',[$listNews[0]->sluggable, $listNews[0]->tid]) }}">
                                <img class="lazyload-image-big" data-src="{{ $listNews[0]->image or '' }}" />
                                <div class="bg-color-hot-news">
                                </div>
                                <div class="content-big-wrap">
                                    <div class="title-hot-news-big">
                                        <div class="title-big-excerpt">{{ $listNews[0]->title or '' }}</div>
                                    </div>
                                    <div class="time-hot-news-big">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                        <b>{{ !empty($listNews[0]->updated_at) ? date('d/m/Y', strtotime($listNews[0]->updated_at)):'' }}</b>
                                    </div>
                                    <div class="description-hot-news-big">
                                        {{ $listNews[0]->summary or '' }}
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="wrap-hot-news-normal">
                            <div class="row">
                                @foreach($listNews as $key => $news)
                                    @php 
                                        if($key == 0){ continue; }  
                                        if($key >= 5) { break; }
                                    @endphp
                                    <div class="col-md-6 col-sm-6 col-xs-6 wrap-col-normal">
                                        <div class="box-hot-news-normal">
                                            <a href="{{ route('detailNews',[$news->sluggable, $news->tid]) }}">
                                                <img class="lazyload-image-small" data-src="{{ $news->image }}" />
                                                <div class="bg-color-hot-news-normal">
                                                </div>
                                                <div class="content-normal-wrap">
                                                    <div class="title-hot-news-normal">
                                                        <div class="title-normal-excerpt">
                                                            {{ $news->title }}                                                     
                                                        </div> 
                                                    </div>
                                                    <div class="time-hot-news-normal">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                        <b>{{ !empty($news->updated_at) ? date('d/m/Y', strtotime($news->updated_at)):'' }}</b>
                                                    </div>
                                                    <div class="description-hot-news-normal">
                                                        <div class="description-normal-excerpt">
                                                           {{ $news->summary or '' }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endif
<!--			<div class="row">
				<div class="col-md-12 wrap-advertise-hot-news">
					<div class="advertise-hot-news">
                        <img data-src="https://jinn.vn/news/wp-content/themes/news/img/advertise-hot-news.jpg" class="content-iamge lazyload-content-img">
					</div>
				</div>
			</div>-->
		</div>
	</section>
	<section>
		<div class="container">
			<div class="wrap-category-content">
				<div class="row">
					<div class="col-md-9 col-sm-9">
                        <div class="row">
                            
                            @foreach($subCategorys as $sCate)
                                @php 
                                    $contentBlock = app('ClassHelper')->getNewsByCategory($sCate->tid); 
                                    if($contentBlock == '') {
                                        continue;
                                    }
                                @endphp
                                <div class="col-md-6 col-sm-6 wrap-col-category">
                                    <div class="wrap-marketing">
                                        <div class="marketing-category">
                                            <a href="{{ $sCate->link }}">
                                                <h2>{{ $sCate->name }}</h2>
                                            </a>
                                        </div>
                                        <div class="hr"></div>

                                        {!! $contentBlock !!}

                                    </div>
                                </div>
                            @endforeach
							
                        </div>
					</div>
					<div class="col-md-3 col-sm-3">
                        <div class="wrap-content-right">
							<div class="wrap-new-project">
								<ul>
									<li class="title-new-project">
										<a href="/n2/du-an/n55.html">
											<h2>dự án mới</h2>
										</a>
									</li>
                                    @foreach($listProject as $pro)
                                        <li class="box-new-project">
                                            <a href="{{ route('detailNews',[$pro->sluggable, $pro->tid]) }}">
                                                <div class="img-new-project">
                                                    <img data-src="{{ $pro->image }}" class="lazyload-project">
                                                </div>
                                                <div class="name-new-project">
                                                    <b>{{ $pro->title }}</b>
                                                </div>
                                                <div class="wrap-info-new-project">
                                                    <div class="row">
                                                        {{ $pro->summary }}
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
								</ul>
							</div>
							<div class="wrap-popular">
								<ul>
									<li class="title-popular">
										<a href="#">
											<h2>tin nổi bật</h2>
										</a>
									</li>
									@foreach($listHighlight as $hl)
									<li class="box-popular">
										<div class="row">
											<a href="{{ route('detailNews',[$hl->sluggable, $hl->tid]) }}">
												<div class="col-md-3 col-sm-3">
													<div class="img-popular">
														<div class="content-image-popular">
															<img class="lazyload-popular-small content-image-popular" data-src="{{ $hl->image }}" />
														</div>
													</div>
												</div>
												<div class="col-md-9 col-sm-9">
													<div class="box-content-popular">
														<div class="title-popular-box">
															<b>{{ $hl->title }}</b>
														</div>
														<div class="time-popular-box">
															<span class="glyphicon glyphicon-calendar"></span>
                                                            <b>{{ !empty($hl->updated_at) ? date('d/m/Y', strtotime($hl->updated_at)):'' }}</b>
														</div>
													</div>
												</div>
											</a>
										</div>
									</li>
                                    @endforeach
								</ul>
							</div>
<!--							<div class="wrap-advertise-right">
								<img data-src="" class="content-image-right lazyload-image-right">
							</div>-->
                        </div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

@stop