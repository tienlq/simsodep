@extends('Frontend.Layouts.main')

@section('styleSheet')
<link rel="icon" type="image/x-icon" href="favicon0995.ico"/>
<link rel="alternate" href="index.html" hreflang="vi" />
<link rel="stylesheet" type="text/css"  href="/frontend/css/select2.min0995.css" />
<link rel="stylesheet" type="text/css"  href="/frontend/css/bootstrap.min0995.css" />
<link rel="stylesheet" href="/frontend/css/7ed9c140995.css" />
<link rel="stylesheet" href="/frontend/css/projectDetail.css"/>
@stop

@section('script')
<script type="text/javascript" src="/frontend/js/jquery-3.2.1.min0995.js" ></script>
<script type="text/javascript" src="/frontend/js/lazyImage0995.js"></script>
<script type="text/javascript" src="/frontend/js/bootstrap.min0995.js"></script>
<script type="text/javascript" src="/frontend/js/api_client.js"></script>
<script type="text/javascript" src="/frontend/js/moment.min0995.js"></script>
<script src="/frontend/js/4051b0c0995.js"></script>
<script src="/frontend/js/e6a1ce70995.js" async></script>

<script type="text/javascript">
    var error_img = (function () {
        return {
            project_list: '/jinnV2/images/error/error-project.jpeg?v=0.9.0.4',
            house_list: '/jinnV2/images/error/imageAlternative/HINH-3.jpg?v=0.9.0.4',
            house_view: '/jinnV2/images/error/imageAlternative/HINH-5.jpg?v=0.9.0.4'
        }
    })()
    var generalTranslation = (function () {
        return {
            error_houseModel: "/jinnV2/images/other/nhaMau1.jpg?v=0.9.0.4",
            error_news_img: "/jinnV2/images/img/section5.jpg?v=0.9.0.4",
            error_occured: "Đã có lỗi xảy ra, xin bạn vui lòng thử lại sau",
            sent_mail_success: "Yêu cầu của bạn đã được gửi đi, xin vui lòng kiểm tra email để tiếp tục.",
            sending_button: "Đang gửi",
            field_required_error: "Trường này không được phép bỏ trống",
            currentLocale: "vi",
            project_search: "Tìm kiếm theo tên dự án",
            basic_search: "Tìm kiếm theo căn hộ cần bán",
            lease_search: "Tìm kiếm theo căn hộ cho thuê",
            updating: "Đang cập nhật",
            contact: "Liên hệ",
            I_caring: "Tôi đang quan tâm sản phẩm",
            loading_image: "/jinnV2/images/img/loader.gif?v=0.9.0.4",
            numberBedRoom: "Số phòng ngủ",
            numberBathRoom: "Số phòng tắm",
            houseArea: "Diện tích",
            built_up: "  tim tường: ",
            carpet: " thông thủy:  ",
            ground: " đất:  "
        }
    })();
    var socialLoginData = {
        google_plus_id: "392610411326-qg7tgb26esor4l936iefkro24v0648hm.apps.googleusercontent.com",
        face_book_id: "1036958233116725",
        url: "/vi/user/socialLogin"
    }


</script>
<script>
    $.extend($.validator.messages, {
        required: "Trường này không được để trống",
        remote: "Please fix this field.",
        email: "Email không hợp lệ",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Trường này chỉ chấp nhận kí tự là số",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Vui lòng nhập đúng giá trị bên trên",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Trường này chỉ chứa nhiều nhất {0} ký tự "),
        minlength: jQuery.validator.format("Trường này phải chứa ít nhất {0} ký tự"),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });
</script>

<script src="/frontend/js/224be7a0995.js" async></script>
<script src="/frontend/js/api1221.js" async></script>
<script src="/frontend/js/select2.min0995.js" async></script>

@stop

@section('content')

<div class="content-detailDuAn">
	<div class="background-Search-detailDuAn">
		<div class="search-du-An">
			<form class="form-inline" id="form-search-house-by-projectDetail" action="">
				<div class="input-group">
					<input id="inlineFormInputGroup" type="text" placeholder="Tìm kiếm theo căn hộ " name="query" class="form-control">
					<div onclick='document.getElementById("form-search-house-by-projectDetail").submit()'  class="input-group-addon"  ><i aria-hidden="true" class="fa fa-search fa-2x"></i>
					</div>
				</div>
			</form>
		</div>
		<div class="link-lienHe-scroll">
			<i class="fa fa-envelope-o" aria-hidden="true"></i>
			<a href="javascript:void (0)" type="button">Gửi yêu liên hệ</a>
		</div>
	</div>
	<div class="breadcrumb-page">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="../../index.html">Trang chủ</a>
			</li>
			<li class="breadcrumb-item">
				<a href="../projects.html">Dự án</a>
			</li>
			<li class="breadcrumb-item active">
				Flamingo Cát Bà Beach Resort
			</li>
		</ol>
	</div>
	<div class="content-menuResponsive">
		<div class="background-menuResponsive">
			<div class="col-xs-4 item-menuRes it-clickMenuRes">
				<div class="content-iconRes">
					<div class="menu-res-off">
						<div class="item-icon-menuRes">
							<img src="/jinnV2/images/icon/click-menu-left20995.png?v=0.9.0.4" alt="">
						</div>
					</div>
					<div class="menu-res-on">
						<div class="item-icon-menuRes">
							<img src="/jinnV2/images/icon/close-menu-left0995.png?v=0.9.0.4" alt="">
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-4 item-menuRes">
				<div class="content-iconRes">
					<div class="item-icon-menuRes">
						<a href="../../index.html">
							<img src="/jinnV2/images/icon/menu-home20995.png?v=0.9.0.4">
						</a>
					</div>
				</div>
			</div>
			<div class="col-xs-4 item-menuRes it-clickSocial">
				<div class="content-iconRes">
					<div class="social-res-off">
						<div class="item-icon-menuRes">
							<img src="/jinnV2/images/icon/share-menu-left20995.png?v=0.9.0.4">
						</div>
					</div>
					<div class="social-res-on">
						<div class="item-icon-menuRes">
							<img src="/jinnV2/images/icon/close-menu-left0995.png?v=0.9.0.4">
						</div>
					</div>
				</div>
			</div>
			<div class="contentDropMenu">
				<div class="cnt-menuRes itemDropdown">
					<ul>
						<li class="active"><a id="detailSurveyMobileLink" href="#">Tổng quan dự án</a></li>
						<li><a id="groundProjectMobileLink"  href="#">Mặt bằng dự án & Tiện ích</a></li>
						<li ><a id="reasonChooseProjectMobileLink" href="#">Lý do lựa chọn Flamingo Cát Bà Beach Resort</a></li>
						<li ><a id="newsProjectMobileLink" href="#">Tin Tức</a></li>
						<li><a id="blockProjectMobileLink" href="#">Quỹ căn</a></li>
						<li><a id="contactProjectMobileLink" href="#">Liên hệ</a></li>
					</ul>
				</div>
				<div class="cnt-socialMenuRes itemDropdown">
					<div class="item-socialMenuRes">
						<a id="hrefFacebookMobile" class="share-facebook linkShareHerf" href="flamingo-catba-beach-resort-5a2a4a264f898.html" target="_blank" >
							<div class="cnt-iconSVG">
								<svg id="Layer_1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 474.294 474.294" style="enable-background:new 0 0 474.294 474.294; width: 30px; height: 30px;" xml:space="preserve">
								<circle style="fill:#3A5A98;" cx="237.111" cy="236.966" r="236.966"></circle>
								<path style="fill:#345387;" d="M404.742,69.754c92.541,92.541,92.545,242.586-0.004,335.134                                                      c-92.545,92.541-242.593,92.541-335.134,0L404.742,69.754z"></path>
								<path style="fill:#2E4D72;" d="M472.543,263.656L301.129,92.238l-88.998,88.998l5.302,5.302l-50.671,50.667l41.474,41.474                                                      l-5.455,5.452l44.901,44.901l-51.764,51.764l88.429,88.429C384.065,449.045,461.037,366.255,472.543,263.656z"></path>
								<path style="fill:#FFFFFF;" d="M195.682,148.937c0,7.27,0,39.741,0,39.741h-29.115v48.598h29.115v144.402h59.808V237.276h40.134                                                      c0,0,3.76-23.307,5.579-48.781c-5.224,0-45.485,0-45.485,0s0-28.276,0-33.231c0-4.962,6.518-11.641,12.965-11.641                                                      c6.436,0,20.015,0,32.587,0c0-6.623,0-29.481,0-50.592c-16.786,0-35.883,0-44.306,0C194.201,93.028,195.682,141.671,195.682,148.937                                                      z"></path>
								</svg>
							</div>
							<span>Facebook</span>
						</a>
					</div>
					<div class="item-socialMenuRes">
						<a id="hrefGooglePlusMobile" class="share-g-plus linkShareHerf" href="flamingo-catba-beach-resort-5a2a4a264f898.html" target="_blank" >
							<div class="cnt-iconSVG">
								<svg id="Layer_1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 512 512" style="enable-background:new 0 0 512 512; width: 30px; height: 30px" xml:space="preserve">
								<circle style="fill:#CF4C3C;" cx="256" cy="256" r="256"></circle>
								<path style="fill:#AD3228;" d="M372.97,215.509l-36.521,43.939l-68.763-71.518h-95.008l-38.453,41.637v89.912L318.85,504.217                                                      c83.594-21.102,150.816-83.318,178.916-163.887L372.97,215.509z"></path>
								<path style="fill:#FFFFFF;" d="M212.289,275.344h45.789c-8.037,22.721-29.806,39.012-55.287,38.826                                                      c-30.92-0.228-56.491-24.964-57.689-55.863c-1.286-33.12,25.285-60.478,58.123-60.478c15.017,0,28.72,5.723,39.05,15.098                                                      c2.448,2.22,6.17,2.236,8.578-0.031l16.818-15.825c2.631-2.476,2.639-6.658,0.016-9.14c-16.382-15.524-38.359-25.198-62.595-25.669                                                      c-51.69-1.012-95.261,41.37-95.62,93.07c-0.365,52.09,41.75,94.429,93.753,94.429c50.014,0,90.869-39.159,93.605-88.485                                                      c0.072-0.619,0.121-21.52,0.121-21.52H212.29c-3.47,0-6.282,2.813-6.282,6.282v23.024                                                      C206.007,272.531,208.82,275.344,212.289,275.344L212.289,275.344z"></path>
								<path style="fill:#D1D1D1;" d="M374.531,241.847V219.35c0-3.041-2.463-5.504-5.504-5.504h-18.934c-3.041,0-5.506,2.463-5.506,5.504                                                      v22.497h-22.492c-3.041,0-5.51,2.463-5.51,5.506v18.932c0,3.039,2.467,5.506,5.51,5.506h22.492v22.494                                                      c0,3.041,2.463,5.506,5.506,5.506h18.934c3.041,0,5.504-2.465,5.504-5.506v-22.494h22.497c3.039,0,5.506-2.467,5.506-5.506v-18.932                                                      c0-3.041-2.467-5.506-5.506-5.506H374.531z"></path>
								</svg>
							</div>
							<span>Google Plus</span>
						</a>
					</div>
					<div class="item-socialMenuRes">
						<a id="hrefTwitterMobile" class="share-twitter linkShareHerf"  href="flamingo-catba-beach-resort-5a2a4a264f898.html" target="_blank" >
							<div class="cnt-iconSVG">
								<svg id="Layer_1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 512 512" style="enable-background:new 0 0 512 512; width: 30px; height: 30px" xml:space="preserve">
								<circle style="fill:#65A2D9;" cx="256" cy="256" r="256"></circle>
								<path style="fill:#3A7CA5;" d="M393.014,139.326c-26.703,23.169-53.253,43.475-74.954,71.852                                                       c-53.381,64.372-118.613,155.7-207.386,142.086l158.61,158.396c134.456-6.873,241.497-117.493,242.686-253.376L393.014,139.326z"></path>
								<path style="fill:#FFFFFF;" d="M397.872,162.471c-6.513,2.889-13.271,5.167-20.208,6.815c7.644-7.261,13.39-16.346,16.631-26.484                                                       c0.926-2.893-2.219-5.398-4.832-3.848c-9.65,5.725-20.044,10.016-30.894,12.762c-0.628,0.16-1.276,0.24-1.929,0.24                                                       c-1.979,0-3.896-0.733-5.411-2.065c-11.542-10.174-26.39-15.777-41.805-15.777c-6.672,0-13.405,1.04-20.016,3.091                                                       c-20.487,6.353-36.295,23.254-41.257,44.103c-1.86,7.818-2.362,15.648-1.496,23.264c0.097,0.876-0.314,1.486-0.569,1.772                                                       c-0.45,0.502-1.084,0.791-1.745,0.791c-0.072,0-0.15-0.003-0.224-0.01c-44.846-4.168-85.287-25.772-113.869-60.837                                                       c-1.455-1.789-4.253-1.569-5.415,0.422c-5.596,9.606-8.554,20.589-8.554,31.766c0,17.127,6.884,33.27,18.837,45.039                                                       c-5.027-1.193-9.893-3.07-14.414-5.582c-2.188-1.214-4.877,0.35-4.908,2.851c-0.31,25.445,14.588,48.087,36.905,58.282                                                       c-0.45,0.01-0.9,0.014-1.35,0.014c-3.537,0-7.121-0.338-10.645-1.015c-2.463-0.467-4.532,1.867-3.768,4.253                                                       c7.246,22.618,26.717,39.288,50.021,43.07c-19.339,12.983-41.863,19.83-65.302,19.83l-7.306-0.003c-2.255,0-4.16,1.469-4.73,3.65                                                       c-0.565,2.145,0.474,4.413,2.396,5.53c26.412,15.372,56.541,23.495,87.138,23.495c26.784,0,51.838-5.313,74.466-15.798                                                       c20.745-9.609,39.076-23.345,54.486-40.827c14.357-16.286,25.581-35.085,33.365-55.879c7.418-19.816,11.34-40.967,11.34-61.154                                                       v-0.964c0-3.241,1.465-6.291,4.024-8.37c9.706-7.882,18.16-17.158,25.122-27.572C403.796,164.578,400.896,161.13,397.872,162.471                                                       L397.872,162.471z"></path>
								<path style="fill:#D1D1D1;" d="M397.872,162.471c-6.515,2.889-13.271,5.167-20.208,6.815c7.644-7.261,13.39-16.346,16.632-26.484                                                       c0.926-2.893-2.219-5.398-4.832-3.848c-9.65,5.725-20.044,10.016-30.894,12.762c-0.628,0.16-1.276,0.24-1.929,0.24                                                       c-1.979,0-3.896-0.733-5.411-2.065c-11.542-10.174-26.39-15.777-41.805-15.777c-6.671,0-13.405,1.04-20.016,3.091                                                       c-14.322,4.441-26.343,14.048-33.985,26.546v205.477c6.222-2.029,12.293-4.403,18.198-7.139                                                       c20.745-9.609,39.076-23.345,54.486-40.827c14.357-16.287,25.581-35.085,33.365-55.879c7.418-19.816,11.34-40.967,11.34-61.154                                                       v-0.964c0-3.241,1.465-6.291,4.024-8.37c9.706-7.882,18.16-17.158,25.122-27.572C403.796,164.578,400.896,161.13,397.872,162.471z"></path>
								</svg>
							</div>
							<span>Twitter</span>
						</a>
					</div>
					<div class="iconDropDown"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="content-siteDetailDuAn">
		<!--Sidebar left-->
		<div class="content-sidebar-menuLeft">
			<div class="sidebar-left-detailDuAn">
				<div class="sidebar-left">
					<div class="background-sidebar-left">
						<div class="menuLeft-detailDuAn">
							<div class="click-slide-menuLeft menu-sider-off">
								<img src="/jinnV2/images/icon/click-menu-left0995.png?v=0.9.0.4" class="menu-on">
								<img src="/jinnV2/images/icon/click-menu-left20995.png?v=0.9.0.4" class="menu-off">
							</div>
							<div class="click-slide-menuLeft menu-sider-on">
								<img src="/jinnV2/images/icon/close-menu-left0995.png?v=0.9.0.4" class="menu-on">
								<img src="/jinnV2/images/icon/close-menu-left-20995.png?v=0.9.0.4" class="menu-off">
							</div>
						</div>
						<div class="dot-menu-titleSection">
							<div class="dot-menu">
								<ul>
									<li id="detailSurveyDot" class="item-dot active">
										<a href="javascript: void (0)"></a>
									</li>
									<li id="groundProjectDot" class="item-dot">
										<a href="javascript: void (0)"></a>
									</li>
									<li id="reasonChooseProjectDot" class="item-dot">
										<a href="javascript: void (0)"></a>
									</li>
									<li  id="newsProjectDot" class="item-dot">
										<a href="javascript: void (0)"></a>
									</li>
									<li id="blockProjectDot" class="item-dot">
										<a href="javascript: void (0)"></a>
									</li>
									<li id="contactProjectDot" class="item-dot">
										<a href="javascript: void (0)"></a>
									</li>
								</ul>
							</div>
							<div class="title-sectionLeft">Tổng quan dự án</div>
						</div>
					</div>
				</div>
			</div>
			<!--Site menu left-->
			<div class="site-menuLeft">
				<div class="content-side-MenuLeft">
					<ul>
						<h2>Flamingo Cát Bà Beach Resort</h2>
						<li class="menuLeft-item active"><a id="detailSurveyLink" href="#">Tổng quan dự án</a>
						</li>
						<li class="menuLeft-item"><a id="groundProjectLink" href="#">Mặt bằng dự án & Tiện ích</a>
						</li>
						<li class="menuLeft-item"><a id="reasonChooseProjectLink" href="#">Lý do lựa chọn Flamingo Cát Bà Beach Resort</a>
						</li>
						<li class="menuLeft-item"><a id="newsProjectLink" href="#">Tin Tức</a>
						</li>
						<li class="menuLeft-item"><a id="blockProjectLink" href="#">Quỹ căn</a>
						</li>
						<li class="menuLeft-item"><a id="contactProjectLink" href="#">Liên hệ</a>
						</li>
					</ul>
					<div class="social-menuLeft">
						<div class="facebook-menuLeft item-social-menuLeft">
							<a id="hrefFacebookDesktop" class="linkShareHerf share-facebook" href="http://www.facebook.com/sharer.php?t=Flamingo%20C%c3%a1t%20B%c3%a0%20Beach%20Resort&amp;u=jinn.vn/vi/projects/flamingo-catba-beach-resort-5a2a4a264f898" target="_blank" >
								<svg id="Layer_1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 474.294 474.294" style="enable-background:new 0 0 474.294 474.294; width: 30px; height: 30px;" xml:space="preserve">
								<circle style="fill:#3A5A98;" cx="237.111" cy="236.966" r="236.966"></circle>
								<path style="fill:#345387;" d="M404.742,69.754c92.541,92.541,92.545,242.586-0.004,335.134                          c-92.545,92.541-242.593,92.541-335.134,0L404.742,69.754z"></path>
								<path style="fill:#2E4D72;" d="M472.543,263.656L301.129,92.238l-88.998,88.998l5.302,5.302l-50.671,50.667l41.474,41.474                          l-5.455,5.452l44.901,44.901l-51.764,51.764l88.429,88.429C384.065,449.045,461.037,366.255,472.543,263.656z"></path>
								<path style="fill:#FFFFFF;" d="M195.682,148.937c0,7.27,0,39.741,0,39.741h-29.115v48.598h29.115v144.402h59.808V237.276h40.134                          c0,0,3.76-23.307,5.579-48.781c-5.224,0-45.485,0-45.485,0s0-28.276,0-33.231c0-4.962,6.518-11.641,12.965-11.641                          c6.436,0,20.015,0,32.587,0c0-6.623,0-29.481,0-50.592c-16.786,0-35.883,0-44.306,0C194.201,93.028,195.682,141.671,195.682,148.937                          z"></path>
								</svg>
							</a>
						</div>
						<div class="google-plus-menuLeft item-social-menuLeft">
							<a id="hrefGooglePlusDesktop" class="linkShareHerf share-g-plus" href="https://plus.google.com/share?url=jinn.vn/vi/projects/flamingo-catba-beach-resort-5a2a4a264f898" target="_blank" >
								<svg id="Layer_1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 512 512" style="enable-background:new 0 0 512 512; width: 30px; height: 30px" xml:space="preserve">
								<circle style="fill:#CF4C3C;" cx="256" cy="256" r="256"></circle>
								<path style="fill:#AD3228;" d="M372.97,215.509l-36.521,43.939l-68.763-71.518h-95.008l-38.453,41.637v89.912L318.85,504.217                          c83.594-21.102,150.816-83.318,178.916-163.887L372.97,215.509z"></path>
								<path style="fill:#FFFFFF;" d="M212.289,275.344h45.789c-8.037,22.721-29.806,39.012-55.287,38.826                          c-30.92-0.228-56.491-24.964-57.689-55.863c-1.286-33.12,25.285-60.478,58.123-60.478c15.017,0,28.72,5.723,39.05,15.098                          c2.448,2.22,6.17,2.236,8.578-0.031l16.818-15.825c2.631-2.476,2.639-6.658,0.016-9.14c-16.382-15.524-38.359-25.198-62.595-25.669                          c-51.69-1.012-95.261,41.37-95.62,93.07c-0.365,52.09,41.75,94.429,93.753,94.429c50.014,0,90.869-39.159,93.605-88.485                          c0.072-0.619,0.121-21.52,0.121-21.52H212.29c-3.47,0-6.282,2.813-6.282,6.282v23.024                          C206.007,272.531,208.82,275.344,212.289,275.344L212.289,275.344z"></path>
								<path style="fill:#D1D1D1;" d="M374.531,241.847V219.35c0-3.041-2.463-5.504-5.504-5.504h-18.934c-3.041,0-5.506,2.463-5.506,5.504                          v22.497h-22.492c-3.041,0-5.51,2.463-5.51,5.506v18.932c0,3.039,2.467,5.506,5.51,5.506h22.492v22.494                          c0,3.041,2.463,5.506,5.506,5.506h18.934c3.041,0,5.504-2.465,5.504-5.506v-22.494h22.497c3.039,0,5.506-2.467,5.506-5.506v-18.932                          c0-3.041-2.467-5.506-5.506-5.506H374.531z"></path>
								</svg>
							</a>
						</div>
						<div class="twitter-menuLeft item-social-menuLeft">
							<a id="hrefTwitterDesktop" class="linkShareHerf share-twitter" href="http://twitter.com/share?text=Flamingo%20C%c3%a1t%20B%c3%a0%20Beach%20Resort&amp;url=jinn.vn/vi/projects/flamingo-catba-beach-resort-5a2a4a264f898" target="_blank" >
								<svg id="Layer_1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 512 512" style="enable-background:new 0 0 512 512; width: 30px; height: 30px" xml:space="preserve">
								<circle style="fill:#65A2D9;" cx="256" cy="256" r="256"></circle>
								<path style="fill:#3A7CA5;" d="M393.014,139.326c-26.703,23.169-53.253,43.475-74.954,71.852                           c-53.381,64.372-118.613,155.7-207.386,142.086l158.61,158.396c134.456-6.873,241.497-117.493,242.686-253.376L393.014,139.326z"></path>
								<path style="fill:#FFFFFF;" d="M397.872,162.471c-6.513,2.889-13.271,5.167-20.208,6.815c7.644-7.261,13.39-16.346,16.631-26.484                           c0.926-2.893-2.219-5.398-4.832-3.848c-9.65,5.725-20.044,10.016-30.894,12.762c-0.628,0.16-1.276,0.24-1.929,0.24                           c-1.979,0-3.896-0.733-5.411-2.065c-11.542-10.174-26.39-15.777-41.805-15.777c-6.672,0-13.405,1.04-20.016,3.091                           c-20.487,6.353-36.295,23.254-41.257,44.103c-1.86,7.818-2.362,15.648-1.496,23.264c0.097,0.876-0.314,1.486-0.569,1.772                           c-0.45,0.502-1.084,0.791-1.745,0.791c-0.072,0-0.15-0.003-0.224-0.01c-44.846-4.168-85.287-25.772-113.869-60.837                           c-1.455-1.789-4.253-1.569-5.415,0.422c-5.596,9.606-8.554,20.589-8.554,31.766c0,17.127,6.884,33.27,18.837,45.039                           c-5.027-1.193-9.893-3.07-14.414-5.582c-2.188-1.214-4.877,0.35-4.908,2.851c-0.31,25.445,14.588,48.087,36.905,58.282                           c-0.45,0.01-0.9,0.014-1.35,0.014c-3.537,0-7.121-0.338-10.645-1.015c-2.463-0.467-4.532,1.867-3.768,4.253                           c7.246,22.618,26.717,39.288,50.021,43.07c-19.339,12.983-41.863,19.83-65.302,19.83l-7.306-0.003c-2.255,0-4.16,1.469-4.73,3.65                           c-0.565,2.145,0.474,4.413,2.396,5.53c26.412,15.372,56.541,23.495,87.138,23.495c26.784,0,51.838-5.313,74.466-15.798                           c20.745-9.609,39.076-23.345,54.486-40.827c14.357-16.286,25.581-35.085,33.365-55.879c7.418-19.816,11.34-40.967,11.34-61.154                           v-0.964c0-3.241,1.465-6.291,4.024-8.37c9.706-7.882,18.16-17.158,25.122-27.572C403.796,164.578,400.896,161.13,397.872,162.471                           L397.872,162.471z"></path>
								<path style="fill:#D1D1D1;" d="M397.872,162.471c-6.515,2.889-13.271,5.167-20.208,6.815c7.644-7.261,13.39-16.346,16.632-26.484                           c0.926-2.893-2.219-5.398-4.832-3.848c-9.65,5.725-20.044,10.016-30.894,12.762c-0.628,0.16-1.276,0.24-1.929,0.24                           c-1.979,0-3.896-0.733-5.411-2.065c-11.542-10.174-26.39-15.777-41.805-15.777c-6.671,0-13.405,1.04-20.016,3.091                           c-14.322,4.441-26.343,14.048-33.985,26.546v205.477c6.222-2.029,12.293-4.403,18.198-7.139                           c20.745-9.609,39.076-23.345,54.486-40.827c14.357-16.287,25.581-35.085,33.365-55.879c7.418-19.816,11.34-40.967,11.34-61.154                           v-0.964c0-3.241,1.465-6.291,4.024-8.37c9.706-7.882,18.16-17.158,25.122-27.572C403.796,164.578,400.896,161.13,397.872,162.471z"></path>
								</svg>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--Content Detail 1-->
		<div data-src="https://images.jinn.vn/gt1-jpgb8l6f5iuumfg00at1uug.jpeg"  class="background-detail1 lazyImage" >
			<!--Content Detail 1-->
			<div class="content-detail1">
				<div class="background-content-detail1">
					<h2>Flamingo Cát Bà Beach Resort</h2>
					<div class="text-content-detail1">Flamingo Cát Bà Beach Resort sở hữu vị trí đắc địa trên bãi biển đẹp nhất của Cát Bà, với kiến trúc siêu phẩm “Cả tòa nhà là một cánh rừng, cả tòa nhà là một vườn hoa” cùng với vô vàn tiện ích 5 sao vượt trội, là một nơi phong thủy cát tường vượng lộc cho chủ nhân, xứng tầm là “ Bản Giao hưởng 4 mùa, khúc ca tuyệt vời giao hòa trời biển”</div>
				</div>
			</div>
		</div>
		<!--Content Deatail 2-->
		<div class="content-tongQuanDuAn" id="detailSurvey">
			<div data-src="/jinnV2/images/img/background-d2.jpg?v=0.9.0.4" class="background-tongQuanDuAn lazyImage">
				<!--section 2 left-->
				<div class="background-section2-left">
					<div class="title-section2-Responsive">
						<h2 class="h2Left">Flamingo Cát Bà Beach Resort</h2>
						<h2 class="h2Right">Bản giao hưởng 4 mùa</h2>
					</div>
					<div class="content-section2-left" >
						<div class="title-section2-left">
							<h2>Flamingo Cát Bà Beach Resort</h2>
						</div>
						<div class="content-tabs-section2">
							<div class="cnt-tabsSection2">
								<ul role="tablist" class="nav nav-tabs">
									<li class="nav-item active"><a data-toggle="tab" href="#tong_quan" role="tab" class="nav-link">Tổng quan</a>
									</li>
									<Settings></Settings>
								</ul>
							</div>
							<!-- Tab panes-->
							<div class="tab-content">
								<div id="tong_quan" role="tabpanel" class="tab-pane active">
									<div class="content-list-infor-duAn">
										<div class="infor-address-tongQuan">
											<div class="icon-svg-infor">
												<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
													 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; width: 15px; height: 15px;" xml:space="preserve">
												<style type="text/css">
													.st-address{fill:#FFFFFF;}
												</style>
												<g>
												<path class="st-address" d="M259,222.5c32.3,0,58.6-26.3,58.6-58.6s-26.3-58.6-58.6-58.6s-58.6,26.3-58.6,58.6S226.7,222.5,259,222.5z
													  M259,122.1c23.1,0,41.9,18.8,41.9,41.9S282,205.8,259,205.8c-23.1,0-41.9-18.8-41.9-41.9S235.9,122.1,259,122.1z"></path>
												<path class="st-address" d="M420.3,314.6h-59.9l25.5-36.8C434.1,213.5,427.1,108,371,51.9c-30.3-30.3-70.6-47-113.5-47
													  c-42.9,0-83.2,16.7-113.5,47C87.9,108,80.9,213.6,128.9,277.5l25.7,37.1H91.7L0,507.1h512L420.3,314.6z M142.5,267.7
													  c-43.5-58-37.3-153.3,13.4-204c27.2-27.2,63.3-42.1,101.7-42.1s74.5,15,101.7,42.1c50.6,50.6,56.9,146,13.2,204.2L257.5,433.8
													  l-82.6-119.2l0,0L142.5,267.7z M102.3,331.3h63.9l91.3,131.9l91.3-131.9h60.9l75.7,159h-459L102.3,331.3z"></path>
												</g>
												</svg>
											</div>
											<div class="value-infor-tongQuan">
												Bãi tắm Cát Cò 1, Cát Cò 2, thị trấn Cát Bà, huyện Cát Hải, thành phố Hải Phòng.
											</div>
										</div>
										<div class="item-listTQ">
											<div class="item-listtq">
												<div class="icon-svg-infor">
													<svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="width: 15px; height: 15px;">
													<defs>
													<style>
														.cls-huong {
															fill: #fff;
														}
													</style>
													</defs>
													<title>navigation1 trang</title>
													<path d="M44.4,13.1,8.6,29.5a1.1,1.1,0,0,0-.6,1,1.1,1.1,0,0,0,.8.9l15.8,2.7,1.8,15a1.2,1.2,0,0,0,.8.9h.2a1,1,0,0,0,.9-.6L45.6,14.5a.9.9,0,0,0-.2-1.1A1,1,0,0,0,44.4,13.1ZM28.1,45.5,26.6,33.1a1.2,1.2,0,0,0-.8-.9L12.4,30,42.6,16.1Z" class="cls-huong"></path>
													<path d="M30,0A30,30,0,1,0,60,30,30.1,30.1,0,0,0,30,0Zm0,58A28,28,0,1,1,58,30,28.1,28.1,0,0,1,30,58Z" class="cls-huong"></path>
													</svg>
												</div>
												<div class="value-infor-tongQuan">
													<span>Đông Nam</span>
												</div>
											</div>
											<div class="item-listtq">
												<div class="icon-svg-infor">
													<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
														 viewBox="0 0 537 542" style="enable-background:new 0 0 537 542; width: 15px; height: 15px;" xml:space="preserve">
													<style type="text/css">
														.st0{fill:#FFFFFF;}
														.st1{fill:#323232;}
														.st2{fill:none;stroke:#FFFFFF;stroke-width:14;stroke-miterlimit:10;}
													</style>
													<g>
													<circle class="st0" cx="-605" cy="64" r="256"/>
													<g>
													<g>
													<g>
													<path class="st1" d="M-649.1,205.6c0,0.1,0,0.2,0,0.3c0,24.4,19.8,44.1,44.1,44.1s44.1-19.8,44.1-44.1c0-0.1,0-0.2,0-0.3
														  L-649.1,205.6L-649.1,205.6z"></path>
													<path class="st1" d="M-464.2,150.5l-39.7-58.3c0-17.9,0-61.5,0-72.8c0-49.1-34.9-90-81.3-99.2v-22.4c0-11-8.9-19.8-19.8-19.8
														  c-11,0-19.8,8.9-19.8,19.8v22.4c-46.4,9.2-81.3,50.1-81.3,99.2c0,19.2,0,61.6,0,72.8l-39.7,58.3c-4.2,6.1-4.6,14.1-1.1,20.7
														  c3.5,6.6,10.3,10.7,17.7,10.7h248.5c7.4,0,14.3-4.1,17.7-10.7C-459.5,164.6-460,156.6-464.2,150.5z"></path>
													</g>
													</g>
													</g>
													</g>
													<g>
													<g>
													<path class="st0" d="M607,54.1c0-80.1,0-160.2,0-240.2c0.5,1,1,2.1,1.5,3.1c3,6.5,10,10.4,16.4,9.2c7.9-1.5,12.9-7,13-15
														  c0.2-14.6,0.2-29.2,0-43.8c-0.1-11.3,3.6-21.4,10.2-30.1c6.4-8.4,15.9-11.1,26.2-11.2c15.1-0.1,30.2,0,45.3,0
														  c7,0,12.3-4,14.4-10.5c3.5-10.7-3.9-19.9-16.6-20.1c-15.9-0.2-31.8-0.1-47.7,0.4c-27.7,0.8-49.6,17.8-57.7,44.5
														  c-1.8,5.8-3.3,11.6-4.9,17.4c0-27.6,0-55.1,0-82.7c170.7,0,341.3,0,512,0c0,27.6,0,55.1,0,82.7c-1.7-6.1-3.1-12.4-5.2-18.4
														  c-7.9-22.5-22.7-38.9-46.8-42.1c-20.2-2.7-40.9-1.6-61.3-1.5c-9,0-15,7.4-14.5,15.9s7.1,14.3,16.2,14.3c14.9,0,29.9-0.1,44.8,0.1
														  c10.5,0.1,20,3.1,26.3,11.9c6.9,9.5,9.9,20.4,9.6,32.3c-0.3,13.6-0.2,27.2,0,40.8c0.1,8,5.1,13.5,13,15c6.4,1.2,13.3-2.7,16.4-9.2
														  c0.5-1,1-2.1,1.5-3.1c0,80.1,0,160.2,0,240.2c-0.6-1.2-1.1-2.3-1.7-3.5c-3.7-7.6-12.1-11-19.8-7.7c-6.6,2.8-9.3,8.2-9.4,15.3
														  c0,16.2,0.9,32.5-0.6,48.6c-2,21.7-14.5,35.6-37.9,35c-14.6-0.4-29.2-0.1-43.8,0.1c-6.6,0.1-11.1,3.7-13.4,9.7
														  c-3.7,9.5,2.3,19.9,12.6,20.4c13.4,0.7,26.9,0.5,40.3,0.5c6.2,0,12.5,0.1,18.6-0.9c32.6-5.1,46.9-29.2,52.8-51
														  c0.9-3.4,1.6-7,2.4-10.5c0,27.6,0,55.1,0,82.7c-170.7,0-341.3,0-512,0c0-27.6,0-55.1,0-82.7c0.7,3.2,1.5,6.3,2.2,9.5
														  c5.1,22,20.8,47.7,51,51.2c20.4,2.3,41.2,1.1,61.8,1.1c4.9,0,8.4-3.3,10.8-7.6c5.4-10-1.2-22.2-12.6-22.5
														  c-15.9-0.4-31.8,0-47.7-0.3c-13.9-0.2-24.5-6.6-29.8-19.4c-2.9-6.9-4.2-14.8-4.6-22.4c-0.7-14.4-0.1-28.8-0.3-43.3
														  c-0.1-8-5.1-13.4-13-14.9c-6.3-1.2-13.3,2.8-16.3,9.2C608,52.1,607.5,53.1,607,54.1z M852.8,87.9c-2.8,0-4.5,0-6.3,0
														  c-18.7,0-37.4,0-56.1,0c-7.3,0-11.6,3.7-11.9,9.6c-0.4,6.7,3.7,10.7,11,10.7c44.1,0,88.3,0,132.4,0c5.6,0,11.2,0,16.7,0
														  c4.2,0,7.2-2,8.9-5.8c3.2-7.2-1.9-14.4-10.6-14.5c-13.8-0.2-27.6,0-41.3,0c-7.2,0-14.4,0-22.2,0c0-14.3-0.1-28.2,0.1-42.1
														  c0-0.9,2-2.4,3.2-2.5c38.4-5,69.5-22,91.7-54.3c27.4-39.9,31.7-82.7,13.7-127.1c-21.7-53.4-77.5-85.1-134.7-78.2
														  c-64.9,7.9-113.7,62.8-114.1,128.5c-0.2,31.5,10.1,59.6,29.5,84.1c19.9,25.1,46.5,40,78,45.7c11.9,2.2,12,1.9,12,13.9
														  C852.8,66.3,852.8,76.8,852.8,87.9z"></path>
													<path d="M607-242.2c1.6-5.8,3.1-11.6,4.9-17.4c8.1-26.7,30-43.7,57.7-44.5c15.9-0.5,31.8-0.5,47.7-0.4
														  c12.7,0.1,20.1,9.3,16.6,20.1c-2.1,6.5-7.4,10.5-14.4,10.5c-15.1,0.1-30.2,0-45.3,0c-10.3,0.1-19.8,2.7-26.2,11.2
														  c-6.7,8.8-10.3,18.9-10.2,30.1c0.1,14.6,0.1,29.2,0,43.8c-0.1,8-5.1,13.5-13,15c-6.4,1.2-13.3-2.7-16.4-9.2c-0.5-1-1-2.1-1.5-3.1
														  C607-204.8,607-223.5,607-242.2z"></path>
													<path d="M1119-186.1c-0.5,1-1,2.1-1.5,3.1c-3.1,6.5-10,10.4-16.4,9.2c-7.9-1.5-12.9-7-13-15c-0.2-13.6-0.3-27.2,0-40.8
														  c0.3-11.9-2.8-22.8-9.6-32.3c-6.3-8.8-15.9-11.8-26.3-11.9c-14.9-0.1-29.9,0-44.8-0.1c-9.1,0-15.7-5.9-16.2-14.3
														  c-0.5-8.5,5.4-15.9,14.5-15.9c20.5-0.1,41.2-1.1,61.3,1.5c24.1,3.2,38.9,19.5,46.8,42.1c2.1,6,3.5,12.2,5.2,18.4
														  C1119-223.5,1119-204.8,1119-186.1z"></path>
													<path d="M607,54.1c0.5-1,1-2,1.5-3.1c3-6.5,10-10.4,16.3-9.2c7.9,1.5,12.9,6.9,13,14.9c0.2,14.4-0.4,28.9,0.3,43.3
														  c0.4,7.5,1.8,15.4,4.6,22.4c5.3,12.8,15.9,19.2,29.8,19.4c15.9,0.2,31.8-0.1,47.7,0.3c11.4,0.3,18,12.4,12.6,22.5
														  c-2.3,4.3-5.9,7.6-10.8,7.6c-20.6,0-41.4,1.2-61.8-1.1c-30.3-3.4-46-29.2-51-51.2c-0.7-3.2-1.5-6.3-2.2-9.5
														  C607,91.6,607,72.8,607,54.1z"></path>
													<path d="M1119,110.3c-0.8,3.5-1.4,7-2.4,10.5c-5.9,21.8-20.2,45.9-52.8,51c-6.1,1-12.4,0.9-18.6,0.9c-13.4,0-26.9,0.3-40.3-0.5
														  c-10.2-0.5-16.3-10.9-12.6-20.4c2.3-6,6.8-9.6,13.4-9.7c14.6-0.2,29.2-0.5,43.8-0.1c23.5,0.6,35.9-13.3,37.9-35
														  c1.5-16.1,0.6-32.4,0.6-48.6c0-7.1,2.7-12.5,9.4-15.3c7.7-3.3,16.2,0.1,19.8,7.7c0.6,1.2,1.1,2.3,1.7,3.5
														  C1119,72.8,1119,91.6,1119,110.3z"></path>
													<path d="M852.8,87.9c0-11.1,0-21.6,0-32c0-12,0-11.7-12-13.9c-31.5-5.7-58.1-20.5-78-45.7c-19.4-24.5-29.7-52.6-29.5-84.1
														  c0.4-65.7,49.3-120.6,114.1-128.5c57.2-7,113,24.8,134.7,78.2c18,44.4,13.6,87.2-13.7,127.1c-22.2,32.3-53.3,49.3-91.7,54.3
														  c-1.2,0.2-3.2,1.7-3.2,2.5c-0.2,13.9-0.1,27.8-0.1,42.1c7.8,0,15,0,22.2,0c13.8,0,27.6-0.1,41.3,0c8.7,0.1,13.8,7.3,10.6,14.5
														  c-1.7,3.8-4.7,5.8-8.9,5.8c-5.6,0-11.2,0-16.7,0c-44.1,0-88.3,0-132.4,0c-7.3,0-11.3-4-11-10.7c0.3-6,4.7-9.6,11.9-9.6
														  c18.7-0.1,37.4,0,56.1,0C848.3,87.9,850,87.9,852.8,87.9z M873.9-33.7c0-12.9,0.1-24.5-0.1-36.1c-0.1-3.5,1.2-5.7,3.9-7.7
														  c12.2-9.4,24.3-19,36.3-28.6c5.7-4.5,6.6-10.5,2.6-15.5c-3.9-5-10.1-5.6-15.6-1.3c-7.5,5.8-14.9,11.8-22.4,17.7
														  c-1.3,1-2.7,2-4.8,3.5c0-2.9,0-4.7,0-6.5c0-14.1,0.2-28.2-0.2-42.3c-0.1-3.5-1.6-7.4-3.8-10.1c-2.5-3.2-6.7-3.8-10.8-2.1
														  c-5.1,2.1-7,6.3-7,11.4c-0.1,14.6,0,29.2,0,43.8c0,1.7,0,3.4,0,6c-9.4-7.4-17.6-14.3-26.2-20.5c-2.7-2-6.5-3.4-9.7-3.4
														  c-4.6,0-7.7,3.4-8.9,8c-1.2,4.7,0.8,8.3,4.4,11.2c12.3,9.8,24.8,19.5,37,29.4c1.7,1.4,3.2,4.2,3.3,6.4c0.3,11,0.2,22,0.1,33
														  c0,1-0.3,2-0.6,3.5c-9.8-7.3-19.1-14.3-28.4-21.3c-9.4-7.1-18.8-14.3-28.3-21.3c-6.6-4.8-14.7-2.2-17.1,5.2
														  c-1.5,4.5,0.3,8.8,5.3,12.6c21.6,16.3,43.2,32.6,65,48.7c3.2,2.3,4.3,4.8,4.2,8.6c-0.2,11.5-0.1,23,0,34.5c0,4.6,0.8,8.9,5.9,10.8
														  c9.2,3.4,15.7-1.1,15.8-11c0.1-11.6,0.1-23.3,0-34.9c0-3.3,1.1-5.4,3.7-7.4c16.6-12.3,33.1-24.8,49.6-37.2
														  c5.8-4.3,11.6-8.6,17.3-13.1c5.8-4.7,6-11.9,0.6-16.6c-4.1-3.6-9.4-3.4-14.8,0.6c-13.6,10.2-27.3,20.5-40.9,30.7
														  C884.5-41.6,879.7-38,873.9-33.7z"></path>
													<path class="st0" d="M873.9-33.7c5.8-4.4,10.6-8,15.4-11.6c13.6-10.2,27.3-20.5,40.9-30.7c5.4-4.1,10.7-4.2,14.8-0.6
														  c5.4,4.7,5.2,11.9-0.6,16.6c-5.6,4.5-11.5,8.7-17.3,13.1c-16.5,12.4-33,24.9-49.6,37.2c-2.7,2-3.8,4.1-3.7,7.4
														  c0.1,11.6,0.1,23.3,0,34.9c-0.1,9.9-6.6,14.3-15.8,11c-5.1-1.9-5.9-6.2-5.9-10.8c-0.1-11.5-0.2-23,0-34.5c0.1-3.8-1.1-6.3-4.2-8.6
														  c-21.7-16.1-43.3-32.4-65-48.7c-5-3.8-6.8-8.1-5.3-12.6c2.4-7.4,10.5-10,17.1-5.2c9.5,7,18.9,14.2,28.3,21.3
														  c9.3,7,18.6,13.9,28.4,21.3c0.3-1.5,0.6-2.5,0.6-3.5c0-11,0.2-22-0.1-33c-0.1-2.2-1.6-5-3.3-6.4c-12.2-10-24.6-19.6-37-29.4
														  c-3.6-2.8-5.6-6.4-4.4-11.2c1.2-4.6,4.3-7.9,8.9-8c3.3,0,7,1.4,9.7,3.4c8.6,6.3,16.8,13.1,26.2,20.5c0-2.7,0-4.4,0-6
														  c0-14.6-0.1-29.2,0-43.8c0-5.1,1.9-9.2,7-11.4c4.1-1.7,8.3-1.1,10.8,2.1c2.1,2.7,3.7,6.7,3.8,10.1c0.4,14.1,0.2,28.2,0.2,42.3
														  c0,1.8,0,3.6,0,6.5c2.1-1.5,3.4-2.5,4.8-3.5c7.5-5.9,14.9-11.9,22.4-17.7c5.5-4.3,11.7-3.6,15.6,1.3c3.9,5,3,11-2.6,15.5
														  c-12,9.6-24.1,19.2-36.3,28.6c-2.7,2.1-4,4.2-3.9,7.7C874-58.2,873.9-46.5,873.9-33.7z"></path>
													</g>
													</g>
													<g>
													<path class="st2" d="M427.1,497.6h-318V30.4c0-12.9,10.5-23.4,23.4-23.4h271.2c12.9,0,23.4,10.5,23.4,23.4V497.6z"></path>
													<rect x="167.6" y="101.5" class="st2" width="71" height="74.7"></rect>
													<rect x="298.1" y="101.5" class="st2" width="71" height="74.7"></rect>
													<rect x="167.6" y="236.5" class="st2" width="71" height="74.7"></rect>
													<rect x="298.1" y="236.5" class="st2" width="71" height="74.7"></rect>
													<rect x="167.6" y="371.5" class="st2" width="71" height="74.7"></rect>
													<rect x="298.1" y="371.5" class="st2" width="71" height="74.7"></rect>
													<path class="st2" d="M513.3,527H23.7C15.6,527,9,520.4,9,512.3l0,0c0-8.1,6.6-14.7,14.7-14.7h489.7c8.1,0,14.7,6.6,14.7,14.7l0,0
														  C528.1,520.4,521.4,527,513.3,527z"></path>
													</g>
													</svg>
												</div>
												<div class="value-infor-tongQuan">
													3 block
												</div>
											</div>
											<div class="item-listtq">
												<div class="icon-svg-infor">
													<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
														 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; width: 15px; height: 15px;" xml:space="preserve">
													<style type="text/css">
														.st-homes{fill:#FFFFFF;}
													</style>
													<g>
													<g>
													<path class="st-homes" d="M439.5,183.1V75.3h-89.2v39.2L256,42L0,238.9l53.6,69.7l26.3-20.2V470h151.5V337h55.2v133h145.5V288.4
														  l26.3,20.2l53.6-69.7L439.5,183.1z M402.1,440h-85.5v-133H201.4v133h-91.4V265.3L256,153l146.1,112.4L402.1,440L402.1,440z
														  M452.9,266.5L256,115.1L59.1,266.5l-17-22.1L256,79.9l124.3,95.6v-70.2h29.1v92.6l60.4,46.5L452.9,266.5z"></path>
													</g>
													</g>
													</svg>
												</div>
												<div class="value-infor-tongQuan">
													810 Sản phẩm
												</div>
											</div>
											<div class="item-listtq">
												<div class="icon-svg-infor">
													<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
														 viewBox="0 0 17 17" style="enable-background:new 0 0 17 17; width: 15px; height: 15px;" xml:space="preserve">
													<style type="text/css">
														.st-tree{fill:#FFFFFF;}
														.st1-tree{fill:none;stroke:#FFFFFF;stroke-miterlimit:10;}
														.st2-tree{fill:none;stroke:#323232;stroke-miterlimit:10;}
													</style>
													<g>
													<path class="st-tree" d="M8,12.4v3.2H1.2c-0.3,0-0.6,0.2-0.6,0.5c0,0.3,0.3,0.5,0.6,0.5H16c0.3,0,0.6-0.2,0.6-0.5
														  c0-0.3-0.3-0.5-0.6-0.5H9.2v-3.2 M5.8,8.9C4.4,8.1,3.5,7.6,3.5,7.5C3.4,7.4,3.4,7.2,3.6,7.1C3.7,7,3.9,6.9,4.1,7
														  c0.1,0,0.7,0.4,3.7,2l0.2,0.1v-1v-1L6.7,6.3C6,5.9,5.4,5.5,5.2,5.4C5.2,5.3,5.2,5.1,5.4,5C5.6,4.8,5.8,4.8,6,4.9
														  c0.1,0,0.6,0.3,1.1,0.5L8,5.9V4.6c0-1.4,0-1.4,0.4-1.5C8.6,3,8.6,3,8.8,3.1c0.1,0,0.2,0.1,0.3,0.1c0,0.2,0,0.3,0,1.5
														  c0,0.7,0,1.3,0,1.3s0.5-0.3,1-0.6s1-0.6,1.1-0.6c0.2,0,0.5,0.1,0.6,0.2c0.1,0.1,0.1,0.3,0,0.4c0,0.1-0.7,0.4-1.4,0.9L9.1,7.1v1
														  c0,0.8,0,1,0.1,1c0,0,0.9-0.5,1.9-1.1s1.9-1,2-1.1c0.2,0,0.4,0.1,0.6,0.2c0.1,0.1,0.1,0.4,0,0.5c-0.1,0.1-1.1,0.6-2.3,1.3l-2.3,1.2
														  v1v1l-0.1,0.1c-0.2,0.2-0.4,0.2-0.7,0.1c-0.3-0.1-0.4-0.2-0.4-1.3v-1L5.8,8.9z"></path>
													<ellipse class="st1-tree" cx="8.4" cy="6.4" rx="8" ry="6"></ellipse>
													</g>
													<path class="st2-tree" d="M8.5,3.4"></path>
													</svg>
												</div>
												<div class="value-infor-tongQuan">
													---
												</div>
											</div>
										</div>
									</div>
									<div class="description active">
										<div class="short" style="overflow: hidden;">
											<p class="MsoNormal" style="text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Flamingo Cát Bà Beach Resort</span></p>
											<p class="MsoNormal" style="text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Bản Giao hưởng 4 mùa, khúc ca tuyệt vời giao hòa trời biển</span></p>
											<p class="MsoNormal" style="text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Flamingo Cát Bà Beach Resort chính thức được tập đoàn Flamingo Group đầu tư với quy mô lớn, trở thành một quần thể phức hợp nghỉ dưỡng vô cùng đẳng cấp. Tọa lạc trên diện tích 77.843m2 với vị trí đắc địa trên bãi biển đẹp nhất của Cát Bà, thành phố Hải Phòng: Bãi tắm Cát Cò 1 và Cát Cò 2 – nơi được xem là thiên đường nghỉ dưỡng, cùng với tầm nhìn đắt giá ôm trọn quang cảnh núi và Vịnh Lan Mạ, quần thể phức hợp nghỉ dưỡng đẳng cấp Flamingo Cát Bà Beach Resort sẽ mang đến cho khách hàng những trải nghiệm tinh tế và đầy cảm xúc.</span></p>
											<p class="MsoNormal" style="text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Ứng dụng triết lý kiến trúc xanh một cách sáng tạo và độc đáo, với công nghệ xanh cực kỳ tiên tiến, Flamingo Cát Bà Beach Resort được mệnh danh là tòa nhà xanh nhất thế giới khi: “Cả tòa nhà là một cánh rừng, cả tòa nhà là một vườn hoa”.</span></p>
											<p class="MsoNormal" style="text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">10 điểm vàng chỉ có tại Flamingo Cát Bà Beach Resort:</span></p>
											<p class="MsoListParagraphCxSpFirst" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Vị trí đắc địa sở hữu bãi biển riêng độc đáo: cạnh vườn quốc gia Cát Bà, cách trung tâm thành phố Hải Phòng 45 phút, cách Hà Nội 2 giờ bằng ô tô,...</span></p>
											<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Sở hữu siêu phẩm nghỉ dưỡng đầu tiên và đón đầu cơ hội sinh lời chưa từng có.</span></p>
											<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Tòa nhà xanh nhất thế giới – Biểu tượng kiến trúc xanh của tương lai.</span></p>
											<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Phong thủy cát tường vượng lộc cho chủ nhân.</span></p>
											<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Ba khu rừng xanh: Forest On The Sea; Forest In The Sun; Forest On The Sand.</span></p>
											<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Biệt thự trên trời – Forest Sky Villa.</span></p>
											<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Sở hữu thẻ All in Passport Diamond với vô vàn dịch vụ tiện ích vượt trội.</span></p>
											<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Phương án tài chính linh hoạt và chính sách đặc quyền cư dân tối đa.</span></p>
											<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Bản giao hưởng bốn mùa cho cảm xúc thăng hoa, sức khỏe trường thọ cho chủ nhân.</span></p>
											<p class="MsoListParagraphCxSpLast" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Chủ đầu tư uy tín: Flamingo Group, chủ đầu tư thương hiệu nghỉ dưỡng Top 10 Resort đẹp nhất thế giới.</span></p>
											<p class="MsoNormal" style="margin-left:.25in;text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Tiện ích 5 sao nổi bật.</span></p>
											<p class="MsoNormal" style="margin-left:.25in;text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Flamingo Cát Bà Beach Resort với điểm nổi bật là siêu tổ hợp dịch vụ khép kín trên cao 5 sao với quy mô hoành tráng xứng tầm thế giới. Tất cả đều được đầu tư tối đa với công nghệ tiến hiện đại hàng đầu, đáp ứng nhu cầu vui chơi, giải trí, thư giãn và chăm sóc sức khỏe.</span></p>
											<p class="MsoNormal" style="margin-left:.25in;text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Một trong những điểm nổi bật nữa là khu vực tiếp đón, hội thảo, trung tâm ẩm thực với những nhà hàng cao cấp phục vụ các món ăn đa dạng từ các đầu bếp nổi tiếng thế giới; Skay Bar, Bar bãi biển; Bể bơi ốc đảo, bốn mùa, vô cực; Trung tâm mua sắm; Dịch vụ bến du thuyền, cano, moto Flyboard; Cùng rất nhiều tiện ích vượt trội khác đang chờ đón bạn.</span></p>
										</div>
										<div class="xemthem-section2">
											<div class="long projectline style-scroll" style="overflow-y:scroll ; ">
												<p class="MsoNormal" style="text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Flamingo Cát Bà Beach Resort</span></p>
												<p class="MsoNormal" style="text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Bản Giao hưởng 4 mùa, khúc ca tuyệt vời giao hòa trời biển</span></p>
												<p class="MsoNormal" style="text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Flamingo Cát Bà Beach Resort chính thức được tập đoàn Flamingo Group đầu tư với quy mô lớn, trở thành một quần thể phức hợp nghỉ dưỡng vô cùng đẳng cấp. Tọa lạc trên diện tích 77.843m2 với vị trí đắc địa trên bãi biển đẹp nhất của Cát Bà, thành phố Hải Phòng: Bãi tắm Cát Cò 1 và Cát Cò 2 – nơi được xem là thiên đường nghỉ dưỡng, cùng với tầm nhìn đắt giá ôm trọn quang cảnh núi và Vịnh Lan Mạ, quần thể phức hợp nghỉ dưỡng đẳng cấp Flamingo Cát Bà Beach Resort sẽ mang đến cho khách hàng những trải nghiệm tinh tế và đầy cảm xúc.</span></p>
												<p class="MsoNormal" style="text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Ứng dụng triết lý kiến trúc xanh một cách sáng tạo và độc đáo, với công nghệ xanh cực kỳ tiên tiến, Flamingo Cát Bà Beach Resort được mệnh danh là tòa nhà xanh nhất thế giới khi: “Cả tòa nhà là một cánh rừng, cả tòa nhà là một vườn hoa”.</span></p>
												<p class="MsoNormal" style="text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">10 điểm vàng chỉ có tại Flamingo Cát Bà Beach Resort:</span></p>
												<p class="MsoListParagraphCxSpFirst" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Vị trí đắc địa sở hữu bãi biển riêng độc đáo: cạnh vườn quốc gia Cát Bà, cách trung tâm thành phố Hải Phòng 45 phút, cách Hà Nội 2 giờ bằng ô tô,...</span></p>
												<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Sở hữu siêu phẩm nghỉ dưỡng đầu tiên và đón đầu cơ hội sinh lời chưa từng có.</span></p>
												<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Tòa nhà xanh nhất thế giới – Biểu tượng kiến trúc xanh của tương lai.</span></p>
												<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Phong thủy cát tường vượng lộc cho chủ nhân.</span></p>
												<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Ba khu rừng xanh: Forest On The Sea; Forest In The Sun; Forest On The Sand.</span></p>
												<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Biệt thự trên trời – Forest Sky Villa.</span></p>
												<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Sở hữu thẻ All in Passport Diamond với vô vàn dịch vụ tiện ích vượt trội.</span></p>
												<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Phương án tài chính linh hoạt và chính sách đặc quyền cư dân tối đa.</span></p>
												<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Bản giao hưởng bốn mùa cho cảm xúc thăng hoa, sức khỏe trường thọ cho chủ nhân.</span></p>
												<p class="MsoListParagraphCxSpLast" style="text-align:justify;text-indent:-.25in;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">-<span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">          </span></span><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Chủ đầu tư uy tín: Flamingo Group, chủ đầu tư thương hiệu nghỉ dưỡng Top 10 Resort đẹp nhất thế giới.</span></p>
												<p class="MsoNormal" style="margin-left:.25in;text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Tiện ích 5 sao nổi bật.</span></p>
												<p class="MsoNormal" style="margin-left:.25in;text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Flamingo Cát Bà Beach Resort với điểm nổi bật là siêu tổ hợp dịch vụ khép kín trên cao 5 sao với quy mô hoành tráng xứng tầm thế giới. Tất cả đều được đầu tư tối đa với công nghệ tiến hiện đại hàng đầu, đáp ứng nhu cầu vui chơi, giải trí, thư giãn và chăm sóc sức khỏe.</span></p>
												<p class="MsoNormal" style="margin-left:.25in;text-align:justify;"><span style="font-size:12pt;line-height:115%;font-family:'Times New Roman', serif;">Một trong những điểm nổi bật nữa là khu vực tiếp đón, hội thảo, trung tâm ẩm thực với những nhà hàng cao cấp phục vụ các món ăn đa dạng từ các đầu bếp nổi tiếng thế giới; Skay Bar, Bar bãi biển; Bể bơi ốc đảo, bốn mùa, vô cực; Trung tâm mua sắm; Dịch vụ bến du thuyền, cano, moto Flyboard; Cùng rất nhiều tiện ích vượt trội khác đang chờ đón bạn.</span></p>
											</div>
											<div class="more-link">
												<a class="link" data-collapse="1" data-rg="Rút gọn" data-xt="Xem thêm" >Xem thêm</a><i aria-hidden="true" class="fa fa-angle-right"></i>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--section 2 right-->
				<div class="background-section2-right">
					<div class="content-section2-right">
						<div class="title-section2-right">
							<h2>Bản giao hưởng 4 mùa</h2>
						</div>
					</div>
				</div>
				<!-- Slider Slick-->
				<div class="content-sliderSlick-detail2">
					<div class="slider-detail2">
						<div data-src="https://images.jinn.vn/9-jpgb8l5lsquumfg00at1tag.jpeg" class="item-slider-detail2 lazyImage"></div>
						<div data-src="https://images.jinn.vn/1-jpgb8l5ltiuumfg00at1tb0.jpeg" class="item-slider-detail2 lazyImage"></div>
						<div data-src="https://images.jinn.vn/10-jpgb8l5nfquumfg00at1tbg.jpeg" class="item-slider-detail2 lazyImage"></div>
						<div data-src="https://images.jinn.vn/gt-1-jpgb8np2vquumfg00at25dg.jpeg" class="item-slider-detail2 lazyImage"></div>
						<div data-src="https://images.jinn.vn/gt2-jpgb8p2mvvu8fbg00bestgg.jpeg" class="item-slider-detail2 lazyImage"></div>
					</div>
				</div>
			</div>
		</div>
		<!--Detail 3 - Mat bang du an-->
		<!--Detail 3 - Mat bang du an-->
		<div class="content-section3" id="groundProject">
			<div class="background-detail3">
				<div class="title-content-section3">
					<h2>Mặt bằng dự án</h2>
				</div>
				<div class="content-matBangDuAn">
					<div class="block-map">
						<img data-src="https://images.jinn.vn/mb-jpgb8l4i1quumfg00at1s70.jpeg" data-loaded="0" usemap="#planetmap1" class="img-responsive map lazyImage" id="project-map"/>
						<map id="map-tag" name="planetmap1">
							<area data-defaultname="Khu nhà" data-type="block" shape="poly" data-id="1" title="Forest on the Sea" data-link="/vi/blocks/forest-on-the-sea-5a2a4ab148119" data-href="/vi/blocks/forest-on-the-sea-5a2a4ab148119" coords="382,422.1875,400,391.1875,434,379.1875,443,401.1875,410,413.1875,397,433.1875"target="_blank" />
							<area data-defaultname="Khu nhà" data-type="block" shape="poly" data-id="2" title="Forest in the Sun" data-link="/vi/blocks/forest-in-the-sun-5a2a4b53309a9" data-href="/vi/blocks/forest-in-the-sun-5a2a4b53309a9" coords="480,318.1875,508,331.1875,559,261.1875,535,241.1875"target="_blank" />
							<area data-defaultname="Khu nhà" data-type="block" shape="poly" data-id="3" title="Forest on the Sand" data-link="/vi/blocks/forest-on-the-sand-5a2a4b750e645" data-href="/vi/blocks/forest-on-the-sand-5a2a4b750e645" coords="626,186.1875,637,196.1875,644,193.1875,650,197.1875,683,165.1875,719,162.1875,716,139.1875,678,138.1875,665,142.1875"target="_blank" />
						</map>
					</div>
				</div>
				<div class="popup-map blocks-popup-map" style="display: none;overflow: hidden">
					<h3 class="title">
						<a href="#" class="popup-header popupbox-name">
							Tên Căn Hộ: 
						</a>
						<span class="close-map-block close">&times;</span>
					</h3>
					<div class="entry popup-entry popup-content">
						<div class="dienTich-price-popup">
							<div class="dientich">
								<div class="line-icon m_f_17">
									<div class="icon">
										<span class="icon-svg-popup">
										</span>
									</div>
									DT<span class="popup-dientich"></span>
								</div>
							</div>
							<div class="pirce">
								<div class="line-icon m_f_17">
									<span class="icon"><i class="jinn-icon jinn-price-black"></i></span>
									Giá tiền: <span class="popup-pirce" ></span>
								</div>
							</div>
						</div>
						<div class="phongNgu-phongTam-popup">
							<div class="col-md-6 col-xs-6 numberBedRoom">
								<div class="line-icon m_f_17">
									<span class="icon"><i class="jinn-icon jinn-bedroom-black"></i></span>
									Phòng ngủ:  <span class="popup-numberBedRoom"></span>
								</div>
							</div>
							<div class="col-md-6 col-xs-6 numberBathRoom">
								<div class="line-icon m_f_17">
									<span class="icon"><i class="jinn-icon jinn-bathroom-black"></i></span>
									Phòng tắm:  <span class="popup-numberBathRoom"></span>
								</div>
							</div>
						</div>
						<div class="tamNhin-quyCan-popup">
							<div class="p_f_0">
								<div class="line-icon m_f_17">
									<div class="icon">
										<span class="icon-svg-popup">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512.001 512.001" style="enable-background:new 0 0 512.001 512.001;" xml:space="preserve" width="17px" height="17px">
											<path d="M503.698,231.895c-28.735-36.843-65.956-67.318-107.637-88.128c-42.548-21.243-88.321-32.265-136.104-32.843    c-1.316-0.036-6.6-0.036-7.916,0c-47.782,0.579-93.556,11.6-136.104,32.843c-41.681,20.81-78.9,51.284-107.636,88.128    c-11.07,14.193-11.07,34.018,0,48.211c28.735,36.843,65.955,67.318,107.636,88.128c42.548,21.243,88.321,32.265,136.104,32.843    c1.316,0.036,6.6,0.036,7.916,0c47.782-0.579,93.556-11.6,136.104-32.843c41.681-20.81,78.901-51.284,107.637-88.128    C514.768,265.911,514.768,246.088,503.698,231.895z M125.242,349.599c-38.92-19.432-73.678-47.892-100.517-82.303    c-5.187-6.651-5.187-15.94,0-22.591c26.838-34.411,61.596-62.871,100.517-82.303c11.054-5.518,22.342-10.29,33.839-14.33    c-29.578,26.588-48.213,65.12-48.213,107.928c0,42.81,18.636,81.345,48.217,107.932    C147.588,359.892,136.297,355.118,125.242,349.599z M256,380.303c-68.542,0-124.304-55.762-124.304-124.304    S187.458,131.696,256,131.696S380.304,187.458,380.304,256S324.542,380.303,256,380.303z M487.275,267.295    c-26.838,34.411-61.596,62.871-100.517,82.303c-11.041,5.512-22.322,10.263-33.805,14.299    c29.558-26.587,48.179-65.107,48.179-107.898c0-42.814-18.64-81.351-48.223-107.939c11.5,4.041,22.793,8.819,33.85,14.34    c38.92,19.432,73.678,47.892,100.517,82.303C492.462,251.355,492.462,260.644,487.275,267.295z" fill="#323232"/>
											<path d="M256,202.804c-29.332,0-53.195,23.863-53.195,53.195s23.863,53.195,53.195,53.195s53.195-23.863,53.195-53.195    C309.196,226.667,285.333,202.804,256,202.804z M256,288.367c-17.847,0-32.368-14.519-32.368-32.368    c0-17.848,14.519-32.367,32.368-32.367c17.847,0,32.367,14.519,32.367,32.367C288.368,273.848,273.847,288.367,256,288.367z" fill="#323232"/>
											</svg>
										</span>
									</div>
									Tầm nhìn: 
								</div>
							</div>
							<div class="show p_f_20">
								<div class="popup-views"></div>
							</div>
						</div>
						<div class="link-botom block-popup-more-detail"><a class="link popup-link" href="#" > Xem chi tiết</a></div>
					</div>
				</div>
				<div class="popup-map project-popup-map"
					 style="display: none; overflow: hidden;">
					<h3 class="title">
						<a href="#" class="popup-header  name">Tên Block</a>
						<span class="close-map-block close">&times;</span>
					</h3>
					<div class="entry  popup-content">
						<div class="line-icon popup-item-no-space">
							<div class="popup col-md-4 col-xs-4 text-center">
								<span>Còn trống</span>
								<div class="stillempty"></div>
							</div>
							<div class="popup col-md-4 col-xs-4 text-center">
								<span>Lock</span>
								<div class="numberProcessingHouse"></div>
							</div>
							<div class="popup col-md-4 col-xs-4 text-center">
								<span>Đã bán</span>
								<div class="numberSoldHouse"></div>
							</div>
						</div>
						<div class="content-soTang-soCH">
							<div class="col-md-6 col-xs-6">
								<div class="line-icon NumberFloor">
									<span class="icon-svg-popup">
										<svg style="width: 17px; height: 17px;" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 537 542">
										<defs>
										<style>.cls-1-lock{fill:none;stroke:#323232;stroke-miterlimit:10;stroke-width:10px;}</style>
										</defs>
										<path class="cls-1-lock" d="M204.7,105.7a12,12,0,0,0-12-12H98.8a12,12,0,0,0-12,12V435.4H204.7Z"></path>
										<path class="cls-1-lock" d="M280,191.7a12,12,0,0,0-12-12H206.1V435.4H280Z"></path>
										<path class="cls-1-lock" d="M362.3,281.7a12,12,0,0,0-12-12H282V435.4h80.3Z"></path>
										<path class="cls-1-lock" d="M444.3,363a12,12,0,0,0-12-12h-69v84.3h81Z"></path>
										<rect class="cls-1-lock" x="14" y="437.4" width="509" height="24.33" rx="12.2" ry="12.2"></rect>
										</svg>
									</span>
									Số tầng: <span class="numberFloor"></span>
								</div>
							</div>
							<div class="col-md-6 col-xs-6">
								<div class="line-icon">
									<span class="icon-svg-popup">
										<svg id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="17px" height="17px">
										<g>
										<g>
										<path d="M439.481,183.132V75.29h-89.184v39.234l-94.298-72.542L0,238.919l53.634,69.718l26.261-20.202v181.583h151.519V336.973    h55.151v133.045h145.543V288.435l26.261,20.202L512,238.92L439.481,183.132z M402.072,439.983h-85.473V306.938H201.378v133.045    h-91.449V265.329L256,152.965l146.071,112.364V439.983z M452.875,266.518L256,115.064L59.125,266.518l-17.006-22.106L256,79.876    l124.333,95.648v-70.199h29.114v92.596l60.433,46.491L452.875,266.518z" fill="#323232"></path>
										</g>
										</g>
										</svg>
									</span>
									Số căn hộ:  <span class="numberHouse"></span>
								</div>
							</div>
						</div>
						<div class="content-tamNhin-popup">
							<div class="p_f_3">
								<div class="line-icon">
									<span class="icon-svg-popup">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512.001 512.001" style="enable-background:new 0 0 512.001 512.001;" xml:space="preserve" width="17px" height="17px">
										<path d="M503.698,231.895c-28.735-36.843-65.956-67.318-107.637-88.128c-42.548-21.243-88.321-32.265-136.104-32.843    c-1.316-0.036-6.6-0.036-7.916,0c-47.782,0.579-93.556,11.6-136.104,32.843c-41.681,20.81-78.9,51.284-107.636,88.128    c-11.07,14.193-11.07,34.018,0,48.211c28.735,36.843,65.955,67.318,107.636,88.128c42.548,21.243,88.321,32.265,136.104,32.843    c1.316,0.036,6.6,0.036,7.916,0c47.782-0.579,93.556-11.6,136.104-32.843c41.681-20.81,78.901-51.284,107.637-88.128    C514.768,265.911,514.768,246.088,503.698,231.895z M125.242,349.599c-38.92-19.432-73.678-47.892-100.517-82.303    c-5.187-6.651-5.187-15.94,0-22.591c26.838-34.411,61.596-62.871,100.517-82.303c11.054-5.518,22.342-10.29,33.839-14.33    c-29.578,26.588-48.213,65.12-48.213,107.928c0,42.81,18.636,81.345,48.217,107.932    C147.588,359.892,136.297,355.118,125.242,349.599z M256,380.303c-68.542,0-124.304-55.762-124.304-124.304    S187.458,131.696,256,131.696S380.304,187.458,380.304,256S324.542,380.303,256,380.303z M487.275,267.295    c-26.838,34.411-61.596,62.871-100.517,82.303c-11.041,5.512-22.322,10.263-33.805,14.299    c29.558-26.587,48.179-65.107,48.179-107.898c0-42.814-18.64-81.351-48.223-107.939c11.5,4.041,22.793,8.819,33.85,14.34    c38.92,19.432,73.678,47.892,100.517,82.303C492.462,251.355,492.462,260.644,487.275,267.295z" fill="#323232"/>
										<path d="M256,202.804c-29.332,0-53.195,23.863-53.195,53.195s23.863,53.195,53.195,53.195s53.195-23.863,53.195-53.195    C309.196,226.667,285.333,202.804,256,202.804z M256,288.367c-17.847,0-32.368-14.519-32.368-32.368    c0-17.848,14.519-32.367,32.368-32.367c17.847,0,32.367,14.519,32.367,32.367C288.368,273.848,273.847,288.367,256,288.367z" fill="#323232"/>
										</svg>
									</span>
									Tầm nhìn:
								</div>
							</div>
							<div class="list-itemTamNhin-popup">
								<div class="show p_f_20">
									<div class="views"></div>
								</div>
							</div>
						</div>
						<div class="link-botom project-popup-more-detail"><a class="link block-link" href="#">Xem chi tiết </a></div>
					</div>
				</div>
				<div class="note-matBangDuAn">
					<div class="loaiBietThu">
						<div class="label-loaiBietThu">Loại sản phẩm</div>
						<div class="content-LSP">
							<div class="bietThu1 item-bietThu">
								<div class="cnt-item-value"><span class="color-bietThu "></span><span class="value-bietThu">Căn Hộ (20)</span>
								</div>
							</div>
							<div class="bietThu1 item-bietThu">
								<div class="cnt-item-value"><span class="color-bietThu "></span><span class="value-bietThu">Biệt thự (0)</span>
								</div>
							</div>
							<div class="bietThu1 item-bietThu">
								<div class="cnt-item-value"><span class="color-bietThu "></span><span class="value-bietThu">Căn hộ khách sạn (0)</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="content-phanKhu">
		<div class="background-phanKhu">
			<div class="content-slickSliderPhanKhu">
				<div onclick='loadPageBLockInMobile("/vi/blocks/forest-on-the-sea-5a2a4ab148119")' 
					 class="item-slickSliderPhanKhu" 
					 style="background-image: url('/images.jinn.vn/f1-jpgb8l5qgauumfg00at1td0.jpg')">
					<div class="infor-itemPhanKhu">
						<h2 title="Forest on the Sea">Forest on the Sea</h2>
						<div class="excerpt-itemPhanKhu">
							Forest
							On The Sea Flamingo Cát Bà được mệnh danh là “Tòa nhà rực rỡ giữa biển xanh” mang
							đặc trưng của biển và rừng. Nơi đây hiện hữu như một khu rừng xanh với chiều
							cao ấn tượng thuộc khu vực bãi tắm Cát Cò 1, tòa nhà Forest On The Sea là tổ hợp
							của những căn hộ nghỉ dưỡng trên cao có hướng view ra biển Cát Bà tuyệt đẹp. Forest
							On The Sea có diện tích 2.031 m2 và mật độ xây dựng 64%, bao gồm 173 căn hộ. Với
							những căn biệt thự trên cao chưa từng có vừa tinh tế, sang trọng lại vừa hài
							hòa với thiên nhiên. Với phong cách nội thất tiện nghi cùng hệ thống cây xanh,
							vườn treo hài hòa, tòa tháp này sẽ là nơi thể hiện đẳng cấp và gu thẩm mỹ hoàn
							hảo của các chủ nhân.
							Bên
							cạnh đó, khách hàng có thể tìm thấy những dịch vụ đẳng cấp và sang trọng hàng đầu
							ngay tại tòa nhà, bao gồm bể bơi vô cực trên cao, nhà hàng cao cấp có khả năng
							phục vụ tới 300 khách cùng lúc. Đây sẽ là địa điểm lý tưởng để mỗi người tìm thấy
							sự bình yên của kỳ nghỉ dưỡng khi đồng thời vẫn trải nghiệm tất cả các dịch vụ
							chất lượng nhất. Vị trí Forest On The Sea Flamingo Cát Bà không chỉ mang đầy
							đủ ưu điểm của toàn dự án mà còn nổi bật hơn khi là tòa nằm gần đường chính nhất,
							rất thuận tiện để khách hàng di chuyển đến các khu vực xung quanh. 
						</div>
					</div>
					<div class="link-itemPhanKhu">
						<div class="link-cnt-PK">
							<a href="../blocks/forest-on-the-sea-5a2a4ab148119.html">
								<img src="/jinnV2/images/icon/link-PK0995.png?v=0.9.0.4" alt="">
							</a>
						</div>
					</div>
				</div>
				<div onclick='loadPageBLockInMobile("/vi/blocks/forest-in-the-sun-5a2a4b53309a9")' class="item-slickSliderPhanKhu" style="background-image: url('/images.jinn.vn/f2-jpgb8l5qmauumfg00at1te0.jpg')">
					<div class="infor-itemPhanKhu">
						<h2 title="Forest in the Sun">Forest in the Sun</h2>
						<div class="excerpt-itemPhanKhu">
							Forest
							In The Sun là một trong ba tòa nhà đẳng cấp của dự án Flamingo Cát Bà. Tòa nhà
							nằm trên bãi Cát Cò 2A, ôm trọn một vùng biển trời tuyệt đẹp, thanh bình lại
							pha chút huyền bí của đảo Cát Bà. Với vị thế “tựa sơn hướng hải”, được bao bọc
							bởi biển xanh, lại tựa lưng vào núi, tòa dự án có phong thủy đắc địa hứa hẹn
							mang đến sự bình an, may mắn và tài lộc cho gia chủ.Rự rỡ với những vườn
							treo Babylon trên cao, tòa Forest In The Sun Flamingo Cát Bà là tòa nhà xanh rực
							rỡ được ví tựa như “Viên ngọc quý giá” dưới ánh mặt trời. Đây là tòa nhà có diện
							tích 4.600 m2 - lớn nhất trong ba tòa nhà tại Flamingo Cát Bà Resort với 254
							căn biệt thự trên cao độc đáo cùng mật độ xây dựng 58%. Nơi đây là điểm đến lý
							tưởng cho những khách hàng thượng lưu.
							Với kiến trúc xanh đặc
							trưng, toàn bộ tòa nhà được bao bọc bởi hệ thống cây xanh tầng tầng lớp lớp từ
							chân tòa cho đến từng căn biệt thự. Đến đây, chủ nhân biệt thự được trải nghiệm
							một không gian tràn ngập màu xanh, chan hòa với tự nhiên, nghỉ dưỡng cùng thiên
							nhiên đúng nghĩa. Đặc biệt các chủ nhân những căn biệt thự Forest In The Sun
							còn được trải nghiệm sự sang trọng với hệ thống nội thất và thụ hưởng những tiện
							ích dịch vụ đỉnh cao dành riêng cho tòa nhà như: Nhà hàng đa năng, Trung tâm hội
							nghị - hội thảo, Khu shopping tour, Sky Bar trong nhà và ngoài trời, Bể bơi vô
							cực trên tầng thượng…
						</div>
					</div>
					<div class="link-itemPhanKhu">
						<div class="link-cnt-PK">
							<a href="../blocks/forest-in-the-sun-5a2a4b53309a9.html">
								<img src="/jinnV2/images/icon/link-PK0995.png?v=0.9.0.4" alt="">
							</a>
						</div>
					</div>
				</div>
				<div onclick='loadPageBLockInMobile("/vi/blocks/forest-on-the-sand-5a2a4b750e645")' class="item-slickSliderPhanKhu" style="background-image: url('/images.jinn.vn/f1-jpgb8l5r1iuumfg00at1tgg.jpg')">
					<div class="infor-itemPhanKhu">
						<h2 title="Forest on the Sand">Forest on the Sand</h2>
						<div class="excerpt-itemPhanKhu">
							Forest
							on the Sand được mệnh danh “Biệt thự đẳng cấp bên bờ cát trắng” được xây dựng tại
							khu vực bãi biển Cát Cò 2B của đảo Cát Bà, Forest On The Sand cũng được hưởng lợi
							thế đắc địa “tựa sơn hướng thủy” và được bao bọc bởi không gian xanh của những
							rặng núi bao quanh, phía trước là biển cả.Forest
							on the Sand có diện tích 4.190 m2 và mật độ xây dựng 63%, bao gồm 383 căn hộ. Với
							triết lý kiến trúc đã xuyên suốt và trở thành “linh hồn”, thương hiệu của những
							công trình bao năm qua với Flamingo Cát Bà Beach Resort, Flamingo Group đã thổi
							hồn vào đó chút mặn mòi phóng khoáng của biển khơi, vươn mình ra đảo lớn để kiến
							trúc xanh thực sự được thăng hoa trong không gian khoáng đạt dành riêng cho một
							công trình đẳng cấp.Với
							dự án Flamingo Cát Bà, chủ đầu tư đã rất mạnh tay trong viêc trang bị chuỗi tiện
							ích, tiêu biểu là việc từng tòa đều có riêng các khu dịch vụ đáp ứng nhu cầu của
							khách nghỉ dưỡng.Tại tòa Forest On The Sand, các dịch vụ sẽ được cung cấp bao gồm:
							Bể bơi ốc đảo ngoài trời, Bar beer, Bar Cafe, Nhà hàng.
							Tại
							đây, quý khách hàng sẽ được tận hưởng vẻ đẹp thơ mộng của những bãi cát trắng
							trải dài miên man, được sải bước trên những con sóng nối đuôi nhau liên tiếp và
							hòa mình cùng với thiên nhiên tươi đẹp của đảo Cát Bà. 
						</div>
					</div>
					<div class="link-itemPhanKhu">
						<div class="link-cnt-PK">
							<a href="../blocks/forest-on-the-sand-5a2a4b750e645.html">
								<img src="/jinnV2/images/icon/link-PK0995.png?v=0.9.0.4" alt="">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Content detail 4-->
	<div class="content-section4" id="reasonChooseProject">
		<div class="background-detail4">
			<div class="title-content-section4">
				<h2>10 Lý do lựa chọn Flamingo Cát Bà Beach Resort</h2>
			</div>
			<div class="content-lyDoLuaChon">
				<div class="item-lyDoLuaChon">
					<div class="thumbnail-lyDoLuaChon">
						<div class="item-img4">
							<img data-src="https://images.jinn.vn/vi-tri-01-pngb8l6lj2uumfg00at1vt0.png" style="max-height: 100px;max-width: 100px;" class="lazyImage">
						</div>
					</div>
					<div class="item-textLyDoLuaChon">
						<div class="item-infor4">
							<h3>Vị trí đắc địa</h3>
							<p>Sở hữu bãi biển riêng độc đáo</p>
						</div>
					</div>
				</div>
				<div class="item-lyDoLuaChon">
					<div class="thumbnail-lyDoLuaChon">
						<div class="item-img4">
							<img data-src="https://images.jinn.vn/sinh-loi-01-pngb8l6mbiuumfg00at1vtg.png" style="max-height: 100px;max-width: 100px;" class="lazyImage">
						</div>
					</div>
					<div class="item-textLyDoLuaChon">
						<div class="item-infor4">
							<h3>Cơ hội sinh lời</h3>
							<p>Sở hữu siêu phẩm nghỉ dưỡng đầu tiên và đón đầu cơ hội sinh lời chưa từng có</p>
						</div>
					</div>
				</div>
				<div class="item-lyDoLuaChon">
					<div class="thumbnail-lyDoLuaChon">
						<div class="item-img4">
							<img data-src="https://images.jinn.vn/toa-nha-xanh-01-pngb8l6mlauumfg00at1vu0.png" style="max-height: 100px;max-width: 100px;" class="lazyImage">
						</div>
					</div>
					<div class="item-textLyDoLuaChon">
						<div class="item-infor4">
							<h3>Tòa nhà xanh nhất thế giới</h3>
							<p>Biểu tượng kiến trúc xanh của tương lai</p>
						</div>
					</div>
				</div>
				<div class="item-lyDoLuaChon">
					<div class="thumbnail-lyDoLuaChon">
						<div class="item-img4">
							<img data-src="https://images.jinn.vn/phong-thuy-01-pngb8l6mtauumfg00at1vug.png" style="max-height: 100px;max-width: 100px;" class="lazyImage">
						</div>
					</div>
					<div class="item-textLyDoLuaChon">
						<div class="item-infor4">
							<h3>Phong thủy</h3>
							<p>Cát tường vượng lộc</p>
						</div>
					</div>
				</div>
				<div class="item-lyDoLuaChon">
					<div class="thumbnail-lyDoLuaChon">
						<div class="item-img4">
							<img data-src="https://images.jinn.vn/3-rung-01-pngb8l6n5auumfg00at1vv0.png" style="max-height: 100px;max-width: 100px;" class="lazyImage">
						</div>
					</div>
					<div class="item-textLyDoLuaChon">
						<div class="item-infor4">
							<h3>Ba khu rừng xanh</h3>
							<p>Forest on the Sea, Forest in the Sun, Forest On the Sand</p>
						</div>
					</div>
				</div>
				<div class="item-lyDoLuaChon">
					<div class="thumbnail-lyDoLuaChon">
						<div class="item-img4">
							<img data-src="https://images.jinn.vn/biet-thu-tren-cao-01-pngb8l6ncauumfg00at1vvg.png" style="max-height: 100px;max-width: 100px;" class="lazyImage">
						</div>
					</div>
					<div class="item-textLyDoLuaChon">
						<div class="item-infor4">
							<h3>Forest Sky Villa</h3>
							<p>Biệt thự trên trời</p>
						</div>
					</div>
				</div>
				<div class="item-lyDoLuaChon">
					<div class="thumbnail-lyDoLuaChon">
						<div class="item-img4">
							<img data-src="https://images.jinn.vn/the-ngan-hang-01-pngb8l6ni2uumfg00at2000.png" style="max-height: 100px;max-width: 100px;" class="lazyImage">
						</div>
					</div>
					<div class="item-textLyDoLuaChon">
						<div class="item-infor4">
							<h3>Thẻ All in Passport Diamond</h3>
							<p>Sở hữu thẻ All in Passport Diamond với vô vàn dịch vụ tiện ích vượt trội</p>
						</div>
					</div>
				</div>
				<div class="item-lyDoLuaChon">
					<div class="thumbnail-lyDoLuaChon">
						<div class="item-img4">
							<img data-src="https://images.jinn.vn/tien-linh-hoat-01-pngb8l6nsiuumfg00at200g.png" style="max-height: 100px;max-width: 100px;" class="lazyImage">
						</div>
					</div>
					<div class="item-textLyDoLuaChon">
						<div class="item-infor4">
							<h3>Tài chính - Đặc quyền cư dân</h3>
							<p>Phương án tài chính linh hoạt và chính sách đặc quyền cư dân tối đa</p>
						</div>
					</div>
				</div>
				<div class="item-lyDoLuaChon">
					<div class="thumbnail-lyDoLuaChon">
						<div class="item-img4">
							<img data-src="https://images.jinn.vn/4-mua-01-pngb8l6o7auumfg00at2010.png" style="max-height: 100px;max-width: 100px;" class="lazyImage">
						</div>
					</div>
					<div class="item-textLyDoLuaChon">
						<div class="item-infor4">
							<h3>Bản giao hưởng bốn mùa</h3>
							<p>Cho cảm xúc thăng hoa, sức khỏe trường thọ của vùng đất Chúa</p>
						</div>
					</div>
				</div>
				<div class="item-lyDoLuaChon">
					<div class="thumbnail-lyDoLuaChon">
						<div class="item-img4">
							<img data-src="https://images.jinn.vn/chu-dau-tu-01-pngb8l6odauumfg00at201g.png" style="max-height: 100px;max-width: 100px;" class="lazyImage">
						</div>
					</div>
					<div class="item-textLyDoLuaChon">
						<div class="item-infor4">
							<h3>Chủ đầu tư uy tín</h3>
							<p>Flamingo Group, chủ đầu tư thương hiệu nghỉ dưỡng Top 10 Resort đẹp nhất thế giới</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Content section 5-->
	<div class="content-section5">
		<div class="background-detail5">
			<div class="title-content-section5">
				<h2>Tiện ích nội khu</h2>
			</div>
			<div class="content-detail5">
				<div class="item-utility utility1">
					<div class="cnt-item-utility">
						<div data-src="https://images.jinn.vn/tt-giai-tri-tren-bo-bien-2-jpgb8p5dlvu8fbg00besv0g.jpeg" class="lazyImage background-images-item">
							<span>
								<p>Khu thể thao ngoài trời</p>
							</span>
						</div>
					</div>
				</div>
				<div class="item-utility utility2">
					<div class="cnt-item-utility">
						<div data-src="https://images.jinn.vn/bar-bai-bien-jpgb8p5b3fu8fbg00besuvg.jpeg" class="lazyImage background-images-item">
							<span>
								<p>Khuôn viên thư giãn</p>
							</span>
						</div>
					</div>
				</div>
				<div class="item-utility utility3">
					<div class="cnt-item-utility">
						<div data-src="https://images.jinn.vn/trungtamgiaitri-jpgb8rj7fibmtlg009qn9k0.jpeg" class="lazyImage background-images-item">
							<span>
								<p>Trung tâm giải trí</p>
							</span>
						</div>
					</div>
				</div>
				<div class="item-utility utility4">
					<div class="cnt-item-utility">
						<div data-src="https://images.jinn.vn/sky-bar-jpgb8p5a77u8fbg00besutg.jpeg" class="lazyImage background-images-item">
							<span>
								<p>Sky bar</p>
							</span>
						</div>
					</div>
				</div>
				<div class="item-utility utility5">
					<div class="cnt-item-utility">
						<div data-src="https://images.jinn.vn/to-hop-onsen-jpgb8rj8eqbmtlg009qn9l0.jpeg" class="lazyImage background-images-item">
							<span>
								<p>Tổ hợp onsen-gym-yoga</p>
							</span>
						</div>
					</div>
				</div>
				<div class="item-utility utility6">
					<div class="cnt-item-utility">
						<div data-src="https://images.jinn.vn/trungtamhoinghi-jpgb8rj8babmtlg009qn9kg.jpeg" class="lazyImage background-images-item">
							<span>
								<p>Trung tâm hội nghị - hội thảo</p>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="content-section6" id="newsProject">
		<div class="background-detail6">
			<div class="title-content-section5">
				<h2>Tin Tức</h2>
			</div>
			<div class="content-detail6" style="position: relative">
				<div class="content-tabs-slider6">
					<ul>
						<li onclick="showNewsSlider('/vi/getAjaxReponse', 'tin-bao-chi', 10, 33)" class="item-tabs-news6 active">Tin báo chí</li>
						<li onclick="showNewsSlider('/vi/getAjaxReponse', 'tin-cong-ty', 10, 33)" class="item-tabs-news6 ">Tin công ty</li>
						<li onclick="showNewsSlider('/vi/getAjaxReponse', 'tin-tien-do-du-an', 10, 33)" class="item-tabs-news6 ">Tiến độ dự án</li>
					</ul>
				</div>
				<div class="content-slider6" >
					<div class="item-slider6" >
						<div class="item-shadow-slider">
							<a href="../../news/2017/12/15/cat-ba-suc-nong-cua-thien-duong-nghi-duong/index.html">
								<div class="thumbnail-sider6">
									<div data-src="https://images.jinn.vn/24862264-1179318842199327-7361431696671669132-n-jpgb8p31nfu8fbg00besuk0.jpeg" class="thumbnail-child lazyImage"
										 ></div>
								</div>
								<div class="infor-slider6">
									<div class="date-slider6">2017-12-15 14:33:07</div>
									<div class="title-slider6">Cát Bà – Sức nóng của thiên đường nghỉ dưỡng</div>
								</div>
							</a>
						</div>
					</div>
					<div class="item-slider6" >
						<div class="item-shadow-slider">
							<a href="../../news/2017/12/14/ha-tang-but-pha-nang-tam-bat-dong-san-cat-ba/index.html">
								<div class="thumbnail-sider6">
									<div data-src="https://images.jinn.vn/15bd5c28ba6b876274-jpgb8p3jovu8fbg00besurg.jpeg" class="thumbnail-child lazyImage"
										 ></div>
								</div>
								<div class="infor-slider6">
									<div class="date-slider6">2017-12-14 18:34:00</div>
									<div class="title-slider6">Hạ tầng bứt phá nâng tầm bất động sản Cát Bà</div>
								</div>
							</a>
						</div>
					</div>
					<div class="item-slider6" >
						<div class="item-shadow-slider">
							<a href="../../news/2017/12/11/du-an-flamingo-cat-ba-beach-resort/index.html">
								<div class="thumbnail-sider6">
									<div data-src="https://images.jinn.vn/1-6-1-jpgb8l6h0quumfg00at1vk0.jpeg" class="thumbnail-child lazyImage"
										 ></div>
								</div>
								<div class="infor-slider6">
									<div class="date-slider6">2017-12-11 11:41:15</div>
									<div class="title-slider6">Dự án Flamingo Cát Bà Beach Resort</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--include ../detailDuAn/detail10-->
	<div class="content-section7" id="blockProject">
		<div class="background-detail7">
			<div class="title-content-section5">
				<h2>Quỹ căn
				</h2>
			</div>
			<div class="content-quyCan">
				<div data-src="/jinnV2/images/img/background-QC.jpg?v=0.9.0.4" class="background-quyCan lazyImage"></div>
				<div class="check-Login">
					<div class="form-checkLogin">
						<div class="background-frm-checkLg"><span class="color-detail7-1">Vui lòng</span><span><a href="#" data-toggle="modal" data-target="#login" class="color-detail7-2">Đăng nhập</a></span><span class="color-detail7-1">để xem nội dung</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="content-section8" id="contactProject">
		<div data-src="https://images.jinn.vn/6-jpgb8l4i8auumfg00at1s80.jpeg" class="background-detail8 lazyImage">
			<!--section 8 left-->
			<div class="content-section8-left">
				<div class="background-section8-left"></div>
			</div>
			<!--section 8 right-->
			<div class="content-section8-right">
				<div class="background-section8-right">
					<div class="content-form-lienHe8">
						<div class="title-form-lienhe8">Liên hệ</div>
						<form name="agent_form" method="post" class="form-contact" id="form-contact">
							<div class="form-group frmLH-name">
								<div><input type="text" id="agent_form_firstName" name="agent_form[firstName]" required="required" class="form-control" id="cfirstname" value="" placeholder="Họ tên (*)" /></div>
								<div><input type="text" id="agent_form_lastName" name="agent_form[lastName]" required="required" style="display:none" value="  " /></div>
							</div>
							<div class="form-group frmLH-email">
								<div><input type="email" id="agent_form_email" name="agent_form[email]" required="required" class="form-control" value="" placeholder="Email (*)" /></div>
							</div>
							<div class="form-group frmLH-sdt">
								<div><input type="text" id="agent_form_phone" name="agent_form[phone]" required="required" class="form-control agent_form_phone" value="" placeholder="Số điện thoại (*)" /></div>
							</div>
							<div class="form-group txtLH-noidung">
								<div><textarea id="agent_form_content" name="agent_form[content]" required="required" class="form-control agent_form_content" rows="5" placeholder="Nội dung"></textarea></div>
							</div>
							<input hidden type="text" value="flamingo-catba-beach-resort-5a2a4a264f898" name="uniqueSlug" class="hidden-slug"/>
							<input hidden type="text" value="Flamingo Cát Bà Beach Resort" name="projectName" class="hidden-slug"/>
							<input hidden type="text" value="agentProjectContact" name="check" class="hidden-slug"/>

							<div class="form-group btn-guiYeuCau">
								<button type="submit" class="btn btn-primary"> Gửi</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal-->
	<div id="dieuKhoan" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" data-dismiss="modal" class="close">×</button>
					<h4 class="modal-title">Điều khoản và chính sách của Jinn</h4>
				</div>
				<div class="modal-body"><span>A.   GIỚI THIỆU</span> Jinn là công cụ tìm kiếm bất động sản thông minh, lần đầu có mặt tại thị trường Việt Nam dành cho khách hàng mua lẻ và nhà đầu tư. Đến với Jinn.vn, bạn sẽ được trải nghiệm thế giới bất động sản, với sản phẩm sự đa dạng, phong phú giúp cho bạn chọn lựa căn hộ cao cấp, biệt thự biển lý tưởng mà bạn thích. Đặc biệt, thông tin sẽ liên tục cập nhật giúp bạn nắm rõ tiến độ dự án, tình trạng mua bán của căn hộ và những thông tin cần thiết khác. Nơi đây còn là cầu nối của các chủ đầu tư, đại lý, đưa các sản phẩm căn hộ cao cấp mang thương hiệu Vinhomes tới tay khách hàng Jinn chính thức ra mắt thị trường Việt Nam vào tháng 11 năm 2015 do ông Nguyễn Tuấn Nam sáng lập. Jinn là công cụ tìm kiếm bất động sản thông minh<span> B.   CHÍNH SÁCH BẢO MẬT </span>Chính sách và Quy định chung (Bao gồm các điều kiện hạn chế, tiêu chuẩn dịch vụ, quy trình, quy định… nếu có) a. Chấp nhận các Điều kiện sử dụng Khi sử dụng website Jinn.vn, Khách hàng đã mặc nhiên chấp thuận các điều khoản các điều kiện sử dụng được quy định dưới đây. Để biết các sửa đổi mới nhất, Khách hàng thường xuyên kiểm tra lại Điều kiện sử dụng. Jinn.vn có quyền thay đổi, điều chỉnh, thêm hay bớt bất kỳ các nội dung của Điều kiện sử dụng tại bất kỳ thời điểm nào. Nếu Khách hàng vẫn tiếp tục sử dụng website sau khi có các thay đổi như vậy thì có nghĩa là Khách hàng đã chấp nhận các thay đổi đó. b. Hướng dẫn sử dụng website Khi sử dụng website của chúng tôi, Khách hàng đảm bảo đủ 18 tuổi, hoặc truy cập dưới sự giám sát của cha mẹ hay người giám hộ hợp pháp. Khách hàng đảm bảo có đầy đủ hành vi dân sự để thực hiện các giao dịch mua bán hàng hóa theo quy định hiện hành của pháp luật Việt Nam. Chúng tôi sẽ cung cấp một tài khoản (Account) sử dụng để Khách hàng có thể sử dụng các dịch vụ trên website Jinn.vn trong khuôn khổ Điều khoản và Điều kiện sử dụng đã đề ra. Quý khách hàng sẽ phải đăng ký tài khoản với thông tin xác thực về bản thân và phải cập nhật nếu có bất kỳ thay đổi nào. Mỗi người truy cập phải có trách nhiệm với mật khẩu, tài khoản và hoạt động của mình trên web. Hơn nữa, Khách hàng phải thông báo cho chúng tôi biết khi tài khoản bị truy cập trái phép. Chúng tôi không chịu bất kỳ trách nhiệm nào, dù trực tiếp hay gián tiếp, đối với những thiệt hại hoặc mất mát gây ra do quý khách không tuân thủ quy định. Nghiêm cấm sử dụng bất kỳ phần nào của trang web này với mục đích thương mại hoặc nhân danh bất kỳ đối tác thứ ba nào nếu không được chúng tôi cho phép bằng văn bản. Nếu vi phạm bất cứ điều nào trong đây, chúng tôi sẽ hủy tài khoản của khách mà không cần báo trước. Trong suốt quá trình đăng ký, quý khách đồng ý nhận email quảng cáo từ website. Nếu không muốn tiếp tục nhận mail, quý khách có thể từ chối bằng cách nhấp vào đường link ở dưới cùng trong mọi email quảng cáo. c. Ý kiến khách hàng Tất cả nội dung trang web và ý kiến phê bình của Khách hàng đều là tài sản của chúng tôi. Nếu chúng tôi phát hiện bất kỳ thông tin giả mạo nào, chúng tôi sẽ khóa tài khoản của quý khách ngay lập tức hoặc áp dụng các biện pháp khác theo quy định của pháp luật Việt Nam. d. Quy định chung Không được đăng ký tài khoản và khai báo những thông tin giả mạo gây hiểu nhầm với những thành viên khác, nick trùng tên với tên của các chính trị gia,… Không được phép đăng tin liên quan đến các vấn đề Pháp luật Việt Nam không cho phép. Không được đăng những bài viết, thông tin có nội dung vi phạm pháp luật, đả kích, bôi nhọ, chỉ trích hay bàn luận về chính trị, tôn giáo, phản động, kỳ thị văn hóa, dân tộc, cũng như vi phạm khác liên quan đến thuần phong mỹ tục của dân tộc Việt Nam. Không được xâm phạm quyền lợi, uy tín, đời tư các cá nhân khác hay thành viên khác, không được dùng ngôn từ tục tĩu, thóa mạ thông tin tham gia. Không được lợi dụng website để tuyên truyền, đề xướng, lôi kéo với những nội dung không lành mạnh. Phải dùng ngôn từ trong sáng, rõ ràng, đúng chính tả, nghiêm cấm mọi hình thức viết nhịu, viết tắc theo hình thức chat, ngôn ngữ tối nghĩa. e. Thương hiệu và bản quyền. Mọi quyền sở hữu trí tuệ (đã đăng ký hoặc chưa đăng ký), nội dung thông tin và tất cả các thiết kế, văn bản, đồ họa, phần mềm, hình ảnh, video, âm nhạc, âm thanh, biên dịch phần mềm, mã nguồn và phần mềm cơ bản đều là tài sản của chúng tôi. Toàn bộ nội dung của trang web được bảo vệ bởi luật bản quyền của Việt Nam và các công ước quốc tế. Bản quyền đã được bảo lưu. f. Quyền pháp lý Các điều kiện, điều khoản và nội dung của trang web này được điều chỉnh bởi pháp luật Việt Nam và Tòa án có thẩm quyền tại Việt Nam sẽ được giải quyết bất kỳ tranh chấp nào phát sinh từ việc sử dụng trái phép trang web này. 2. Quy định và hình thức thanh toán Việc thanh toán đơn hàng giữa người bán, người mua do hai bên tự trao đổi, thỏa thuận. Jinn.vn không tham gia quá trình cũng như nội dung thỏa thuận mua bán giữa hai bên. Jinn.vn khuyến khích người mua và người bán thực hiện thanh toán sau giao dịch qua các hình thức đảm bảo như chuyển khoản, và phải có hóa đơn chứng từ làm cơ sở cho việc giải quyết tranh chấp, khiếu nại nếu có sau giao dịch. 3. Chính sách vận chuyển /giao nhận/cài đặt. Các giao dịch vận chuyển hàng hóa, sản phẩm do người mua, người bán chủ động thỏa thuận và quyết định thời gian, địa điểm, cách thức giao dịch. 4. Chính sách bảo hành/bảo trì (nhóm hàng hóa/dịch vụ có bảo hành) Người đứng tên đăng kí tài khoản (Khách hàng) sẽ phải chịu hoàn toàn trách nhiệm về các thông tin mình đăng tải. Ban quản lý website kiểm soát về tính xác thực và trung thực của nội dung do Khách hàng đăng lên cũng như các vấn đề liên quan đến việc thực hiện giao dịch giữa chủ đầu tư và người mua bất động sản. Ban quản lý Jinn.vn luôn cố gắng để có chất lượng thông tin tốt nhất để thông tin đến khách hàng. Bảo hành/bảo trì sản phẩm sau giao dịch do người mua và người bán tự thỏa thuận. Chúng tôi khuyến khích người bán bên có các hình thức bảo hành quy chuẩn, đảm bảo quyền lợi của cả hai bên. 5. Chính sách bảo mật thông tin cá nhân a. Mục đích và phạm vi thu thập thông tin Với đặc tính của thị trường bất động sản rất phong phú, đa dạng, chúng tôi luôn cố gắng giữ cho những thông tin trang Jinn.vn hữu ích và chính xác nhất. Để thực hiện điều đó, Jinn.vn yêu cầu khách hàng cung cấp thông tin cá nhân trong phạm vi bắt buộc. Jinn.vn không bán, chia sẻ hay trao đổi thông tin cá nhân của khách hàng thu thập trên trang web cho bên thứ ba nào khác. Khi bạn liên hệ đăng kí dịch vụ, thông tin cá nhân mà Jinn.vn thu thập bao gồm: - Họ và tên - Địa chỉ - Điện thoại - Email Tất cả các thông tin lưu trữ này được chúng tôi bảo vệ chặt chẽ bằng tiêu chuẩn an ninh thông tin cấp cao. b. Phạm vi sử dụng thông tin cá nhân Thông tin cá nhân thu thập được sẽ chỉ được Jinn.vn sử dụng trong nội bộ và cho một hoặc tất cả các mục đích sau đây: - Hỗ trợ khách hàng - Cung cấp thông tin liên quan đến dịch vụ - Xử lý đơn đặt hàng và cung cấp dịch vụ và thông tin qua trang web của chúng tôi theo yêu cầu của bạn. - Chúng tôi có thể sẽ gửi thông tin sản phẩm, dịch vụ mới, thông tin về các sự kiên sắp tới hoặc thông tin tuyển dụng nếu quý khách đăng kí nhận email thông báo. - Nâng cao mối tương tác và liên kết với Khách hàng. - Sử dụng thông tin bạn cung cấp để hỗ trợ quản lý tài khoản Khách hàng: xác nhận và thực hiện các giao dịch tài chính liên quan đến các khoản thanh toán trực tuyến của bạn. Nếu không có sự đồng ý của Khách hàng, Jinn.vn sẽ không cung cấp dữ liệu cho bên thứ ba để dùng vào mục đích quảng cáo. c. Thời gian lưu trữ thông tin cá nhân Các thông tin cá nhân của Khách hàng được lưu trữ trong một thời gian cần thiết, nhằm phục vụ cho các yêu cầu Khách hàng đưa ra. Jinn.vn chỉ xóa dữ liệu này nếu Khách hàng có yêu cầu, Khách hàng yêu cầu gửi email về info@jinn.vn d. Địa chỉ của đơn vị thu thập và quản lý thông tin cá nhân Công ty Cổ Phần Dịch Vụ Công Nghệ Jinn Địa chỉ: 135/1/58 Nguyễn Hữu Cảnh, Phường 22, Quận Bình Thạnh, TP. Hồ Chí Minh. Điện thoại: (028) 71 060 789 Email: info@jinn.vn e. Phương tiện va công cụ để người dùng tiếp cận và chỉnh sửa dữ liệu cá nhân của mình. Khách hàng được cung cấp một tài khoản bao gồm User và Password để truy cập Jinn.vn. Sau khi đăng nhập, thành viên có quyền sử dụng mọi dịch vụ/tiện ích được cung cấp trên website theo đúng chức năng, quyền hạn được phân cấp. Jinn.vn thu thập thông tin Khách hàng thông qua tài khoản đăng nhập website, qua email của chúng tôi: info@jinn.vn hoặc số điện thoại liên hệ về dịch vụ gọi về (028) 71 060 789 f. Cam kết bảo mật thông tin khách hàng. Tại Jinn.vn, việc bảo vệ thông tin cá nhân Khách hàng của bạn là rất quan trọng, bạn được đảm bảo rằng thông tin cung cấp cho chúng tôi được bảo mật. Chúng tôi cam kết không chia sẻ, bán hoặc cho thuê thông tin cá nhân của bạn cho bất kỳ người nào khác. Khách hàng cần cung cấp thông tin cá nhân trong các tình huống sau: - Truy cập, sử dụng website; - Yêu cầu báo giá, cung cấp thông tin, dịch vụ hoặc hỗ trợ; - Đặt mua và sử dụng dịch vụ của website; - Tham gia khảo sát ý kiến, dự thi có thưởng, hoặc các hoạt động khuyến mại, quảng bá khác; - Đăng ký nhận tin thư, email quảng cáo hoặc các loại tin tức khác; - Đăng ký tham dự tuyển dụng, gửi hồ sơ, lý lịch; - Đơn vị tổ chức thu thập và sử dụng thông tin cá nhân của Khách hàng trên website chỉ sau khi Khách hàng đã đăng ký tài khoản, tức đã được sự đồng ý trước của Khách hàng. </div>
			</div>
		</div>
	</div>
	<div class="similar-housing similar-detail similar-detail9">
		<div class="title-similar-housing"><span>Tuyệt phẩm nghỉ dưỡng đẳng cấp</span>
		</div>
		<div style="position: relative;" class="content-sliderTuongtu">
			<h3>Dự án khác</h3>
			<div class="detailDATuongTu">
				<div class="item-detailTuongTu">
					<div class="itemContentDATTu">
						<div class="background-itemContentDATTu">
							<a href="#">
								<div class="view1-left" style="background-color: rgba(51,183,177,0.36)" >
									<a href="vinhomes-imperia-58cb51156df42.html">
										<div class="background-view-itemContentDATTu">
											<div data-src="https://images.jinn.vn/gioi-thieu-jpgb8l73dauumfg00at21a0.jpeg" style="background-image: url('/images.jinn.vn/gioi-thieu-jpgb8l73dauumfg00at21a0.jpg');" class="img-view1 lazyImage"></div>
											<div class="infor-DATTu">
												<h4 class="title-infor-DATTu">Vinhomes Imperia</h4>
												<div class="address-infor-DATTu">Hải Phòng</div>
											</div>
										</div>
									</a>
								</div>
							</a>
						</div>
					</div>
				</div>
				<div class="item-detailTuongTu">
					<div class="itemContentDATTu">
						<div class="background-itemContentDATTu">
							<a href="#">
								<div class="view1-left" style="background-color: rgba(51,183,177,0.36)" >
									<a href="flamingo-catba-beach-resort-5a2a4a264f898.html">
										<div class="background-view-itemContentDATTu">
											<div data-src="https://images.jinn.vn/gt1-jpgb8l6f5iuumfg00at1uug.jpeg" style="background-image: url('/images.jinn.vn/gt1-jpgb8l6f5iuumfg00at1uug.jpg');" class="img-view1 lazyImage"></div>
											<div class="infor-DATTu">
												<h4 class="title-infor-DATTu">Flamingo Cát Bà Beach Resort</h4>
												<div class="address-infor-DATTu">Hải Phòng</div>
											</div>
										</div>
									</a>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<footer class="footer-site">
	<div class="background-footer">
		<div class="container-footer">
			<div class="footer-desktop">
				<div class="content-logo-footer-desktop">
					<a href="index.html">
						<img src="/logo/linkvietnam-51.png">
					</a>
					<div class="copyright-footer-desktop">Copyright &copy; JINN 2015 All Rights Reserved </div>
				</div>

				@include('Frontend.Elements.Home.footer')


			</div>
			@include('Frontend.Elements.Home.footerMobile')
		</div>
	</div>
</footer>

@stop