@extends('Frontend.Layouts.main')



@section('content')


<div class='container'>

    <div class="detail-news">
        <h1 id="lblTitle" class="title-news">{{ $news->title }}</h1>
    {!! $news->content !!}
    </div>

</div>


@stop