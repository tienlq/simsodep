


@extends('Frontend.Layouts.main')

@section('content')

<div class="container content-basic">
    <div class="row">
        <h1>{{ $news->title or '' }}</h1>

        {!! $news->content or 'Nội dung đang được cập nhật, Vui lòng click <a href="/">vào đây</a> để về lại <a href="/">trang chủ</a>' !!}
    </div>
</div>

@include('Frontend.Elements.Layout.news')

<br><br><br>
@stop