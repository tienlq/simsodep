-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 16, 2019 lúc 07:05 PM
-- Phiên bản máy phục vụ: 10.1.25-MariaDB
-- Phiên bản PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `simsodep`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `softOrder` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `address`
--

INSERT INTO `address` (`id`, `title`, `name`, `softOrder`, `created_at`, `updated_at`) VALUES
(1, 'TRỤ SỞ CHÍNH ĐÀ NẴNG', 'Đường Trường Sa, Quận Ngũ Hành Sơn, Đà Nẵng\r\nĐT: +84 236 3954 666', 1, '2017-04-22 16:01:30', '2017-10-09 18:25:33'),
(2, 'VĂN PHÒNG HÀ NỘI', '614 Bis Lạc Long Quân, P. Nhật Tân, Q. Tây Hồ, Hà Nội\r\nĐT: +84 948 666 690', 2, '2017-04-22 16:01:30', '2017-10-09 18:25:49'),
(8, 'VĂN PHÒNG TP.HCM', 'Tầng 19 tòa nhà Pearl Plaza, 561A Điện Biên Phủ, P.25, Q. Bình Thạnh, Hồ Chí Minh\r\nĐT: +84 909 949 996', 3, '2017-05-16 06:45:26', '2017-10-09 18:26:03');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `block`
--

CREATE TABLE `block` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price01` int(11) DEFAULT '0',
  `price02` int(11) DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `block`
--

INSERT INTO `block` (`id`, `parent_id`, `image`, `link`, `type`, `price01`, `price02`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, 0, '/photos/3/a3-800x800.png', 'http://nveducation.com.vn/l1/tu-van-du-hoc/n16.html', 'highlights', 0, 0, 2, NULL, '2017-11-07'),
(2, 0, '/photos/3/a4-640x960.png', 'http://nveducation.com.vn/l1/tu-van-nganh-hoc/n20.html', 'highlights', 0, 0, 1, NULL, '2017-11-07'),
(3, 0, '/photos/3/A1-800X800.png', 'http://nveducation.com.vn/l1/tu-van-san-hoc-bong/n19.html', 'highlights', 0, 0, 1, NULL, '2017-11-07'),
(4, 0, '/photos/3/A1-800X800.png', '#', 'lifestyle', 0, 0, 0, '2017-10-01', '2017-11-06'),
(5, 0, '/photos/3/Hình-1-Học-bổng-du-học-Canada.jpg', NULL, 'lifestyle', 0, 0, 0, '2017-10-01', '2017-11-06'),
(6, 0, '/photos/3/hoc-bong-du-hoc-canada.jpg', '#', 'lifestyle', 0, 0, 0, '2017-10-01', '2017-11-06'),
(7, 0, NULL, NULL, 'categoryJob', 0, 0, 1, '2017-10-16', '2017-10-16'),
(8, 7, NULL, NULL, 'categoryJob', 0, 0, 1, '2017-10-16', '2017-10-16'),
(9, 7, NULL, NULL, 'categoryJob', 0, 0, 2, '2017-10-16', '2017-10-16'),
(10, 0, '/photos/3/2.jpg', '#', 'lifestyle', 0, 0, 0, '2017-11-06', '2017-11-06'),
(11, 0, '/photos/3/Student-Work-Visa-2.jpg', NULL, 'lifestyle', 0, 0, 0, '2017-11-06', '2017-11-06'),
(12, 0, '/photos/3/4.jpg', NULL, 'lifestyle', 0, 0, 0, '2017-11-06', '2017-11-06'),
(13, 0, '2', NULL, 'price', 1, 500001, 0, '2018-10-18', '2018-12-30'),
(14, 0, '2', NULL, 'price', 500000, 1000000, 0, '2018-10-18', '2018-12-30'),
(15, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-18', '2018-10-18'),
(16, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-18', '2018-10-18'),
(17, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-18', '2018-10-18'),
(18, 0, NULL, NULL, 'sporder', 0, 0, 0, '2018-10-28', '2018-10-28'),
(19, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-29', '2018-10-29'),
(20, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-29', '2018-10-29'),
(21, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-29', '2018-10-29'),
(22, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-29', '2018-10-29'),
(23, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-29', '2018-10-29'),
(24, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-29', '2018-10-29'),
(25, 0, NULL, NULL, 'fcate', 0, 0, 3, '2018-11-03', '2018-11-03'),
(26, 25, NULL, '#', 'fcate', 0, 0, 1, '2018-11-03', '2018-11-03'),
(27, 25, NULL, '#', 'fcate', 0, 0, 2, '2018-11-03', '2018-11-03'),
(28, 25, NULL, '#', 'fcate', 0, 0, 3, '2018-11-03', '2018-11-03'),
(29, 0, NULL, NULL, 'fcate', 0, 0, 4, '2018-11-03', '2018-11-03'),
(30, 29, NULL, NULL, 'fcate', 0, 0, 1, '2018-11-03', '2018-11-03'),
(31, 29, NULL, NULL, 'fcate', 0, 0, 2, '2018-11-03', '2018-11-03'),
(32, 29, NULL, NULL, 'fcate', 0, 0, 3, '2018-11-03', '2018-11-03'),
(33, 29, NULL, NULL, 'fcate', 0, 0, 4, '2018-11-03', '2018-11-03'),
(34, 0, NULL, NULL, 'fcate', 0, 0, 2, '2018-11-03', '2018-11-03'),
(35, 34, NULL, NULL, 'fcate', 0, 0, 1, '2018-11-03', '2018-11-03'),
(36, 34, NULL, NULL, 'fcate', 0, 0, 2, '2018-11-03', '2018-11-03'),
(37, 0, NULL, NULL, 'fcate', 0, 0, 1, '2018-11-03', '2018-11-03'),
(38, 37, NULL, NULL, 'fcate', 0, 0, 1, '2018-11-03', '2018-11-03'),
(39, 0, NULL, NULL, 'fdes1', 0, 0, 0, '2018-11-03', '2018-11-03'),
(40, 0, NULL, NULL, 'fdes2', 0, 0, 0, '2018-11-03', '2018-11-03'),
(41, 0, '1', NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(42, 0, '1', NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(43, 0, '1', NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(44, 0, '1', NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(45, 0, '1', NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(46, 0, '1', NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(47, 0, '1', NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(48, 0, '1', NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(49, 0, '1', NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(50, 0, '1', NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(51, 0, '1', NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(52, 0, '1', NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(53, 0, '1', NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(54, 0, '1', NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(55, 0, '3', '/', 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-18'),
(56, 0, '3', '/', 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-18'),
(57, 0, '3', '/', 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-18'),
(58, 0, '2', NULL, 'typeSim', 10000000, 1000000000, 0, '2018-11-11', '2018-11-11'),
(59, 0, NULL, '/', 'fcate02', 0, 0, 0, '2018-11-25', '2018-11-25'),
(60, 0, NULL, '#', 'fcate02', 0, 0, 0, '2018-11-25', '2018-11-25'),
(61, 0, NULL, '#', 'fcate02', 0, 0, 0, '2018-11-25', '2018-11-25'),
(62, 0, '1', NULL, 'dauso', 0, 0, 0, '2018-12-30', '2018-12-30'),
(63, 0, '1', NULL, 'dauso', 0, 0, 0, '2018-12-30', '2018-12-30'),
(64, 0, '4', NULL, 'sortorder', 0, 0, 0, '2018-12-30', '2018-12-30'),
(65, 0, '5', NULL, 'sortorder', 0, 0, 0, '2018-12-30', '2018-12-30');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `block_config`
--

CREATE TABLE `block_config` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `icon` text COLLATE utf8_unicode_ci,
  `link` text COLLATE utf8_unicode_ci,
  `config01` text COLLATE utf8_unicode_ci,
  `config02` text COLLATE utf8_unicode_ci,
  `config03` text COLLATE utf8_unicode_ci,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `block_config`
--

INSERT INTO `block_config` (`id`, `name`, `type`, `sort_order`, `icon`, `link`, `config01`, `config02`, `config03`, `created_at`, `updated_at`) VALUES
(1, 'SIM THEO GIÁ', 'price', 0, '<i class=\"fa fa-american-sign-language-interpreting\" aria-hidden=\"true\"></i>', NULL, NULL, NULL, NULL, NULL, '2018-11-25'),
(2, 'SIM THEO MẠNG', 'SIM_BY_SUPPLIER', 0, '<i class=\"fa fa-credit-card-alt\" aria-hidden=\"true\"></i>', NULL, NULL, NULL, NULL, NULL, '2018-11-25'),
(3, 'BÁN HÀNG ONLINE', 'support', 0, '<i class=\"ion-android-call fa fix-icon-font\"></i>', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Đơn hàng mới', 'NEW_ORDERS', 0, '<i class=\"fa ion-ios-cart fix-icon-font\"></i>', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Tin mới cập nhật', 'NEWS', 0, '<i class=\"fa ion-ios-paper fix-icon-font\"></i>', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Sim vip doanh nhân', 'SIM_VIP_DOANH_NHAN', 0, '<i class=\"fa ion-briefcase icon-content\"></i>', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `block_data`
--

CREATE TABLE `block_data` (
  `id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `language` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'vi',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sluggable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `block_data`
--

INSERT INTO `block_data` (`id`, `block_id`, `language`, `name`, `sluggable`, `description`, `created_at`, `updated_at`) VALUES
(25, 13, 'vi', 'Sim dưới 500 nghìn', 'sim-duoi-500-nghin', NULL, '2018-10-18', '2018-10-18'),
(26, 14, 'vi', 'Sim giá 500 - 1 triệu', 'sim-gia-500-1-trieu', NULL, '2018-10-18', '2018-10-18'),
(27, 15, 'vi', 'Nguyễn Khuyên', 'nguyen-khuyen', '038.5954.811', '2018-10-18', '2018-10-29'),
(28, 16, 'vi', 'Quang Tiến', 'quang-tien', '036.365.1500', '2018-10-18', '2018-10-29'),
(29, 17, 'vi', 'Thanh Tâm', 'thanh-tam', '0941.90.6699', '2018-10-18', '2018-10-29'),
(30, 18, 'vi', 'Hướng dẫn cách thức mua sim', 'huong-dan-cach-thuc-mua-sim', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">▪ Cách 1: Asimdep giao sim và thu tiền tận nhà miễn phí (áp dụng Hà Nội, Tp.HCM và các tỉnh/thành có đại lý)</span><br style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\"><span style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">▪ Cách 2: Quý khách đến cửa hàng tại Hà Nội (Xem Địa chỉ ở cuối trang)</span><br style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\"><span style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">▪ Cách 3: Đặt cọc và thanh toán tiền còn lại khi nhận sim (áp dụng tại các tỉnh không có đại lý): Quý khách đảm bảo việc mua hàng bằng cách đặt cọc tối thiểu 10% giá trị sim qua chuyển khoản hoặc mã thẻ cào. Chúng tôi sẽ gửi bưu điện phát sim đến tay quý khách và thu tiền còn lại&nbsp;</span><em style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">(Hệ thống bưu điện trên cả nước đều cung cấp dịch vụ thu hộ tiền cho người bán - gọi tắt là COD. Theo đó, bưu điện sẽ giao hàng (sim) đến tận tay quý khách và thu tiền cho chúng tôi)</em><span style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">&nbsp;</span><br></p>', '2018-10-28', '2018-12-30'),
(31, 19, 'vi', 'Trần Ánh', 'tran-anh', '0888.04.6699', '2018-10-29', '2018-10-29'),
(32, 20, 'vi', 'Phương Diễm', 'phuong-diem', '0911.47.6699', '2018-10-29', '2018-10-29'),
(33, 21, 'vi', 'Phương Thảo', 'phuong-thao', '0967.10.6699', '2018-10-29', '2018-10-29'),
(34, 22, 'vi', 'Mai Linh', 'mai-linh', '0911.48.6699', '2018-10-29', '2018-10-29'),
(35, 23, 'vi', 'Ngọc Hân', 'ngoc-han', '0941.73.6699', '2018-10-29', '2018-10-29'),
(36, 24, 'vi', 'Nguyễn Mến', 'nguyen-men', '0976.16.6699', '2018-10-29', '2018-10-29'),
(37, 25, 'vi', 'Sim theo đầu số', 'sim-theo-dau-so', NULL, '2018-11-03', '2018-11-03'),
(38, 26, 'vi', 'Đầu số 097', 'dau-so-097', NULL, '2018-11-03', '2018-11-03'),
(39, 27, 'vi', 'Đầu số 098', 'dau-so-098', NULL, '2018-11-03', '2018-11-03'),
(40, 28, 'vi', 'Đầu số 096', 'dau-so-096', NULL, '2018-11-03', '2018-11-03'),
(41, 29, 'vi', 'Sim theo năm sinh', 'sim-theo-nam-sinh', NULL, '2018-11-03', '2018-11-03'),
(42, 30, 'vi', 'Sim đuôi 1991', 'sim-duoi-1991', NULL, '2018-11-03', '2018-11-03'),
(43, 31, 'vi', 'Sim đuôi 1992', 'sim-duoi-1992', NULL, '2018-11-03', '2018-11-03'),
(44, 32, 'vi', 'Sim đuôi 1993', 'sim-duoi-1993', NULL, '2018-11-03', '2018-11-03'),
(45, 33, 'vi', 'Sim đuôi 1994', 'sim-duoi-1994', NULL, '2018-11-03', '2018-11-03'),
(46, 34, 'vi', 'Sim theo nhà mạng', 'sim-theo-nha-mang', NULL, '2018-11-03', '2018-11-03'),
(47, 35, 'vi', 'Nhà mạng viettel', 'nha-mang-viettel', NULL, '2018-11-03', '2018-11-03'),
(48, 36, 'vi', 'Nhà mạng Vinaphone', 'nha-mang-vinaphone', NULL, '2018-11-03', '2018-11-03'),
(49, 37, 'vi', 'Sim theo giá tiền', 'sim-theo-gia-tien', NULL, '2018-11-03', '2018-11-03'),
(50, 38, 'vi', 'Sim dưới 500 nghìn', 'sim-duoi-500-nghin', NULL, '2018-11-03', '2018-11-03'),
(51, 39, 'vi', 'Footer left', 'footer-left', '<p>1</p>', '2018-11-03', '2018-11-03'),
(52, 40, 'vi', 'Footer right', 'footer-right', '<p>2</p>', '2018-11-03', '2018-11-03'),
(53, 41, 'vi', 'Sim Lục Quý', 'sim-luc-quy', '*111111\r\n*222222\r\n*333333\r\n*444444\r\n*555555\r\n*666666\r\n*777777\r\n*888888\r\n*999999', '2018-11-11', '2018-11-11'),
(54, 42, 'vi', 'Sim Ngũ Quý', 'sim-ngu-quy', '*11111\r\n*22222\r\n*33333\r\n*44444\r\n*55555\r\n*66666\r\n*77777\r\n*88888\r\n*99999', '2018-11-11', '2018-11-11'),
(55, 43, 'vi', 'Sim Tứ Quý', 'sim-tu-quy', '*1111\r\n*2222\r\n*3333\r\n*4444\r\n*5555\r\n*6666\r\n*7777\r\n*8888\r\n*9999', '2018-11-11', '2018-11-11'),
(56, 44, 'vi', 'Sim Tam Hoa', 'sim-tam-hoa', '*111\r\n*222\r\n*333\r\n*444\r\n*555\r\n*666\r\n*777\r\n*888\r\n*999', '2018-11-11', '2018-11-11'),
(57, 45, 'vi', 'Sim Tam Hoa Kép', 'sim-tam-hoa-kep', '*111222\r\n*111333\r\n*111444\r\n*111555\r\n*111666\r\n*111777\r\n*111888\r\n*111999\r\n*222111\r\n*222333\r\n*222444\r\n*222555\r\n*222666\r\n*222777\r\n*222888\r\n*222999\r\n*333111\r\n*333222\r\n*333444\r\n*333555\r\n*333666\r\n*333777\r\n*333888\r\n*333999\r\n*444111\r\n*444222\r\n*444333\r\n*444555\r\n*444666\r\n*444777\r\n*444888\r\n*444999\r\n*555111\r\n*555222\r\n*555333\r\n*555444\r\n*555666\r\n*555777\r\n*555888\r\n*555999\r\n*666111\r\n*666222\r\n*666333\r\n*666444\r\n*666555\r\n*666777\r\n*666888\r\n*666999\r\n*777111\r\n*777222\r\n*777333\r\n*777444\r\n*777555\r\n*777666\r\n*777888\r\n*777999\r\n*888111\r\n*888222\r\n*888333\r\n*888444\r\n*888555\r\n*888666\r\n*888777\r\n*888999\r\n*999111\r\n*999222\r\n*999333\r\n*999444\r\n*999555\r\n*999666\r\n*999777\r\n*999888', '2018-11-11', '2018-11-11'),
(58, 46, 'vi', 'Sim Lộc Phát', 'sim-loc-phat', '*66\r\n*68\r\n*69\r\n*86\r\n*88\r\n*96', '2018-11-11', '2018-11-11'),
(59, 47, 'vi', 'Sim Thần Tài', 'sim-than-tai', '*39\r\n*79', '2018-11-11', '2018-11-11'),
(60, 48, 'vi', 'Sim Ông Địa', 'sim-ong-dia', '*38\r\n*78', '2018-11-11', '2018-11-11'),
(61, 49, 'vi', 'Sim Lặp kép', 'sim-lap-kep', 'abab\r\naabb', '2018-11-11', '2018-11-11'),
(62, 50, 'vi', 'Sim Gánh Đảo', 'sim-ganh-dao', '*abba', '2018-11-11', '2018-11-11'),
(63, 51, 'vi', 'Sim Tiến lên', 'sim-tien-len', '*123\r\n*234\r\n*345\r\n*456\r\n*567\r\n*678\r\n*789', '2018-11-11', '2018-11-11'),
(64, 52, 'vi', 'Sim Lục Quý Giữa', 'sim-luc-quy-giua', '*111111*\r\n*222222*\r\n*333333*\r\n*444444*\r\n*555555*\r\n*666666*\r\n*777777*\r\n*888888*\r\n*999999*', '2018-11-11', '2018-11-11'),
(65, 53, 'vi', 'Sim Ngũ Quý Giữa', 'sim-ngu-quy-giua', '*11111*\r\n*22222*\r\n*33333*\r\n*44444*\r\n*55555*\r\n*66666*\r\n*77777*\r\n*88888*\r\n*99999*', '2018-11-11', '2018-11-11'),
(66, 54, 'vi', 'Sim Tứ Quý Giữa', 'sim-tu-quy-giua', '*1111*\r\n*2222*\r\n*3333*\r\n*4444*\r\n*5555*\r\n*6666*\r\n*7777*\r\n*8888*\r\n*9999*', '2018-11-11', '2018-11-11'),
(67, 55, 'vi', 'Sim Đầu Số Cổ', 'sim-dau-so-co', '0983*\r\n0903*\r\n0913*', '2018-11-11', '2018-11-11'),
(68, 56, 'vi', 'Sim Năm Sinh', 'sim-nam-sinh', NULL, '2018-11-11', '2018-11-11'),
(69, 57, 'vi', 'Sim Tự Chọn', 'sim-tu-chon', NULL, '2018-11-11', '2018-11-11'),
(70, 58, 'vi', 'Sim Vip', 'sim-vip', NULL, '2018-11-11', '2018-11-11'),
(71, 59, 'vi', 'Trang chủ', 'trang-chu', NULL, '2018-11-25', '2018-11-25'),
(72, 60, 'vi', 'Sim phong thủy', 'sim-phong-thuy', NULL, '2018-11-25', '2018-11-25'),
(73, 61, 'vi', 'Sim trả góp', 'sim-tra-gop', NULL, '2018-11-25', '2018-11-25'),
(74, 62, 'vi', 'Đầu số cũ', 'dau-so-cu', '090*\r\n098*', '2018-12-30', '2018-12-30'),
(75, 63, 'vi', 'Đầu số mới', 'dau-so-moi', '036*\r\n037*\r\n088*', '2018-12-30', '2018-12-30'),
(76, 64, 'vi', 'Giá tăng dần', 'gia-tang-dan', NULL, '2018-12-30', '2018-12-30'),
(77, 65, 'vi', 'Giá giảm dần', 'gia-giam-dan', NULL, '2018-12-30', '2018-12-30');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `block_home`
--

CREATE TABLE `block_home` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `block_home`
--

INSERT INTO `block_home` (`id`, `name`, `description`, `images`, `type`, `sort_order`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Tư vấn du học tổng quát', 'Lorem ipsum dolor sit amet timeam deleniti mnesarchum ex sed alii\r\nex nam paulo temporibus ea vis id odio adhuc', 'block-home/icon-cor3.jpg', 'cooperate', 1, 1, NULL, NULL),
(2, 'Săn học bổng', 'Lorem ipsum dolor sit amet timeam deleniti mnesarchum ex sed alii\r\nex nam paulo temporibus ea vis id odio adhuc', 'block-home/photofacefun_com_1493452992.jpg', 'service', 2, 1, NULL, NULL),
(3, 'Tư vấn ngành', 'Lorem ipsum dolor sit amet timeam deleniti mnesarchum ex sed alii\r\nex nam paulo temporibus ea vis id odio adhuc', 'block-home/Untitled-design.png', 'product', 3, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `block_landingpage`
--

CREATE TABLE `block_landingpage` (
  `id` int(11) NOT NULL,
  `landingpage_id` int(11) NOT NULL DEFAULT '0',
  `news_id` int(11) NOT NULL DEFAULT '0',
  `name` text COLLATE utf8_unicode_ci,
  `sluggable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_vi` longtext COLLATE utf8_unicode_ci,
  `content_en` longtext COLLATE utf8_unicode_ci,
  `script` text COLLATE utf8_unicode_ci,
  `postData` longtext COLLATE utf8_unicode_ci,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `block_landingpage`
--

INSERT INTO `block_landingpage` (`id`, `landingpage_id`, `news_id`, `name`, `sluggable`, `content_vi`, `content_en`, `script`, `postData`, `sort_order`, `parent_id`, `created_at`, `updated_at`) VALUES
(16, 11, 1, '[Block 01] Slide', 'slide', '<section class=\"section slider\">\r\n									<div>\r\n										<ul class=\"bx-slider\">\r\n											<li data-title=\"<strong> </strong>\" data-description=\"\">\r\n							<div class=\"main-img\" style=\"background-image: url(/photos/3/10-jpgb8l5nfquumfg00at1tbg.jpeg)\"></div>\r\n						</li><li data-title=\"<strong> </strong>\" data-description=\"\">\r\n							<div class=\"main-img\" style=\"background-image: url(/photos/3/9-jpgb8l5lsquumfg00at1tag.jpeg)\"></div>\r\n						</li>\r\n										</ul>\r\n										<div class=\"control\"></div>\r\n										<div class=\"headline\">\r\n											<div class=\"container-headline\"></div>\r\n										</div>\r\n										<a href=\"#dl-panorama\" class=\"go-bottom\"></a>\r\n									</div>\r\n								</section>', NULL, NULL, '{\"_token\":\"rYbF4ayNGcHdvgvsQ00FS8OsP1ouY0ptZDuq0cdd\",\"block\":\"15\",\"title\":{\"vi\":\"Slide\"},\"img11\":{\"vi\":[\"\\/photos\\/3\\/10-jpgb8l5nfquumfg00at1tbg.jpeg\",\"\\/photos\\/3\\/9-jpgb8l5lsquumfg00at1tag.jpeg\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]}}', 1, 0, '2018-02-09', '2018-02-09'),
(17, 12, 1, '[Block 02] 2222222222', '2222222222', '<section class=\"section dl-panorama\" style=\"background: url(\'template/tint/images/dl-panorama.jpg\');\">\r\n								<div class=\"position-relative \">\r\n									<div class=\"container animatedParent \">\r\n										<div class=\"row mo-ta\">\r\n											<div class=\"col-xs-12 col-sm-6 col-md-4 col-lg-4  animated fadeInDownShort delay-250\">\r\n												<h2 class=\"title-4\">2222222222</h2>\r\n											</div>\r\n											<div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6  animated fadeInDownShort delay-500\">\r\n												<p>Trong cuộc sống, với bao lo toan, hối hả... đã bao giờ ta chợt nhận thấy cần lắm một cái nắm tay siết chặt, cần lắm một sự kết nối quyện hòa, cần lắm những phút giây tĩnh tại để trở về chính con người thật của mình, ...</p>\r\n											</div>\r\n										</div>\r\n									</div>\r\n									<div class=\" animatedParent \">\r\n										<div class=\"container-fluid\">\r\n											<div class=\"img-absolute img-left img-1-2 responsive animated fadeInLeftShort delay-750\" style=\"background-image: url(/photos/3/tt-giai-tri-tren-bo-bien-2-jpgb8p5dlvu8fbg00besv0g.jpeg);\"></div>\r\n											<div class=\"col-xs-12 col-sm-12 col-md-4 col-md-offset-7 none-padding content animated fadeInRightShort delay-750\">\r\n												<div class=\"right\">\r\n													<div class=\"\">\r\n														<p><p><span style=\"color: rgb(51, 51, 51); font-family: Maitree-Regular, sans-serif; font-size: 15px;\">... chỉ cần nghe tiếng gió xào xạc, được đánh thức bởi tiếng chim hót xa xa trên bụi thông già, chỉ cần cảm được thở ấm áp trong tiết trời se lạnh, lặng ngắm những bông hoa tươi sắc sớm mai... chỉ cần ... một nơi chốn cho ta cảm giác bình an ...</span></p></p>\r\n													</div>\r\n												</div>\r\n											</div>\r\n										</div>\r\n									</div>\r\n								</div>\r\n							</section>', NULL, NULL, '{\"_token\":\"rYbF4ayNGcHdvgvsQ00FS8OsP1ouY0ptZDuq0cdd\",\"title\":{\"vi\":\"2222222222\"},\"description_12\":{\"vi\":\"Trong cu\\u1ed9c s\\u1ed1ng, v\\u1edbi bao lo toan, h\\u1ed1i h\\u1ea3... \\u0111\\u00e3 bao gi\\u1edd ta ch\\u1ee3t nh\\u1eadn th\\u1ea5y c\\u1ea7n l\\u1eafm m\\u1ed9t c\\u00e1i n\\u1eafm tay si\\u1ebft ch\\u1eb7t, c\\u1ea7n l\\u1eafm m\\u1ed9t s\\u1ef1 k\\u1ebft n\\u1ed1i quy\\u1ec7n h\\u00f2a, c\\u1ea7n l\\u1eafm nh\\u1eefng ph\\u00fat gi\\u00e2y t\\u0129nh t\\u1ea1i \\u0111\\u1ec3 tr\\u1edf v\\u1ec1 ch\\u00ednh con ng\\u01b0\\u1eddi th\\u1eadt c\\u1ee7a m\\u00ecnh, ...\"},\"img12\":{\"vi\":\"\\/photos\\/3\\/tt-giai-tri-tren-bo-bien-2-jpgb8p5dlvu8fbg00besv0g.jpeg\"},\"content\":{\"vi\":\"<p><span style=\\\"color: rgb(51, 51, 51); font-family: Maitree-Regular, sans-serif; font-size: 15px;\\\">... ch\\u1ec9 c\\u1ea7n nghe ti\\u1ebfng gi\\u00f3 x\\u00e0o x\\u1ea1c, \\u0111\\u01b0\\u1ee3c \\u0111\\u00e1nh th\\u1ee9c b\\u1edfi ti\\u1ebfng chim h\\u00f3t xa xa tr\\u00ean b\\u1ee5i th\\u00f4ng gi\\u00e0, ch\\u1ec9 c\\u1ea7n c\\u1ea3m \\u0111\\u01b0\\u1ee3c th\\u1edf \\u1ea5m \\u00e1p trong ti\\u1ebft tr\\u1eddi se l\\u1ea1nh, l\\u1eb7ng ng\\u1eafm nh\\u1eefng b\\u00f4ng hoa t\\u01b0\\u01a1i s\\u1eafc s\\u1edbm mai... ch\\u1ec9 c\\u1ea7n ... m\\u1ed9t n\\u01a1i ch\\u1ed1n cho ta c\\u1ea3m gi\\u00e1c b\\u00ecnh an ...<\\/span><\\/p>\"}}', 2, 0, '2018-02-09', '2018-02-09'),
(19, 15, 1, '[Block 05] 55555555555', '55555555555', '<section class=\"section dang-cap animatedParent \" style=\"background: url(template/tint/images/background-2.jpg);\">\r\n                                <div class=\"position-relative\">\r\n                                    <div class=\"container-fluid\">\r\n                                        <div class=\"img-absolute img-right img-1-2 responsive animated fadeInRightShort delay-250\" style=\"background-image: url(/photos/3/tt-giai-tri-tren-bo-bien-2-jpgb8p5dlvu8fbg00besv0g.jpeg);\"></div>\r\n                                        <div class=\"col-xs-12 col-sm-12 col-md-4 none-padding content\">\r\n                                            <div class=\"left\">\r\n                                                <h2 class=\"title-3 animated fadeInLeftShort delay-500\">55555555555</h2>\r\n                                                <div class=\"animated fadeInLeft delay-750\">\r\n                                                    <p><p style=\"color: rgb(51, 51, 51); font-family: Maitree-Regular, sans-serif; font-size: 15px;\">Đà Lạt là thành phố may mắn được sở hữu một di sản kiến trúc giá trị, ví như một bảo tàng kiến trúc châu Âu thế kỷ 20. Từ một đô thị nghỉ dưỡng do người Pháp xây dựng. Lịch sử phát triển quy hoạch đô thị Đà Lạt nửa đầu thế kỷ 20 dường như gắn liền với sự phát triển nghệ thuật quy hoạch đương đại của thế giới.</p><p style=\"color: rgb(51, 51, 51); font-family: Maitree-Regular, sans-serif; font-size: 15px;\">The Panorama Đà Lạt nằm ngay trên trục đường di sản của thành phố, nơi kiến trúc cổ điển đậm phong cách Pháp.</p></p>\r\n                                                    <a href=\"#\" target=\"_blank\">\r\n                                                        <p class=\"readmore\">Xem thêm</p>\r\n                                                    </a>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </section>', NULL, NULL, '{\"_token\":\"rYbF4ayNGcHdvgvsQ00FS8OsP1ouY0ptZDuq0cdd\",\"block\":\"15\",\"title\":{\"vi\":\"55555555555\"},\"link_15\":{\"vi\":\"#\"},\"img15\":{\"vi\":\"\\/photos\\/3\\/tt-giai-tri-tren-bo-bien-2-jpgb8p5dlvu8fbg00besv0g.jpeg\"},\"content\":{\"vi\":\"<p style=\\\"color: rgb(51, 51, 51); font-family: Maitree-Regular, sans-serif; font-size: 15px;\\\">\\u0110\\u00e0 L\\u1ea1t l\\u00e0 th\\u00e0nh ph\\u1ed1 may m\\u1eafn \\u0111\\u01b0\\u1ee3c s\\u1edf h\\u1eefu m\\u1ed9t di s\\u1ea3n ki\\u1ebfn tr\\u00fac gi\\u00e1 tr\\u1ecb, v\\u00ed nh\\u01b0 m\\u1ed9t b\\u1ea3o t\\u00e0ng ki\\u1ebfn tr\\u00fac ch\\u00e2u \\u00c2u th\\u1ebf k\\u1ef7 20. T\\u1eeb m\\u1ed9t \\u0111\\u00f4 th\\u1ecb ngh\\u1ec9 d\\u01b0\\u1ee1ng do ng\\u01b0\\u1eddi Ph\\u00e1p x\\u00e2y d\\u1ef1ng. L\\u1ecbch s\\u1eed ph\\u00e1t tri\\u1ec3n quy ho\\u1ea1ch \\u0111\\u00f4 th\\u1ecb \\u0110\\u00e0 L\\u1ea1t n\\u1eeda \\u0111\\u1ea7u th\\u1ebf k\\u1ef7 20 d\\u01b0\\u1eddng nh\\u01b0 g\\u1eafn li\\u1ec1n v\\u1edbi s\\u1ef1 ph\\u00e1t tri\\u1ec3n ngh\\u1ec7 thu\\u1eadt quy ho\\u1ea1ch \\u0111\\u01b0\\u01a1ng \\u0111\\u1ea1i c\\u1ee7a th\\u1ebf gi\\u1edbi.<\\/p><p style=\\\"color: rgb(51, 51, 51); font-family: Maitree-Regular, sans-serif; font-size: 15px;\\\">The Panorama \\u0110\\u00e0 L\\u1ea1t n\\u1eb1m ngay tr\\u00ean tr\\u1ee5c \\u0111\\u01b0\\u1eddng di s\\u1ea3n c\\u1ee7a th\\u00e0nh ph\\u1ed1, n\\u01a1i ki\\u1ebfn tr\\u00fac c\\u1ed5 \\u0111i\\u1ec3n \\u0111\\u1eadm phong c\\u00e1ch Ph\\u00e1p.<\\/p>\"}}', 5, 0, '2018-02-09', '2018-02-09'),
(20, 14, 1, '[Block 04] 444444444444444', '444444444444444', '<section class=\"section toa-lac\" style=\"background: url(\'template/tint/images/background-sen.jpg\');\">\r\n                                <div class=\"position-relative \">\r\n                                    <div class=\"container-fluid\">\r\n                                        <div class=\"animatedParent \">\r\n                                            <div class=\"img-absolute img-left img-1-2 responsive animated fadeInLeftShort delay-250\" \r\n                                                 style=\"background-image: url(\'/photos/3/9-jpgb8l5lsquumfg00at1tag.jpeg\');\"></div>\r\n                                            <div class=\"col-xs-12 col-sm-12 col-md-4 col-md-offset-7 none-padding content\">\r\n                                                <div class=\"right\">\r\n                                                    <h2 class=\"title-3 animated fadeInRight delay-500\">444444444444444</h2>\r\n                                                    <div class=\"animated fadeInRight delay-750\">\r\n                                                        <p><p>4444444444 content</p></p>\r\n                                                        <a href=\"#\" target=\"_blank\">\r\n                                                            <p class=\"readmore\">Xem thêm</p>\r\n                                                        </a>\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </section>', NULL, NULL, '{\"_token\":\"rYbF4ayNGcHdvgvsQ00FS8OsP1ouY0ptZDuq0cdd\",\"block\":\"14\",\"title\":{\"vi\":\"444444444444444\"},\"link_14\":{\"vi\":\"#\"},\"img14\":{\"vi\":\"\\/photos\\/3\\/9-jpgb8l5lsquumfg00at1tag.jpeg\"},\"content\":{\"vi\":\"<p>4444444444 content<\\/p>\"}}', 4, 0, '2018-02-09', '2018-02-09'),
(21, 13, 1, '[Block 03] 33333333\r\n333333', '33333333333333', '<section class=\"section duy-nhat \" style=\"background: url(template/tint/images/background-3.jpg);\">\r\n                                <div class=\"position-relative animatedParent \">\r\n                                    <div class=\"container-fluid\">\r\n                                        <div class=\"row\">\r\n                                            <div class=\"img-absolute  img-1-2 responsive animated fadeInLeftShort delay-250\" style=\"background-image: url(/photos/3/9-jpgb8l5lsquumfg00at1tag.jpeg);\"></div>\r\n                                            <div class=\"container\">\r\n                                                \r\n                                                <div class=\"col-xs-12 col-sm-12 col-md-6 col-lg-4 col-md-offset-4 col-lg-offset-2 none-padding content\">\r\n                                                    <div class=\"left\">\r\n                                                        <h2 class=\"title-3 animated fadeInRight delay-750\">\r\n                                                            33333333<br />\r\n333333\r\n                                                        </h2>\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </section>', NULL, NULL, '{\"_token\":\"rYbF4ayNGcHdvgvsQ00FS8OsP1ouY0ptZDuq0cdd\",\"block\":\"13\",\"title\":{\"vi\":\"33333333\\r\\n333333\"},\"img13\":{\"vi\":\"\\/photos\\/3\\/9-jpgb8l5lsquumfg00at1tag.jpeg\"}}', 3, 0, '2018-02-09', '2018-02-09'),
(23, 21, 2, '[Block 01] Tọa lạc trên\r\ntrục đường di sản\r\ncổ kính đậm phong cách Pháp', '', '<section class=\" section animatedParent animateOnce \" style=\"background: url(../template/tint/images/background-sen.jpg);\">\r\n								<div class=\"position-relative \">\r\n									<div class=\"container-fluid\">\r\n										<div class=\" animatedParent animateOnce\">\r\n											<div class=\"img-absolute img-left img-1-2 responsive animated fadeInLeftShort delay-500\" \r\n												 style=\"background-image: url(/photos/3/9-jpgb8l5lsquumfg00at1tag.jpeg);\"></div>\r\n											<div class=\"col-xs-12 col-sm-12 col-md-4 col-md-offset-7 none-padding content\">\r\n												<div class=\"right\">\r\n													<div id=\"divContent\">\r\n														<h2 class=\"title-3 animated fadeInRight delay-500\">Tọa lạc trên<br />\r\ntrục đường di sản<br />\r\ncổ kính đậm phong cách Pháp</h2>\r\n														<p><p style=\"color: rgb(51, 51, 51); font-family: Maitree-Regular, sans-serif; font-size: 15px;\">Toạ lạc trên trục đường di sản mang đậm phong cách Pháp của 13 ngôi biệt thự cổ độc tôn và trường tồn với thời gian.</p><p style=\"color: rgb(51, 51, 51); font-family: Maitree-Regular, sans-serif; font-size: 15px;\">Từ đây, lữ khách còn được thỏa mãn niềm đam mê khám phá vẻ đẹp lôi cuốn và sức hút hấp dẫn của thành phố ngàn hoa, và dễ dàng di chuyển đến tất cả các địa danh du lịch nổi tiếng của Đà Lạt trong thời gian ngắn nhất.</p><ul class=\"list-location\" style=\"padding-left: 0px; color: rgb(51, 51, 51); font-family: Maitree-Regular, sans-serif; font-size: 15px;\"><li style=\"list-style-image: url(\"../images/ico-arrow.png\"); line-height: 30px;\">Siêu thị Big C: <b style=\"font-family: Maitree-SemiBold;\">1,9km - 4 phút</b></li><li style=\"list-style-image: url(\"../images/ico-arrow.png\"); line-height: 30px;\">Quảng trường trung tâm HXH: <b style=\"font-family: Maitree-SemiBold;\">1,5km - 4 phút</b></li><li style=\"list-style-image: url(\"../images/ico-arrow.png\"); line-height: 30px;\">Chợ Đà Lạt: <b style=\"font-family: Maitree-SemiBold;\">2,9km - 6 phút</b></li><li style=\"list-style-image: url(\"../images/ico-arrow.png\"); line-height: 30px;\">Bệnh viện Đa Khoa Lâm Đồng: <b style=\"font-family: Maitree-SemiBold;\">4,2km - 10 phút</b></li><li style=\"list-style-image: url(\"../images/ico-arrow.png\"); line-height: 30px;\">Bệnh viện Hoàn Mỹ: <b style=\"font-family: Maitree-SemiBold;\">2,4km - 6 phút</b></li><li style=\"list-style-image: url(\"../images/ico-arrow.png\"); line-height: 30px;\">Trường THPT Bùi Thị Xuân: <b style=\"font-family: Maitree-SemiBold;\">4,3km - 10 phút</b></li><li style=\"list-style-image: url(\"../images/ico-arrow.png\"); line-height: 30px;\">Trường ĐH Đà Lạt: <b style=\"font-family: Maitree-SemiBold;\">4km - 9 phút</b></li><li style=\"list-style-image: url(\"../images/ico-arrow.png\"); line-height: 30px;\">Trường THPT Trần Phú: <b style=\"font-family: Maitree-SemiBold;\">1,3km - 3 phút</b></li></ul></p>\r\n													</div>\r\n												</div>\r\n											</div>\r\n										</div>\r\n									</div>\r\n								</div>\r\n							</section>', NULL, NULL, '{\"_token\":\"rYbF4ayNGcHdvgvsQ00FS8OsP1ouY0ptZDuq0cdd\",\"title\":{\"vi\":\"T\\u1ecda l\\u1ea1c tr\\u00ean\\r\\ntr\\u1ee5c \\u0111\\u01b0\\u1eddng di s\\u1ea3n\\r\\nc\\u1ed5 k\\u00ednh \\u0111\\u1eadm phong c\\u00e1ch Ph\\u00e1p\"},\"img\":{\"vi\":\"\\/photos\\/3\\/9-jpgb8l5lsquumfg00at1tag.jpeg\"},\"content\":{\"vi\":\"<p style=\\\"color: rgb(51, 51, 51); font-family: Maitree-Regular, sans-serif; font-size: 15px;\\\">To\\u1ea1 l\\u1ea1c tr\\u00ean tr\\u1ee5c \\u0111\\u01b0\\u1eddng di s\\u1ea3n mang \\u0111\\u1eadm phong c\\u00e1ch Ph\\u00e1p c\\u1ee7a 13 ng\\u00f4i bi\\u1ec7t th\\u1ef1 c\\u1ed5 \\u0111\\u1ed9c t\\u00f4n v\\u00e0 tr\\u01b0\\u1eddng t\\u1ed3n v\\u1edbi th\\u1eddi gian.<\\/p><p style=\\\"color: rgb(51, 51, 51); font-family: Maitree-Regular, sans-serif; font-size: 15px;\\\">T\\u1eeb \\u0111\\u00e2y, l\\u1eef kh\\u00e1ch c\\u00f2n \\u0111\\u01b0\\u1ee3c th\\u1ecfa m\\u00e3n ni\\u1ec1m \\u0111am m\\u00ea kh\\u00e1m ph\\u00e1 v\\u1ebb \\u0111\\u1eb9p l\\u00f4i cu\\u1ed1n v\\u00e0 s\\u1ee9c h\\u00fat h\\u1ea5p d\\u1eabn c\\u1ee7a th\\u00e0nh ph\\u1ed1 ng\\u00e0n hoa, v\\u00e0 d\\u1ec5 d\\u00e0ng di chuy\\u1ec3n \\u0111\\u1ebfn t\\u1ea5t c\\u1ea3 c\\u00e1c \\u0111\\u1ecba danh du l\\u1ecbch n\\u1ed5i ti\\u1ebfng c\\u1ee7a \\u0110\\u00e0 L\\u1ea1t trong th\\u1eddi gian ng\\u1eafn nh\\u1ea5t.<\\/p><ul class=\\\"list-location\\\" style=\\\"padding-left: 0px; color: rgb(51, 51, 51); font-family: Maitree-Regular, sans-serif; font-size: 15px;\\\"><li style=\\\"list-style-image: url(\\\"..\\/images\\/ico-arrow.png\\\"); line-height: 30px;\\\">Si\\u00eau th\\u1ecb Big C:\\u00a0<b style=\\\"font-family: Maitree-SemiBold;\\\">1,9km - 4 ph\\u00fat<\\/b><\\/li><li style=\\\"list-style-image: url(\\\"..\\/images\\/ico-arrow.png\\\"); line-height: 30px;\\\">Qu\\u1ea3ng tr\\u01b0\\u1eddng trung t\\u00e2m HXH:\\u00a0<b style=\\\"font-family: Maitree-SemiBold;\\\">1,5km - 4 ph\\u00fat<\\/b><\\/li><li style=\\\"list-style-image: url(\\\"..\\/images\\/ico-arrow.png\\\"); line-height: 30px;\\\">Ch\\u1ee3 \\u0110\\u00e0 L\\u1ea1t:\\u00a0<b style=\\\"font-family: Maitree-SemiBold;\\\">2,9km - 6 ph\\u00fat<\\/b><\\/li><li style=\\\"list-style-image: url(\\\"..\\/images\\/ico-arrow.png\\\"); line-height: 30px;\\\">B\\u1ec7nh vi\\u1ec7n \\u0110a Khoa L\\u00e2m \\u0110\\u1ed3ng:\\u00a0<b style=\\\"font-family: Maitree-SemiBold;\\\">4,2km - 10 ph\\u00fat<\\/b><\\/li><li style=\\\"list-style-image: url(\\\"..\\/images\\/ico-arrow.png\\\"); line-height: 30px;\\\">B\\u1ec7nh vi\\u1ec7n Ho\\u00e0n M\\u1ef9:\\u00a0<b style=\\\"font-family: Maitree-SemiBold;\\\">2,4km - 6 ph\\u00fat<\\/b><\\/li><li style=\\\"list-style-image: url(\\\"..\\/images\\/ico-arrow.png\\\"); line-height: 30px;\\\">Tr\\u01b0\\u1eddng THPT B\\u00f9i Th\\u1ecb Xu\\u00e2n:\\u00a0<b style=\\\"font-family: Maitree-SemiBold;\\\">4,3km - 10 ph\\u00fat<\\/b><\\/li><li style=\\\"list-style-image: url(\\\"..\\/images\\/ico-arrow.png\\\"); line-height: 30px;\\\">Tr\\u01b0\\u1eddng \\u0110H \\u0110\\u00e0 L\\u1ea1t:\\u00a0<b style=\\\"font-family: Maitree-SemiBold;\\\">4km - 9 ph\\u00fat<\\/b><\\/li><li style=\\\"list-style-image: url(\\\"..\\/images\\/ico-arrow.png\\\"); line-height: 30px;\\\">Tr\\u01b0\\u1eddng THPT Tr\\u1ea7n Ph\\u00fa:\\u00a0<b style=\\\"font-family: Maitree-SemiBold;\\\">1,3km - 3 ph\\u00fat<\\/b><\\/li><\\/ul>\"}}', 22, 0, '2018-02-09', '2018-02-09'),
(24, 22, 2, '[Block 02] 2222222222', '', '<section class=\" section slide-2 animatedParent animateOnce \" style=\"background: url(/assets/uploads/myfiles/images/utilities/background-sen.jpg);\">\r\n								<div class=\"position-relative\">\r\n									<div class=\"slick-slider\">\r\n										<div class=\"slider-1 list-album\">\r\n											<div class=\"col-xs-12 col-sm-6 none-padding item\" style=\"background-image: url(/photos/3/9-jpgb8l5lsquumfg00at1tag.jpeg)\">\r\n							<a class=\"item1\" data-id=\"831\" data-href=\"/photos/3/9-jpgb8l5lsquumfg00at1tag.jpeg\">\r\n								<div class=\"name\">\r\n									<p>2222222222</p>\r\n								</div>\r\n							</a>\r\n						</div><div class=\"col-xs-12 col-sm-6 none-padding item\" style=\"background-image: url(/photos/3/trungtamgiaitri-jpgb8rj7fibmtlg009qn9k0.jpeg)\">\r\n							<a class=\"item1\" data-id=\"831\" data-href=\"/photos/3/trungtamgiaitri-jpgb8rj7fibmtlg009qn9k0.jpeg\">\r\n								<div class=\"name\">\r\n									<p>222 2222 222</p>\r\n								</div>\r\n							</a>\r\n						</div><div class=\"col-xs-12 col-sm-6 none-padding item\" style=\"background-image: url(/photos/3/bar-bai-bien-jpgb8p5b3fu8fbg00besuvg.jpeg)\">\r\n							<a class=\"item1\" data-id=\"831\" data-href=\"/photos/3/bar-bai-bien-jpgb8p5b3fu8fbg00besuvg.jpeg\">\r\n								<div class=\"name\">\r\n									<p>wewq eqwe wqeqwe</p>\r\n								</div>\r\n							</a>\r\n						</div>\r\n										</div>\r\n									</div>\r\n								</div>\r\n							</section>', NULL, NULL, '{\"_token\":\"rYbF4ayNGcHdvgvsQ00FS8OsP1ouY0ptZDuq0cdd\",\"block\":\"21\",\"title\":{\"vi\":[\"2222222222\",\"222 2222 222\",\"wewq eqwe wqeqwe\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]},\"img\":{\"vi\":[\"\\/photos\\/3\\/9-jpgb8l5lsquumfg00at1tag.jpeg\",\"\\/photos\\/3\\/trungtamgiaitri-jpgb8rj7fibmtlg009qn9k0.jpeg\",\"\\/photos\\/3\\/bar-bai-bien-jpgb8p5b3fu8fbg00besuvg.jpeg\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]}}', 22, 0, '2018-02-09', '2018-02-09');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(15) NOT NULL,
  `parent_id` int(15) DEFAULT '0',
  `type` varchar(20) DEFAULT 'menu',
  `route_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_cate_style` int(11) NOT NULL DEFAULT '0' COMMENT '0: none, 1: multiple',
  `sort_order` int(11) DEFAULT '0',
  `image` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_menu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `style` varchar(50) NOT NULL DEFAULT 'first',
  `footer` int(3) NOT NULL DEFAULT '0',
  `mobile` int(3) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `parent_id`, `type`, `route_name`, `sub_cate_style`, `sort_order`, `image`, `image_menu`, `style`, `footer`, `mobile`, `updated_at`, `created_at`) VALUES
(66, 0, 'menu', 'singleNews', 0, 6, NULL, NULL, 'first', 0, 0, '2018-10-29 11:39:12', '2018-01-07 10:25:38'),
(63, 0, 'menu', 'singleNews', 0, 3, NULL, NULL, 'first', 0, 0, '2018-10-29 11:39:12', '2018-01-06 04:04:36'),
(62, 0, 'menu', 'singleNews', 0, 4, NULL, NULL, 'first', 0, 0, '2018-10-29 11:39:12', '2018-01-06 04:04:12'),
(56, 0, 'menu', 'singleNews', 0, 2, NULL, NULL, 'first', 0, 0, '2018-10-29 11:39:12', '2018-01-04 08:26:48'),
(55, 0, 'menu', 'singleNews', 0, 1, NULL, NULL, 'first', 0, 0, '2018-10-29 11:39:12', '2018-01-04 08:26:30'),
(68, 0, 'menu', 'listNews', 0, 5, NULL, NULL, 'first', 0, 0, '2018-10-29 11:39:12', '2018-10-29 04:39:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category_data`
--

CREATE TABLE `category_data` (
  `id` int(15) NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '#',
  `seo_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL DEFAULT 'vi',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `category_data`
--

INSERT INTO `category_data` (`id`, `name`, `link`, `seo_description`, `seo_keyword`, `category_id`, `language`, `updated_at`, `created_at`) VALUES
(131, 'Liên hệ', '/lien-he/s66.html', NULL, NULL, 66, 'vi', '2018-10-16 10:40:42', '2018-01-07 10:25:38'),
(125, 'THU MUA SIM', '/thu-mua-sim/s63.html', NULL, NULL, 63, 'vi', '2018-10-16 10:00:32', '2018-01-06 04:04:36'),
(123, 'THANH TOÁN', '/thanh-toan/s62.html', NULL, NULL, 62, 'vi', '2018-10-16 10:00:43', '2018-01-06 04:04:12'),
(111, 'SIM TRẢ GÓP', '/sim-tra-gop/s56.html', NULL, NULL, 56, 'vi', '2018-10-16 10:00:16', '2018-01-04 08:26:48'),
(109, 'SIM PHONG THỦY', '/sim-phong-thuy/s55.html', NULL, NULL, 55, 'vi', '2018-10-16 09:58:52', '2018-01-04 08:26:30'),
(134, 'Tin tức', '/tin-tuc/n68.html', NULL, NULL, 68, 'vi', '2018-10-29 04:39:00', '2018-10-29 04:39:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `message`, `phone`, `created_at`, `updated_at`) VALUES
(4, 'tienlq', 'tienlq@gmail.com', 'tienlq tét', '098765432', '2017-11-09', '2017-11-09'),
(5, 'tienlq', 'tienlq@gmail.com', 'tienlq tét', '098765432', '2017-11-09', '2017-11-09'),
(6, 'tienlq', 'tienlq@gmail.com', 'tienlq tét', '098765432', '2017-11-09', '2017-11-09'),
(7, NULL, NULL, NULL, NULL, '2018-01-07', '2018-01-07');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `images`
--

CREATE TABLE `images` (
  `id` int(255) NOT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkImage` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkClick` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `layout` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `images`
--

INSERT INTO `images` (`id`, `title`, `linkImage`, `linkClick`, `description`, `type`, `sort_order`, `layout`, `updated_at`, `created_at`) VALUES
(8, 'Nữ Lãnh Đạo Quốc Tế Trải Nghiệm Dịch Vụ Tại Himalaya Spa', 'img/category/1.jpg', 'http://himalayaspa.vn/nu-lanh-dao-quoc-te-trai-nghiem-dich-vu-tai-himalaya-spa/v6.html', 'Nữ Lãnh Đạo Quốc Tế Trải Nghiệm Dịch Vụ Tại Himalaya Spa', 'slide', 0, NULL, '2017-09-17 08:11:17', '2017-05-23 03:05:45'),
(9, 'Khai Trương Cơ Sơ 6 Phù Đổng Thiên Vương', 'img/category/2.jpg', '#', 'Ngày 16/05/2017 Himalaya Spa chính thức khai trương cơ sở 6 tại Phù Đổng Thiên Vương, do bà Phạm Ngọc Phượng làm giám đốc điều hành.', 'slide', 0, NULL, '2017-09-17 08:11:34', '2017-05-27 01:54:41'),
(10, 'Xông Đá Muối Himalaya - Dát Vàng Đá Quý Thạch Anh', 'img/category/3.jpg', 'http://himalayaspa.vn/xong-da-muoi-himalaya/sn175.html', 'Xông hơi đá muối là liệu pháp chăm sóc sức khỏe, làm đẹp có từ lâu đời của người Hàn, đã trở thành một thói quen chăm sóc sức khỏe lành mạnh, tiết kiệm, an toàn.', 'slide', 0, NULL, '2017-09-17 08:11:48', '2017-06-01 14:44:56');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `lifestyle`
--

CREATE TABLE `lifestyle` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `lifestyle`
--

INSERT INTO `lifestyle` (`id`, `image`, `link`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, 'https://www.cocobay.vn/wp-content/uploads/2016/11/photofacefun_com_1493452992.jpg', '#', 0, NULL, NULL),
(2, 'https://www.cocobay.vn/wp-content/uploads/2016/11/Ocean-1.jpg', '#', 0, NULL, NULL),
(3, 'https://www.cocobay.vn/wp-content/uploads/2016/11/Wonderland.png', '#', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `lifestyle_data`
--

CREATE TABLE `lifestyle_data` (
  `id` int(11) NOT NULL,
  `highlights_id` int(11) NOT NULL,
  `language` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'vi',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `lifestyle_data`
--

INSERT INTO `lifestyle_data` (`id`, `highlights_id`, `language`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'vi', 'Tư vấn du học tổng quát', 'Đánh thức giấc mơ cổ tích tại \"Xứ sở thần tiên\"', NULL, NULL),
(2, 1, 'en', 'en - Tư vấn du học tổng quát', 'en - Đánh thức giấc mơ cổ tích tại \"Xứ sở thần tiên\"', NULL, NULL),
(3, 2, 'vi', 'Tư vấn ngành', 'tu van nganh - Đánh thức giấc mơ cổ tích tại \"Xứ sở thần tiên\"', NULL, NULL),
(4, 2, 'en', 'en - Tư vấn ngành', 'en - tu van nghanh - Đánh thức giấc mơ cổ tích tại \"Xứ sở thần tiên\"', NULL, NULL),
(5, 3, 'vi', 'Tư vấn trường', 'tu van truong - Đánh thức giấc mơ cổ tích tại \"Xứ sở thần tiên\"', NULL, NULL),
(6, 3, 'en', 'en - Tư vấn trường', 'en - tu van truong - Đánh thức giấc mơ cổ tích tại \"Xứ sở thần tiên\"', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `id` bigint(15) NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(15) DEFAULT NULL,
  `active` int(2) DEFAULT NULL,
  `front` int(11) NOT NULL DEFAULT '0',
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `landingpage_id` int(11) DEFAULT '0',
  `dataLandingpage` longtext COLLATE utf8_unicode_ci,
  `background_mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background_pc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`id`, `image`, `image_thumb`, `video`, `user_id`, `category_id`, `active`, `front`, `type`, `landingpage_id`, `dataLandingpage`, `background_mobile`, `background_pc`, `updated_at`, `created_at`) VALUES
(1, '/photos/3/9-jpgb8l5lsquumfg00at1tag.jpeg', NULL, NULL, NULL, 66, NULL, 0, NULL, 1, '{\"_token\":\"rYbF4ayNGcHdvgvsQ00FS8OsP1ouY0ptZDuq0cdd\",\"category_id\":\"66\",\"type\":\"\",\"image\":\"\\/photos\\/3\\/9-jpgb8l5lsquumfg00at1tag.jpeg\",\"landingpage_id\":\"1\",\"title\":{\"vi\":\"home\"},\"summary\":{\"vi\":\"\"},\"keyword\":{\"vi\":\"\"},\"description\":{\"vi\":\"\"}}', NULL, NULL, '2018-02-08 21:54:26', '2018-01-04 08:33:25'),
(2, NULL, NULL, NULL, NULL, 56, NULL, 0, NULL, 0, '{\"_token\":\"rYbF4ayNGcHdvgvsQ00FS8OsP1ouY0ptZDuq0cdd\",\"category_id\":\"56\",\"type\":\"\",\"image\":\"\",\"landingpage_id\":\"1\",\"title\":{\"vi\":\"Quang c\\u1ea3nh\"},\"summary\":{\"vi\":\"\"},\"keyword\":{\"vi\":\"\"},\"description\":{\"vi\":\"\"}}', NULL, NULL, '2018-10-29 09:07:00', '2018-02-09 03:50:29');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news_data`
--

CREATE TABLE `news_data` (
  `id` bigint(15) NOT NULL,
  `news_id` int(11) DEFAULT '0',
  `language` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'vi',
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sluggable` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `content02` text COLLATE utf8_unicode_ci,
  `script` text COLLATE utf8_unicode_ci,
  `landingpage_data` longtext COLLATE utf8_unicode_ci,
  `summary` text COLLATE utf8_unicode_ci,
  `keyword` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `news_data`
--

INSERT INTO `news_data` (`id`, `news_id`, `language`, `title`, `youtube_code`, `sluggable`, `content`, `content02`, `script`, `landingpage_data`, `summary`, `keyword`, `description`, `updated_at`, `created_at`) VALUES
(135, 1, 'vi', 'home', NULL, 'home', '<section class=\"section slider\">\r\n									<div>\r\n										<ul class=\"bx-slider\">\r\n											<li data-title=\"<strong> </strong>\" data-description=\"\">\r\n							<div class=\"main-img\" style=\"background-image: url(/photos/3/10-jpgb8l5nfquumfg00at1tbg.jpeg)\"></div>\r\n						</li><li data-title=\"<strong> </strong>\" data-description=\"\">\r\n							<div class=\"main-img\" style=\"background-image: url(/photos/3/9-jpgb8l5lsquumfg00at1tag.jpeg)\"></div>\r\n						</li>\r\n										</ul>\r\n										<div class=\"control\"></div>\r\n										<div class=\"headline\">\r\n											<div class=\"container-headline\"></div>\r\n										</div>\r\n										<a href=\"#dl-panorama\" class=\"go-bottom\"></a>\r\n									</div>\r\n								</section><section class=\"section dl-panorama\" style=\"background: url(\'template/tint/images/dl-panorama.jpg\');\">\r\n								<div class=\"position-relative \">\r\n									<div class=\"container animatedParent \">\r\n										<div class=\"row mo-ta\">\r\n											<div class=\"col-xs-12 col-sm-6 col-md-4 col-lg-4  animated fadeInDownShort delay-250\">\r\n												<h2 class=\"title-4\">2222222222</h2>\r\n											</div>\r\n											<div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6  animated fadeInDownShort delay-500\">\r\n												<p>Trong cuộc sống, với bao lo toan, hối hả... đã bao giờ ta chợt nhận thấy cần lắm một cái nắm tay siết chặt, cần lắm một sự kết nối quyện hòa, cần lắm những phút giây tĩnh tại để trở về chính con người thật của mình, ...</p>\r\n											</div>\r\n										</div>\r\n									</div>\r\n									<div class=\" animatedParent \">\r\n										<div class=\"container-fluid\">\r\n											<div class=\"img-absolute img-left img-1-2 responsive animated fadeInLeftShort delay-750\" style=\"background-image: url(/photos/3/tt-giai-tri-tren-bo-bien-2-jpgb8p5dlvu8fbg00besv0g.jpeg);\"></div>\r\n											<div class=\"col-xs-12 col-sm-12 col-md-4 col-md-offset-7 none-padding content animated fadeInRightShort delay-750\">\r\n												<div class=\"right\">\r\n													<div class=\"\">\r\n														<p><p><span style=\"color: rgb(51, 51, 51); font-family: Maitree-Regular, sans-serif; font-size: 15px;\">... chỉ cần nghe tiếng gió xào xạc, được đánh thức bởi tiếng chim hót xa xa trên bụi thông già, chỉ cần cảm được thở ấm áp trong tiết trời se lạnh, lặng ngắm những bông hoa tươi sắc sớm mai... chỉ cần ... một nơi chốn cho ta cảm giác bình an ...</span></p></p>\r\n													</div>\r\n												</div>\r\n											</div>\r\n										</div>\r\n									</div>\r\n								</div>\r\n							</section><section class=\"section duy-nhat \" style=\"background: url(template/tint/images/background-3.jpg);\">\r\n                                <div class=\"position-relative animatedParent \">\r\n                                    <div class=\"container-fluid\">\r\n                                        <div class=\"row\">\r\n                                            <div class=\"img-absolute  img-1-2 responsive animated fadeInLeftShort delay-250\" style=\"background-image: url(/photos/3/9-jpgb8l5lsquumfg00at1tag.jpeg);\"></div>\r\n                                            <div class=\"container\">\r\n                                                \r\n                                                <div class=\"col-xs-12 col-sm-12 col-md-6 col-lg-4 col-md-offset-4 col-lg-offset-2 none-padding content\">\r\n                                                    <div class=\"left\">\r\n                                                        <h2 class=\"title-3 animated fadeInRight delay-750\">\r\n                                                            33333333<br />\r\n333333\r\n                                                        </h2>\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </section><section class=\"section toa-lac\" style=\"background: url(\'template/tint/images/background-sen.jpg\');\">\r\n                                <div class=\"position-relative \">\r\n                                    <div class=\"container-fluid\">\r\n                                        <div class=\"animatedParent \">\r\n                                            <div class=\"img-absolute img-left img-1-2 responsive animated fadeInLeftShort delay-250\" \r\n                                                 style=\"background-image: url(\'/photos/3/9-jpgb8l5lsquumfg00at1tag.jpeg\');\"></div>\r\n                                            <div class=\"col-xs-12 col-sm-12 col-md-4 col-md-offset-7 none-padding content\">\r\n                                                <div class=\"right\">\r\n                                                    <h2 class=\"title-3 animated fadeInRight delay-500\">444444444444444</h2>\r\n                                                    <div class=\"animated fadeInRight delay-750\">\r\n                                                        <p><p>4444444444 content</p></p>\r\n                                                        <a href=\"#\" target=\"_blank\">\r\n                                                            <p class=\"readmore\">Xem thêm</p>\r\n                                                        </a>\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </section><section class=\"section dang-cap animatedParent \" style=\"background: url(template/tint/images/background-2.jpg);\">\r\n                                <div class=\"position-relative\">\r\n                                    <div class=\"container-fluid\">\r\n                                        <div class=\"img-absolute img-right img-1-2 responsive animated fadeInRightShort delay-250\" style=\"background-image: url(/photos/3/tt-giai-tri-tren-bo-bien-2-jpgb8p5dlvu8fbg00besv0g.jpeg);\"></div>\r\n                                        <div class=\"col-xs-12 col-sm-12 col-md-4 none-padding content\">\r\n                                            <div class=\"left\">\r\n                                                <h2 class=\"title-3 animated fadeInLeftShort delay-500\">55555555555</h2>\r\n                                                <div class=\"animated fadeInLeft delay-750\">\r\n                                                    <p><p style=\"color: rgb(51, 51, 51); font-family: Maitree-Regular, sans-serif; font-size: 15px;\">Đà Lạt là thành phố may mắn được sở hữu một di sản kiến trúc giá trị, ví như một bảo tàng kiến trúc châu Âu thế kỷ 20. Từ một đô thị nghỉ dưỡng do người Pháp xây dựng. Lịch sử phát triển quy hoạch đô thị Đà Lạt nửa đầu thế kỷ 20 dường như gắn liền với sự phát triển nghệ thuật quy hoạch đương đại của thế giới.</p><p style=\"color: rgb(51, 51, 51); font-family: Maitree-Regular, sans-serif; font-size: 15px;\">The Panorama Đà Lạt nằm ngay trên trục đường di sản của thành phố, nơi kiến trúc cổ điển đậm phong cách Pháp.</p></p>\r\n                                                    <a href=\"#\" target=\"_blank\">\r\n                                                        <p class=\"readmore\">Xem thêm</p>\r\n                                                    </a>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </section>', NULL, '<script>\r\n											jQuery(document).ready(function ($) {\r\n												var sections = $(\"#page-tong-quan.fullpage .section\");\r\n												var fullpage = $(\"#page-tong-quan.fullpage\");\r\n\r\n												fullpage.fullpage({\r\n													verticalCentered: false,\r\n													anchors: [\'slide\',\'2222222222\',\'33333333333333\',\'444444444444444\',\'55555555555\',\'footer\'],\r\n													onLeave: function (index, nextIndex, direction) {\r\n\r\n														if (sections[nextIndex - 1]) {\r\n															$(sections[nextIndex - 1]).find(\".animated\").addClass(\"go\");\r\n														}\r\n														if (nextIndex > 1) {\r\n															$(\".fixed\").addClass(\"top-head-sticky\");\r\n															$(\"header\").addClass(\"head_sticky\");\r\n															$(\"a#gotop\").fadeIn(250);\r\n\r\n														} else {\r\n\r\n															$(\".fixed\").removeClass(\"top-head-sticky\");\r\n															$(\"header\").removeClass(\"head_sticky\");\r\n															$(\"a#gotop\").fadeOut(250);\r\n														}\r\n													},\r\n\r\n													afterRender: function (anchorLink, index) {\r\n														sections.find(\".animated\").removeClass(\"go\")\r\n													},\r\n													responsiveWidth: 992\r\n												});\r\n											});\r\n										</script>', NULL, NULL, NULL, NULL, '2018-02-08 23:05:47', '2018-01-09 08:52:19'),
(136, 2, 'vi', 'Quang cảnh', NULL, 'quang-canh', '<span style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: small;\"><u><span style=\"color: rgb(0, 0, 255);\"><strong>Bước 1</strong></span></u><span style=\"color: rgb(0, 0, 255);\"><strong>:</strong></span>&nbsp;<strong>ĐẶT SIM</strong><br>Quý khách chọn số sim và đặt hàng trên web hoặc gọi điện đến một trong các số hotline hỗ trợ khách hàng của&nbsp;</span><span style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">Asimdep</span><span style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: small;\">.<br><br><u><span style=\"color: rgb(0, 0, 255);\"><strong>Bước 2</strong></span></u><span style=\"color: rgb(0, 0, 255);\"><strong>:</strong></span>&nbsp;<strong>XÁC NHẬN</strong><br>Khi nhận được đơn hàng nhân viên bán hàng sẽ kiểm tra số trong kho và gọi điện lại báo cho Quý khách.<br><br><u><span style=\"color: rgb(0, 0, 255);\"><strong>Bước 3</strong></span></u><span style=\"color: rgb(0, 0, 255);\"><strong>:</strong></span>&nbsp;<strong>GIAO HÀNG</strong></span><br style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\"><strong style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">• Khách hàng ở Hà Nội, TP.HCM:&nbsp;</strong><span style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">Chúng tôi sẽ giao sim và thu tiền tại nhà Quý khách.</span><br style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\"><strong style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">• Khách hàng ở tỉnh thành khác:</strong><span style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">&nbsp;Quý khách chuyển tiền mua sim vào tài khoản ngân hàng của Asimdep, công ty sẽ gửi sim về nhà quý khách qua chuyển phát nhanh, thời gian 24h. Thông tin về tài khoản ngân hàng xem phía dưới đây.</span>', NULL, '', NULL, NULL, NULL, NULL, '2018-10-29 09:07:00', '2018-02-09 03:50:29');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news_landingpages`
--

CREATE TABLE `news_landingpages` (
  `id` int(11) NOT NULL,
  `language` varchar(10) COLLATE utf8_unicode_ci DEFAULT 'vn',
  `news_id` int(11) NOT NULL DEFAULT '0',
  `template_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` text COLLATE utf8_unicode_ci,
  `img_thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_large` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `project`
--

INSERT INTO `project` (`id`, `image`, `name`, `content`, `link`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, 'frontend/img/5_1502870295_jpg_50ebafb77f3831c6adb4ecbea0d02f60.jpg', 'Chải Thông Kinh Lạc', 'Lorem ipsum dolor sit amet timeam deleniti mnesarchum ex sed alii hinc dolores ad cum. Urbanitas similique ex nam paulo temporibus ea vis id odio adhuc nostrum eos', 'http://himalayaspa.vn/chai-luu-thong-khi-huyet/sn209.html', 0, NULL, '2017-06-01 20:57:25'),
(2, 'frontend/img/5_1502870295_jpg_50ebafb77f3831c6adb4ecbea0d02f60.jpg', 'Massage Body Đá Muối', 'Lorem ipsum dolor sit amet timeam deleniti mnesarchum ex sed alii hinc dolores ad cum. Urbanitas similique ex nam paulo temporibus ea vis id odio adhuc nostrum eos', '#http://himalayaspa.vn/massage-body-da-muoi/sn207.html', 0, NULL, '2017-06-01 20:47:04'),
(3, 'frontend/img/5_1502870295_jpg_50ebafb77f3831c6adb4ecbea0d02f60.jpg', 'Tắm trắng tăng sinh Collagen', 'Lorem ipsum dolor sit amet timeam deleniti mnesarchum ex sed alii hinc dolores ad cum. Urbanitas similique ex nam paulo temporibus ea vis id odio adhuc nostrum eos', 'http://himalayaspa.vn/tam-trang-tang-sinh-collagen/sn181.html', 0, NULL, '2017-06-01 20:50:50'),
(4, 'frontend/img/5_1502870295_jpg_50ebafb77f3831c6adb4ecbea0d02f60.jpg', 'Trị mụn tinh chất hồng sâm', 'Lorem ipsum dolor sit amet timeam deleniti mnesarchum ex sed alii hinc dolores ad cum. Urbanitas similique ex nam paulo temporibus ea vis id odio adhuc nostrum eos', 'http://himalayaspa.vn/tri-mun-tinh-chat-hong-sam/sn182.html', 0, NULL, '2017-06-01 20:55:55'),
(5, 'frontend/img/5_1502870295_jpg_50ebafb77f3831c6adb4ecbea0d02f60.jpg', 'Giảm béo và giảm cân lưng', 'Lorem ipsum dolor sit amet timeam deleniti mnesarchum ex sed alii hinc dolores ad cum. Urbanitas similique ex nam paulo temporibus ea vis id odio adhuc nostrum eos', 'http://himalayaspa.vn/giam-beo-va-giam-can-lung/sn210.html', 0, NULL, '2017-06-01 21:01:54'),
(7, 'frontend/img/5_1502870295_jpg_50ebafb77f3831c6adb4ecbea0d02f60.jpg', 'Trị Liệu Đau Mỏi Vai Gáy Ayurveda', 'Lorem ipsum dolor sit amet timeam deleniti mnesarchum ex sed alii hinc dolores ad cum. Urbanitas similique ex nam paulo temporibus ea vis id odio adhuc nostrum eos', 'http://himalayaspa.vn/ayurveda-tri-dau-moi-vai-gay/sn233.html', 0, '2017-08-23 02:26:36', '2017-08-23 09:29:04');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sim`
--

CREATE TABLE `sim` (
  `id` int(11) NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sluggable` varchar(100) COLLATE utf8_unicode_ci DEFAULT '0',
  `price` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `image` varchar(266) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sim`
--

INSERT INTO `sim` (`id`, `phone`, `sluggable`, `price`, `user_id`, `image`, `supplier_id`, `created_at`, `updated_at`) VALUES
(309, '096.4010.777', '0964010777', 500000, 7, '/images/sims/phone/096.4010.777.png', 1, NULL, NULL),
(310, '0986.658.000', '0986658000', 500000, 7, '/images/sims/phone/0986.658.000.png', 1, NULL, NULL),
(311, '0967.381.000', '0967381000', 500000, 7, '/images/sims/phone/0967.381.000.png', 1, NULL, NULL),
(312, '0967.36.8228', '0967368228', 500000, 7, '/images/sims/phone/0967.36.8228.png', 1, NULL, NULL),
(313, '098.121.50.50', '0981215050', 1000000, 7, '/images/sims/phone/098.121.50.50.png', 1, NULL, NULL),
(314, '097.131.50.50', '0971315050', 500000, 7, '/images/sims/phone/097.131.50.50.png', 1, NULL, NULL),
(315, '088.6789.660', '0886789660', 500000, 7, '/images/sims/phone/088.6789.660.png', 2, NULL, NULL),
(316, '0942.116.268', '0942116268', 500000, 7, '/images/sims/phone/0942.116.268.png', 2, NULL, NULL),
(317, '0911.347.868', '0911347868', 500000, 7, '/images/sims/phone/0911.347.868.png', 2, NULL, NULL),
(318, '0927.63.3979', '0927633979', 500000, 7, '/images/sims/phone/0927.63.3979.png', 4, NULL, NULL),
(319, '0925.5567.98', '0925556798', 1000000, 7, '/images/sims/phone/0925.5567.98.png', 4, NULL, NULL),
(320, '0929.68.8386', '0929688386', 500000, 7, '/images/sims/phone/0929.68.8386.png', 4, NULL, NULL),
(321, '0904.615.608', '0904615608', 500000, 7, '/images/sims/phone/0904.615.608.png', 3, NULL, NULL),
(322, '0908.32.1286', '0908321286', 500000, 7, '/images/sims/phone/0908.32.1286.png', 3, NULL, NULL),
(323, '0931.666.390', '0931666390', 500000, 7, '/images/sims/phone/0931.666.390.png', 3, NULL, NULL),
(324, '0994.85.3939', '0994853939', 500000, 7, '/images/sims/phone/0994.85.3939.png', 5, NULL, NULL),
(325, '0994.59.3939', '0994593939', 1000000, 7, '/images/sims/phone/0994.59.3939.png', 5, NULL, NULL),
(326, '0834.381.777', '0834381777', 1000000, 7, '/images/sims/phone/0834.381.777.png', 2, NULL, NULL),
(327, '0832.514.777', '0832514777', 1000000, 7, '/images/sims/phone/0832.514.777.png', 2, NULL, NULL),
(328, '0856.558.777', '0856558777', 1000000, 7, '/images/sims/phone/0856.558.777.png', 2, NULL, NULL),
(329, '0784.944.777', '0784944777', 1000000, 7, '/images/sims/phone/0784.944.777.png', 3, NULL, NULL),
(330, '0377748466', '377748466', 1000000, 7, '/images/sims/phone/0377748466.png', 1, NULL, NULL),
(331, '0377744839', '377744839', 1000000, 7, '/images/sims/phone/0377744839.png', 1, NULL, NULL),
(332, '0377725839', '377725839', 1000000, 7, '/images/sims/phone/0377725839.png', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `site_config`
--

CREATE TABLE `site_config` (
  `id` int(15) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skyper` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `yahoo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer` text COLLATE utf8_unicode_ci,
  `youtube` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtubeMobie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_home` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background_pc` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background_mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code02` text COLLATE utf8_unicode_ci,
  `code01` text COLLATE utf8_unicode_ci,
  `block01` text COLLATE utf8_unicode_ci,
  `block02` text COLLATE utf8_unicode_ci,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `site_config`
--

INSERT INTO `site_config` (`id`, `name`, `email`, `skyper`, `yahoo`, `phone`, `title`, `description`, `keyword`, `footer`, `youtube`, `youtubeMobie`, `video_home`, `background_pc`, `background_mobile`, `code02`, `code01`, `block01`, `block02`, `logo`, `favicon`, `updated_at`, `created_at`) VALUES
(1, 'Asimdep', 'xx@Asimdep.vn', 'quangtien262', '', '0901 799 666', 'Asimdep.vn', 'Asimdep.vn', 'Asimdep.vn', '<p>NVEDUCATION CO., LTD.</p>\r\n            <p>Email: customer.service@nveducation.com.vn</p>\r\n            <p>Địa chỉ:&nbsp;&nbsp;T10- OF04- KĐT Times City, 458 Minh Khai,&nbsp;<span style=\"color: rgb(0, 0, 0);\">Hai Bà Trưng, Hà Nội</span></p>', NULL, NULL, NULL, '/photos/3/banner.png', NULL, '0', '300', '<p>NVEDUCA NVEDUCATION<br />\r\nGI&Uacute;P BẠN MỞ CỬA SỔ DU HỌC CANADA&nbsp;</p>', '<h3>Du học Canada: Th&ocirc;ng tin chi tiết về c&aacute;c ch&iacute;nh s&aacute;ch nhập cư, <strong>hồ sơ xin&nbsp;visa du học Canada</strong>&nbsp;c&ugrave;ng những tin tức mới nhất về lịch diễn ra c&aacute;c buổi hội thảo&nbsp;<strong>tư vấn du học Canada</strong>&nbsp;từ những c&ocirc;ng ty tư vấn du học uy t&iacute;n sẽ được cập nhật li&ecirc;n tục mỗi ng&agrave;y.</h3>\r\n\r\n<h3>&nbsp;</h3>\r\n\r\n<p><strong>ĐĂNG K&Yacute; TƯ VẤN NGAY</strong></p>\r\n\r\n<p>&nbsp;</p>', '/photos/shares/logo.png', '', '2018-11-24 11:28:36', '0000-00-00 00:00:00'),
(2, 'NVEDUCATION', 'customer.service@nveducation.com.vn', 'quangtien262', '', '0901 799 666', 'nveducation.com.vn', 'nveducation.com.vn', 'nveducation.com.vn', '<ul>\r\n        <li><b>Website:</b> http://himalayaspa.vn</li>\r\n        <li>Chuyên gia tư vấn (24/7) <b>19000252&nbsp;</b></li>\r\n    </ul>', NULL, NULL, '/photos/3/A1-``1900x900.jpg', '/photos/shares/background/hdetail_bg.jpg', '/photos/shares/background/about_bg_mobile.jpg', '0', '<div id=\"https://www.facebook.com/pg/Nveducation-Du-học-Canada-367951910331459/\"></div>\r\n<script>(function(d, s, id) {\r\n  var js, fjs = d.getElementsByTagName(s)[0];\r\n  if (d.getElementById(id)) return;\r\n  js = d.createElement(s); js.id = id;\r\n  js.src = \'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10&appId=118779825390560\';\r\n  fjs.parentNode.insertBefore(js, fjs);\r\n}(document, \'script\', \'facebook-jssdk\'));</script>\r\n\r\n<div class=\"https://www.facebook.com/Nveducation-Du-học-Canada-367951910331459/\" data-href=\"https://www.facebook.com/facebook\" data-tabs=\"timeline\" data-height=\"100\" data-small-header=\"false\" data-adapt-container-width=\"true\" data-hide-cover=\"false\" data-show-facepile=\"true\"><blockquote cite=\"https://www.facebook.com/Nveducation-Du-học-Canada-367951910331459/\" class=\"fb-xfbml-parse-ignore\"><a href=\"https://www.facebook.com/Nveducation-Du-học-Canada-367951910331459/\">Facebook</a></blockquote></div>', '<p>Combination of tourism and recreation</p>\r\n\r\n<p>Classiest in Southeast Asia</p>\r\n\r\n<p>Welcome to July 2017</p>', '<p>Stylish destination for the first time in Vietnam is ready to welcome you!</p>\r\n\r\n<p><strong>Experience it now, do not delay</strong></p>', '/photos/3/avatar.jpg', '', '2017-11-07 23:35:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sluggable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sim_template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `elementClass` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `supplier`
--

INSERT INTO `supplier` (`id`, `name`, `sluggable`, `logo`, `sim_template`, `elementClass`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, 'Viettel', 'viettel', '/images/logo/viettel.gif', 'images/sims/supplier_template/viettel.png', 'nha-mang-viettel', 1, NULL, NULL),
(2, 'Vinaphone', 'vinaphone', '/images/logo/vinaphone.gif', 'images/sims/supplier_template/vinaphone.png', 'nha-mang-vinaphone', 2, NULL, NULL),
(3, 'Mobifone', 'mobifone', '/images/logo/mobifone.gif', 'images/sims/supplier_template/mobifone.png', 'nha-mang-mobifone', 3, NULL, NULL),
(4, 'Vietnamobile', 'vietnamobile', '/images/logo/vietnamobile.gif', 'images/sims/supplier_template/vietnammobile.png', 'nha-mang-vietnamobile', 4, NULL, NULL),
(5, 'Gmobile', 'gmobile', '/images/logo/gmobile.gif', 'images/sims/supplier_template/gmobile.png', 'nha-mang-gmobile', 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `userType` tinyint(3) UNSIGNED DEFAULT '0',
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `countryCode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phoneNumber` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skypeAccount` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebookID` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebookLink` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `username`, `userType`, `password`, `firstName`, `lastName`, `fullName`, `city`, `zipcode`, `countryCode`, `region`, `avatar`, `email`, `phoneNumber`, `skypeAccount`, `facebookID`, `facebookLink`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'admin', 1, '$2y$10$0ofVsFfJV1cixPallt5Q1uUTfYouwkiGh73F36kk6Kh15vyFbenP2', 'admin', 'admin', 'admin', NULL, NULL, NULL, NULL, NULL, 'te@gmail.com', NULL, NULL, NULL, NULL, 'QI10jmE0AeW7vb2ZNrDx63UynN2v7xtERhHifYuDyLYOHPbi7VaqX8xVj2LX', '2017-02-16 05:10:42', '2017-05-04 07:40:54'),
(7, 'ogai@agjp.biz', 0, '$2y$10$4mFcxUETOiaQl.jwVbPCHOTg32kZFttouMK4KipFUfmvyj7kRfoHC', 'Thợ 01', NULL, 'Thợ 01', NULL, NULL, NULL, NULL, NULL, 'ogai@agjp.biz', NULL, NULL, NULL, NULL, 'lAa5RtDxqEGEt3TyBUswriJXHnQcYMDnU2uRIqZive5rfu5ubEkm6bqCGYGP', '2017-02-16 21:44:33', '2017-02-16 21:45:02'),
(8, 'tienlq3@gmail.com', 0, '$2y$10$U1QPM1a6.vzIhkZsFGfvsu.d7x14dPbwj4VQuF7aCHjdMLM370KQG', 'Thợ 02', NULL, 'Thợ 02', NULL, NULL, NULL, NULL, NULL, 'tienlq3@gmail.com', NULL, NULL, NULL, NULL, 'fSL5Hd0RJLdPPUjXPIOzVUKIUSrZPWe5ZHVQz04VvLMGxcmkuIVdlfHB38qP', '2017-02-25 02:01:07', '2017-03-01 20:49:17'),
(9, 'tho03', 0, '123', 'Thợ 03', NULL, 'Thợ 0333', NULL, NULL, NULL, NULL, NULL, 'tienlq4@gmail.com', NULL, NULL, NULL, NULL, 'hkMMgX0vxWJMiTQH6qn9f5mKSTecPEHfUmFX3Y4MMz4qDH171Knam96akuA5', '2017-02-25 02:01:59', '2018-12-31 03:51:41');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `block`
--
ALTER TABLE `block`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `block_config`
--
ALTER TABLE `block_config`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `block_data`
--
ALTER TABLE `block_data`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `block_home`
--
ALTER TABLE `block_home`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `block_landingpage`
--
ALTER TABLE `block_landingpage`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Chỉ mục cho bảng `category_data`
--
ALTER TABLE `category_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Chỉ mục cho bảng `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `lifestyle`
--
ALTER TABLE `lifestyle`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `lifestyle_data`
--
ALTER TABLE `lifestyle_data`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news_data`
--
ALTER TABLE `news_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `title` (`title`);

--
-- Chỉ mục cho bảng `news_landingpages`
--
ALTER TABLE `news_landingpages`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `sim`
--
ALTER TABLE `sim`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `site_config`
--
ALTER TABLE `site_config`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `zzz_user_username_unique` (`username`),
  ADD UNIQUE KEY `zzz_user_email_unique` (`email`),
  ADD UNIQUE KEY `zzz_user_phonenumber_unique` (`phoneNumber`),
  ADD UNIQUE KEY `zzz_user_skypeaccount_unique` (`skypeAccount`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT cho bảng `block`
--
ALTER TABLE `block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT cho bảng `block_config`
--
ALTER TABLE `block_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT cho bảng `block_data`
--
ALTER TABLE `block_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT cho bảng `block_home`
--
ALTER TABLE `block_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT cho bảng `block_landingpage`
--
ALTER TABLE `block_landingpage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT cho bảng `category_data`
--
ALTER TABLE `category_data`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;
--
-- AUTO_INCREMENT cho bảng `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT cho bảng `images`
--
ALTER TABLE `images`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT cho bảng `lifestyle`
--
ALTER TABLE `lifestyle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT cho bảng `lifestyle_data`
--
ALTER TABLE `lifestyle_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT cho bảng `news_data`
--
ALTER TABLE `news_data`
  MODIFY `id` bigint(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT cho bảng `news_landingpages`
--
ALTER TABLE `news_landingpages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT cho bảng `sim`
--
ALTER TABLE `sim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=333;
--
-- AUTO_INCREMENT cho bảng `site_config`
--
ALTER TABLE `site_config`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT cho bảng `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
