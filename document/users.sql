-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 22, 2018 lúc 06:02 PM
-- Phiên bản máy phục vụ: 10.1.25-MariaDB
-- Phiên bản PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `pione`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `userType` tinyint(3) UNSIGNED DEFAULT '0',
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `countryCode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phoneNumber` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skypeAccount` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebookID` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebookLink` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `username`, `userType`, `password`, `firstName`, `lastName`, `fullName`, `city`, `zipcode`, `countryCode`, `region`, `avatar`, `email`, `phoneNumber`, `skypeAccount`, `facebookID`, `facebookLink`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'admin', 1, '$2y$10$0ofVsFfJV1cixPallt5Q1uUTfYouwkiGh73F36kk6Kh15vyFbenP2', 'admin', 'admin', 'admin', NULL, NULL, NULL, NULL, NULL, 'te@gmail.com', NULL, NULL, NULL, NULL, 'ZwMgi6MsVvpRVGNTAU6OELmPo2SAqalZLJgAgxl5YRQYl4Mx8sPMEHOhhLFL', '2017-02-16 05:10:42', '2017-05-04 07:40:54'),
(7, 'ogai@agjp.biz', 0, '$2y$10$4mFcxUETOiaQl.jwVbPCHOTg32kZFttouMK4KipFUfmvyj7kRfoHC', NULL, NULL, 'osami ogai', NULL, NULL, NULL, NULL, NULL, 'ogai@agjp.biz', NULL, NULL, NULL, NULL, 'lAa5RtDxqEGEt3TyBUswriJXHnQcYMDnU2uRIqZive5rfu5ubEkm6bqCGYGP', '2017-02-16 21:44:33', '2017-02-16 21:45:02'),
(8, 'tienlq3@gmail.com', 0, '$2y$10$U1QPM1a6.vzIhkZsFGfvsu.d7x14dPbwj4VQuF7aCHjdMLM370KQG', 'admin', NULL, 'tienlq1', NULL, NULL, NULL, NULL, NULL, 'tienlq3@gmail.com', NULL, NULL, NULL, NULL, 'fSL5Hd0RJLdPPUjXPIOzVUKIUSrZPWe5ZHVQz04VvLMGxcmkuIVdlfHB38qP', '2017-02-25 02:01:07', '2017-03-01 20:49:17'),
(9, 'tienlq4@gmail.com', 0, '$2y$10$bFUcus429mk5WOkI7bjZlusAHQopiU.eG1JqQgFuYI8dUMgLOsNWm', 'admin', NULL, 'tienlq4', NULL, NULL, NULL, NULL, NULL, 'tienlq4@gmail.com', NULL, NULL, NULL, NULL, 'hkMMgX0vxWJMiTQH6qn9f5mKSTecPEHfUmFX3Y4MMz4qDH171Knam96akuA5', '2017-02-25 02:01:59', '2017-03-01 20:52:43');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `zzz_user_username_unique` (`username`),
  ADD UNIQUE KEY `zzz_user_email_unique` (`email`),
  ADD UNIQUE KEY `zzz_user_phonenumber_unique` (`phoneNumber`),
  ADD UNIQUE KEY `zzz_user_skypeaccount_unique` (`skypeAccount`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
