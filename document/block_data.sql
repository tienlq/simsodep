-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 11, 2018 lúc 08:52 AM
-- Phiên bản máy phục vụ: 10.1.25-MariaDB
-- Phiên bản PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `simsodep`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `block_data`
--

CREATE TABLE `block_data` (
  `id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `language` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'vi',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sluggable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `block_data`
--

INSERT INTO `block_data` (`id`, `block_id`, `language`, `name`, `sluggable`, `description`, `created_at`, `updated_at`) VALUES
(25, 13, 'vi', 'Sim dưới 500 nghìn', 'sim-duoi-500-nghin', NULL, '2018-10-18', '2018-10-18'),
(26, 14, 'vi', 'Sim giá 500 - 1 triệu', 'sim-gia-500-1-trieu', NULL, '2018-10-18', '2018-10-18'),
(27, 15, 'vi', 'Nguyễn Khuyên', 'nguyen-khuyen', '038.5954.811', '2018-10-18', '2018-10-29'),
(28, 16, 'vi', 'Quang Tiến', 'quang-tien', '036.365.1500', '2018-10-18', '2018-10-29'),
(29, 17, 'vi', 'Thanh Tâm', 'thanh-tam', '0941.90.6699', '2018-10-18', '2018-10-29'),
(30, 18, 'vi', 'Hướng dẫn cách thức mua sim', 'huong-dan-cach-thuc-mua-sim', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">▪ Cách 1: Asimdep giao sim và thu tiền tận nhà miễn phí (áp dụng Hà Nội, Tp.HCM và các tỉnh/thành có đại lý)</span><br style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\"><span style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">▪ Cách 2: Quý khách đến cửa hàng tại Hà Nội hoặc Tp.HCM (Xem Địa chỉ ở cuối trang)</span><br style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\"><span style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">▪ Cách 3: Đặt cọc và thanh toán tiền còn lại khi nhận sim (áp dụng tại các tỉnh không có đại lý): Quý khách đảm bảo việc mua hàng bằng cách đặt cọc tối thiểu 10% giá trị sim qua chuyển khoản hoặc mã thẻ cào. Chúng tôi sẽ gửi bưu điện phát sim đến tay quý khách và thu tiền còn lại&nbsp;</span><em style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">(Hệ thống bưu điện trên cả nước đều cung cấp dịch vụ thu hộ tiền cho người bán - gọi tắt là COD. Theo đó, bưu điện sẽ giao hàng (sim) đến tận tay quý khách và thu tiền cho chúng tôi)</em><span style=\"color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">&nbsp;</span><br></p>', '2018-10-28', '2018-10-28'),
(31, 19, 'vi', 'Trần Ánh', 'tran-anh', '0888.04.6699', '2018-10-29', '2018-10-29'),
(32, 20, 'vi', 'Phương Diễm', 'phuong-diem', '0911.47.6699', '2018-10-29', '2018-10-29'),
(33, 21, 'vi', 'Phương Thảo', 'phuong-thao', '0967.10.6699', '2018-10-29', '2018-10-29'),
(34, 22, 'vi', 'Mai Linh', 'mai-linh', '0911.48.6699', '2018-10-29', '2018-10-29'),
(35, 23, 'vi', 'Ngọc Hân', 'ngoc-han', '0941.73.6699', '2018-10-29', '2018-10-29'),
(36, 24, 'vi', 'Nguyễn Mến', 'nguyen-men', '0976.16.6699', '2018-10-29', '2018-10-29'),
(37, 25, 'vi', 'Sim theo đầu số', 'sim-theo-dau-so', NULL, '2018-11-03', '2018-11-03'),
(38, 26, 'vi', 'Đầu số 097', 'dau-so-097', NULL, '2018-11-03', '2018-11-03'),
(39, 27, 'vi', 'Đầu số 098', 'dau-so-098', NULL, '2018-11-03', '2018-11-03'),
(40, 28, 'vi', 'Đầu số 096', 'dau-so-096', NULL, '2018-11-03', '2018-11-03'),
(41, 29, 'vi', 'Sim theo năm sinh', 'sim-theo-nam-sinh', NULL, '2018-11-03', '2018-11-03'),
(42, 30, 'vi', 'Sim đuôi 1991', 'sim-duoi-1991', NULL, '2018-11-03', '2018-11-03'),
(43, 31, 'vi', 'Sim đuôi 1992', 'sim-duoi-1992', NULL, '2018-11-03', '2018-11-03'),
(44, 32, 'vi', 'Sim đuôi 1993', 'sim-duoi-1993', NULL, '2018-11-03', '2018-11-03'),
(45, 33, 'vi', 'Sim đuôi 1994', 'sim-duoi-1994', NULL, '2018-11-03', '2018-11-03'),
(46, 34, 'vi', 'Sim theo nhà mạng', 'sim-theo-nha-mang', NULL, '2018-11-03', '2018-11-03'),
(47, 35, 'vi', 'Nhà mạng viettel', 'nha-mang-viettel', NULL, '2018-11-03', '2018-11-03'),
(48, 36, 'vi', 'Nhà mạng Vinaphone', 'nha-mang-vinaphone', NULL, '2018-11-03', '2018-11-03'),
(49, 37, 'vi', 'Sim theo giá tiền', 'sim-theo-gia-tien', NULL, '2018-11-03', '2018-11-03'),
(50, 38, 'vi', 'Sim dưới 500 nghìn', 'sim-duoi-500-nghin', NULL, '2018-11-03', '2018-11-03'),
(51, 39, 'vi', 'Footer left', 'footer-left', '<p>1</p>', '2018-11-03', '2018-11-03'),
(52, 40, 'vi', 'Footer right', 'footer-right', '<p>2</p>', '2018-11-03', '2018-11-03'),
(53, 41, 'vi', 'Sim Lục Quý', 'sim-luc-quy', '*111111\r\n*222222\r\n*333333\r\n*444444\r\n*555555\r\n*666666\r\n*777777\r\n*888888\r\n*999999', '2018-11-11', '2018-11-11'),
(54, 42, 'vi', 'Sim Ngũ Quý', 'sim-ngu-quy', '*11111\r\n*22222\r\n*33333\r\n*44444\r\n*55555\r\n*66666\r\n*77777\r\n*88888\r\n*99999', '2018-11-11', '2018-11-11'),
(55, 43, 'vi', 'Sim Tứ Quý', 'sim-tu-quy', '*1111\r\n*2222\r\n*3333\r\n*4444\r\n*5555\r\n*6666\r\n*7777\r\n*8888\r\n*9999', '2018-11-11', '2018-11-11'),
(56, 44, 'vi', 'Sim Tam Hoa', 'sim-tam-hoa', '*111\r\n*222\r\n*333\r\n*444\r\n*555\r\n*666\r\n*777\r\n*888\r\n*999', '2018-11-11', '2018-11-11'),
(57, 45, 'vi', 'Sim Tam Hoa Kép', 'sim-tam-hoa-kep', '*111222\r\n*111333\r\n*111444\r\n*111555\r\n*111666\r\n*111777\r\n*111888\r\n*111999\r\n*222111\r\n*222333\r\n*222444\r\n*222555\r\n*222666\r\n*222777\r\n*222888\r\n*222999\r\n*333111\r\n*333222\r\n*333444\r\n*333555\r\n*333666\r\n*333777\r\n*333888\r\n*333999\r\n*444111\r\n*444222\r\n*444333\r\n*444555\r\n*444666\r\n*444777\r\n*444888\r\n*444999\r\n*555111\r\n*555222\r\n*555333\r\n*555444\r\n*555666\r\n*555777\r\n*555888\r\n*555999\r\n*666111\r\n*666222\r\n*666333\r\n*666444\r\n*666555\r\n*666777\r\n*666888\r\n*666999\r\n*777111\r\n*777222\r\n*777333\r\n*777444\r\n*777555\r\n*777666\r\n*777888\r\n*777999\r\n*888111\r\n*888222\r\n*888333\r\n*888444\r\n*888555\r\n*888666\r\n*888777\r\n*888999\r\n*999111\r\n*999222\r\n*999333\r\n*999444\r\n*999555\r\n*999666\r\n*999777\r\n*999888', '2018-11-11', '2018-11-11'),
(58, 46, 'vi', 'Sim Lộc Phát', 'sim-loc-phat', '*66\r\n*68\r\n*69\r\n*86\r\n*88\r\n*96', '2018-11-11', '2018-11-11'),
(59, 47, 'vi', 'Sim Thần Tài', 'sim-than-tai', '*39\r\n*79', '2018-11-11', '2018-11-11'),
(60, 48, 'vi', 'Sim Ông Địa', 'sim-ong-dia', '*38\r\n*78', '2018-11-11', '2018-11-11'),
(61, 49, 'vi', 'Sim Lặp kép', 'sim-lap-kep', 'abab\r\naabb', '2018-11-11', '2018-11-11'),
(62, 50, 'vi', 'Sim Gánh Đảo', 'sim-ganh-dao', '*abba', '2018-11-11', '2018-11-11'),
(63, 51, 'vi', 'Sim Tiến lên', 'sim-tien-len', '*123\r\n*234\r\n*345\r\n*456\r\n*567\r\n*678\r\n*789', '2018-11-11', '2018-11-11'),
(64, 52, 'vi', 'Sim Lục Quý Giữa', 'sim-luc-quy-giua', '*111111*\r\n*222222*\r\n*333333*\r\n*444444*\r\n*555555*\r\n*666666*\r\n*777777*\r\n*888888*\r\n*999999*', '2018-11-11', '2018-11-11'),
(65, 53, 'vi', 'Sim Ngũ Quý Giữa', 'sim-ngu-quy-giua', '*11111*\r\n*22222*\r\n*33333*\r\n*44444*\r\n*55555*\r\n*66666*\r\n*77777*\r\n*88888*\r\n*99999*', '2018-11-11', '2018-11-11'),
(66, 54, 'vi', 'Sim Tứ Quý Giữa', 'sim-tu-quy-giua', '*1111*\r\n*2222*\r\n*3333*\r\n*4444*\r\n*5555*\r\n*6666*\r\n*7777*\r\n*8888*\r\n*9999*', '2018-11-11', '2018-11-11'),
(67, 55, 'vi', 'Sim Đầu Số Cổ', 'sim-dau-so-co', '0983*\r\n0903*\r\n0913*', '2018-11-11', '2018-11-11'),
(68, 56, 'vi', 'Sim Năm Sinh', 'sim-nam-sinh', NULL, '2018-11-11', '2018-11-11'),
(69, 57, 'vi', 'Sim Tự Chọn', 'sim-tu-chon', NULL, '2018-11-11', '2018-11-11'),
(70, 58, 'vi', 'Sim Vip', 'sim-vip', NULL, '2018-11-11', '2018-11-11');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `block_data`
--
ALTER TABLE `block_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `block_data`
--
ALTER TABLE `block_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
