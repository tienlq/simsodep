-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 11, 2018 lúc 08:52 AM
-- Phiên bản máy phục vụ: 10.1.25-MariaDB
-- Phiên bản PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `simsodep`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `block`
--

CREATE TABLE `block` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price01` int(11) DEFAULT '0',
  `price02` int(11) DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `block`
--

INSERT INTO `block` (`id`, `parent_id`, `image`, `link`, `type`, `price01`, `price02`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, 0, '/photos/3/a3-800x800.png', 'http://nveducation.com.vn/l1/tu-van-du-hoc/n16.html', 'highlights', 0, 0, 2, NULL, '2017-11-07'),
(2, 0, '/photos/3/a4-640x960.png', 'http://nveducation.com.vn/l1/tu-van-nganh-hoc/n20.html', 'highlights', 0, 0, 1, NULL, '2017-11-07'),
(3, 0, '/photos/3/A1-800X800.png', 'http://nveducation.com.vn/l1/tu-van-san-hoc-bong/n19.html', 'highlights', 0, 0, 1, NULL, '2017-11-07'),
(4, 0, '/photos/3/A1-800X800.png', '#', 'lifestyle', 0, 0, 0, '2017-10-01', '2017-11-06'),
(5, 0, '/photos/3/Hình-1-Học-bổng-du-học-Canada.jpg', NULL, 'lifestyle', 0, 0, 0, '2017-10-01', '2017-11-06'),
(6, 0, '/photos/3/hoc-bong-du-hoc-canada.jpg', '#', 'lifestyle', 0, 0, 0, '2017-10-01', '2017-11-06'),
(7, 0, NULL, NULL, 'categoryJob', 0, 0, 1, '2017-10-16', '2017-10-16'),
(8, 7, NULL, NULL, 'categoryJob', 0, 0, 1, '2017-10-16', '2017-10-16'),
(9, 7, NULL, NULL, 'categoryJob', 0, 0, 2, '2017-10-16', '2017-10-16'),
(10, 0, '/photos/3/2.jpg', '#', 'lifestyle', 0, 0, 0, '2017-11-06', '2017-11-06'),
(11, 0, '/photos/3/Student-Work-Visa-2.jpg', NULL, 'lifestyle', 0, 0, 0, '2017-11-06', '2017-11-06'),
(12, 0, '/photos/3/4.jpg', NULL, 'lifestyle', 0, 0, 0, '2017-11-06', '2017-11-06'),
(13, 0, NULL, NULL, 'price', 1, 500000, 0, '2018-10-18', '2018-10-18'),
(14, 0, NULL, NULL, 'price', 500000, 1000000, 0, '2018-10-18', '2018-10-18'),
(15, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-18', '2018-10-18'),
(16, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-18', '2018-10-18'),
(17, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-18', '2018-10-18'),
(18, 0, NULL, NULL, 'sporder', 0, 0, 0, '2018-10-28', '2018-10-28'),
(19, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-29', '2018-10-29'),
(20, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-29', '2018-10-29'),
(21, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-29', '2018-10-29'),
(22, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-29', '2018-10-29'),
(23, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-29', '2018-10-29'),
(24, 0, NULL, NULL, 'support', 0, 0, 0, '2018-10-29', '2018-10-29'),
(25, 0, NULL, NULL, 'fcate', 0, 0, 3, '2018-11-03', '2018-11-03'),
(26, 25, NULL, '#', 'fcate', 0, 0, 1, '2018-11-03', '2018-11-03'),
(27, 25, NULL, '#', 'fcate', 0, 0, 2, '2018-11-03', '2018-11-03'),
(28, 25, NULL, '#', 'fcate', 0, 0, 3, '2018-11-03', '2018-11-03'),
(29, 0, NULL, NULL, 'fcate', 0, 0, 4, '2018-11-03', '2018-11-03'),
(30, 29, NULL, NULL, 'fcate', 0, 0, 1, '2018-11-03', '2018-11-03'),
(31, 29, NULL, NULL, 'fcate', 0, 0, 2, '2018-11-03', '2018-11-03'),
(32, 29, NULL, NULL, 'fcate', 0, 0, 3, '2018-11-03', '2018-11-03'),
(33, 29, NULL, NULL, 'fcate', 0, 0, 4, '2018-11-03', '2018-11-03'),
(34, 0, NULL, NULL, 'fcate', 0, 0, 2, '2018-11-03', '2018-11-03'),
(35, 34, NULL, NULL, 'fcate', 0, 0, 1, '2018-11-03', '2018-11-03'),
(36, 34, NULL, NULL, 'fcate', 0, 0, 2, '2018-11-03', '2018-11-03'),
(37, 0, NULL, NULL, 'fcate', 0, 0, 1, '2018-11-03', '2018-11-03'),
(38, 37, NULL, NULL, 'fcate', 0, 0, 1, '2018-11-03', '2018-11-03'),
(39, 0, NULL, NULL, 'fdes1', 0, 0, 0, '2018-11-03', '2018-11-03'),
(40, 0, NULL, NULL, 'fdes2', 0, 0, 0, '2018-11-03', '2018-11-03'),
(41, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(42, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(43, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(44, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(45, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(46, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(47, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(48, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(49, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(50, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(51, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(52, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(53, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(54, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(55, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(56, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(57, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11'),
(58, 0, NULL, NULL, 'typeSim', 0, 0, 0, '2018-11-11', '2018-11-11');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `block`
--
ALTER TABLE `block`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `block`
--
ALTER TABLE `block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
